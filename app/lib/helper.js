
import {
    Platform,
  } from 'react-native';
class Helper {

  static DateStamp(...msg) {
    console.debug(new Date().toLocaleString(), msg);
  }

  static checkPasswordValidity(password) {
    let regexp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*/gm;
    return regexp.exec(password) !== null && (password.length >= 6 || password <= 50);
  }
  
  static checkValidInput(field) {
    return !(typeof field === 'undefined' || field === "");
  }

  static isEmailValid(email) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return reg.test(email);
}

  static parseCoordinatesArray(arr) {
      
    var polygonArray = [];
      for (var i=0; i < arr.length; i+=2){
        var obj = new Object();
      var res = arr.slice(i, i+2);
        obj.latitude = res[0];
        obj.longitude = res[1];
        polygonArray.push(obj);
      }
    return polygonArray;
  }

  static parseResultArrayToData(arr) {
    
  var temp = [];
    for (var i=0; i < arr.length; i++){
      var obj = new Object();
      obj.y = arr[i];
      temp.push(obj);
    }
  return temp;
  }

  static parseChoicesArrayToData(arr) {
    
  var temp = [];
    for (var i=0; i < arr.length; i++){
      var obj = new Object();
      obj.y = arr[i]['SelectionCount'];
      temp.push(obj);
    }
  return temp;
  }

  static extractColorsFromChoices(arr) {
    
  var temp = [];
    for (var i=0; i < arr.length; i++){
      temp.push(arr[i]['ChartColour']);
    }
  return temp;
  }

  static indexOfMax(arr) {

    var max = 0;
    var maxIndex = 0;
    var allEqual = false;

    for (var i = 0; i < arr.length; i++) {
      
      if (arr[i] != 0){
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
            allEqual = false;
        }else if (arr[i] == max){
          allEqual = true;
        }
      }
    }
    if (allEqual){
      return -1;
    }

    return maxIndex;
  }

  static findElement(arr, propName, propValue, indexOnly = false) {
    for (var i=0; i < arr.length; i++)
      if (arr[i][propName] == propValue)
      if (indexOnly){
        return i;
      }  
      return arr[i];

    return null;
  }

  static durationOutput(dt) {
    return parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) <= 24 
    ? parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) + 'h' 
      : (Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) < 2)
        ? '1d' 
        : (Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) < 31) 
          ? Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) + 'd' 
            : ((Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) == 31)) 
              ? '1m' 
                : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(dt)) / 36e5) / 24) / 30) + 'm' 
  }

}
export default Helper