import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import { Button, ButtonGroup, Divider} from 'react-native-elements'
import Helper from '../../lib/helper'

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

const Screen = Dimensions.get('window');

class OverallChart extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      selected: 'FR'
    }
    //this.renderRow = this.renderRow.bind(this);
    //this.submitAnswer = this.submitAnswer.bind(this);
  }

  componentDidMount(){
   // this.props.nextQuestion(this.props.applicationToken).then(() => {});
  }

  render() {
    
    return (
      <View style={styles.container}>
        
        <View>
        
            
          
        
        </View>
        
        </View>
       );
  }
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e4e4e4',
        marginBottom: 50,
        //marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#ffffff',
        borderRadius: 15,
        borderColor: '#ffffff',
        borderWidth: 0,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
          height: 1,
          width: 2,
        },
      },
      normal: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)', flexGrow:1, justifyContent: 'center',padding: 5,paddingTop: 8, paddingBottom: 8,paddingLeft: 15, paddingRight: 15, alignItems: 'center'},
      selected: {flexGrow:1,backgroundColor: '#1EA6CC',padding: 5,paddingTop: 8,paddingBottom: 8, paddingLeft: 15, paddingRight: 15,justifyContent: 'center', alignItems: 'center'},
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    width: 50,
  },
  image: {
    width: 30,
  },
})


function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question.Question,
    poll: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility
  }
}

export default connect(mapStateToProps)(OverallChart);
