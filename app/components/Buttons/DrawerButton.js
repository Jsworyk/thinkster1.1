import React, {Component, PropTypes} from 'react';

import {TouchableOpacity, View, Image} from 'react-native';

import styles from './styles'

const DrawerButton = ({drawerPress}) => (
<TouchableOpacity style={styles.buttonContainer} rejectResponderTermination  onPress={() => drawerPress()} underlayColor='#999'>
    <View style={styles.button}>
      <Image source={require('../../assets/menu.png')} style={styles.buttonImage} />
    </View>
</TouchableOpacity>
);

export default DrawerButton;