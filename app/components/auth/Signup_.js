import React, {Component} from 'react';
var t = require('tcomb-form-native');
import DatePicker from 'react-native-datepicker'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
var _ = require('lodash');
//import NavBar, { NavButton, NavButtonText, NavTitle } from 'react-native-nav'
import Icon from 'react-native-vector-icons/FontAwesome';

import {
  StyleSheet,
  Text,
  View,
  Platform,
  StatusBar,
  Image,
  TouchableHighlight,
} from 'react-native';
import { Logo } from '../Images';

const stylesheet = _.cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.height = 45;
stylesheet.textbox.error.height = 45;
stylesheet.textbox.normal.fontSize = 16;
stylesheet.textbox.error.fontSize = 16;
stylesheet.textbox.normal.borderWidth = 1;
stylesheet.textbox.error.borderWidth = 2;
stylesheet.textbox.normal.marginBottom = 0;
stylesheet.textbox.error.marginBottom = 0;
stylesheet.textbox.normal.color = '#000';
stylesheet.textbox.error.color = '#000';
stylesheet.helpBlock.normal.color = '#fff';

stylesheet.textboxView.normal.backgroundColor = '#fff';
stylesheet.textboxView.error.backgroundColor = '#fff';
stylesheet.textboxView.normal.opacity = 0.9;
stylesheet.textboxView.normal.borderWidth = 0;
stylesheet.textboxView.normal.borderColor = '#fff';
stylesheet.textboxView.error.borderColor = 'red';
stylesheet.textboxView.error.borderWidth = 0;
stylesheet.textboxView.normal.borderRadius = 5;
stylesheet.textboxView.error.borderRadius = 5;
stylesheet.textbox.normal.marginBottom = 0;
stylesheet.textbox.error.marginBottom = 0;

var Form = t.form.Form;

// here we are: define your domain model
var Person = t.struct({
  firstName: t.String,
  lastName: t.String,
  email: t.String,
  //city: t.String,
  password: t.String
});



var options = {
  stylesheet: stylesheet,
  fields: {
    firstName: {
      placeholderTextColor: '#a7a7aa',     
    },
    lastName: {
      placeholderTextColor: '#a7a7aa',     
    },
    email: {
      placeholderTextColor: '#a7a7aa',     
    },
    city: {
      placeholderTextColor: '#a7a7aa',     
    },
    password: {
      placeholderTextColor: '#a7a7aa',     
      secureTextEntry: true
    }
  },
   auto: 'placeholders',
  //  fields: {
  //   age: {
  //     // you can use strings or JSX
  //     error: 'Age must be in integers'
  //   }
  // }
}; // optional rendering options (see documentation)

class Signup extends Component{

  constructor(props){
    super(props)
    this.state = {date:""}
  }

  componentDidMount(){
   // this.props.toggleDrawer();
    //this.refs.form.getComponent('firstName').refs.input.focus();
  }

  componentWillUnmount(){
   // this.props.toggleDrawer();
  }

  onPress() {
    // call getValue() to get the values of the form
    var value = this.refs.form.getValue();
    if (value) { // if validation fails, value will be null
      console.log(value); // value here is an instance of Person
      this_.setState({
        visible: true,
        value: value
      })
    }
  }

  onChange (value) {
    if (value.firstName !== '') {

    }

  }

  render(){
    return (
       <View style={styles.container}>
          {Platform.OS != 'ios' ?
          <StatusBar
     backgroundColor="#134fa1"
     setBarStyle={{borderBottomWidth: 0, opacity: 0.9}}
     barStyle="light-content"/>
     : null
          }
          {/*<NavBar style={NavStyles}>
          <NavButton style={[NavStyles.navButton,{marginLeft: 0}]} >
              <Icon style={NavStyles.icon} name='chevron-left' size={20} color='#134fa1' />
          </NavButton>
          <NavTitle style={NavStyles.title}>
            {"Signup"}
          </NavTitle>
          <NavButton style={[NavStyles.navButton,{marginLeft: 0}]} >
              <Icon style={NavStyles.icon} name='chevron-left' size={20} color='#fff' />
          </NavButton>
        </NavBar>*/}
        <Logo></Logo>
        
        <Image source={require('../../assets/loginbackground.png')} style={{flex: 1, justifyContent: 'space-around', width: undefined,height: undefined}}>
        <View style={styles.formContainer}>
        
        <Form
          ref="form"
          type={Person}
          options={options}
          value={this.state.value}
        />
        {/*<DatePicker
        style={{width: undefined}}
        date={this.state.date}
        mode="date"
        placeholder="Birth Date"
        format="YYYY-MM-DD"
        minDate="1950-01-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        showIcon= {false}
        customStyles={{
          placeholderText: {
           position: 'absolute',
           color: '#a7a7aa',
           fontSize: 14,
            left: 5,
            top: 0
          },
          dateText: {
            position: 'absolute',
            color: '#fff',
            fontSize: 14,
            left: 5,
            top: 0
          },
          dateInput: {
            borderWidth : 0,
            borderBottomWidth: 0.5,
            borderBottomColor: '#fff',
            marginBottom: 5,
            height: 30,
            marginLeft: 0,

          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {
          var value = this.refs.form.getValue();
            
              this.setState({
                visible: false,
                value: value,
                date: date
              })
          }}
      />*/}
        <View style={{ marginTop: 5 }}>
          <TouchableHighlight style={styles.button} onPress={() => { this.onPress(); }} underlayColor='#fff'>
            <Text style={styles.buttonText}>Signup</Text>
          </TouchableHighlight>
        </View>
        </View>
        </Image>

        <View style={{ justifyContent: 'flex-end', alignItems: 'center', padding: 15, height: 100 }}>
          <Image source={require('../../assets/loginFacebook.png')} style={{ marginBottom: 5, height: 30, resizeMode: 'contain' }}></Image>
          <Image source={require('../../assets/loginTwitter.png')} style={{ marginBottom: 5, height: 30, resizeMode: 'contain' }}></Image>
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: '#fff' }}
          position='top'
          positionValue={200}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: 'red' }}
        />
        <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
      </View>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    backgroundColor: '#ffffff',
    flex: 1
  },
  formContainer:{
    paddingTop: 10,
    paddingBottom: 0,
    paddingLeft: 10,
    paddingRight: 10
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    },
    imageContainer: {
    width: undefined,
    height: Platform.OS === 'ios'? 64 : 54,
    backgroundColor:'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#fff',
    opacity: 0.9
  },
  navBar: {
    backgroundColor: '#fff',
    borderBottomColor: '#fff'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#134fa1',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  image: {
    width: 30,
  },
})

export default Signup;
