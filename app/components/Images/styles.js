import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    logoContainer: {
        justifyContent: 'center', 
        alignItems: 'center', 
        padding: 15,
        height: 80
    },
    logo: {
        width: 220,height: 40
    },
    spinnerContainer: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    }
});

export default styles;