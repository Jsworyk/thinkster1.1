import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import * as Progress from 'react-native-progress';

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  FlatList,
  TouchableHighlight,
  StyleSheet,
  Dimensions
} = ReactNative

var width = Dimensions.get('window').width;
class UserCompatibility extends Component{

  constructor(props){
    super(props);
    
    this.state = {
      dataSource: [],
      showLoading: true,
      showQuestions: false,

    }
    this.renderItem = this.renderItem.bind(this);
  }
  
  componentDidMount(){
    if (this.props.isLoggedin) {
      this.getCompatibility();
    }
  }

  getCompatibility(){
    const _this = this;
    this.props.getCompatibility(this.props.applicationToken, this.props.ProfileId).then((items) => {
      _this.setState({
        dataSource: items,
        showLoading: false
      })
    });
  }

  renderItem (l, i) {
   return (
     <View key={i} style={{ flexDirection: 'column',  margin: 5,  justifyContent: 'center', alignItems: 'center' }}>
       <View style={{marginLeft: 10, alignSelf: 'flex-start', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
         <Text style={{ color: '#2B82BE', fontWeight: 'normal' }}>{l.Name}</Text>
       </View>
       <View style={{marginTop: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
       <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 0, marginRight: 5 }}>
         <Progress.Bar color='#76DDFB' unfilledColor='#F6F6F7' borderWidth={0} progress={l.matchingAnswer == 0 ? 0 : Math.ceil(l.matchingAnswer * 100 / l.sameQuestionAnswered)/100} height={8} width={width-100} />
       </View>
       
       <View style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}>
         <Text style={{ color: '#2B82BE' }}>{l.matchingAnswer == 0 ? '0' : Math.ceil(l.matchingAnswer * 100 / l.sameQuestionAnswered)}%</Text>
       </View>
       </View>
     </View>
   )
 }

 _keyExtractor = (item, index) => index

  render(){
    return (
      <View style={{ flex: 1, paddingBottom: 5, backgroundColor: '#fff'}}>
      {this.state.showLoading
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#134fa2'/>
        </View>
        :
        this.state.dataSource.length == 0 ?
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10}}>
          <Text>No Compatibility Found</Text>
        </View>
        :
        <View style={{marginTop: 10}}>
        <FlatList
            extraData={this.state.dataSource}
            data={this.state.dataSource}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </View>
        
      }

     </View>
    )
  }
}

const styles = StyleSheet.create({

  subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  countText: {
    fontSize: 14,
    color: '#2b8abb',
    margin: 2,
  },
  username: {
    color: '#696868',
    fontWeight: 'bold',
    fontSize: 16,
  },
})


function mapStateToProps(state){

  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile
  }
}

export default connect (mapStateToProps)(UserCompatibility);
