import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements'
import ActionSheet from 'react-native-actionsheet'
import SocketIOClient from 'socket.io-client';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  BackAndroid,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  StatusBar,
  ListView,
  Platform,
  TextInput,
  Keyboard
} from 'react-native';

var width = Dimensions.get('window').width;
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 2;
const options = [ 'Cancel', 'Report', 'Block', 'Unfollow']

import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import Friends from './Friends'
import UserImages from './UserImages'
import Responses from './Responses'
import UserCompatibility from './UserCompatibility'
import FacebookTabBar from './Tabs';
var Spinner = require('react-native-spinkit');
import Modal from 'react-native-simple-modal';

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class UserProfile extends Component {

  constructor(props) {
    super(props);
    
    this.state =  {
    index: 0,
    showProfile: false,
    profile: null,
    openMessageModal: false,
    dialogOffset: 0
    };
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    
  }

  _handleChangeTab = (index) => {
    this.setState({ index });
  };


  _keyboardDidShow () {
    this.setState({
      dialogOffset: -100
    })
  }

  _keyboardDidHide () {
    this.setState({
      dialogOffset: 0
    })
  }

  componentDidMount(){
   
  }

  componentWillMount(){
  //  this.props.toggleDrawer();
  
  const _this = this;
  this.props.getCurrentUserProfile(this.props.applicationToken, this.props.ProfileName).then((profile) => {
      
        _this.setState({
        showProfile: true,
        profile: profile,
        following: profile.Following == 1 ? true : false
    })  
    
    })
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  openMessageModal(){
    this.setState({
      openMessageModal: true,
      message: ''
    })
  }

  sendMessage(){
    this.props.setLastSendingMessage(this.state.profile.ProfileId,this.state.profile.DisplayName,this.state.profile.AvatarUrl,this.state.message)
    this.props.sendMessage(this.props.applicationToken, this.state.profile.ProfileId, this.state.message);
    this.setState({
      openMessageModal: false
    })
    // this.props.getChatrooms(this.props.applicationToken).then(() => { 
      
    // })
  }

  follow(){
    if (this.state.following == 0){
    this.props.follow(this.props.applicationToken, this.state.profile.ProfileId).then(() => {
      this.setState({
        following: true
      })
    })
  }else{
    this.ActionSheet.show()
  }
  }

  onMenuItemPress(i){
    if (i == 1){
        
    }else if (i == 2){
      this.props.block(this.props.applicationToken, this.state.profile.ProfileId).then(() => {
        
      })
    }else if (i == 3){
      this.props.unfollow(this.props.applicationToken, this.state.profile.ProfileId).then(() => {
        this.setState({
          following: false
        })
      })
    }
   }

  render() {
    return (
      <View style={{backgroundColor: '#1EA6CC', flex:1}}>
        <StatusBar
                backgroundColor="#057AB8"
                setBarStyle={{ borderBottomWidth: 0, opacity: 0.7 }}
                barStyle="light-content" />

{this.state.showProfile ?
    
  
  <View style={{}}>
  {this.state.profile.BannerUrl != '' ? 
  <TouchableOpacity rejectResponderTermination 
  onPress = { () => {
    Actions.ImagePost({Selector: this.state.profile.BannerSelector})
  }}>
<Image source={{uri: this.state.profile.BannerUrl}} style={styles.imageContainer} />
</TouchableOpacity>
    
    :
    <View style={{width: undefined,height: 120,justifyContent: 'center', alignItems: 'center', backgroundColor: '#999'}} >
    </View>
}
    <View style={[styles.profileContainer]}>
           
    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 20}}>
    <TouchableOpacity rejectResponderTermination  onPress={() => { this.openMessageModal() }} underlayColor='#999'>
        <View style={[{backgroundColor: '#30415F', padding: 5, borderRadius: 20, height: 40, width: 40, justifyContent: 'center', alignItems: 'center'},styles.shadow]}>
        {/* <Icon style={{}} name='comment' size={20} color='white' /> */}
        <Image source={require('../../assets/message-icon.png')} style={{width: 15, height: 15}}/>
        </View>
    </TouchableOpacity>
    <TouchableOpacity rejectResponderTermination  onPress={() => { this.follow() }} underlayColor='#999'>
        <View style={[{backgroundColor: '#30415F', padding: 5, borderRadius: 20, height: 40, width: 40, justifyContent: 'center', alignItems: 'center'},styles.shadow]}>
        {this.state.following ?
        <Icon style={{}} name='check' size={20} color='white' />
        :
        <Icon style={{}} name='plus' size={20} color='white' />
        }
        </View>
    </TouchableOpacity>
    </View>
    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginTop: 20}}>
    <TouchableOpacity rejectResponderTermination  style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} onPress={() => { Actions.Followers({ProfileId: this.state.profile.ProfileId}) }} underlayColor='#999'>
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{ color: '#fff',fontSize: 14, textAlign: 'center',fontWeight: 'bold'}}>{this.state.profile.FollowerCount}</Text>
        <Text style={{ color: '#fff',fontSize: 14, textAlign: 'center',}}>Followers</Text>
      </View>
      </TouchableOpacity>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{ color: '#fff',fontSize: 22,textAlign: 'right', fontWeight: 'bold'}}>{this.state.profile.Compatibility.Agree == 0 ? '0' : Math.ceil(this.state.profile.Compatibility.Agree * 100 / this.state.profile.Compatibility.Total)}%</Text>
      {/* <Text style={{ color: '#fff',fontSize: 14, textAlign: 'center'}}>Responses</Text> */}
      </View>
      <TouchableOpacity rejectResponderTermination  style={{flex: 1,justifyContent: 'center', alignItems: 'center'}} onPress={() => { Actions.Following({ProfileId: this.state.profile.ProfileId}) }} underlayColor='#999'>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{ color: '#fff',fontSize: 14, textAlign: 'center',fontWeight: 'bold'}}>{this.state.profile.FollowingCount}</Text>
      <Text style={{ color: '#fff',fontSize: 14, textAlign: 'center',}}>Following</Text>
      </View>
      </TouchableOpacity>
    </View>

  </View>
  <View style={{position: 'absolute', alignSelf: 'center', top: 70 }}>
            <TouchableOpacity rejectResponderTermination 
              onPress = { () => {
                //Actions.Modal({image: this.state.profile.AvatarUrl})
                Actions.ImagePost({Selector: this.state.profile.AvatarSelector})
              }}>
              {this.state.profile.AvatarUrl ?
            <Image
              style={{borderWidth: 1, borderColor: '#fff', borderRadius: 50, width: 100, height: 100,alignSelf: 'center' }}
              source={{ uri: this.state.profile.AvatarUrl }}
            /> 
            :
            <View style={{backgroundColor: '#0074CD', width: 100, height: 100, borderRadius: 50, justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
            <Text style={{color: '#fff', fontSize: 20}}>{(this.state.profile.DisplayName).split(" ").map((n)=>n[0])}</Text>
            </View>
            // <Image source={require('../../assets/user.png')} style={{borderWidth: 1, borderColor: '#fff', borderRadius: 50, width: 100, height: 100,alignSelf: 'center', }}></Image>
              }
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, fontWeight: '600', textAlign: 'center', marginTop: 5 }}>
                {this.state.profile.DisplayName}
            </Text>
            <View style={{flexDirection: 'row', paddingHorizontal: 10, paddingTop: 5}}>
            <Image source={require('../../assets/markergrey.png')} style={{resizeMode: 'contain', marginRight: 2, width: 12, height: 14}}/>
            <Text style={{ color: '#e4e4e4',fontSize: 11, textAlign: 'center'}}>
              {this.state.profile.Location}, {this.state.profile.Country} 
            </Text>
            </View>
            
            </View>
  </View>
   :
   <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#fff'/>
        </View>
}
{this.state.showProfile ?
<ScrollableTabView
  renderTabBar={() => <FacebookTabBar />}
  tabBarBackgroundColor='#fff'
  tabBarUnderlineStyle={{ backgroundColor: '#fff' }}
  tabBarActiveTextColor='#134fa1'
  tabBarInactiveTextColor='#1EA6CC'
  tabBarTextStyle={{ fontSize: 14,  }}
>
    
  <Responses ProfileId={this.state.profile.ProfileId} tabLabel="Responses" {...this.props}/>
  <UserImages ProfileId={this.state.profile.ProfileId} tabLabel="Images" {...this.props}/>
  <UserCompatibility ProfileId={this.state.profile.ProfileId} tabLabel="Compatibility" {...this.props}/>
</ScrollableTabView>
:
  null
}
        <Modal
          open={this.state.openMessageModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openMessageModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
            <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{this.state.profile ? this.state.profile.DisplayName : ''} </Text>
          </View>
              <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5
              }}>
                  <UselessTextInput
                      style={{ paddingBottom: 10, color: '#444', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={message => this.setState({ message })}
                      value={this.state.message}
                      placeholder='Type a message...'
                      placeholderTextColor='#d7d7d7'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={true}
                      editable={true}
                  />
              </View>
              <Button
                title='Send Message'
                buttonStyle={{margin: 5,marginBottom: 10, height: 35}}
                backgroundColor='#1EA6CC'
                color='#fff'
                textStyle={{ fontSize: 14, fontWeight: 'normal' }}
                onPress={() => {this.sendMessage()}}
              />
        </Modal>
        <ActionSheet
            ref={o => this.ActionSheet = o}
            options={options}
            cancelButtonIndex={CANCEL_INDEX}
            destructiveButtonIndex={DESTRUCTIVE_INDEX}
            onPress={(i) => {this.onMenuItemPress(i)}}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileContainer: {
     padding: 20,
     backgroundColor: '#1EA6CC',
     //marginTop: Platform.OS === 'ios'? 64 : 54,
   },
   centerItem: {
     flexDirection: 'column',
     alignItems: 'center',
     justifyContent: 'center'
   },
   circle: {
   width: 60,
   height: 60,
   borderRadius: 100/2
 },
  tabView: {
    flex: 1,
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  card: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 5,
    height: 150,
    padding: 15,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  indicatorContainer: {
       backgroundColor: '#134fa1',
       height: 40,
       opacity: 0.9
   },
   indicatorText: {
       fontSize: 14,
       color: '#ffffff',
  
   },
   indicatorSelectedText: {
       fontSize: 14,
       color: '#ffffff',
      
   },
   selectedBorderStyle: {
       height: 4,
       backgroundColor: '#fff'
   },
   statusBar: {
       height: 24,
       backgroundColor: 0x00000044
   },
   toolbarContainer: {
       height: 56,
       backgroundColor: 0x00000020,
       flexDirection: 'row',
       alignItems: 'center',
       paddingHorizontal: 16
   },
   backImg: {
       width: 16,
       height: 17
   },
   titleTxt: {
       marginLeft: 36,
       color: 'white',
       fontSize: 20,
      
   },
   PhotoGrid: {
     marginTop: 5,
     backgroundColor:'#fff'
   },
   imageContainer: {
    width: undefined,
    height: 120,
    backgroundColor:'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'cover'
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    height: 4,
    backgroundColor: '#fff',
  },
  label: {
    color: '#fff',
    fontSize: 12,
    
  },
  tabbar: {
    opacity: 0.9,
    backgroundColor: '#134fa1',
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.90)',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 2,
    },
  }
});

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    followers: state.FOLLOWERS.followers,
    following: state.FOLLOWING.following
  }
}

export default connect (mapStateToProps)(UserProfile);
