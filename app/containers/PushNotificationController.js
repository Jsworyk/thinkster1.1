import React, { Component } from "react";
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { Platform, AsyncStorage, AppState } from 'react-native';

import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from "react-native-fcm";

class PushNotificationController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
    }
  }
  
  componentDidMount() {
    
    FCM.requestPermissions();

    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
      if (!this.props.isLoggedin){
              
        Actions.Signin({ type: 'reset', token: token });
        return;
      }

      AsyncStorage.getItem('reduxPersist:saveApplicationToken',(err, res) => {
        
        if (token){
        if (res == null){
          this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
           // alert(this.props.applicationToken)
            if (!this.props.isLoggedin){
              
              Actions.Signin({ type: 'reset', token: token });
            }else {
              this.props.toggleDrawer(false);
              Actions.tabbar({ type: 'reset', });
            }
          });
        }else{
          var result = JSON.parse(res);
          if (result.applicationToken == null){
            this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
              if (!this.props.isLoggedin){
                Actions.Signin({ type: 'reset',token: token });
              }else {
                this.props.toggleDrawer(false);
                Actions.tabbar({ type: 'reset' });
              }
            });
          }else{
            this.props.checkApplicationToken(this.props.applicationToken).then(() => {
              if (!this.props.applicationTokenStatus){
                this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
                  if (!this.props.isLoggedin){
                    Actions.Signin({ type: 'reset', token: token });
                  }else {
                    this.props.toggleDrawer(false);
                    Actions.tabbar({ type: 'reset' });
                  }
                });
              }else{
                if (!this.props.isLoggedin){
                  Actions.Signin({ type: 'reset', token: token });
                }else {
                  this.props.toggleDrawer(false);
                  Actions.tabbar({ type: 'reset' });
                }
              }
            });
          }
        }
      }
    })
    });

    FCM.getInitialNotification().then(notif => {
      console.log("INITIAL NOTIFICATION", notif)
    });

    this.notificationListner = FCM.on(FCMEvent.Notification, notif => {
      console.log("Notification", notif);
      if(notif.local_notification){
        return;
      }
      if(notif.opened_from_tray){
        return;
      }

      if(Platform.OS ==='ios'){
              //optional
              //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
              //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
              //notif._notificationType is available for iOS platfrom
              switch(notif._notificationType){
                case NotificationType.Remote:
                  notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                  break;
                case NotificationType.NotificationResponse:
                  notif.finish();
                  break;
                case NotificationType.WillPresent:
                  notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                  break;
              }
            }
      this.showLocalNotification(notif);
    });

    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, token => {
      
      
      AsyncStorage.getItem('reduxPersist:saveApplicationToken',(err, res) => {
        
        if (res == null){
          this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
           // alert(this.props.applicationToken)
            if (!this.props.isLoggedin){
              
              Actions.Signin({ type: 'reset', token: token });
            }else {
              this.props.toggleDrawer(false);
              Actions.tabbar({ type: 'reset', });
            }
          });
        }else{
          var result = JSON.parse(res);
          if (result.applicationToken == null){
            this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
              if (!this.props.isLoggedin){
                Actions.Signin({ type: 'reset',token: token });
              }else {
                this.props.toggleDrawer(false);
                Actions.tabbar({ type: 'reset' });
              }
            });
          }else{
            this.props.checkApplicationToken(this.props.applicationToken).then(() => {
              if (!this.props.applicationTokenStatus){
                this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', token).then(() => {
                  if (!this.props.isLoggedin){
                    Actions.Signin({ type: 'reset', token: token });
                  }else {
                    this.props.toggleDrawer(false);
                    Actions.tabbar({ type: 'reset' });
                  }
                });
              }else{
                if (!this.props.isLoggedin){
                  Actions.Signin({ type: 'reset', token: token });
                }else {
                  this.props.toggleDrawer(false);
                  Actions.tabbar({ type: 'reset' });
                }
              }
            });
          }
        }
    })


    });

    

  }

  showLocalNotification(notif) {
    FCM.presentLocalNotification({
      title: notif.title,
      body: notif.body,
      priority: "high",
      click_action: notif.click_action,
      show_in_foreground: true,
      local: true
    });
  }

  componentWillUnmount() {
    this.notificationListner.remove();
    this.refreshTokenListener.remove();
  }


  render() {
    return null;
  }
}



function mapStateToProps(state) {
  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationTokenStatus: state.SET_APPLICATION_TOKEN_STATUS.applicationTokenStatus,
    applicationToken: state.saveApplicationToken.applicationToken
  }
}

export default connect(mapStateToProps)(PushNotificationController);
