import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import { bindActionCreators } from 'redux';
import Routes from '../components/routes/Routes';
import Signin from '../components/auth/Signin';
import ReactNative from 'react-native'


const {
  View,
  StyleSheet
} = ReactNative


class AppContainer extends Component {
  constructor(props){
    super(props);
  }


  render(){
    return (
       <Routes {...this.props} />
    )
  }
}


var styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'column'
  }
});

function mapStateToProps(state){

  return {

  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators , dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
