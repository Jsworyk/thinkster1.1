import React, {Component} from 'react';

import {
    TouchableOpacity,
    View,
    Touchable,
    Image,
    StyleSheet,
    Text,
    StatusBar
} from 'react-native';
import { Divider } from 'react-native-elements';
import styles from './styles';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import DrawerButton from '../Buttons/DrawerButton';
import SearchButton from '../Buttons/SearchButton';
import { Actions } from 'react-native-router-flux';
import { ThinksterNavBar } from '../Layout';
import Helper from '../../lib/helper';

class Landing extends Component{

    constructor(props) {
        super(props);
        this.state = {
            drawerOpen: false,
        }
        this.toggleDrawer = this.toggleDrawer.bind(this);

    }

    goToPolls() {
        Actions.Polls();
    }

    goToDiscovery() {
        Actions.FriendSuggest();
    }

    toggleDrawer(){
        Helper.DateStamp("Drawer State is", this.state.drawerOpen)
        
        this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
        this.setState({
          drawerOpen: !this.state.drawerOpen
        })
    
      }

    render() {
        return (
        <View style={styles.container}>
            <StatusBar
                    backgroundColor="#057AB8"
                    setBarStyle={{ borderBottomWidth: 0, opacity: 0.7 }}
                    barStyle="light-content" />
            <ThinksterNavBar toggleDrawer={this.toggleDrawer} title="Landing"></ThinksterNavBar>


            <View style={[styles.landingContainer]}>
                <TouchableOpacity style={[styles.landingImageContainer]} onPress={() => this.goToPolls()}>
                    <Image source={require('../../assets/landing-question.png')} style={[styles.opinionImage]}/>
                    <Text style={[styles.landingText]}>
                        Validate your opinion on things that matter!
                    </Text>
                </TouchableOpacity>
                <Divider>
                </Divider>
                <TouchableOpacity style={[styles.landingImageContainer]} onPress={() => this.goToDiscovery()}>
                    <View style={[styles.rectangleOutline]}>
                        <Image source={require('../../assets/discover-people.png')} style={[styles.discoverImage]}/>
                    </View>
                    <Text style={[styles.landingText]}>
                        Discover new people!
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
        )
    }
}


function mapStateToProps(state){

    return {
      applicationToken: state.saveApplicationToken.applicationToken,
    }
  }
  
  export default connect (mapStateToProps)(Landing);