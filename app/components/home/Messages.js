import React, {Component} from 'react';
import {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  RecyclerViewBackedScrollView,
  Text,
  Alert,
  View,
  StatusBar,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  TextInput,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
var SpinKit = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import { List, Card, ListItem, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import SocketIOClient from 'socket.io-client';
import ActionSheet from 'react-native-actionsheet'
import Modal from 'react-native-simple-modal';
import LinearGradient from 'react-native-linear-gradient';
import {SearchButton, DrawerButton} from '../Buttons';
import SafeAreaView from 'react-native-safe-area-view';
import { ThinksterNavBar } from '../Layout';



const CANCEL_INDEX = 0
const options = [ 'Cancel', 'Flag Content']
var reportReasons = [{name: 'Solicitation/Spam/bot', id: 'SO'}, {name: 'Copyright Infringement', id: 'CO'}, {name: 'Links to illegal content or filesharing sites', id: 'IL'}, {name: 'Pornographic Content', id: 'PO'}, {name: 'Other (describe below)', id: 'OT'}]

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class Messages extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      visible: true,
      list: [],
      openReportModal: false,
      dialogOffset: 0,
      details: '',
      drawerOpen: false
    }
    this.onMenuItemPress = this.onMenuItemPress.bind(this)
    this.showActionSheet = this.showActionSheet.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentWillUnmount(){
    this.socket.close();
  }

  showActionSheet(TargetTypeCode,TargetId) {
    this.setState({
      TargetTypeCode: TargetTypeCode,
      TargetId: TargetId
    },
    () => {
      this.ActionSheet.show()
    }
  )
    
  }

  selectReason(id,i){
    this.setState({
      reasonCode: id,
      selectedReason: i
    })
  }

  componentWillMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentDidMount(){
    const _this = this;

    this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false,transports: ['websocket']});
    this.socket.on('connect', () => {
      this.socket.emit('authenticate', this.props.applicationToken);
    });
    // this.socket.on('ChatMessage', (data) => {
      
    //   var messageToUpdate = this.state.list.filter(function(item) { 
    //     return item.ChatRoomId === data.ChatRoomId;  
    //   });
    //   var messagesList = this.state.list.filter(function(item) { 
    //     return item.ChatRoomId !== data.ChatRoomId;  
    //   });
      
    //   if (data.Messages[0].ProfileId !== this.props.profileId){
    //    var newMessage = {
    //       ChatRoomId: data.ChatRoomId,
    //       ChatPreview: data.Messages[0].Message,
    //       ChatRoomAvatar: data.Messages[0].AvatarUrl,
    //       RoomName: data.Messages[0].DisplayName,
    //       Modified: data.Messages.Created,
    //       Unread: 1
    //     }
    //     messagesList.unshift(newMessage)
    //   }else{
    //     messageToUpdate[0].ChatPreview = data.Messages[0].Message;
    //     messagesList.unshift(messageToUpdate[0])
    //   }
    //   this.setState({
    //     list: messagesList
    //   })
    // });

      _this.setState({
        visible: false,
      })
  
    //Actions.refresh({hideNavBar: false})
  }

  renderItem(l, i){
  
    return (
      <TouchableWithoutFeedback key={i} onLongPress={() => {this.showActionSheet('CH', l.ChatRoomId)}} onPress={() => { this.onMessagePress(l.ChatRoomId, l.RoomName) }} underlayColor='#999' >
      <View style={{marginBottom: 0, backgroundColor: '#1FA8CD', }}>
        <View style={{flexDirection: 'row',padding: 8, backgroundColor: l.Unread == 1 ? 'rgba(255,255,255,0.2)' : 'transparent' }}>
        <View style={{margin: 5,marginLeft: 15}}>
          { l.ChatRoomAvatar ?
          <Image source={{uri: l.ChatRoomAvatar}} style={[styles.avatarStyle]} ></Image>
          :
          <View style={{backgroundColor: '#0074CD', width: 46, height: 46, borderRadius: 23, justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
          <Text style={{color: '#fff', fontSize: 20}}>{(l.RoomName).split(" ").map((n)=>n[0])}</Text>
          </View>
          }
        </View>
        <View style={{flex: 1, margin: 3}}>
          <View style={{marginBottom: 3, flexDirection: 'row', justifyContent: 'space-between'}}>
          {/* <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: l.LinkId, hideNavBar: false})}} underlayColor='#999'> */}
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'normal'}}>{l.RoomName}</Text>
          {/* </TouchableOpacity> */}
            <View style={{backgroundColor: '#fff', padding: 3,paddingLeft: 8, paddingRight: 8, borderRadius: 40}}>
              <Text style={{color: '#1EA6CC', fontSize: 10, backgroundColor: 'transparent'}}>
              {
                parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) == 0 ? parseInt( (Math.abs(new Date() - new Date(l.LastMessage)) / 1000) / 60 ) + 'min' :  parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(l.LastMessage)) / 36e5) / 24) / 30) + 'm' 
              }
              </Text>
            </View>
          </View>
          <View style={{marginBottom: 5, flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: '#fff', fontSize: 14}}>{l.ChatPreview}</Text>
          </View>
        </View>
        </View>
      </View>
      </TouchableWithoutFeedback>
    )
  }

onMessagePress(chatRoomId, title){
    const _this = this;
    this.setTimeout(function (){
      this.props.toggleDrawer(true);
     this.props.updateReadDate(this.props.applicationToken,chatRoomId).then(()=>{
        this.props.getChatrooms(this.props.applicationToken).then(() => {
        _this.setState({
          visible: false,
          list: _this.props.chatrooms
        });
      });
    })
      Actions.Chat({chatRoomId: chatRoomId, title: title});
    },500)
}


onMenuItemPress(i){
  
  if (i == 1){
    this.openReportModal()
  }
 }


 openReportModal(){
  this.setState({
    openReportModal: true,
  })
}

_keyboardDidShow () {
  this.setState({
    dialogOffset: -100
  })
}

_keyboardDidHide () {
  this.setState({
    dialogOffset: 0
  })
}

flagContent(){
  this.props.flagContent(this.props.applicationToken, this.state.TargetTypeCode, this.state.TargetId, this.state.reasonCode, this.state.details).then((status) => {
    if (status){
      this.setState({
        openReportModal: false
      },
      () => {
        alert('Content reported!')
      }
    )
    }
  })
}

renderReportReasons(l,i){
  return (
    <TouchableOpacity key={i} onPress={() => {this.selectReason(l.id, i)}} underlayColor='#999'>
      <View style={{backgroundColor: this.state.selectedReason == i ? '#1EA6CC' : '#ccc', borderRadius: 15, margin: 5, padding: 8}}>
        <Text style={{fontSize: 12}}>{l.name}</Text>
      </View>
    </TouchableOpacity>
  )
}

toggleDrawer(){
  
  this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
  this.setState({
    drawerOpen: !this.state.drawerOpen
  })
}

_keyExtractor = (item, index) => index

render() {
    return (

      <View style={styles.container}>
        <StatusBar
                backgroundColor="#057AB8"
                setBarStyle={{ borderBottomWidth: 0, opacity: 0.7 }}
                barStyle="light-content" />

        <ThinksterNavBar toggleDrawer={this.toggleDrawer} title="Messages"></ThinksterNavBar>

        {
          !this.state.visible ?
          this.props.chatrooms.length > 0 ?
          <FlatList
            extraData={this.props.chatrooms}
            data={this.props.chatrooms}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
          
         
          :
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}>
          <View style={[{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderColor: '#ccc', borderWidth: 0.5, padding: 10, margin: 10}, styles.shadow]}>
          <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
          <Icon style={{backgroundColor: 'transparent'}} name='envelope' size={24} color={'#1EA2CC'} />
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 10, margin: 5}}>
            <Text style={{color: '#444', fontSize: 16, fontWeight: 'bold'}}>Your Messages</Text>
            <Text style={{color: '#999', fontSize: 14, fontWeight: 'normal', margin: 5, textAlign: 'center'}}>Messages will appear here. Start a conversation with someone!</Text>
          </View>
        </View>
        </View>
         
          : 
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
              <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#fff' />
          </View>
        }
        <Modal
          open={this.state.openReportModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openReportModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
            <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{'Report this Content'} </Text>
          </View>

          <ScrollView style={{height: 230}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 5}}>
            {reportReasons.map((l,i) => (
              this.renderReportReasons(l,i)
            ))}
          </View>
          </ScrollView>

              <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5
              }}>
                  <UselessTextInput
                      style={{ paddingBottom: 10, color: '#444', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={details => this.setState({ details })}
                      value={this.state.details}
                      placeholder='Details...'
                      placeholderTextColor='#d7d7d7'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={true}
                      editable={true}
                  />
              </View>
              <Button
                title='Report'
                buttonStyle={{margin: 5,marginBottom: 10, height: 35}}
                backgroundColor='#1EA6CC'
                color='#fff'
                textStyle={{ fontSize: 14, fontWeight: 'normal' }}
                onPress={() => { this.flagContent() }}
              />
        </Modal>
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          onPress={this.onMenuItemPress}
        />
    </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1EA2CC',
    //marginTop: Platform.OS === 'ios' ? 64 : 54,
    marginBottom: 50,
    justifyContent: 'center',
  },
  messagesContainer:{
    padding: 10
  },
  row:{
    flex:1,
    flexDirection:'row',
    padding:18,
    borderBottomWidth: 1,
    borderColor: '#d7d7d7',
  },
  selectionText:{
    fontSize:15,
    paddingTop:3,
    color:'#b5b5b5',
    textAlign:'right'
  },
  subtitleView: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5,
    justifyContent: 'space-between',
  },

  ratingText: {
    textAlign: 'right',
    color: 'grey',

  },
  username: {
    color: '#2b8abb',
    fontSize: 14,
  },
  unreadUsername:{
    color: '#2b8abb',
    fontSize: 14,
    fontWeight: 'bold'
  },
  subtitle: {
    color: '#696868'
  },
  unreadSubtitle: {
    color: '#696868',
    fontWeight: 'bold'
  },
  listContainer: {
    marginRight: 10,marginLeft: 10, marginBottom: 50,marginTop: 0
  },
  avatarStyle: {
    width: 46,
    height: 46,
    borderRadius: 23,
    marginRight: 5
  },
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
});

reactMixin(Messages.prototype, TimerMixin)
function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    chatrooms: state.chatrooms.chatrooms,
    profileId: state.CURRENT_USER.ProfileId
  }
}

export default connect (mapStateToProps)(Messages);
