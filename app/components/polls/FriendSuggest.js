import React, { Component } from 'react'
import ReactNative, { ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import { Card, ButtonGroup, Divider, SwipeDeck, Button} from 'react-native-elements'
import ActionButton from 'react-native-action-button';
import * as Progress from 'react-native-progress';
//import { BlurView, VibrancyView } from 'react-native-blur';
import Swiper from 'react-native-deck-swiper';
import LinearGradient from 'react-native-linear-gradient';
import FadeinView from '../core/FadeinView';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import Modal from 'react-native-simple-modal';
import ActionSheet from 'react-native-actionsheet'
import { PollCategoryImage } from '../Images';
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  
  Keyboard,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,
  findNodeHandle

} = ReactNative

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CANCEL_INDEX = 0
const options = [ 'Cancel', 'Flag Content'];
var reportReasons = [{name: 'Solicitation/Spam/bot', id: 'SO'}, {name: 'Copyright Infringement', id: 'CO'}, {name: 'Links to illegal content or filesharing sites', id: 'IL'}, {name: 'Pornographic Content', id: 'PO'}, {name: 'Other (describe below)', id: 'OT'}]
const config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
};

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class FriendSuggest extends Component {

  constructor(props) {
    super(props);
    this.imageLoaded = this.imageLoaded.bind(this)
    this.renderCard = this.renderCard.bind(this)
    this.state = {
      showContent: false,
      listViewData: [],
      visible: false,
      slideVisible: false,
      message: '',
      showMessage: false,
      viewRef: null,
      showCompatibility: false,
      cardIndex: 0,
      showModal: false,
      dialogOffset: 0
    }
    
    this.onSwipeRight = this.onSwipeRight.bind(this)
    this.onSwipeLeft = this.onSwipeLeft.bind(this) 
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this.onMenuItemPress = this.onMenuItemPress.bind(this)
  }


  _keyboardDidShow () {
    this.setState({
      dialogOffset: -200
    })
  }

  _keyboardDidHide () {
    this.setState({
      dialogOffset: 0
    })
  }

  componentWillMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount(){
    this.props.toggleDrawer(false);
  }

  componentDidMount(){
    this.props.toggleDrawer(true);
   this.props.nextSuggestedPerson(this.props.applicationToken, 10).then(() => {
     
    this.setState({
      showContent: true,
      cardIndex: 0
    })
   })
  }

  imageLoaded() {
    // this.setState({ viewRef: findNodeHandle(this.backgroundImage),
      
    // });
  }

  toggleCompatibility(){
    this.setState({
      showCompatibility: !this.state.showCompatibility
    })
  }

  showActionSheet(ProfileId) {
    this.setState({
      ProfileId: ProfileId
    },
    () => {
      this.ActionSheet.show()
    }
  )
  
  }

  renderCard(card) {
    
    return (
      // card.placeholder ?

      // <View style={[{flex: 1, justifyContent: 'center', backgroundColor: 'transparent', margin: 10, borderRadius: 8}]}>
      // <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
      //       <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
      //     </View>
      // </View>
      //   : 
        
        <View style={[{flex: 1, height: this.props.sideMenu ? Platform.OS == 'ios' ? SCREEN_HEIGHT - 80 : SCREEN_HEIGHT - 120 : Platform.OS == 'ios' ? SCREEN_HEIGHT - 80 : SCREEN_HEIGHT - 120, marginTop: this.props.sideMenu ? 0 : Platform.OS == 'ios' ? 24 : 24, backgroundColor: '#000', borderRadius: 8}]}>
        
        {card.Suggestion.AvatarUrl ?

            <ImageBackground
                style={[{width: SCREEN_WIDTH - 20,  height: this.props.sideMenu ? Platform.OS == 'ios' ? SCREEN_HEIGHT - 80 : SCREEN_HEIGHT - 120 : Platform.OS == 'ios' ? SCREEN_HEIGHT - 80 : SCREEN_HEIGHT - 120, borderRadius: 8}]}
                source={{uri: card.Suggestion.AvatarUrl}}
            >
            
            <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10, backgroundColor: 'transparent'}}>
                <View>
                <Text style={[{color: '#fff',fontSize: 18, fontWeight: '600', backgroundColor: 'transparent', },styles.shadowDark]}>{card.Suggestion.DisplayName}</Text>
                <Text style={[{color: '#fff',fontSize: 14, fontWeight: '600', backgroundColor: 'transparent', },styles.shadowDark]}>{card.Suggestion.Location}</Text>
                  </View>
                  <View>
                    <View style={{flexDirection: 'row'}}>
                <Text style={[{color: '#fff', fontSize: 16, fontWeight: '500'},styles.shadowDark]}>
                {card.Suggestion.Agree == 0 ? '0' : Math.ceil(card.Suggestion.Agree * 100 / card.Suggestion.Total)}%
                </Text>

                <TouchableOpacity style={{marginRight: 10, marginLeft: 15, }}  onPress={() => {this.showActionSheet(card.Suggestion.ProfileId)} } underlayColor='#999'>
                  <Icon style={{}} name='ellipsis-v' size={22} color={'#fff'} />
                </TouchableOpacity>
                </View>
                </View>
            </View>
            
            { this.state.showCompatibility ?
             <LinearGradient colors={['rgba(0,0,0,0.3)', 'rgba(0,0,0,0.8)']} style={[styles.linearGradient,{zIndex: 100, position: 'absolute', left: 0, right: 0, top: 0,bottom: 60}]}>
                <FadeinView style={{flex:1, justifyContent: 'center', alignItems: 'center', padding: 0, }}>
                  <View style={{marginBottom: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}>
                    <TouchableOpacity onPress={() => { this.toggleCompatibility() }} underlayColor='#999'>
                      <Icon style={{ textAlign: 'center', opacity: 1}} name='chevron-down' size={28} color={'#fff'} />
                    </TouchableOpacity>
              </View>
                {
                this.props.Suggestions[this.state.cardIndex].Compatilibity.map((l,i) => (
                  <View key={i} >
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', margin: 5 }}>
                      <View style={{ width: 50, justifyContent: 'flex-start', alignItems: 'flex-start', backgroundColor: 'transparent' }}>
                        <PollCategoryImage requestorProfileId={null} category={l.Name}></PollCategoryImage>
                      </View>
                      <View style={{ width: 100, justifyContent: 'flex-start', alignItems: 'flex-start', backgroundColor: 'transparent' }}>
                        <Text style={{ color: '#fff' }}>{l.Name}</Text>
                      </View>
                      <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', backgroundColor: 'transparent' }}>
                        <Text style={{ color: '#fff' }}>{l.matchingAnswer == 0 ? '0' : Math.ceil(l.matchingAnswer * 100 / l.sameQuestionAnswered)}%</Text>
                    </View>
                    </View>
                  <View key={i} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', margin: 5 }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 5, marginRight: 5 }}>
                      <Progress.Bar color='#2ECC71' unfilledColor='#F6F6F7' borderWidth={0} progress={l.matchingAnswer == 0 ? 0 : Math.ceil(l.matchingAnswer * 100 / l.sameQuestionAnswered)/100} height={16} width={SCREEN_WIDTH-100} />
                    </View>

                  </View>
                  </View>
                ))

              } 
              
              </FadeinView>
              </LinearGradient> 
              :
              null
            }
              
            <View style={{flex: 1, justifyContent: 'flex-end'}}>
            { !this.state.showCompatibility ?
            <View style={{marginBottom: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent'}}>
            <TouchableOpacity onPress={() => { this.toggleCompatibility() }} underlayColor='#999'>
            <Icon style={{ textAlign: 'center', opacity: 1,  }} name='chevron-up' size={28} color={'#fff'} />
            </TouchableOpacity>
            </View>
            :
            null
            }
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 0, margin: 0}}>
                <TouchableOpacity style={{backgroundColor: '#ff3566',flex: 1, padding: 5,height: 60}} onPress={() => { this.pass(card) }} underlayColor='#999'>
                  <View style={{backgroundColor: '#ff3566',flex: 1, borderRadius: 0, padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                      <Text style={{color: '#fff', backgroundColor: 'transparent', fontWeight: 'bold'}}>Pass</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor: '#1EA6CC',flex: 1, padding: 5,height: 60}} onPress={() => { this.follow(card) }} underlayColor='#999'>
                <View style={{backgroundColor: '#1EA6CC',flex: 1, borderRadius: 0, padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: '#fff', backgroundColor: 'transparent', fontWeight: 'bold'}}>Follow</Text>
                </View>
                </TouchableOpacity>
            </View>
            </View>
            
            </ImageBackground>
            : null }
            
        </View>
      
    )
  }

  sendMessage(){
    this.props.setLastSendingMessage(this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId,this.props.Suggestions[this.state.cardIndex].Suggestion.DisplayName,this.props.Suggestions[this.state.cardIndex].Suggestion.AvatarThumbnailUrl,this.state.message)
    this.props.sendMessage(this.props.applicationToken, this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId, this.state.message);
    this.setState({
      showModal: false,
      cardIndex: ++this.state.cardIndex
    })
    
  }

  follow(){
    
    this.props.follow(this.props.applicationToken, this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId).then(() => {
     
      if (this.props.Suggestions[this.state.cardIndex].Suggestion.FollowingMe == 1){
        this.setState({
          showModal: true
        })
      }else{
        this.setState({
          cardIndex: ++this.state.cardIndex
        })
        if (this.state.cardIndex == 9){
          this.setState({
            showContent: false
          })
          this.props.nextSuggestedPerson(this.props.applicationToken, 10).then(() => {
            this.setState({
              showContent: true,
              cardIndex: 0
            })
          })
        }
      }

      
     })
  }

  pass(){
    
    this.props.pass(this.props.applicationToken, this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId).then(() => {
     // this.props.setNextSuggestedPerson({Suggestion: [], Compatibility: []})
      // this.setState({
      //   showCompatibility: false
      // })
      this.setState({
        cardIndex: ++this.state.cardIndex
      })
      if (this.state.cardIndex == 9){
        this.setState({
          showContent: false
        })
        this.props.nextSuggestedPerson(this.props.applicationToken, 10).then(() => {
          this.setState({
            showContent: true,
            cardIndex: 0
          })
        })
      }
      // this.props.nextSuggestedPerson(this.props.applicationToken).then(() => {
        
      //  })
     })
  }

  onSwipeRight(i) {
    
    this.props.follow(this.props.applicationToken, this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId).then(() => {
     
      if (this.props.Suggestions[this.state.cardIndex].Suggestion.FollowingMe == 1){
        this.setState({
          showModal: true
        })
      }else{
        this.setState({
          cardIndex: ++this.state.cardIndex
        })
        if (this.state.cardIndex == 9){
          this.setState({
            showContent: false
          })
          this.props.nextSuggestedPerson(this.props.applicationToken, 10).then(() => {
            this.setState({
              showContent: true,
              cardIndex: 0
            })
          })
        }
      }
     })
  }
  
  onSwipeLeft(i) {
    
    this.props.pass(this.props.applicationToken, this.props.Suggestions[this.state.cardIndex].Suggestion.ProfileId).then(() => {
      //this.props.setNextSuggestedPerson({Suggestion: [], Compatibility: []})
      // this.setState({
      //   showCompatibility: false
      // })
      this.setState({
        cardIndex: ++this.state.cardIndex
      })
      if (this.state.cardIndex == 9){
        this.setState({
          showContent: false
        })
        this.props.nextSuggestedPerson(this.props.applicationToken, 10).then(() => {
          this.setState({
            showContent: true,
            cardIndex: 0
          })
        })
      }
     }) 
  }

  render() {
    return (
      
      <View style={[styles.container, {paddingBottom: this.props.sideMenu ? 0 : Platform.OS == 'ios' ? 50 : 0}]}>
       { this.props.sideMenu ?
        <LinearGradient
          start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
          locations={[0.2, 0.6, 1]}
          colors={['#1EA6CC', '#2FBCDA', '#3FD1E7']}
          style={{}}>

          <View style={{ height: 64, padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent' }}>
            <TouchableOpacity onPress={() => Actions.pop()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
              <Icon style={{opacity: 1}} name='chevron-left' size={22} color='#fff' />
              </View>
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, marginTop: 12 }}>Discovery</Text>
            <TouchableOpacity onPress={() => Actions.Search()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
                <Image source={require('../../assets/search.png')} style={{ resizeMode: 'contain', width: 22, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
        :
        null
       }
        {this.state.showContent
          ?
          <View style={{ flex: 1 }}>
          
            <Swiper
              ref={swiper => {
                this.swiper = swiper
              }}
              backgroundColor={'#000'}
              onSwipedTop={this.onSwipedTop}
              onSwipedBottom={this.onSwipedBottom}
              onSwipedLeft={this.onSwipeLeft}
              onSwipedRight={this.onSwipeRight}
              cards={this.props.Suggestions}
              cardIndex={this.state.cardIndex}
              cardVerticalMargin={0}
              marginBottom={15}
              marginTop={0}
              cardHorizontalMargin={10}
              showSecondCard={false}
              verticalSwipe={false}
              renderCard={this.renderCard}
              overlayLabels={{
              }}
              
              animateOverlayLabelsOpacity
              animateCardOpacity
            >
            </Swiper>
            
          </View>
          : 
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
            <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
          </View>
        }
          <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />

<Modal
          open={this.state.showModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({
            showResults: false
          })
          }
          closeOnTouchOutside={true}
          containerStyle={{
            flex: 1,
            
          }}
          modalStyle={{
            flex: 1,
            borderRadius: 0,
            margin: 0,
            padding: 20,
            paddingTop: 25,
            elevation: 5,
            backgroundColor: '#0f6aa6'
          }}
          disableOnBackPress={false}>
         { this.state.showModal ?
        <View style={{margin: 0,elevation: 5,flex: 1, }}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={[{backgroundColor:'transparent', textAlign: 'center', color: '#fff', fontSize: 32, fontWeight: '400', marginBottom: 15},styles.shadowDark]}>It's Mutual</Text>
            <Text style={[{backgroundColor:'transparent', textAlign: 'center', color: '#fff', fontSize: 14},styles.shadowDark]}>You and {this.props.Suggestions[this.state.cardIndex].Suggestion.DisplayName} now follow each other</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
            {this.props.currentUserProfile.AvatarUrl ? 
            <View style={[{width: 100, height: 100, borderRadius: 50, backgroundColor: 'transparent'}, styles.shadowDarker]}>
                <Image source={{uri: this.props.currentUserProfile.AvatarUrl}} style={[{width: 100, height: 100, borderRadius: 50}]}/>
                </View>
                :
                 <View style={[{backgroundColor: '#0074CD', width: 100, height: 100, borderRadius: 50,justifyContent: 'center', alignItems: 'center'},styles.shadowDarker]}>
                 <Text style={{color: '#fff'}}>{(this.props.currentUserProfile.DisplayName).split(" ").map((n)=>n[0])}</Text>
                 </View>
                }
            {this.props.Suggestions[this.state.cardIndex].Suggestion.AvatarThumbnailUrl ? 
            <View style={[{width: 100, height: 100, borderRadius: 50, backgroundColor: 'transparent'}, styles.shadowDarker]}>
                <Image source={{uri: this.props.Suggestions[this.state.cardIndex].Suggestion.AvatarThumbnailUrl}} style={[{width: 100, height: 100, borderRadius: 50}]}/>
                </View>
                :
                <View style={[{backgroundColor: '#0074CD', width: 100, height: 100, borderRadius: 50,justifyContent: 'center', alignItems: 'center'},styles.shadowDarker]}>
                 <Text style={{color: '#fff'}}>{(this.props.Suggestion[this.state.cardIndex].Suggestion.DisplayName).split(" ").map((n)=>n[0])}</Text>
                 </View>
                }
            

            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center'}}>
            <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5,
                  flex: 1
              }}>
                  <UselessTextInput
                      style={{marginRight: 10, paddingBottom: 10, color: '#fff', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={message => this.setState({ message })}
                      value={this.state.message}
                      placeholder='Send a message...'
                      placeholderTextColor='#fff'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={false}
                      editable={true}
                  />
                  
              </View>
              <TouchableOpacity onPress={() => { this.sendMessage() }} underlayColor='#999'>
              <Icon style={{opacity: 0.9,margin: 2}} name='send' size={18} color= {'#fff'} />
              </TouchableOpacity>
            </View>
            <View style={{ marginVertical: 15, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity style={{backgroundColor: 'transparent',flex: 1, padding: 5,}} onPress={() => { this.setState({showModal: false,cardIndex: ++this.state.cardIndex}) }} underlayColor='#999'>
                <View style={{backgroundColor: 'transparent',flex: 1, borderWidth: 1, borderColor: '#fff', padding: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: '#fff', backgroundColor: 'transparent', }}>Keep swiping</Text>
                </View>
                </TouchableOpacity>
            </View>
        </View>
      : null }
        </Modal>
        <Modal
          open={this.state.openReportModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openReportModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
            <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{'Report this Content'} </Text>
          </View>

          <ScrollView style={{height: 230}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 5}}>
            {reportReasons.map((l,i) => (
              this.renderReportReasons(l,i)
            ))}
          </View>
          </ScrollView>

              <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5
              }}>
                  <UselessTextInput
                      style={{ paddingBottom: 10, color: '#444', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={details => this.setState({ details })}
                      value={this.state.details}
                      placeholder='Details...'
                      placeholderTextColor='#d7d7d7'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={true}
                      editable={true}
                  />
              </View>
              <Button
                title='Report'
                buttonStyle={{margin: 5,marginBottom: 10, height: 35}}
                backgroundColor='#1EA6CC'
                color='#fff'
                textStyle={{ fontSize: 14, fontWeight: 'normal' }}
                onPress={() => { this.flagContent() }}
              />
        </Modal>
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          onPress={this.onMenuItemPress}
        />
        </View>
       
       );
  }


flagContent(){
  this.props.flagContent(this.props.applicationToken, 'PR', this.state.ProfileId, this.state.reasonCode, this.state.details).then((status) => {
    if (status){
      this.setState({
        openReportModal: false
      },
      () => {
        alert('Profile reported!')
      }
    )
    }
  })
}

selectReason(id,i){
  this.setState({
    reasonCode: id,
    selectedReason: i
  })
}

renderReportReasons(l,i){
  return (
    <TouchableOpacity key={i} onPress={() => {this.selectReason(l.id, i)}} underlayColor='#999'>
      <View style={{backgroundColor: this.state.selectedReason == i ? '#1EA6CC' : '#ccc', borderRadius: 15, margin: 5, padding: 8}}>
        <Text style={{fontSize: 12}}>{l.name}</Text>
      </View>
    </TouchableOpacity>
  )
}


 openReportModal(){
  this.setState({
    openReportModal: true,
  })
}

  onMenuItemPress(i){
    
   if (i== 1){
      this.openReportModal()
    }
   }

  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
      },
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#58C1F2',
        borderRadius: 15,
        borderColor: '#58C1F2',
        borderWidth: 0,
        shadowColor: '#999',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 3,
          width: 2,
        },
      },
      linearGradient: {
        flex: 1,
        paddingLeft: 0,
        paddingRight: 0,
        borderRadius: 0
      },
      absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        shadowOffset: {
          height: 1,
          width: 2,
        },
      },
      shadowDark:{
        shadowColor: '#444',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
          height: 2,
          width: 2,
        },
      },
      shadowDarker:{
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 7,
        shadowOffset: {
          height: 6,
          width: 4,
        },
      },
    imageContainer: {
    width: undefined,
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},

})

function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
  //  AvatarUrl: state.CURRENT_USER.AvatarUrl,
    Suggestions: state.NEXT_SUGGESTED_PERSON.Suggestions,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
  // Compatibility: state.NEXT_SUGGESTED_PERSON.Compatibility,
  }
}

export default connect(mapStateToProps)(FriendSuggest);