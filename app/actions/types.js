export const SET_SEARCHED_RECIPES = 'SET_SEARCHED_RECIPES';
export const ADD_RECIPE = 'ADD_RECIPE';
export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';
export const CURRENT_QUESTION = 'CURRENT_QUESTION';
export const CURRENT_ANSWERS = 'CURRENT_ANSWERS';
export const SUBMISSION_STATUS = 'SUBMISSION_STATUS';
export const GOTO_QUESTION = 'GOTO_QUESTION';
export const SAVE_APPLICATION_TOKEN = 'SAVE_APPLICATION_TOKEN';
export const SET_LOGGEDIN_STATUS = 'SET_LOGGEDIN_STATUS';
export const SET_APPLICATION_TOKEN_STATUS = 'SET_APPLICATION_TOKEN_STATUS';
export const LOGIN = 'LOGIN';
export const LOGIN_WITH_FACEBOOK = 'LOGIN_WITH_FACEBOOK';
export const LOGOUT = 'LOGOUT';
export const SET_USER = 'SET_USER';
export const CURRENT_USER = 'CURRENT_USER';
export const COMMENTS = 'COMMENTS';
export const REPLIES = 'REPLIES';
export const REACTION_TYPES = 'REACTION_TYPES';
export const SET_POLL_RESPONSES = 'SET_POLL_RESPONSES';
export const SET_RESPONSE_SETTINGS = 'SET_RESPONSE_SETTINGS';
export const SET_CHATROOMS = 'SET_CHATROOMS';
export const SET_CHAT_MESSAGES = 'SET_CHAT_MESSAGES';
export const SET_PENDING_RECEIVED = 'SET_PENDING_RECEIVED';
export const SET_FRIENDS = 'SET_FRIENDS';
export const TIMELINE = 'TIMELINE'
export const CURRENT_USER_PROFILE = 'CURRENT_USER_PROFILE';
export const IMAGE_UPLOAD_TOKEN = 'IMAGE_UPLOAD_TOKEN';
export const PROFILE_RESPONSES = 'PROFILE_RESPONSES'
export const FOLLOWING_COMPATIBILITY = 'FOLLOWING_COMPATIBILITY'
export const PROFILE_IMAGES = 'PROFILE_IMAGES'
export const NOTIFICATIONS = 'NOTIFICATIONS'
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION'
export const REMOVE_UNREAD_NOTIFICATION = 'REMOVE_UNREAD_NOTIFICATION'
export const IMAGE_POST_DIALOG = 'IMAGE_POST_DIALOG'
export const MAP_DATA = 'MAP_DATA'
export const POLL_CATEGORIES = 'POLL_CATEGORIES'
export const MAP_FILTERS = 'MAP_FILTERS'
export const FOLLOWING = 'FOLLOWING'
export const FOLLOWERS = 'FOLLOWERS'
export const FRIEND_RESPONSES = 'FRIEND_RESPONSES'
export const ADD_REACTION = 'ADD_REACTION'
export const ADD_COMMENT_REACTION = 'ADD_COMMENT_REACTION'
export const ADD_REPLY_REACTION = 'ADD_REPLY_REACTION'
export const NEXT_SUGGESTED_PERSON = 'NEXT_SUGGESTED_PERSON'
export const ADD_COMMENT = 'ADD_COMMENT'
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION'
export const REMOVE_LOCAL_IMAGE = 'REMOVE_LOCAL_IMAGE'
export const NEWS_FEED_LAST_TIMESTAMP = 'NEWS_FEED_LAST_TIMESTAMP'
export const NEWS_FEED_FIRST_TIMESTAMP = 'NEWS_FEED_FIRST_TIMESTAMP'
export const ADD_TIMELINE_ITEMS_BOTTOM = 'ADD_TIMELINE_ITEMS_BOTTOM'
export const ADD_TIMELINE_ITEMS_TOP = 'ADD_TIMELINE_ITEMS_TOP'
export const ADD_TIMELINE_REACTION = 'ADD_TIMELINE_REACTION'
export const NOTIFICATIONS_LAST_TIMESTAMP = 'NOTIFICATIONS_LAST_TIMESTAMP'
export const ADD_NOTIFICATIONS_BOTTOM = 'ADD_NOTIFICATIONS_BOTTOM'
export const COUNTRIES = 'COUNTRIES'
export const CLIENT_CODE = 'CLIENT_CODE'
export const CHATROOMS = 'CHATROOMS'
export const ADD_CHATROOM = 'ADD_CHATROOM'
export const TOTAL = 'TOTAL'
export const SET_LAST_SENDING_MESSAGE = 'SET_LAST_SENDING_MESSAGE'
export const ADD_PROFILE_RESPONSES_ITEMS_BOTTOM = 'ADD_PROFILE_RESPONSES_ITEMS_BOTTOM'
export const ADD_OTHER_PROFILE_RESPONSES_ITEMS_BOTTOM = 'ADD_OTHER_PROFILE_RESPONSES_ITEMS_BOTTOM'
export const OTHER_PROFILE_RESPONSES = 'OTHER_PROFILE_RESPONSES'
export const ADD_PROFILE_RESPONSE_REACTION = 'ADD_PROFILE_RESPONSE_REACTION'