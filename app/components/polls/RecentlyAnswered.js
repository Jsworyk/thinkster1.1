import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  FlatList,
  TouchableHighlight,
  StyleSheet,

} = ReactNative


class RecentlyAnswered extends Component{

  constructor(props){
    super(props);
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });
    this.state = {
      ds:[],
      dataSource:ds,
      showLoading: true,
      showQuestions: false,

    }
    this.renderItem = this.renderItem.bind(this);
  }
  
  componentWillMount(){
    if (this.props.isLoggedin) {
        if (this.props.pollResponses == null){
          //this.getPollResponses();
        }else {
          this.setState({
          dataSource: this.props.pollResponses,
          showQuestions: true,
          showLoading: false
        })
      }
    }
  }

  componentDidMount(){
    if (this.props.isLoggedin) {
      this.getPollResponses();
    }
  }

  getPollResponses(){
    const _this = this;
    this.props.getPollResponses(this.props.applicationToken, this.props.ProfileId).then(() => {
      _this.setState({
        dataSource: _this.props.pollResponses,
        showQuestions: true,
        showLoading: false
      })
    });
  }


  onQuestionPress(question,answers){

    this.props.gotoQuestion(question,answers);
    this.setTimeout(
      () => {
          Actions.Answer();
      },
      300
    )

  }

  renderItem (l, i) {
   return (

              //  <ListItem
              //   containerStyle={{padding: 0, margin: 0}}
              //    key={sectionID}
              //    title={rowData.Question}
              //    titleStyle={styles.username}
              //    onPress={() => {}//this.onQuestionPress(rowData.question,rowData.answers)
              //    }
              //    subtitle={
              //      <View style={styles.subtitleView}>

              //      <View style={{flexDirection: 'row',paddingLeft: 10}}>
              //        <Icon style={{opacity: 1,margin: 2}} name='comment-o' size={16} color='#2b8abb' />
              //        <Text style={[styles.countText, {}]}>
              //          {rowData.Response}
              //        </Text>
              //      </View>

              //      </View>

              //    }
              //    hideChevron={true}
              //  />
              <View key={i} style={{marginBottom: 5, backgroundColor: '#fff', }}>
              <View style={{borderBottomColor: '#134fa1',borderBottomWidth: 0.5, flexDirection: 'row',padding: 8,backgroundColor: 'rgba(255,255,255,0.2)' }}>
              <View style={{margin: 5,marginLeft: 15}}>
                <Image source={{uri: l.ChatRoomAvatar}} style={[styles.avatarStyle]} ></Image>
              </View>
              <View style={{flex: 1, margin: 3}}>
                <View style={{marginBottom: 3, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{flexDirection: 'row', }}>
                  <Text style={{color: '#134fa1', fontSize: 14}}>{this.props.currentUserProfile.DisplayName}</Text>
                  <Text style={{color: '#1EA6CC', fontSize: 14}}>{' answered the question'}</Text>
                  </View>
                  <View style={{backgroundColor: '#fff', padding: 3,paddingLeft: 8, paddingRight: 8, borderRadius: 40}}>
                    <Text style={{color: '#1EA6CC', fontSize: 11, backgroundColor: 'transparent'}}>9 june</Text>
                  </View>
                </View>
                <View style={{marginBottom: 5, flexDirection: 'row', justifyContent: 'flex-start'}}>
                  <Text style={{color: '#134fa1', fontSize: 14}}>{l.Question}</Text>
                </View>
              </View>
              </View>
              <View style={{flexDirection: 'row',justifyContent: 'space-around', padding: 6,backgroundColor: 'rgba(255,255,255,0.2)' }}>
                <View style={{flexDirection: 'row'}}>
                  <Icon style={{ textAlign: 'center', opacity: 1, marginTop: 1 }} name='thumbs-o-up' size={14} color='#1EA6CC' />
                  <Text style={{color: '#1EA6CC', marginLeft: 10}}>Like</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Icon style={{ textAlign: 'center', opacity: 1, marginTop: 1 }} name='comment' size={14} color='#1EA6CC' />
                  <Text style={{color: '#1EA6CC', marginLeft: 10}}>Comment</Text>
                </View>
              </View>
            </View>

   )
 }

 _keyExtractor = (item, index) => index

  render(){
    return (
      <View style={{ flex: 1, marginBottom: 50, backgroundColor: '#E5EBEF'}}>
      {this.state.showLoading
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#134fa2'/>
        </View>
        :
        <FlatList
            extraData={this.state.dataSource}
            data={this.state.dataSource}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        
      
      }

     </View>
    )
  }
}

const styles = StyleSheet.create({

  subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  countText: {
    fontSize: 14,
    color: '#2b8abb',
    margin: 2,
  },
  username: {
    color: '#696868',
    fontWeight: 'bold',
    fontSize: 16,
  },
})

reactMixin(RecentlyAnswered.prototype, TimerMixin)
function mapStateToProps(state){

  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    ProfileId: state.CURRENT_USER.ProfileId,
    pollResponses: state.SET_POLL_RESPONSES.pollResponses,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile
  }
}

export default connect (mapStateToProps)(RecentlyAnswered);
