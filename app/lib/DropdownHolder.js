export default class DropDownHolder {
    static dropDown = {};

    static setDropDown(ref, dropDown) {
        this.dropDown[ref] = dropDown;
    }

    static getDropDown(ref) {
        return this.dropDown[ref];
    }
}