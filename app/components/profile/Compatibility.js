import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import * as Progress from 'react-native-progress';
import ProgressCircle from 'react-native-progress-circle'
import ActionSheet from 'react-native-actionsheet'

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  FlatList,
  TouchableHighlight,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} = ReactNative
var index = 0;
var width = Dimensions.get('window').width;
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 2;
const options = [ 'Cancel', 'Report', 'Block', 'Unfollow']
class Compatibility extends Component{

  constructor(props){
    super(props);
    
    this.state = {
      dataSource: [],
      showLoading: true,
      showQuestions: false,
      selectedPollCategory: null,
      
    }
    this.renderItem = this.renderItem.bind(this);
  }
  
  follow(FollowerId){
    this.setState({
        showLoading: true
    })
    this.props.follow(this.props.applicationToken, FollowerId).then(() => {
        this.props.getFollowing(this.props.applicationToken, this.props.ProfileId).then(() => {
        this.getFollowers()
        })
    })
  
  }

unfollow(ProfileId){
    this.setState({
      ProfileId: ProfileId
    })
   this.ActionSheet.show()
}

  onMenuItemPress(i){
    if (i == 1){
        
    }else if (i == 2){
      this.props.block(this.props.applicationToken, this.state.ProfileId).then(() => {
        this.setState({
          ProfileId: null
        })
        this.getFollowingCompatibility('');
      })
    }else if (i == 3){
      this.props.unfollow(this.props.applicationToken, this.state.ProfileId).then(() => {
        this.setState({
          ProfileId: null
        })
        
          this.getFollowingCompatibility('');
        
      })
    }
   }

  componentDidMount(){
    if (this.props.isLoggedin) {
      this.props.getPollCategories(this.props.applicationToken).then(() => {
        this.getFollowingCompatibility('');
      })
    }
  }

  getFollowingCompatibility(pollCategoryId){
    const _this = this;
    this.props.getFollowingCompatibility(this.props.applicationToken, pollCategoryId).then(() => {
      _this.setState({
        dataSource: _this.props.followingCompatibility,
        showLoading: false
      })
    });
  }

  onCategoryChange(direction){
    this.setState({
      showLoading: true
    })
    if (this.state.selectedPollCategory == null){
      if (direction == 'right'){
      this.setState({
        selectedPollCategory: index,
      },
      () => {
        this.getFollowingCompatibility(this.props.pollCategories[index].PollCategoryId)
      }
    )
  }else {
    this.setState({
      selectedPollCategory: this.props.pollCategories.length-1,
    },
    () => {
      index = this.props.pollCategories.length-1;
      this.getFollowingCompatibility(this.props.pollCategories[index].PollCategoryId)
    }
  )
  }
    }else{
      if (direction == 'right'){
          if (index == this.props.pollCategories.length-1){
            index = 0;
            this.setState({
              selectedPollCategory: null
            },
            () => {
              this.getFollowingCompatibility('')
            }
          )
          }else{
          this.setState({
            selectedPollCategory: index++
          },
          () => {
            this.getFollowingCompatibility(this.props.pollCategories[index].PollCategoryId)
          })
        }
      }else{
        if (index == 0 && this.state.selectedPollCategory == null){
            this.getFollowingCompatibility('')
        }else if (index == 0 ){
          this.setState({
            selectedPollCategory: null
          },
          () => {
            this.getFollowingCompatibility('')
          }
        )
          
        }
        else{
          this.setState({
            selectedPollCategory: index--
          },
          () => {
            this.getFollowingCompatibility(this.props.pollCategories[index].PollCategoryId)
          }
        )
        }
      }
    }
  }

  renderItem (l, i) {
   return (
    <View style={{ paddingHorizontal: 10, flexDirection: 'row', margin: 0, justifyContent: 'space-around', alignItems: 'center', borderBottomColor: 'rgba(255,255,255,0.4)', borderBottomWidth: 0.5 }}>
            <View style={{ flexDirection: 'row', flex: 1, margin: 10, justifyContent: 'flex-start', alignItems: 'center',  }}>
            {l.AvatarMicrothumbUrl ?
                <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                <Image
                    style={{ borderRadius: 20, width: 40, height: 40 }}
                    source={{ uri: l.AvatarMicrothumbUrl }}
                />
                </TouchableOpacity>
                :
                <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: '#ccc' }}></View>
            }
            <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
              <Text style={{flexWrap: 'wrap', marginLeft: 5,marginRight: 35, color: '#fff', fontWeight: 'normal',fontSize: 14 }}>{l.DisplayName}</Text>
            </TouchableOpacity>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 10 }}>
            <ProgressCircle
                percent={Math.ceil(l.Agree * 100 / l.Total)}
                radius={20}
                borderWidth={3}
                color="#fff"
                shadowColor="#999"
                bgColor="#067FB9"
            >
                <Text style={{ color: '#fff', fontSize: 11 }}>{Math.ceil(l.Agree * 100 / l.Total)}%</Text>
            </ProgressCircle>
        </View>
                
        
          <TouchableOpacity onPress={() => { this.unfollow(l.ProfileId) }} underlayColor='#999'>
          <View style={{width: 75, backgroundColor: '#fff', borderColor: '#fff', borderWidth: 1, borderRadius: 5,  margin: 10, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ backgroundColor: 'transparent', padding: 5, fontSize: 11, color: '#067FB9', fontWeight: 'normal' }}>{'Following'}</Text>
          </View>
          </TouchableOpacity>
                

            </View>
   )
 }

 _keyExtractor = (item, index) => index

  render(){
    return (
      <View style={{ flex: 1, marginBottom: 50, backgroundColor: '#067FB9'}}>
      {this.state.showLoading
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#fff'/>
        </View>
        :
        this.state.dataSource.length == 0 ?
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10}}>
          <Text>No Compatibility Found</Text>
        </View>
        :
        <View style={{flex: 1}}>
          <View style={[{paddingHorizontal: 20, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: this.state.selectedPollCategory == null ? '#25AFD2' : this.props.pollCategories[index].BackgroundColor , padding: 10},styles.shadow]}> 
            
            <TouchableOpacity onPress={() => { this.onCategoryChange('left') }} underlayColor='#999'>
            <Icon style={{}} name='arrow-left' size={18} color='white' />
            </TouchableOpacity>
            <View style={{justifyContent: 'center', alignItems: 'center', width: 250, flexWrap: 'wrap', flexDirection: 'row'}}>
            { 
              this.state.selectedPollCategory == null ?
              <Image source={require('../../assets/categories-white/overall-compatibility-white.png')} style={{marginRight: 5, resizeMode: 'contain', width: 18, height: 14}}/>
              :
                  this.props.pollCategories[index].Name == 'Entertainment' ?
                    <Image source={require('../../assets/categories-white/entertainment-white.png')} style={{marginRight: 5,resizeMode: 'contain', width: 20, height: 20}}/>
                    :
                    this.props.pollCategories[index].Name == 'Personality' ?
                    <Image source={require('../../assets/categories-white/personality-white.png')} style={{marginRight: 5,resizeMode: 'contain', width: 16, height: 20}}/>
                    :
                    this.props.pollCategories[index].Name == 'Food' ?
                    <Image source={require('../../assets/categories-white/food-white.png')} style={{marginRight: 5,resizeMode: 'contain', width: 20, height: 20}}/>
                    :
                    this.props.pollCategories[index].Name == 'Relationships' ?
                    <Image source={require('../../assets/categories-white/relationships-white.png')} style={{marginRight: 5,resizeMode: 'contain', width: 18, height: 16}}/>
                    :
                    this.props.pollCategories[index].Name == 'Politics' ?
                    <Image source={require('../../assets/categories-white/politics-white.png')} style={{marginRight: 5,resizeMode: 'contain', width: 18, height: 14}}/>
                    :
                    null
                  }
              <Text style={{textAlign: 'center', color: '#fff', fontSize: 16, fontWeight: 'bold'}}>{this.state.selectedPollCategory == null ? 'Overall Compatibility' : this.props.pollCategories[index].Name}</Text>
            </View>
            <TouchableOpacity onPress={() => { this.onCategoryChange('right') }} underlayColor='#999'>
            <Icon style={{}} name='arrow-right' size={18} color='white' />
            </TouchableOpacity>
          </View>
        <FlatList
            extraData={this.state.dataSource.sort(function(a, b) {
                    return parseFloat(Math.ceil(b.Agree * 100 / b.Total)) - parseFloat(Math.ceil(a.Agree * 100 / a.Total));
                })}
            data={this.state.dataSource.sort(function(a, b) {
                    return parseFloat(Math.ceil(b.Agree * 100 / b.Total)) - parseFloat(Math.ceil(a.Agree * 100 / a.Total));
                })}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </View>
        
      }
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    options={options}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={(i) => {this.onMenuItemPress(i)}}
                />
     </View>
    )
  }
}

const styles = StyleSheet.create({

  subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  countText: {
    fontSize: 14,
    color: '#2b8abb',
    margin: 2,
  },
  username: {
    color: '#696868',
    fontWeight: 'bold',
    fontSize: 16,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.90)',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 2,
    },
  }
})


function mapStateToProps(state){

  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    ProfileId: state.CURRENT_USER.ProfileId,
    followingCompatibility: state.FOLLOWING_COMPATIBILITY.followingCompatibility,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    pollCategories: state.POLL_CATEGORIES.pollCategories
  }
}

export default connect (mapStateToProps)(Compatibility);
