import React, { Component, PropTypes } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
// const FBSDK = require('react-native-fbsdk');
// const {
//   LoginButton,
//   LoginManager,
//   AccessToken
// } = FBSDK;

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,

} = ReactNative

class ControlPanel extends Component {

   constructor(props) {
    super();
    this.state = {
      visible: false
    };
  }

  static propTypes = {
    closeDrawer: PropTypes.func.isRequired
  };

  render() {
    let {closeDrawer} = this.props
    return (
      <ScrollView style={styles.container}>
        <View style={styles.drawerHeader}>
        {this.props.currentUserProfile ?
          <View style={styles.drawerHeaderInner}>
          
          <View style={{flexDirection: 'row', alignItems: 'center' }}>

          { this.props.currentUserProfile.AvatarUrl ?
            <Image
            style={{borderRadius: 15, width: 30, height: 30}}
              source={{uri: this.props.currentUserProfile.AvatarUrl}}
              />
              :
             null
           }
            <Text style={{textAlign: 'center', marginLeft: 10, color: '#fff'}}>
            {this.props.currentUserProfile.DisplayName}
            </Text>
            </View>
             
            
          </View>
          :
          null
        }
        </View>
        <View style={styles.drawerBody}>
        <TouchableOpacity onPress={() => {closeDrawer(); Actions.Search(); }}>
          <View style={styles.LoginButton} >
            <View style={styles.ButtonIconContainer}>
          <Icon style={styles.ButtonIcon} name='search' size={16} color='#fff' />
            </View>
            <View style={styles.ButtonTextContainer}>
              <Text style={styles.ButtonText}>
                Search
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {closeDrawer(); Actions.FriendSuggest({sideMenu: true, }); }}>
          <View style={styles.LoginButton} >
            <View style={styles.ButtonIconContainer}>
          <Icon style={styles.ButtonIcon} name='user-o' size={16} color='#fff' />
            </View>
            <View style={styles.ButtonTextContainer}>
              <Text style={styles.ButtonText}>
                Discovery
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
            closeDrawer();
             this.setState({
              visible: true
            })
            //LoginManager.logOut();
            this.props.logout(this.props.applicationToken).then(() => {
            if (!this.props.isLoggedin){
              this.props.toggleDrawer(true);
              this.setState({
                visible: false
              })
              Actions.Home({ type: 'reset' });
            }
          })
          }}>
          <View style={styles.LoginButton} >
            <View style={styles.ButtonIconContainer}>
          <Icon style={styles.ButtonIcon} name='sign-out' size={16} color='#fff' />
            </View>
            <View style={styles.ButtonTextContainer}>
              <Text style={styles.ButtonText}>
                Logout
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        </View>
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#067BB9',
  },
  drawerHeader: {
    backgroundColor: '#067BB9',
    height: 80,
    padding: 10,
    paddingTop: 25

  },
  drawerHeaderInner: {
    marginTop: 25
  },
  drawerBody: {
    padding: 10,
    paddingTop: 25
  },
  circle: {
  width: 60,
  height: 60,
  borderRadius: 100/2,
  },
  controlText: {
    color: 'white',
  },
  LoginButton: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 35,
    marginBottom: 5,
    width: 173,
    borderRadius: 3
  },
  ButtonIconContainer: {
    alignItems: 'flex-start',
    width: 35,
    marginLeft: 5
  },
  ButtonTextContainer: {
    alignItems: 'flex-end'
  },
  ButtonIcon: {

  },
  ButtonText: {
    color: '#fff',
    fontWeight: 'normal'
  }
});

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    //DisplayName: state.CURRENT_USER.DisplayName,
    //AvatarUrl: state.CURRENT_USER.AvatarUrl
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile
  }
}

export default connect (mapStateToProps)(ControlPanel);
