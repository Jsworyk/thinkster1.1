import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    buttonContainer: {
        width: 42
    },
    button: {
        marginTop: 12 
    },
    buttonImage: { 
        resizeMode: 'contain', width: 22, height: 20
    },
    rightAlign: {
        alignSelf: 'flex-end'
    }
});

export default styles;