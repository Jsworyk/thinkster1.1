
import React, {Component} from 'react';

import {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Platform,
  RecyclerViewBackedScrollView,
  Text,
  FlatList,
  TouchableOpacity,
  StatusBar,
  View,
} from 'react-native';
import { connect } from 'react-redux'
import { List, Card, ListItem, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import { Actions } from 'react-native-router-flux';
import Swipeout from 'react-native-swipeout';
import LinearGradient from 'react-native-linear-gradient';
import {SearchButton, DrawerButton} from '../Buttons';
import { ThinksterNavBar } from '../Layout';



class Notifications extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
     showNotifications: true,
     notifications: null,
     i: null,
     scrollEnabled: true,
     drawerOpen: false,
     refreshing: false
    }
    this.getNotifications = this.getNotifications.bind(this)
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  getNotifications(){
    this.setState({
      refreshing: true
    })
    this.markAllNotificationsRead();
    this.props.getNotifications(this.props.applicationToken, this.props.FromTimestamp, 15, 'bottom').then(() => {
      this.setState({
        refreshing: false
      })
    })
  }

  deleteNotification(NotificationId, i){
    this.props.deleteNotification(this.props.applicationToken, NotificationId).then(() => {
      this.props.removeLocalNotification(i-1);
    })
  }

  componentDidMount(){
    this.markAllNotificationsRead();
    //Actions.refresh({hideNavBar: false})
  }

  markAllNotificationsRead(){
    this.props.markAllNotificationsRead(this.props.applicationToken).then(() => {
      this.props.removeLocalUnreadNotification();
    })
  }

  //TODO Below function is garbage and needs to be replaced
  outputDate(dt) {
    let dateDiff = Math.abs(new Date() - new Date(dt));
    let output = parseInt(dateDiff / 36e5) == 0 ? parseInt( (dateDiff / 1000) / 60 ) + 'min' :  parseInt(dateDiff / 36e5) <= 24 ? parseInt(dateDiff / 36e5) + 'h' : Math.floor(parseInt(dateDiff / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(dateDiff / 36e5) / 24) < 31 ? Math.floor(parseInt(dateDiff / 36e5) / 24) + 'd' : (Math.floor(parseInt(dateDiff / 36e5) / 24) == 30 || Math.floor(parseInt(dateDiff / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(dateDiff / 36e5) / 24) / 30) + 'm';
    
    return output;
  }

  pressedNotification(type, l) {
    if (type === 'p') {
      Actions.Question({pollName: l});
    } else if (type === 'u') {
      Actions.UserProfile({ProfileName: l});
    } else if (type === 'm') {
      Actions.ImagePost({Selector:l});
    } else {
      return null;
    }
  }

  renderItem(l, i){
    
    var regex = /\[\[([upm]):([^:]+):([^\]]+)\]\]/g;
    var matches = l.Text.match(/\[\[([upm]):([^:]+):([^\]]+)\]\]/g);
    var PrasedTextArray = []
    var ids = []
    var inners = []
    var types = []    
   
    for (var i in matches){
      PrasedTextArray.push(matches[i].replace(regex, function(string, first, second, third){
        ids.push(second)
        types.push(first)
        return (
          third
        )
      }))
    }

    var lastMatch = ''
    var lastMatchedIndex = 0;
    var finalIndex = 0;
    while ((m = regex.exec(l.Text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      if (m.index != 0){
        var text_to_get = l.Text.substring(lastMatchedIndex + lastMatch.length, m.index )
          inners.push(text_to_get); 
      }
      lastMatch = m[0]
      lastMatchedIndex = m.index;
      finalIndex = regex.lastIndex;
    }
    if (finalIndex < l.Text.length){
      var text_to_get = l.Text.substring(finalIndex, l.Text.length)
      inners.push(text_to_get); 
    }

    var swipeoutBtns = [
      {
        component: (
        <View style={{flex: 1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center'}}>
           <Icon size={20}
            style={{ opacity: 1, color: '#fff',margin: 2,fontWeight: '500'}} name={'close'} />
          <Text style={{margin: 2,color: '#fff', fontWeight: '500'}}>Hide</Text>
        </View>
        ),
        onPress: () => this.deleteNotification(l.NotificationId, i),
      }
    ]
    return (
      <Swipeout right={swipeoutBtns} scroll={(scrollEnabled) => { this.setState({ scrollEnabled }) }} >
        <View key={i} style={{borderBottomColor: 'rgba(0,118,181,0.4)', borderBottomWidth: 2, backgroundColor: l.IsRead ? '#2DBAD9' : '#3FD2E8'}}>
          {
            ids.map((listItem,count) => (
              <TouchableOpacity key={count} onPress={() => this.pressedNotification(types[count], listItem)} underlayColor='#999' >
              
                <View style={{flexDirection: 'row',padding: 5, }}>
                  <View style={{margin: 5}}>
                    { l.AvatarMicrothumbUrl ? 
                      <Image source={{uri: l.AvatarMicrothumbUrl}} style={styles.userImage} ></Image>
                    : 
                      <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{color: '#fff'}}>{(PrasedTextArray[0]).split(" ").map((n)=>n[0])}</Text>
                      </View>
                    }
                  </View>
                  <View style={{flex: 1, margin: 5}}>
                    <View style={{marginBottom: 5}}>
                      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>

                        <Text style={{lineHeight: 20, fontWeight: 'bold', color: '#fff', fontSize: 14, margin: 0}}>{PrasedTextArray[count]}</Text>
                        {
                          inners[count] ?
                            <Text style={{lineHeight: 20, color: '#fff',fontSize: 14, margin: 0}}>{inners[count]}</Text>
                            :
                            null
                        }
                      </View>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start'}}>
                        {l.Emoticon ?
                          <Text style={{fontSize: 12, padding: 2}}>{l.Emoticon}</Text>
                          :
                          <Text style={{fontSize: 12, padding: 2}}>{''}</Text>
                        }
                        <Text style={{color: '#fff', fontSize: 12, padding: 2}}>
                        {
                          this.outputDate(l.NotificationDate)
                        }
                        </Text>
                      </View>
                    </View>
                  </View>
              
                </View>
              </TouchableOpacity>
            ))
          }
        </View>
      </Swipeout>
    )
  }

  toggleDrawer(){
    
    this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
    this.setState({
      drawerOpen: !this.state.drawerOpen
    })
  }

  _keyExtractor = (item, index) => index

  render() {
    return (
      <View style={styles.container}>
      <StatusBar
     backgroundColor="#057AB8"
     setBarStyle={{borderBottomWidth: 0, opacity: 0.9}}
     barStyle="light-content"/>
           <ThinksterNavBar toggleDrawer={this.toggleDrawer} title="Notifications"></ThinksterNavBar>

          <View style={{flex: 1, justifyContent: 'center'}}>
            {this.state.showNotifications ? 
            this.props.notifications.length > 0 ?
          <FlatList
            // refreshing={this.state.refreshing}
            // onRefresh={this.getNotifications}
            scrollEnabled={this.state.scrollEnabled}
            extraData={this.props.notifications}
            data={this.props.notifications}
            onEndReached={this.getNotifications}
            onEndReachedThreshold={0}
            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
            keyExtractor={this._keyExtractor}
            renderItem={  ({item, index}) =>  this.renderItem(item, index)  }
            />
      
            :
            <View style={[{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderColor: '#ccc', borderWidth: 0.5, padding: 10, margin: 10}, styles.shadow]}>
          <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
            <Text style={{fontSize: 30}}>&#x1F44D;</Text>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 10, margin: 5}}>
            <Text style={{color: '#444', fontSize: 16, fontWeight: 'bold'}}>Your Notifications</Text>
            <Text style={{color: '#999', fontSize: 14, fontWeight: 'normal', margin: 5, textAlign: 'center'}}>Your followers reactions to your responses and photos will appear here.</Text>
          </View>
        </View>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
              <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#fff' />
          </View>
            }
         </View>
     
</View>

    );
  }
}


var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 50,
    //marginTop: Platform.OS === 'ios' ? 64 : 54,
    backgroundColor: '#1EA2CC',
    
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    resizeMode: 'contain'
  },
  row:{
    flex:1,
    flexDirection:'row',
    padding:18,
    borderBottomWidth: 1,
    borderColor: '#0076B5',
  },
  selectionText:{
    fontSize:15,
    paddingTop:3,
    color:'#b5b5b5',
    textAlign:'right'
  },
  subtitleView: {
    flexDirection: 'column',
    paddingLeft: 10,
    paddingTop: 5,
    justifyContent: 'space-between',
  },

  timeText: {
    textAlign: 'right',
    color: '#696868',
    fontSize: 11,
  },
  username: {
    color: '#2b8abb',
    fontWeight: 'bold',
    fontSize: 14,
  },
  subtitle: {
    color: '#696868'
  },
  listContainer: {
    marginRight: 5, marginBottom: 50,marginTop: 0
  },
  avatarStyle: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginRight: 10
  },
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
});

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    ProfileId: state.CURRENT_USER.ProfileId,
    notifications: state.notifications.notifications,
    unreadNotifications: state.notifications.unreadNotifications,
    FromTimestamp: state.NOTIFICATIONS_LAST_TIMESTAMP.FromTimestamp,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
  }
}

export default connect (mapStateToProps)(Notifications);
