import { StyleSheet } from 'react-native';
import { Colors, DefaultStylesObject } from '../../lib/styles';
const styles = StyleSheet.create({
    ...DefaultStylesObject,
    input: {
        flex: 1,
        height: 40,
        fontSize: 14,
        paddingRight: 10,
        paddingLeft: 20,
        backgroundColor: '#fff',
        color: '#444',
    },
    starOutput: {
        fontSize: 30,
        paddingLeft:10,
        paddingRight:10,
        color: Colors.white,
    },
    starTextContainer: {
        borderBottomColor: Colors.white,
        borderBottomWidth: 1,
        marginBottom:20,
    },
    starText: {
        color: Colors.white,  textDecorationLine:'underline', textAlign:'center'
    },
    starContainer:{
        alignItems:'center',
        alignSelf:'stretch',
        backgroundColor: Colors.main,
        padding: 10,
        borderRadius: 5,
    },
    followerContainer:{
        flex:1,
        alignSelf: 'stretch',
    },
    followerImageContainer: {
        height:90,
        padding:5,
        backgroundColor: Colors.gray,
        borderColor: Colors.black,
        flex:1,
    },
    followerInputContainer:{
        height:75,
        padding:5,
        borderColor: Colors.black,
        flex:1,
    },
    userImage: {
        width: 80,
        height: 80,
        borderRadius: 3,
        resizeMode: 'contain'
      },
})

export default styles;