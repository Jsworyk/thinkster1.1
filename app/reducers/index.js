import { combineReducers } from 'redux';
import * as appReducer from './core';

export default combineReducers(Object.assign(appReducer));
