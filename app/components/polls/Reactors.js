import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import * as Progress from 'react-native-progress';
import ProgressCircle from 'react-native-progress-circle'
import ActionSheet from 'react-native-actionsheet'
const {
      ScrollView,
    View,
    TextInput,
    ListView,
    Image,
    Text,
    FlatList,
    TouchableHighlight,
    StyleSheet,
    Dimensions,
    Platform,
    TouchableOpacity,
    
    } = ReactNative

var index = 0;
var width = Dimensions.get('window').width;
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 2;
const options = [ 'Cancel', 'Report', 'Block', 'Unfollow']

class Reactors extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            showLoading: true,
            showQuestions: false,
            selectedPollCategory: null,

        }
        this.renderItem = this.renderItem.bind(this);
    }

    componentDidMount() {
        if (this.props.isLoggedin) {
            this.getReactors(this.props.TargetId, this.props.TargetTypeCode);
        }
    }

    getReactors(TargetId, TargetTypeCode){
        this.props.getReactors(this.props.applicationToken, TargetTypeCode, TargetId).then((reactors) => {
            
          this.setState({
            dataSource: reactors,
            showLoading: false
          })
        })
      }

    follow(ProfileId){
    this.setState({
        showLoading: true
    })
    this.props.follow(this.props.applicationToken, ProfileId).then(() => {
        this.getReactors(this.props.TargetId, this.props.TargetTypeCode);
        this.setState({
            showLoading: false
        })
    })
    
    }

    unfollow(ProfileId){
        this.setState({
            ProfileId: ProfileId
        })
       this.ActionSheet.show()
    }

      onMenuItemPress(i){
        if (i == 1){
            
        }else if (i == 2){
          this.props.block(this.props.applicationToken, this.state.ProfileId).then(() => {
            
            this.getReactors(this.props.TargetId, this.props.TargetTypeCode);
          })
        }else if (i == 3){
          this.props.unfollow(this.props.applicationToken, this.state.ProfileId).then(() => {
            
            this.getReactors(this.props.TargetId, this.props.TargetTypeCode);
          })
        }
       }

    renderItem(l, i) {
        
        return (
            l.ProfileName == this.props.currentUserProfile.ProfileName ? null :
            <TouchableOpacity key={i} onPress={() => { Actions.UserProfile({ProfileName: l.ProfileName}) }} underlayColor='#999'>
            <View  style={{ paddingHorizontal: 10, flexDirection: 'row', margin: 0, justifyContent: 'space-around', alignItems: 'center', borderBottomColor: 'rgba(255,255,255,0.4)', borderBottomWidth: 0.5 }}>
                <View style={{ flexDirection: 'row', flex: 1, margin: 10, justifyContent: 'flex-start', alignItems: 'center',  }}>
                    {l.AvatarMicrothumbUrl ?
                        <Image
                            style={{ borderRadius: 20, width: 40, height: 40 }}
                            source={{ uri: l.AvatarMicrothumbUrl }}
                        />
                        :
                        <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: '#ccc' }}></View>
                    }
                    <View>
                    <Text style={{ flexWrap: 'wrap', marginLeft: 5,marginRight: 35, color: '#fff', fontWeight: 'normal',fontSize: 14 }}>{l.DisplayName}</Text>
                        <Text style={{backgroundColor: 'transparent', fontSize: 14, paddingHorizontal: 5, paddingVertical: 2}}>{l.Emoticon}</Text>
                    </View>
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', margin: 10 }}>
                    <ProgressCircle
                        percent={l.Agree == 0 ? 0 : Math.ceil(l.Agree * 100 / l.Total)}
                        radius={20}
                        borderWidth={3}
                        color="#fff"
                        shadowColor="#999"
                        bgColor="#067FB9"
                    >
                        <Text style={{ color: '#fff', fontSize: 11 }}>{l.Agree == 0 ? '0' : Math.ceil(l.Agree * 100 / l.Total)}%</Text>
                    </ProgressCircle>
                </View>
                {l.Following == 1 ? 
                <TouchableOpacity onPress={() => { this.unfollow(l.ProfileId) }} underlayColor='#999'>
                <View style={{width: 75, backgroundColor: '#fff', borderColor: '#fff', borderWidth: 1, borderRadius: 5,  margin: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ backgroundColor: 'transparent', padding: 5, fontSize: 11, color: '#067FB9', fontWeight: 'normal' }}>{'Following'}</Text>
                </View>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => { this.follow(l.ProfileId) }} underlayColor='#999'>
                <View style={{width: 75, backgroundColor: '#067FB9', borderColor: '#fff', borderWidth: 1,borderRadius: 5,  margin: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ backgroundColor: 'transparent', padding: 5, fontSize: 11, color: '#fff', fontWeight: 'normal' }}>{'Follow'}</Text>
                </View>
                </TouchableOpacity>
                }
            </View>
            </TouchableOpacity>
        )
    }

    _keyExtractor = (item, index) => index

    render() {
        return (
            <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 64 : 54, backgroundColor: '#067FB9' }}>
                {this.state.showLoading
                    ?
                    <View style={{ margin: 1, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#fff' />
                    </View>
                    :
                    this.state.dataSource.length == 0 ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10 }}>
                            <Text>No Followers Found</Text>
                        </View>
                        :
                        <View style={{ flex: 1 }}>

                            <FlatList
                                extraData={
                                    this.state.dataSource.sort(function(a, b) {
                                        return parseFloat(b.Agree == 0 ? 0 : Math.ceil(b.Agree * 100 / b.Total)) - parseFloat(a.Agree == 0 ? 0 : Math.ceil(a.Agree * 100 / a.Total));
                                    })
                                }
                                data={
                                    this.state.dataSource.sort(function(a, b) {
                                        return parseFloat(b.Agree == 0 ? 0 : Math.ceil(b.Agree * 100 / b.Total)) - parseFloat(a.Agree == 0 ? 0 : Math.ceil(a.Agree * 100 / a.Total));
                                    })
                                }
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item, index }) => this.renderItem(item, index)}
                            />
                        </View>

                }
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    options={options}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={(i) => {this.onMenuItemPress(i)}}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({

    subtitleView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: 10
    },
    countText: {
        fontSize: 14,
        color: '#2b8abb',
        margin: 2,
    },
    username: {
        color: '#696868',
        fontWeight: 'bold',
        fontSize: 16,
    },
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.90)',
        shadowOpacity: 0.5,
        shadowRadius: 2,
        shadowOffset: {
            height: 3,
            width: 2,
        },
    }
})


function mapStateToProps(state) {

    return {
        isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
        applicationToken: state.saveApplicationToken.applicationToken,
        followingCompatibility: state.FOLLOWING_COMPATIBILITY.followingCompatibility,
        currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
        pollCategories: state.POLL_CATEGORIES.pollCategories,
        //followers: state.FOLLOWERS.followers,
    }
}

export default connect(mapStateToProps)(Reactors);
