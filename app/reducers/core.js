import createReducer from '../lib/createReducer';
import * as types from '../actions/types';


const notificationsInitialState = {
  notifications: [],
  unreadNotifications: []
}

const messagesInitialState = {
  chatrooms: [],
  unreadChatrooms: []
}

const profileImagesInitialState = {
  profileImages: []
}

const timelineInitialState = {
  timelineItems: []
}

const profileResponsesInitialState = {
  profileResponses: []
}

const otherProfileResponsesInitialState = {
  profileResponses: []
}

const imagePostInitialState = {
  status: false,
  MediaId: null
}

const friendResponsesInitialState = {
  index: null,
  delta: null
}

const commentsInitialState = {
  comments: [],
  index: null,
  delta: null
}

const repliesInitialState = {
  replies: [],
  index: null,
  delta: null
}

const mapDataInitialState = {
  mapData: []
}

export const toggleDrawer = createReducer(false ,{
  [types.TOGGLE_DRAWER](state, action) {
    return {
      drawerState: action.drawerState
    }
  }
});

export const gotoQuestion = createReducer([],{
  [types.GOTO_QUESTION](state, action) {
    return {
      question: action.question,
      answers: action.answers
    }
  }
});


export const SET_APPLICATION_TOKEN_STATUS = createReducer([],{
  [types.SET_APPLICATION_TOKEN_STATUS](state, action) {
    return {
      applicationTokenStatus: action.applicationTokenStatus
    }
  }
});

export const SET_LOGGEDIN_STATUS = createReducer([],{
  [types.SET_LOGGEDIN_STATUS](state, action) {
    return {
      isLoggedin: action.isLoggedin
    }
  }
});

export const saveApplicationToken = createReducer([],{
  [types.SAVE_APPLICATION_TOKEN](state, action) {
    return {
      applicationToken: action.applicationToken
    }
  }
});

export const SET_USER = createReducer([],{
  [types.SET_USER](state, action) {
    return {
      firstname: action.firstname,
      lastname: action.lastname,
      email: action.email,
      city: action.city,
      birthdate: action.birthdate
    }
  }
});

export const CURRENT_USER = createReducer([],{
  [types.CURRENT_USER](state, action) {
    return {
      ProfileId: action.ProfileId,
      DisplayName: action.DisplayName,
      AvatarUrl: action.AvatarUrl
    }
  }
});

export const LOGOUT = createReducer([],{
  [types.LOGOUT](state, action) {
    return {
      logout: action.logout
    }
  }
});

export const SET_LAST_SENDING_MESSAGE = createReducer([],{
  [types.SET_LAST_SENDING_MESSAGE](state, action) {
    return {
      ProfileId: action.ProfileId,
      DisplayName: action.DisplayName,
      AvatarUrl: action.AvatarUrl,
      message: action.message
    }
  }
});

export const currentQuestion = createReducer([],{
  [types.CURRENT_QUESTION](state, action) {
    return {
      question: action.question
    }
  }
});

export const currentAnswers = createReducer([],{
  [types.CURRENT_ANSWERS](state, action) {
    return {
      answers: action.answers
    }
  }
});

export function profileImages(state = profileImagesInitialState, action) {
  switch (action.type) {
      case 'REMOVE_LOCAL_IMAGE':
      return {
        profileImages: [...state.profileImages.slice(0, action.index), ...state.profileImages.slice(action.index + 1)],
      };
      case 'PROFILE_IMAGES':
      return {
        profileImages: action.profileImages
      };
    default:
  }
  return state;
}

export const FOLLOWING_COMPATIBILITY = createReducer([],{
  [types.FOLLOWING_COMPATIBILITY](state, action) {
    return {
      followingCompatibility: action.followingCompatibility
    }
  }
});

export const NOTIFICATIONS_LAST_TIMESTAMP = createReducer('',{
  [types.NOTIFICATIONS_LAST_TIMESTAMP](state, action) {
    return {
      FromTimestamp: action.FromTimestamp
    }
  }
});

export const NEWS_FEED_LAST_TIMESTAMP = createReducer('',{
  [types.NEWS_FEED_LAST_TIMESTAMP](state, action) {
    return {
      FromTimestamp: action.FromTimestamp
    }
  }
});

export const NEWS_FEED_FIRST_TIMESTAMP = createReducer('',{
  [types.NEWS_FEED_FIRST_TIMESTAMP](state, action) {
    return {
      FromTimestamp: action.FromTimestamp
    }
  }
});

export const submissionStatus = createReducer(false,{
  [types.SUBMISSION_STATUS](state, action) {
    return {
      submissionStatus: action.submissionStatus
    }
  }
});

export const SET_POLL_RESPONSES = createReducer([],{
  [types.SET_POLL_RESPONSES](state, action) {
    return {
      pollResponses: action.pollResponses
    }
  }
});

export const SET_RESPONSE_SETTINGS = createReducer(0,{
  [types.SET_RESPONSE_SETTINGS](state, action) {
    return {
      responseVisibility: action.responseVisibility
    }
  }
});

export function replies(state = repliesInitialState, action) {
  switch (action.type) {
      case 'REPLIES':
      return {
        replies: action.replies,
      };
      case 'ADD_REPLY_REACTION':
        var toChange = state.comments.filter((l,i) => {
          return i == action.index
        })
        var NotToChange = state.comments.filter((l,i) => {
          return i != action.index
        })
        toChange[0].ReactionCount = parseInt(toChange[0].ReactionCount) + parseInt(action.delta);
        NotToChange.splice(action.index, 0, toChange[0]);
      return {
        replies: NotToChange,
      };
    default:
  }
  return state;
}

export const REACTION_TYPES = createReducer([],{
  [types.REACTION_TYPES](state, action) {
    return {
      reactionTypes: action.reactionTypes
    }
  }
});

export const NEXT_SUGGESTED_PERSON = createReducer([],{
  [types.NEXT_SUGGESTED_PERSON](state, action) {
    return {
      //Suggestion: {id: action.Suggestion.ProfileId, ...action.Suggestion},
      //Compatibility: action.Compatibility
      Suggestions: action.Suggestions,
    }
  }
});

export const SET_CHAT_MESSAGES = createReducer([],{
  [types.SET_CHAT_MESSAGES](state, action) {
    return {
      messages: action.messages
    }
  }
});

export const SET_PENDING_RECEIVED = createReducer([],{
  [types.SET_PENDING_RECEIVED](state, action) {
    return {
      pendingReceived: action.pendingReceived
    }
  }
});

export const SET_FRIENDS = createReducer([],{
  [types.SET_FRIENDS](state, action) {
    return {
      friends: action.friends
    }
  }
});

export const CURRENT_USER_PROFILE = createReducer([],{
  [types.CURRENT_USER_PROFILE](state, action) {
    return {
      currentUserProfile: action.currentUserProfile
    }
  }
});

export const IMAGE_UPLOAD_TOKEN = createReducer([],{
  [types.IMAGE_UPLOAD_TOKEN](state, action) {
    return {
      imageUploadToken: action.imageUploadToken
    }
  }
});

// export const PROFILE_RESPONSES = createReducer([],{
//   [types.PROFILE_RESPONSES](state, action) {
//     return {
//       profileResponses: action.profileResponses
//     }
//   }
// });

export function PROFILE_RESPONSES(state = profileResponsesInitialState, action) {
  switch (action.type) {
      case 'PROFILE_RESPONSES':
      return {
        profileResponses: [...action.profileResponses]
      };
      
      case 'ADD_PROFILE_RESPONSES_ITEMS_BOTTOM':
      return {
        profileResponses: [...state.profileResponses, ...action.profileResponses]
      };
     
    default:
  }
  return state;
}

export function OTHER_PROFILE_RESPONSES(state = otherProfileResponsesInitialState, action) {
  switch (action.type) {
      case 'OTHER_PROFILE_RESPONSES':
      return {
        profileResponses: [...action.profileResponses]
      };
      case 'ADD_PROFILE_RESPONSE_REACTION':
      var toChange = state.profileResponses.filter((l,i) => {
        return i == action.index
      })
      var NotToChange = state.profileResponses.filter((l,i) => {
        return i != action.index
      })
      
      toChange[0].Reacted = 1;
      toChange[0].Emoticon = action.Emoticon;
      NotToChange.splice(action.index, 0, toChange[0]);
    return {
      profileResponses: NotToChange,
    };
      case 'ADD_OTHER_PROFILE_RESPONSES_ITEMS_BOTTOM':
      return {
        profileResponses: [...state.profileResponses, ...action.profileResponses]
      };
     
    default:
  }
  return state;
}

export function timeline(state = timelineInitialState, action) {
  switch (action.type) {
      case 'TIMELINE':
      return {
        timelineItems: [...action.timelineItems]
      };
      case 'ADD_TIMELINE_REACTION':
      var toChange = state.timelineItems.filter((l,i) => {
        return i == action.index
      })
      var NotToChange = state.timelineItems.filter((l,i) => {
        return i != action.index
      })
      toChange[0].ReactionCount = toChange[0].ReactionCount ? parseInt(toChange[0].ReactionCount) + parseInt(action.data.delta) : parseInt(action.data.delta);
      toChange[0].Reacted = 1;
      toChange[0].Emoticon = action.data.Emoticon;
      NotToChange.splice(action.index, 0, toChange[0]);
    return {
      timelineItems: NotToChange,
    };
      case 'ADD_TIMELINE_ITEMS_BOTTOM':
      return {
        timelineItems: [...state.timelineItems, ...action.timelineItems]
      };
      case 'ADD_TIMELINE_ITEMS_TOP':
      return {
        timelineItems: [...action.timelineItems, ...state.timelineItems]
      };
    default:
  }
  return state;
}

export const COUNTRIES = createReducer(false,{
  [types.COUNTRIES](state, action) {
    return {
      Countries: action.Countries,
    }
  }
});

export const CLIENT_CODE = createReducer(false,{
  [types.CLIENT_CODE](state, action) {
    return {
      ClientCode: action.ClientCode,
    }
  }
});

export const IMAGE_POST_DIALOG = createReducer(false,{
  [types.IMAGE_POST_DIALOG](state, action) {
    return {
      status: action.status,
    }
  }
});

export const POLL_CATEGORIES = createReducer(false,{
  [types.POLL_CATEGORIES](state, action) {
    return {
      pollCategories: action.pollCategories,
    }
  }
});

export const MAP_FILTERS = createReducer(false,{
  [types.MAP_FILTERS](state, action) {
    return {
      mapFilters: action.mapFilters,
    }
  }
});

export const FOLLOWERS = createReducer(false,{
  [types.FOLLOWERS](state, action) {
    return {
      followers: action.followers,
    }
  }
});

export const FRIEND_RESPONSES = createReducer([],{
  [types.FRIEND_RESPONSES](state, action) {
    return {
      friendResponses: action.friendResponses,
    }
  }
});

export const FOLLOWING = createReducer(false,{
  [types.FOLLOWING](state, action) {
    return {
      following: action.following,
    }
  }
});

export function comments(state = commentsInitialState, action) {
  switch (action.type) {
      case 'COMMENTS':
      return {
        comments: action.comments,
      };
      case 'ADD_COMMENT':
      return {
        comments: [...state.comments, action.comment]
      };
      case 'ADD_COMMENT_REACTION':
        var toChange = state.comments.filter((l,i) => {
          return i == action.index
        })
        var NotToChange = state.comments.filter((l,i) => {
          return i != action.index
        })
        toChange[0].ReactionCount = toChange[0].ReactionCount ? (toChange[0].ReactionCount) + (action.delta) : (action.delta);
        toChange[0].Reacted = 1;
        NotToChange.splice(action.index, 0, toChange[0]);
      return {
        comments: NotToChange,
      };
    default:
  }
  return state;
}

export function friendResponses(state = friendResponsesInitialState, action) {
  switch (action.type) {
      case 'FRIEND_RESPONSES':
      return {
        friendResponses: action.friendResponses,
      };
      case 'ADD_REACTION':
      var toChange = state.friendResponses.filter((l,i) => {
        return i == action.index
      })
      var NotToChange = state.friendResponses.filter((l,i) => {
        return i != action.index
      })
      toChange[0].ReactionCount = (toChange[0].ReactionCount) + (action.delta);
      toChange[0].Reacted = 1;
      NotToChange.splice(action.index, 0, toChange[0]);
      return {
        friendResponses: NotToChange,
      };
    default:
  }
  return state;
}

export function imagePostDialog(state = imagePostInitialState, action) {
  switch (action.type) {
      case 'IMAGE_POST_DIALOG':
      return {
        status: action.status,
        MediaId: action.MediaId
      };
    default:
  }
  return state;
}

export function mapData(state = mapDataInitialState, action) {
  switch (action.type) {
      case 'MAP_DATA':
      return {
        mapData: action.mapData,
      };
    default:
  }
  return state;
}

export const SET_CHATROOMS = createReducer([],{
  [types.SET_CHATROOMS](state, action) {
    return {
      chatrooms: action.chatrooms
    }
  }
});

export const TOTAL = createReducer([],{
  [types.TOTAL](state, action) {
    return {
      total: action.total
    }
  }
});

export function chatrooms(state = messagesInitialState, action) {
  switch (action.type) {
      case 'ADD_CHATROOM':
      return { 
        unreadChatrooms: [...action.chatroom, ...state.unreadChatrooms],
        chatrooms: [...action.chatroom, ...state.chatrooms ]
      };
      case 'REMOVE_CHATROOM':
      return {
        unreadChatrooms: [],
        chatrooms: [...state.chatrooms.slice(0, action.index), ...state.chatrooms.slice(action.index + 1)],
      };
      case 'REMOVE_UNREAD_CHATROOMS':
      return {
        unreadChatrooms: [],
        chatrooms: [...state.chatrooms],
      };
      case 'CHATROOMS':
      return {
        unreadChatrooms: action.chatrooms.filter((chatroom) => {
          return chatroom.Unread == 1
        }),
        chatrooms: [...action.chatrooms].sort(function(a, b) {
          return new Date(b.LastMessage) - new Date(a.LastMessage);
        })
      };
    default:
  }
  return state;
}

export function notifications(state = notificationsInitialState, action) {
  switch (action.type) {
      case 'ADD_NOTIFICATION':
      return { 
        unreadNotifications: [...action.notification.Notifications, ...state.unreadNotifications],
        notifications: [...action.notification.Notifications, ...state.notifications ]
      };
      case 'REMOVE_NOTIFICATION':
      return {
        unreadNotifications: [],
        notifications: [...state.notifications.slice(0, action.index), ...state.notifications.slice(action.index + 1)],
      };
      case 'REMOVE_UNREAD_NOTIFICATION':
      return {
        unreadNotifications: [],
        notifications: [...state.notifications],
      };
      case 'ADD_NOTIFICATIONS_BOTTOM':
      return {
        unreadNotifications: [...state.notifications, ...action.notifications].filter((notification) => {
          return notification.IsRead == false
        }),
        notifications: [...state.notifications, ...action.notifications]
      };
      case 'NOTIFICATIONS':
      return {
         unreadNotifications: action.notifications.filter((notification) => {
          return notification.IsRead == false
        }),
         notifications: [...action.notifications],
      };
    default:
  }
  return state;
}




