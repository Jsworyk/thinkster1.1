import React, {Component, PropTypes} from 'react';
import { Actions } from 'react-native-router-flux';

import {TouchableOpacity, View, Image} from 'react-native';

import styles from './styles'

const SearchButton = () => (
<TouchableOpacity style={styles.buttonContainer} rejectResponderTermination  onPress={() => Actions.Search()} underlayColor='#999'>
    <View style={styles.button}>
      <Image source={require('../../assets/search.png')} style={[styles.buttonImage, styles.rightAlign]} />
    </View>
</TouchableOpacity>
)

export default SearchButton;