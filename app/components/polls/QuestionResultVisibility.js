import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';

import CardView from 'react-native-cardview'
const {
    ScrollView,
    View,
    TextInput,
    Image,
    Text,
    Animated,
    Slider,
    Dimensions,
    Platform,
    AlertIOS,
    ListView,
    StatusBar,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    StyleSheet,
  
  } = ReactNative

const styles = StyleSheet.create({
    shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
          height: 1,
          width: 2,
        },
        elevation: 1
      },
    normal: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)', flexGrow:1, justifyContent: 'center',padding: 5,paddingTop: 8, paddingBottom: 8,paddingLeft: 15, paddingRight: 15, alignItems: 'center'},
    selected: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)',flexGrow:1,backgroundColor: '#1EA6CC',padding: 5,paddingTop: 8,paddingBottom: 8, paddingLeft: 15, paddingRight: 15,justifyContent: 'center', alignItems: 'center'},
  
});



const QuestionResultVisibilityWrapper = (WrappedComponent) => {
    class Wrapper extends React.Component { 

        constructor(props) {
            super(props);

        }

        render() {
            let content = [];
            if (Platform.OS !== 'ios') {
                return (<View style={[styles.shadow,{margin: 20, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 0}]}>
                    <WrappedComponent {...this.props}></WrappedComponent>
                </View>)
            } else {
                return (
                <CardView
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={0}
                    style={{backgroundColor: '#fff', margin: 20,justifyContent: 'center', alignItems: 'center',}}
                    >
                    <View style={[{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 0}]}>
                        <WrappedComponent {...this.props}></WrappedComponent>
                    </View>
                </CardView>
                )
            }
        }
    }   

    return Wrapper;

};

class VisibilityComponent extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            selected: 'FR'
        }
    }

    setVisiblityState(sel) {
        this.setState({selected: sel});
        this.props.setSelected(sel);
    }

    render() {
        let extraStyle = {
            margin: 0, paddingBottom: 5
        };
        let extraContainerStyle = {};
        if (Platform.OS === 'ios') {
            extraStyle = {margin: 2};
            extraContainerStyle = [styles.shadow, {margin: 20}]
        }
        return (
            <View style={[...extraContainerStyle,{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 0}]}>
                <TouchableOpacity style={[ this.state.selected == 'PR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setVisiblityState('PR')}}>   
                    <Icon style={[extraStyle, {opacity: 1}]} name='lock' size={18} color= {this.state.selected == 'PR' ? '#fff' : '#1EA6CC'} />
                </TouchableOpacity>
                <TouchableOpacity style={[this.state.selected == 'FR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setVisiblityState( 'FR')}}>
                    <Text style={[extraStyle, {opacity: 1,color:  this.state.selected == 'FR' ? '#fff' : '#1EA6CC'}]}>Friends Only</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[this.state.selected == 'PU' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setVisiblityState('PU')}}>
                    <Icon style={[extraStyle, {opacity: 1}]} name='globe' size={18} color= {this.state.selected == 'PU' ? '#fff' : '#1EA6CC'} />
                </TouchableOpacity>   
            </View>
        )
    }
}

export default QuestionResultVisibility = QuestionResultVisibilityWrapper(VisibilityComponent);