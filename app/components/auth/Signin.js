import React, {Component} from 'react';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { SocialIcon } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropdownAlert from 'react-native-dropdownalert';
import DropDownHolder from '../../lib/DropdownHolder';
import {
  Keyboard,
  TouchableWithoutFeedback,
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  KeyboardAvoidingView, 
  StatusBar,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Logo } from '../Images';


class Signin extends Component{

   constructor(props) {
    super();
    this.state = {
      visible: false,
      username: '',
      password: ''
    };
    
  }

  componentDidMount(){
     this.props.toggleDrawer(true);
     if (this.props.passwordUpdated){
      DropDownHolder.getDropDown("signin").alertWithType('success', '', 'Your password has been reset.');
     }
  }

  componentWillUnmount(){  
  }

  componentWillMount(){
    this.props.getAvailableCountries().then(() => {

    })
  }

  login() {
    const this_ = this;
    if (this.state.username != '' && this.state.password != '') { 
      
      this_.setState({
        visible: true,
      })
      this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', this.props.token).then(() => {
          
        this.props.login(this_.props.applicationToken, this_.state.username, this_.state.password).then(function (response){
          this_.setState({
            visible: false,
          })
          if (this_.props.isLoggedin){
            //this_.props.setUser(value);
            //this_.props.toggleDrawer(false);
            console.log(Date.now(), 'Logged In')
            Actions.tabbar({ type: 'reset' });
          }else {
            //this_.refs.toast.show('Invalid Email or Password!');
            DropDownHolder.getDropDown("signin").alertWithType('error', 'Error', 'Invalid Email or Password')
          }
          
        });
      });
    }else {
         //this_.refs.toast.show('Please enter Email and Password!');
         DropDownHolder.getDropDown("signin").alertWithType('error', 'Validation Error', 'Please fill all the fields')
    }
  }

  render(){
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

        <LinearGradient colors={['#3FD2E8', '#1EA6CC']} style={styles.linearGradient}>
            
            <View style={styles.container}>
                <Logo></Logo>
                <View style={styles.formContainer}>
                    <View style={styles.fieldContainer}>
                        <Icon style={{ paddingTop: 0 }} name='user' size={18} color='#444' />
                        <TextInput
                            style={styles.input}
                            placeholder="EMAIL"
                            onChangeText={(username) => { this.setState({ username }) }}
                            underlineColorAndroid="transparent"
                            
                        />
                    </View>
                    <View style={styles.fieldContainer}>
                        <Icon style={{ paddingTop: 0 }} name='lock' size={18} color='#444' />
                        <TextInput
                            style={styles.input}
                            placeholder="PASSWORD"
                            onChangeText={(password) => { this.setState({ password }) }}
                            underlineColorAndroid="transparent"
                            secureTextEntry={true}
                        />
                    </View>
                    <View >
                        <TouchableOpacity style={styles.loginButton} onPress={() => { this.login() }} underlayColor='#999' >
                            <Text style={styles.loginButtonText}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.socialButtonsContainer}>
                    <View style={styles.signupButton}>
                        <Text style={{ color: '#e4e4e4', marginRight: 20 }}>Or</Text>
                        <TouchableOpacity onPress={() => { Actions.Signup({type: 'replace'}) }} underlayColor='#999' >
                        <View style={{ borderBottomColor: '#fff', borderBottomWidth: 0.5 }}>
                            <Text style={{ color: '#fff' }}>SIGN UP</Text>
                        </View>
                        </TouchableOpacity>
                    </View>
                </View>
                
                <View style={{flex: 1, margin: 20, justifyContent: 'flex-end', alignItems: 'center',backgroundColor: 'transparent' }}>
                <TouchableOpacity style={{padding: 5}} onPress={() => { Actions.ForgotPassword({type: 'replace'}) }} underlayColor='#999' >
                    <Text style={{ color: '#fff', fontSize: 16 }}>Forgot Password?</Text>
                </TouchableOpacity>
                </View>
                
            </View>
            <Toast
                ref="toast"
                style={{ backgroundColor: 'red' }}
                position='bottom'
                positionValue={100}
                fadeInDuration={500}
                fadeOutDuration={1500}
                opacity={0.8}
                textStyle={{ color: '#fff' }}
            />
            <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
            <DropdownAlert ref={ref => DropDownHolder.setDropDown("signin", ref)} onClose={data => {}} messageNumOfLines={5} />            
          </LinearGradient>
      </TouchableWithoutFeedback>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    flex: 1
  },
searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
searchIcon: {
    padding: 10,
},
input: {
    flex: 1,
    fontSize: 14,
    paddingRight: 10,
    paddingLeft: 20,
    backgroundColor: '#fff',
    color: '#444',
},

linearGradient: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 0
  },
  formContainer:{
    backgroundColor: '#fff',
    padding: 10,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center', 
    margin: 10, 
    borderRadius: 10, 
    marginTop: 20,
    shadowColor: '#1EA6CC',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  }, 
 

  fieldContainer: {
    
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    padding: Platform.OS == 'ios' ? 10 : 5,
    paddingLeft: 20,
    paddingRight: 20,
    width: 250,
    borderBottomColor: '#444',
    borderBottomWidth: 1,
    marginBottom: Platform.OS == 'ios' ? 20 : 5
  },
  fieldInner:{
    flex: 1,
    backgroundColor: 'transparent',
    padding: 5,
    marginLeft: 10,
    
  },
  loginButton: {
    backgroundColor: '#06A4CD',
    padding: 5,
    width: 140,
    height: 35,
    borderRadius: 30,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginButtonText:{
      color: '#fff',
      fontSize: 16,
  },
  socialButtonsContainer: {
    flexDirection: 'row',
    marginTop: 25
  },
  signupButton: {
    backgroundColor: '#057AB8',
    padding: 15,
    flexDirection: 'row',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'rgba(0,118,181,0.68)',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    }

});

function mapStateToProps(state){

  return {
  //  disableDrawer: state.disableDrawer
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    Countries: state.COUNTRIES.Countries
  }
}

export default connect (mapStateToProps)(Signin);
