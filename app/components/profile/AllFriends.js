import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Dimensions,
  TouchableHighlight,
  StyleSheet,

} = ReactNative


import { List, Card, ListItem, Button , ButtonGroup} from 'react-native-elements'

const list = [
  {
    name: 'Amy Farha | 17%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson | 41%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Chris Jackson | 89%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha | 29%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson | 4%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha | 77%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson | 20%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha | 54%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson | 63%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha | 37%',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
]

export default class AllFriends extends Component{


  render(){

    return (
      <ScrollView style={{marginTop: 5}}>


        <Card
            containerStyle={{margin: 5}}
            title='Food Tastes'>
            {
              list.map((l, i) => {
                return (
                  <ListItem roundAvatar
                    titleStyle={{}}
                    avatar={{uri:l.avatar_url}}
                    key={i}
                    title={l.name}
                    hideChevron={true}
                  />
                )}
              )

            }
          </Card>

          <Card
              containerStyle={{margin: 5}}
              title='Drinks'>
              {
                list.map((l, i) => {
                  return (
                    <ListItem roundAvatar

                      avatar={{uri:l.avatar_url}}
                      key={i}
                      title={l.name}
                      hideChevron={true}
                    />
                  )}
                )

              }
            </Card>


            <Card
                containerStyle={{margin: 5}}
                title='Sports'>
                {
                  list.map((l, i) => {
                    return (
                      <ListItem roundAvatar

                        avatar={{uri:l.avatar_url}}
                        key={i}
                        title={l.name}
                        hideChevron={true}
                      />
                    )}
                  )

                }
              </Card>


              <Card
                  containerStyle={{margin: 5}}
                  title='TV Shows'>
                  {
                    list.map((l, i) => {
                      return (
                        <ListItem roundAvatar

                          avatar={{uri:l.avatar_url}}
                          key={i}
                          title={l.name}
                          hideChevron={true}
                        />
                      )}
                    )

                  }
                </Card>

        <View style={{marginBottom: 50}}>
        </View>
      </ScrollView>
    )

  }
}
