import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const FacebookTabBar = React.createClass({
  tabIcons: [],

  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array,
  },

  componentDidMount() {
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue);
  },

  setAnimationValue({ value, }) {
    this.tabIcons.forEach((icon, i) => {
      const progress = Math.min(1, Math.abs(value - i))
      icon.setNativeProps({
        style: {
          color: this.iconColor(progress),
        },
      });
    });
  },

  //color between rgb(59,89,152) and rgb(204,204,204)
  iconColor(progress) {
    const red = 59 + (204 - 59) * progress;
    const green = 89 + (204 - 89) * progress;
    const blue = 152 + (204 - 152) * progress;
    return `rgb(${red}, ${green}, ${blue})`;
  },

  render() {
    return <View style={[styles.tabs, this.props.style,  ]}>
      {this.props.tabs.map((tab, i) => {
        return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={
            {
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingBottom: 5,
                borderBottomWidth: 5,
                borderBottomColor: this.props.activeTab === i ? '#134fa1' : '#1EA6CC'                
            }
            }>
            {
              tab == 'Responses' ? 
              <Image source={require('../../assets/responses.png')} style={{resizeMode: 'contain',width: 14, height: 16}}/>
              : tab == 'Images' ? 
              <Image source={require('../../assets/images.png')} style={{resizeMode: 'contain',width: 14, height: 16}}/>
              : tab == 'Compatibility' ? 
              <Image source={require('../../assets/compatibility.png')} style={{resizeMode: 'contain', width: 14, height: 16}}/>
              : null
            }
            <Text style={{fontSize: 12, marginLeft: 3, color: this.props.activeTab === i ? '#134fa1' : '#1EA6CC'}}>{tab}</Text>
        </TouchableOpacity>;
      })}
    </View>;
  },
});

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
    
  },
  tabs: {
    height: 45,
    flexDirection: 'row',
    paddingTop: 0,
    borderWidth: 1,
    borderColor: '#fff',
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    backgroundColor: '#fff',
    opacity: 1,
  }
});

export default FacebookTabBar;