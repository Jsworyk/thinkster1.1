import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  StatusBar,
  View,
  ScrollView,
  Text,
  Image,
  PanResponder,
  Animated,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  Easing,
  Platform
} from 'react-native';
import ActionButton from 'react-native-circular-action-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView , { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';
import Helper from '../../lib/helper'
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

class Menu extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      showParentFilters: true,
      showChildFilters: false,
      isFirstLoad: true,
      mapRegion: undefined,
      mapRegionInput: undefined,
      annotations: [],
      longitude: -97.1384,
      latitude: 49.8951,
      longitudeDelta: 0.0922 * ASPECT_RATIO,
      latitudeDelta: 0.0922,
      coordinate: {
        latitude: 49.8951,
        longitude: -97.1384,
      },
      markers: [],
      viewWidth: Dimensions.get('window').width,
      viewHeight: Dimensions.get('window').height,

      imageWidth: Dimensions.get('window').width * 0.8,

      animatedContentScaleX: new Animated.Value(1),
      animatedContentScaleY: new Animated.Value(1),

      animatedTopMenuMargin: new Animated.Value(-Dimensions.get('window').height),

      activeMenuItem: [ [true, false, false], [false,false, false], [false, false,false]],
      imageSizeCoef: 0.8
    };

    this.getImgHeight = this.getImgHeight.bind(this);
    this.openTopMenu = this.openTopMenu.bind(this);
    this.closeTopMenu = this.closeTopMenu.bind(this);
    this.makeActiveItem = this.makeActiveItem.bind(this);

    this.onPolygonPress = this.onPolygonPress.bind(this)
    this.coordinate = this.coordinate.bind(this)
  }


  getPoll(){
    this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
      this.props.getPollMapData(this.props.applicationToken,
        poll.Question.PollId,
        {south: this.state.latitude - this.state.latitudeDelta, west: this.state.longitude - this.state.longitudeDelta, north: this.state.latitude + this.state.latitudeDelta, east: this.state.longitude+ this.state.longitudeDelta},
        10,
        'GE',
      ).then(() => {

      })
    })
  }

onRegionChangeComplete(region){
  this.setState({
    longitude: region.longitude,
    latitude: region.latitude,
    longitudeDelta: region.longitudeDelta,
    latitudeDelta: region.latitudeDelta
  },
  () => {
   this.getPoll()
  }
)
}

renderActionButtonIcon(text, iconName){
  return(
    <View style={{}}>
        <Icon name={iconName} style={{color: '#fff', fontSize: 22}} />
    </View>
  )
}

coordinate(coordinates) {
	let x = coordinates.map(c => c.latitude)
	let y = coordinates.map(c => c.longitude)

	let minX = Math.min.apply(null, x)
	let maxX = Math.max.apply(null, x)

	let minY = Math.min.apply(null, y)
	let maxY = Math.max.apply(null, y)

	return {
		latitude: (minX + maxX) / 2,
		longitude: (minY + maxY) / 2
	}
}

onPolygonPress(l){
  
  if (l.Responses > 0){ 
  this.setState({
    markers: [
     this.coordinate(Helper.parseCoordinatesArray(l.Polygons[0]))
    ],
    description: l.Responses.toString()
  },
  () => {
    this.marker.showCallout();
  }) 
}
}

  openTopMenu(){
    Animated.parallel([
      Animated.timing(
        this.state.animatedContentScaleY,
        {
          toValue: 0.8,
          easing: Easing.cubic,
          duration: 500
        }),
        Animated.timing(
          this.state.animatedContentScaleX,
          {
            toValue: 0.8,
            easing: Easing.cubic,
            duration: 500
          }),
          Animated.timing(
            this.state.animatedTopMenuMargin,
            {
              toValue:  20,
              easing: Easing.cubic,
              duration: 500
            })
          ]).start()
        }

        closeTopMenu(){
          Animated.parallel([
            Animated.timing(
              this.state.animatedContentScaleY,
              {
                toValue: 1,
                easing: Easing.cubic,
                duration: 300
              }),
              Animated.timing(
                this.state.animatedContentScaleX,
                {
                  toValue: 1,
                  easing: Easing.cubic,
                  duration: 300
                }),
                Animated.timing(
                  this.state.animatedTopMenuMargin,
                  {
                    toValue:  -Dimensions.get('window').height,
                    easing: Easing.cubic,
                    duration: 300
                  })
                ]).start()
              }

              getImgHeight(img, coef) {
                let source = resolveAssetSource(img);
                const scaleFactor = (source.width / (this.state.viewWidth * this.state.imageSizeCoef ));
                const imageHeight = source.height / scaleFactor;

                return imageHeight;
              }

              getCardContainerHeight(img) {
                let source = resolveAssetSource(img);
                const scaleFactor = (source.width / this.state.viewWidth);
                const imageHeight = source.height / scaleFactor;

                return imageHeight;
              }

              makeActiveItem(row,column){
                let menuItemsTemp = this.state.activeMenuItem;
                for (let i = 0; i < 3; i++){
                  for(let j = 0; j < 3; j++){
                    menuItemsTemp[i][j] = false;
                  }
                }
                menuItemsTemp[row][column] = true;
                this.setState({activeMenuItem: menuItemsTemp});
              }

              renderTopMenu(){
                const rows = 2;
                const colums = 3;

                const menuLabels = [['15-20','20-25','25-30'],['30-35','35-40','40-45']];

                const topMenuItems = [];

                const activeMenuItemStyle = {
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: '#fff'
                }
                const menuItemStyle = {
                  fontSize: 12,
                  
                  color: '#fff'
                }
                const menuRowStyle = {
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center'
                }

                for (let i = 0; i < rows; i++){
                  let menuRow = [];
                  for(let j = 0; j < colums; j++){
                    menuRow.push(
                      <TouchableHighlight key={j} onPress={() => this.makeActiveItem(i,j)}>
                        <Text style={this.state.activeMenuItem[i][j] ? activeMenuItemStyle : menuItemStyle}>{menuLabels[i][j]}</Text>
                      </TouchableHighlight>
                    )
                  }
                  topMenuItems.push(
                    <View key={i} style={menuRowStyle}>
                      {menuRow}
                    </View>
                  );
                }

                return topMenuItems;
              }

              showChildFilters(status){
                this.setState({
                  showChildFilters: status,
                  showParentFilters: status ? false: true
                },
                () => {
                 
                }
              )
              }

              render() {

                return (
                  <View style={styles.container}>
                    <Animated.View style={{flex: 1, marginTop: this.state.animatedTopMenuMargin}}>
                      <View style={{}}>
                        
                        <TouchableOpacity style={{margin: 10}} onPress={ ()=> this.closeTopMenu() }>
                          <Icon name='times' size={20} color='white'/>
                        </TouchableOpacity>
                      </View>
                      <View style= {styles.menuContentStyle}>
                        {this.renderTopMenu()}
                        
                      </View>
                    </Animated.View>

                    <Animated.View style={[styles.mainContentStyle,{transform:[{scaleX: this.state.animatedContentScaleX}]}]}>
                      {/* <Animated.View style={[styles.headerContainerStyle,{transform:[{scaleY: this.state.animatedContentScaleX}]}]}>
                        <TouchableOpacity onPress={ ()=> this.openTopMenu() } >
                          <Icon name='filter' size={27} color='#25AFD2' style={{marginLeft: 20}} />
                        </TouchableOpacity>
                      </Animated.View> */}

                      <Animated.View style={[styles.content, {transform:[{scaleY: this.state.animatedContentScaleY}] } ]}>
                      <MapView
        style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
          onRegionChangeComplete={(r) => {this.onRegionChangeComplete(r)}}
        >
        { this.state.markers.map((l,i) => (
          <MapView.Marker key={i}
            ref={ref => { this.marker = ref; }}
            coordinate={l}
            title="Total Responses"
            description={this.state.description}
          />
        ))
        
        }
        {this.props.mapData.map((l,i) => (
          
            <MapView.Polygon key={i}
            coordinates={Helper.parseCoordinatesArray(l.Polygons[0])}
              fillColor={l.Responses > 0 ? "rgba(0,0,0,1)" : "rgba(0,0,0,0.5)"}
              strokeColor="rgba(0,0,0,0.8)"
              strokeWidth={2}
              onPress={()=> { this.onPolygonPress(l) }}
          />
          
        ))}
        <MapView.Callout tooltip={true}/>
        </MapView>
                    </Animated.View>
                  </Animated.View>
                  {this.state.showParentFilters ?
                    <ActionButton buttonColor="#25AFD2" 
                    icon={this.renderActionButtonIcon('Tasks', 'filter')}
                      useNativeFeedback={false}
                      degrees={0}
                      radius={100}
                      
                    >
                      <ActionButton.Item buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                        <Icon name="flag-o" style={{color: '#fff', fontSize: 20}} />
                      </ActionButton.Item>
                      <ActionButton.Item buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                        <Icon name="birthday-cake" style={{color: '#fff', fontSize: 20}} />
                      </ActionButton.Item>
                      <ActionButton.Item buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                        <Icon name="venus-mars" style={{color: '#fff', fontSize: 20}} />
                      </ActionButton.Item>
                      <ActionButton.Item buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                        <Icon name="venus-mars" style={{color: '#fff', fontSize: 20}} />
                      </ActionButton.Item>
                      <ActionButton.Item buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.showChildFilters(true)}}>
                        <Icon name="venus-mars" style={{color: '#fff', fontSize: 20}} />
                      </ActionButton.Item>
                    </ActionButton>
                    :
                    null
  }
{this.state.showChildFilters ?
                  <ActionButton buttonColor="#25AFD2" 
                  icon={this.renderActionButtonIcon('Tasks', 'birthday-cake')}
                    useNativeFeedback={false}
                    degrees={0}
                    radius={100}
                    active={true}
                    
                  >
                  <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.showChildFilters(false)}}>
                      <Icon name="arrow-left" style={{color: '#fff', fontSize: 20}} />  
                    </ActionButton.Item>
                    <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                    <Text style={{backgroundColor: 'transparent', color: '#fff'}}>20-25</Text>
                    </ActionButton.Item>
                    <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                    <Text style={{backgroundColor: 'transparent', color: '#fff'}}>25-30</Text>
                    </ActionButton.Item>
                    <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                    <Text style={{backgroundColor: 'transparent', color: '#fff'}}>30-35</Text>
                    </ActionButton.Item>
                    <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                    <Text style={{backgroundColor: 'transparent', color: '#fff'}}>35-40</Text>
                    </ActionButton.Item>
                    <ActionButton.Item size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.openTopMenu()}}>
                    <Text style={{backgroundColor: 'transparent', color: '#fff'}}>40-45</Text>
                  </ActionButton.Item>
                  </ActionButton>
                  :
                  null
}
                </View>
              )
            }
          }


          const styles = StyleSheet.create({
            container: {
              flex: 1,
              backgroundColor: '#067FB9',
              marginTop: 63
            },
            map: {
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            },
            content: {
              flex: 6
            },
            menuContentStyle: {
              flex: 1, justifyContent: 'center', 
            },
            menuProfileStyle: {
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center'
            },
            userProfileStyle: {
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginLeft: 15
            },
            userLabelStyle: {
              fontSize: 14,
              
              color: 'white',
              marginLeft: 15
            },
            logoutRowStyle: {
              flex: 1.5,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center'
            },
            logoutLabelStyle: {
              marginLeft: 35,
              fontSize: 14,
              
              color: '#c4a1fc'
            },
            mainContentStyle: {
              flex: 1,
              backgroundColor: '#f6f6f6',
            },
            titleLabelStyle: {
              fontSize: 15,
              
              color: 'black',
              fontWeight: 'bold'
            },
            headerContainerStyle: {
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'flex-end',
              marginBottom: 10
            },
            imageContainerStyle: {
              position: 'absolute', top: 60, bottom: 0, left: 55, right: 0,
              backgroundColor: '#FFFFFF',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-end',
            },
            imageLabelStyle: {
              marginBottom: 20,
              marginLeft: 20,
              fontSize: 15,
              fontFamily: 'sans-serif-light',
              color: 'black',
              fontWeight: 'bold'
            },
            imageStyle: {
              position: 'absolute',
              top: 0,
              left: 25,
            },
            counterContainerStyle: {
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center'
            },
            counterLabelStyle: {
              fontSize: 15,
              
              color: 'black'
            }
          });
          function mapStateToProps(state){
            return {
              applicationToken: state.saveApplicationToken.applicationToken,
              mapData: state.mapData.mapData
            }
          }
          
          export default connect (mapStateToProps)(Menu);