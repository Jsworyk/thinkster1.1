import React from 'react';

import {Image} from 'react-native';

const PollCategoryImage = ({requestorProfileId, category}) => {
    //If there is a profile id associated with this poll question, don't dispay a category
    if (requestorProfileId != null) {
        return null;
    }
    let content = null;
    if (category == 'Entertainment') {
        content = <Image source={require('../../assets/categories/entertainment.png')} style={{resizeMode: 'contain', width: 20, height: 20}}/>
    } else if (category == 'Personality') {
        content = <Image source={require('../../assets/categories/personality.png')} style={{resizeMode: 'contain', width: 16, height: 20}}/>
    } else if(category == 'Food') {
        content = <Image source={require('../../assets/categories/food.png')} style={{resizeMode: 'contain', width: 20, height: 20}}/>
    } else if (category == 'Relationships') {
        content = <Image source={require('../../assets/categories/relationships.png')} style={{resizeMode: 'contain', width: 18, height: 16}}/>
    } else if (category == 'Politics') {
        content = <Image source={require('../../assets/categories/politics.png')} style={{resizeMode: 'contain', width: 18, height: 14}}/>
    }
    return content;
}

export default PollCategoryImage;