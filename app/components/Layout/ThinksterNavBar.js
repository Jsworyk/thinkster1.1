import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SafeAreaView from 'react-native-safe-area-view';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text } from 'react-native';
import { DrawerButton, SearchButton } from '../Buttons';
import styles from './styles';

class ThinksterNavBar extends Component {
    constructor(props){
        super(props);
    }

    render() {
        let colors = this.props.transparent ? ['rgba(30, 166, 204, 0.5)', 'rgba(47, 188, 218, 0.5)', 'rgba(63, 209, 231, 0.5)'] :['#1EA6CC', '#2FBCDA', '#3FD1E7'];
        return (
            <SafeAreaView>
                <LinearGradient
                start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
                locations={[0.2, 0.6, 1]}
                colors={colors}
                style={{}}>

                <View style={styles.navContainer}>
                    <DrawerButton drawerPress={this.props.toggleDrawer}></DrawerButton>
                    <Text style={{ color: '#fff', fontSize: 20, marginTop: 16 }}>{this.props.title}</Text>
                    <SearchButton></SearchButton>
                </View>
                </LinearGradient>
            </SafeAreaView>
        )    
    }
}

ThinksterNavBar.propTypes = {
    toggleDrawer: PropTypes.func,
    title: PropTypes.string
}

function mapStateToProps(state){

    return {
      applicationToken: state.saveApplicationToken.applicationToken,
    }
  }
  
  export default connect (mapStateToProps)(ThinksterNavBar);