import React, {Component} from 'react';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { SocialIcon } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from 'react-native-check-box'
import {
    Keyboard,
    TouchableWithoutFeedback,
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  KeyboardAvoidingView, 
  StatusBar,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';

import DropdownAlert from 'react-native-dropdownalert';
import Helper from '../../lib/helper';
import DropDownHolder from '../../lib/DropdownHolder';
import { Logo } from '../Images';

class Signup extends Component{

   constructor(props) {
    super();
    this.state = {
      visible: false,
      username: '',
      password: '',
      checked: false,      
    };
  }

    componentDidMount(){
        this.props.toggleDrawer(true);
    }

    componentWillUnmount(){  
    }


    onClick() {
        this.setState({
            checked: !this.state.checked
        });    
    }

    goToNextPage() {
        let errors = [];
        if (!Helper.checkPasswordValidity(this.state.password)) {
            errors.push("Password entered is not valid.")
        }
        if (!Helper.checkValidInput(this.state.firstName)) {
            errors.push("First Name is required.");
        }
        if (!Helper.checkValidInput(this.state.lastName)) {
            errors.push("Last Name is required.");
        }

        if (!(Helper.checkValidInput(this.state.email) && Helper.isEmailValid(this.state.email))) {
            errors.push("Email is not valid.");
        }

        if (errors.length > 0) {
            DropDownHolder.getDropDown("signup").alertWithType('error', 'Error', errors.join(" "));
            return;
        }
        Keyboard.dismiss();
        Actions.SignupNext({firstName: this.state.firstName, lastName: this.state.lastName, email: this.state.email, password: this.state.password})
    }

  render(){
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        
            <LinearGradient colors={['#58C1F2', '#0076B5']} style={styles.linearGradient}>
                <View style={styles.container}>
                    <Logo></Logo>
                    <View style={styles.formContainer}>
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <View style={[styles.fieldContainer, {flex: 1,width: null,paddingLeft: 5,paddingRight: 5, borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                                <TextInput
                                    style={[styles.input,{}]}
                                    placeholder="FIRST NAME"
                                    onChangeText={(firstName) => { this.setState({ firstName }) }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                            <Icon style={{ paddingBottom: 5,  }} name='user' size={18} color='#444' />   
                            <View style={[styles.fieldContainer, {flex: 1,width: null, paddingLeft: 5,paddingRight: 5, borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                                <TextInput
                                    style={styles.input}
                                    placeholder="LAST NAME"
                                    onChangeText={(lastName) => { this.setState({ lastName }) }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                        </View>
                        <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                            <Icon style={{ paddingTop: 0 }} name='envelope-o' size={18} color='#444' />
                            <TextInput
                                style={[styles.input]}
                                placeholder="EMAIL"
                                onChangeText={(email) => { this.setState({ email }) }}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                            <Icon style={{ paddingTop: 0 }} name='lock' size={20} color='#444' />
                            <TextInput
                                style={styles.input}
                                placeholder="PASSWORD"
                                onChangeText={(password) => { this.setState({ password }) }}
                                underlineColorAndroid="transparent"
                                secureTextEntry={true}
                            />
                        </View>
                        <View>
                            <TouchableOpacity  style={styles.loginButton} onPress={() => { this.goToNextPage(); }} underlayColor='#999' >
                                <Text style={styles.loginButtonText}>NEXT</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ color: '#999', marginRight: 10, fontSize: 11, textAlign: 'center' }}>
                            * Passwords must be between 6 and 50 characters long. Must contain at least one uppercase letter, at least one lowercase letter, and at least one digit.
                        </Text>
                    </View>
                    <View style={styles.socialButtonsContainer}>
                        <View style={styles.signupButton}>
                            <Text style={{ color: '#e4e4e4', marginRight: 20 }}>Or</Text>
                            <TouchableOpacity onPress={() => { Actions.Signin({type: 'replace'}) }} underlayColor='#999' >
                                <View style={{ borderBottomColor: '#fff', borderBottomWidth: 0.5 }}>
                                    <Text style={{ color: '#fff' }}>SIGN IN</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flex: 1, margin: 20, justifyContent: 'flex-end', alignItems: 'center',backgroundColor: 'transparent' }}>
                        <TouchableOpacity style={{padding: 5}} onPress={() => { Actions.ForgotPassword({type: 'replace'}) }} underlayColor='#999' >
                            <Text style={{ color: '#fff', fontSize: 16 }}>Forgot Password?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'red' }}
                    position='bottom'
                    positionValue={100}
                    fadeInDuration={500}
                    fadeOutDuration={1500}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
                <DropdownAlert ref={ref => DropDownHolder.setDropDown("signup", ref)} onClose={data => {}} messageNumOfLines={5} />
            </LinearGradient>
        </TouchableWithoutFeedback>
       
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    flex: 1
  },
searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
searchIcon: {
    padding: 10,
},
input: {
    flex: 1,
    fontSize: 14,
    paddingRight: 10,
    paddingLeft: 20,
    backgroundColor: '#fff',
    color: '#444',
    
},
linearGradient: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 0
  },
  formContainer:{
    backgroundColor: '#fff',
    padding: 10,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center', 
    margin: 10, 
    borderRadius: 10, 
    marginTop: 10,
    shadowColor: '#1EA6CC',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  }, 
  fieldContainer: {
    
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    padding: Platform.OS == 'ios' ? 10 : 5,
    paddingLeft: 20,
    paddingRight: 20,
    width: 250,
    borderBottomColor: '#444',
    borderBottomWidth: 0,
    marginBottom: Platform.OS == 'ios' ? 10 : 5
  },
  tocContainer: {
    flexDirection: 'row', 
    margin: 5
  },
  fieldInner:{
    flex: 1,
    backgroundColor: 'transparent',
    padding: 5,
    marginLeft: 10,
    
  },
  loginButton: {
    backgroundColor: '#0076B5',
    padding: 5,
    width: 140,
    height: 35,
    borderRadius: 30,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginButtonText:{
      color: '#fff',
      fontSize: 16,
  },
  socialButtonsContainer: {
    flexDirection: 'row',
    marginTop: 25
  },
  signupButton: {
    backgroundColor: '#057AB8',
    padding: 15,
    flexDirection: 'row',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'rgba(0,118,181,0.68)',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    }

});

function mapStateToProps(state){

  return {
  //  disableDrawer: state.disableDrawer
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl
  }
}

export default connect (mapStateToProps)(Signup);
