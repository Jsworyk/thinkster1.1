import { Platform } from 'react-native';
export const Background = {
    start:{
        x: 1.0,
        y: 0.0
    },
    end: {
        x: 0.0,
        y: 1.0
    },
    colors: ["#521BAA", "#7C4DFF"],
}

export const Colors = {
    solid: "#521BAA",
    lightSolid: '#7C4DFF',
    placeholderColor: "#FFFFFF",
    iconColor: "#FFFFFF",
    black: "#000000",
    white: "#FFFFFF",
    gray: "#e4e4e4",
    main: "#1EA2CC",
}

export const DefaultStylesObject = {
    smallText: {
        fontSize:10,
    },
    mediumText: {
        fontSize:14,
    },
    largeText: {
        fontSize:18,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.main,
        //marginTop: Platform.OS === 'ios' ? 64 : 54,
        marginBottom: 50,
        justifyContent: 'center',
    },
    fieldContainer: {
        justifyContent: 'center',
        alignItems: 'center', 
        flexDirection: 'row',
        padding: Platform.OS == 'ios' ? 10 : 5,
        paddingLeft: 20,
        paddingRight: 20,
        width: 250,
        borderBottomColor: '#444',
        borderBottomWidth: 1,
        marginBottom: Platform.OS == 'ios' ? 20 : 5
    },
}