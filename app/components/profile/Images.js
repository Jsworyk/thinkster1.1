import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import PhotoGrid from 'react-native-photo-grid';
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from 'react-native-simple-modal';
import { Button } from 'react-native-elements'
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  DeviceEventEmitter,
  NativeModules,
  StyleSheet,
  Keyboard
} = ReactNative

var RNUploader = NativeModules.RNUploader;


class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class Images extends Component{

  constructor() {
    super();
    this.state = { showImages: false,
    avatarSource: null,
    videoSource: null,
    visible: false,
    openMessageModal: false,
    dialogOffset: 0
    };
  this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    
  }

  _keyboardDidShow () {
    this.setState({
      dialogOffset: -100
    })
  }

  _keyboardDidHide () {
    this.setState({
      dialogOffset: 0
    })
  }


  componentWillMount(){
    
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentDidMount() {

   // Build an array of 60 photos.
   this.props.getProfileImages(this.props.applicationToken, this.props.currentUserProfile.ProfileId, '', 30, true).then(() => {
    this.setState({
      showImages: true
    })
   })
   

   // upload progress
	DeviceEventEmitter.addListener('RNUploaderProgress', (data)=>{
	  let bytesWritten = data.totalBytesWritten;
	  let bytesTotal   = data.totalBytesExpectedToWrite;
	  let progress     = data.progress;
	  
	  console.log( "upload progress: " + progress + "%");
	});
 }

 

uploadImage(fileName){
  this.setState({
    visible: true
  })
  
  this.props.getProfileImageUploadToken(this.props.applicationToken).then(() => {
    
    RNFetchBlob.fetch('POST', 'https://thinkster.ca/uploads', {
      'Content-Type' : 'multipart/form-data',
    }, [
      { name : 'files[]', filename: fileName, data: RNFetchBlob.wrap(this.state.avatarSource)},
      { name : 'token', data: this.props.imageUploadToken},
      
    ]).then((resp) => {
      //alert(console.log(resp))
      //this.postImage(resp.json().Files[0].MediaId, 'Temp', 'PU')
      
      this.setState({
        visible: false,
      })
      
      this.props.showImagePostDialog(true, resp.json().Files[0].MediaId);
    }).catch((err) => {
      console.log(err);
    })
  })
}

 selectPhotoTapped() {

   const options = {
     quality: 1.0,
     maxWidth: 500,
     maxHeight: 500,
     storageOptions: {
       skipBackup: true
     }
   };

   ImagePicker.showImagePicker(options, (response) => {
     if (response.didCancel) {
       
     }
     else if (response.error) {
       alert(response.error)
     }
     else if (response.customButton) {
       
     }
     else {
       let source = { uri: response.uri };
       this.setState({
         avatarSource: response.uri.split("file://").pop(),
         data: response.data
       });
       var fileparts = response.uri.split("/");
       this.uploadImage(response.uri.split("/")[fileparts.length-1]);
     }
   });
 }
 
  render(){
    return (
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#fff'}}>
          { this.state.showImages && this.props.profileImages.length > 0 ? 
          <PhotoGrid
            data = { this.props.profileImages }
            itemsPerRow = { 3 }
            itemMargin = { 2 }
            renderItem = { this.renderItem }
          />
          :
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10}}>
          <Text>No Images Shared</Text>
          </View>
          }
          
          <ActionButton
              buttonColor="#2b8abb"
              useNativeFeedback={false}
              onPress={() => { this.selectPhotoTapped()}}
          />
          
          <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
          </View>
    )
  }

   renderItem(item, itemSize) {
     
     return(
       <TouchableHighlight
         key = { item.MediaId }
         style = {{ width: itemSize, height: itemSize }}
         onPress = { () => {
           this.props.toggleDrawer(true);
           Actions.ImagePost({Selector: item.Selector, image: item.Url, MediaId: item.MediaId})
         }}>
         <Image
           resizeMode = "cover"
           style = {{ flex: 1 }}
           source = {{ uri: item.ThumbnailUrl }}
         />
       </TouchableHighlight>
     )
   }

}

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    //ProfileId: state.CURRENT_USER.ProfileId,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    imageUploadToken: state.IMAGE_UPLOAD_TOKEN.imageUploadToken,
    profileImages: state.profileImages.profileImages
  }
}

export default connect (mapStateToProps)(Images);

