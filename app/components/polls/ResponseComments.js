import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import ActionButton from 'react-native-action-button';
import Comments from './Comments'
var fontColorContrast = require('font-color-contrast');
import ModalWrapper from 'react-native-modal-wrapper';
import LinearGradient from 'react-native-linear-gradient';
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
const Screen = Dimensions.get('window');

class ResponseComments extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      pollId: null,
      likeContainerVisible: false
    }
  }

  componentWillMount() {
      this.setState({
          item: this.props.item,
          showItem: true
      })
  }


  open(id, type) {
    this.setState({
        likeContainerVisible: true,
        reactingTo: id,
        type: type
    })
}

close() {
    this.setState({open: false}, () => {
         Animated.timing(this._scaleAnimation, {
          duration: 50,
          toValue: 0
        }).start();
    })
}

getLikeContainerStyle() {
    return {
            transform: [{scaleY: this._scaleAnimation}],
            overflow: this.state.open ? 'visible': 'hidden',
          };
}

React(l){
  
    this.setState({
        likeContainerVisible: false
    })
    this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
      // change locally
      var item = this.state.item;
      item.Reacted = 1;
      item.Emoticon = l.Emoji;
      this.setState({
        item: item
      })
    });
}

extractText( str ){
    var ret = "";
  
    if ( /"/.test( str ) ){
      ret = str.match( /"(.*?)"/ )[1];
    } else {
      ret = str;
    }
  
    return ret;
  }
  
  truncate(string, stringLength){
    if (string.length > stringLength)
       return string.substring(0,stringLength)+'...';
    else
       return string;
  };

  render() {
    
    var regex = /\[\[([upm]):([^:]+):([^\]]+)\]\]/g;
    var matches = this.state.item.Text.match(/\[\[([upm]):([^:]+):([^\]]+)\]\]/g);
    var PrasedTextArray = []
    var ids = []
    var inners = []    
    var types = []
    var pollName = null;
   
    for (var i in matches){
      PrasedTextArray.push(matches[i].replace(regex, function(string, first, second, third){
        if (first == 'p'){
          pollName = second;
        }
        ids.push(second)
        types.push(first)
        return (
          third
        )
      }))
    }

    var lastMatch = ''
    var lastMatchedIndex = 0;
    var finalIndex = 0;
    while ((m = regex.exec(this.state.item.Text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      
      if (m.index != 0){

        var text_to_get = this.state.item.Text.substring(lastMatchedIndex + lastMatch.length, m.index )
          inners.push(text_to_get); 
          
      }
      lastMatch = m[0]
      lastMatchedIndex = m.index;
      finalIndex = regex.lastIndex;
    }
    if (finalIndex < this.state.item.Text.length){
      var text_to_get = this.state.item.Text.substring(finalIndex, this.state.item.Text.length)
      inners.push(text_to_get); 
    }
    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior='padding'>
      <View style={styles.container}>
      <LinearGradient
          start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
          locations={[0.2, 0.6, 1]}
          colors={['#1EA6CC', '#2FBCDA', '#3FD1E7']}
          style={{}}>

          <View style={{ height: 64, padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent' }}>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.pop()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
              <Icon style={{opacity: 1}} name='chevron-left' size={22} color='#fff' />
              </View>
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, marginTop: 12 }}>Comments</Text>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.Search()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
                <Image source={require('../../assets/search.png')} style={{ resizeMode: 'contain', width: 22, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
        {this.state.showItem ?
        <View style={[styles.card, {elevation: 3,backgroundColor: this.state.item.BackgroundColor ? this.state.item.BackgroundColor : '#FFD700'}]}>
            
            <View style={{flex: 1}}>
            <View style={{paddingBottom: 20, backgroundColor: 'transparent', padding: 10,borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
               <View style={{flexDirection: 'row', margin: 5, justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {this.state.item.Image ? 
                <Image source={{uri: this.state.item.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
                :
                <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{color: '#fff'}}>{(this.state.item.DisplayName).split(" ").map((n)=>n[0])}</Text>
                </View>
                }
                <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.UserProfile({ProfileName: this.state.item.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                  <Text style={{color: fontColorContrast(this.state.item.BackgroundColor), marginLeft: 10, fontWeight: '600', fontSize: 12}}>{this.state.item.DisplayName}</Text>
                </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                <Text style={{color: fontColorContrast(this.state.item.BackgroundColor), textAlign: 'center', fontSize: 11}}>
                    {
                        parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) / 30) + 'm' 
                    }
                </Text>
              </View>
              </View>
              <View style={{margin: 5,}}>
                <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.Question({isPopup: false, pollName: pollName})}} underlayColor='#999'>
                    <Text style={{fontSize: 18, lineHeight: 20,fontWeight: 'bold', color: fontColorContrast(this.state.item.BackgroundColor), margin: 0}}>{this.truncate(PrasedTextArray.length > 1 ? PrasedTextArray[1] : PrasedTextArray[0], 200)}</Text>
                </TouchableOpacity>
              </View>
              <View style={{margin: 5,alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
              <Text style={{fontSize: 16, lineHeight: 20, margin: 0, color: fontColorContrast(this.state.item.BackgroundColor)}}>{this.truncate(this.extractText(inners[inners.length-1]), 200)}</Text>
              </View>
            </View>
        
            <View style={{paddingTop: 20, flex: 1, backgroundColor: 'transparent',borderTopLeftRadius: 15, borderTopRightRadius: 15,paddingBottom: 0 }}>
            <View style={[{elevation: 1,borderTopColor: '#000', borderTopWidth: 0, backgroundColor: '#fff',borderTopLeftRadius: 15, borderTopRightRadius: 15, flexDirection: 'row', justifyContent: 'space-between', padding: 15}, styles.shadowTop]}>
              <Text style={{color: '#000', fontSize: 14, fontWeight: '500'}}>{this.props.total > 0 ? this.props.total == 1 ? this.props.total +  ' Comment' : this.props.total + ' Comments' : 'No Comments'}</Text>
          </View>
          
          {/* <View style={{flexDirection: 'row', position: 'absolute',right: 10, top: -18, }}>
          <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({type: 'replace', pollName: this.props.pollName})}} underlayColor='#999'>
          <View style={[{ position: 'absolute', right: 64,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
            <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
          </View>
          </TouchableOpacity>
          <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({type: 'replace', pollName: this.props.pollName})}} underlayColor='#999'>
          <View style={[{ position: 'absolute', right: 32,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
            <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
          </View>
          </TouchableOpacity>
          {this.state.item.Reacted == 0 ?
            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.item.TargetId, this.state.item.TargetTypeCode)  }} underlayColor='#999'>
            <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20, padding: 3,width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
            </View>
            </TouchableOpacity>
            :
            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.item.TargetId, this.state.item.TargetTypeCode)  }} underlayColor='#999'>
            <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20, padding: 3,width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.item.Emoticon}</Text>
            </View>
            </TouchableOpacity>
          }
      </View> */}
            
            
          <View style={{flex: 1,padding: 5,  backgroundColor: '#2BB7D7'}}>
            <Comments {...this.props} targetId={this.state.item.TargetId} targetTypeCode={this.state.item.TargetTypeCode} />
            </View>
            

            <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 74, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  
                  
            <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({ pollName: this.props.pollName})}} underlayColor='#999'>
                              <View style={[{elevation: 4, right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                                {/* <Icon style={{}} name='map' size={16} color='white' /> */}
                                <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                              </View>
                              </TouchableOpacity>
                              
                            </View>
                            <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 42, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({ pollName: this.props.pollName})}} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              {/* <Icon style={{}} name='comment' size={16} color='white' /> */}
                              <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                            </View>
                            </TouchableOpacity>
                            </View>
                            <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 10, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            {this.state.item.Reacted == 0 ?
                            
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.item.TargetId, this.state.item.TargetTypeCode)  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              {/* <Icon style={{}} name='heart' size={16} color={'#fff'} /> */}
                              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                            </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.item.TargetId, this.state.item.TargetTypeCode)  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.item.Emoticon}</Text>
                            </View>
                            </TouchableOpacity>
                            
                          }
                          </View>

          </View>
        
        
      </View>
          
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
        </View>
        :
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
        </View>
      }
        </View>
        <ModalWrapper
            style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
            visible={this.state.likeContainerVisible}
            onRequestClose={() => this.setState({ likeContainerVisible: false })}
            shouldCloseOnOverlayPress={true}
            >
                        <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                            {
                            this.props.reactionTypes.map((l, i) => (
                                <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                    <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                                </TouchableOpacity>
                            ))
                          }

                        </View>
        </ModalWrapper>
        </KeyboardAvoidingView>
       );
  }
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e4e4e4',
        //marginBottom: 50,
       // marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderRadius: 15,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      shadow:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowOffset: {
          height: 3,
          width: 0,
        },
      },
      shadowTop:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: -3,
          width: 0,
        },
      },
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
    total: state.TOTAL.total
  }
}

export default connect(mapStateToProps)(ResponseComments);
