import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView , { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';
import Helper from '../../lib/helper'
import ActionButton from 'react-native-circular-action-menu';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import Modal from 'react-native-simple-modal';
var SpinKit = require('react-native-spinkit');
import fontelloConfig from '../../config.json';
const CustomIcon = createIconSetFromFontello(fontelloConfig);
import {
  VictoryAxis,
  VictoryChart,
  VictoryGroup,
  VictoryStack,
  VictoryCandlestick,
  VictoryErrorBar,
  VictoryBar,
  VictoryLine,
  VictoryArea,
  VictoryScatter,
  VictoryTooltip,
  VictoryZoomContainer,
  VictoryVoronoiContainer,
  VictorySelectionContainer,
  VictoryBrushContainer,
  VictoryCursorContainer,
  VictoryPie,
  VictoryLabel,
  VictoryLegend,
  createContainer
} from "victory-native";
import { VictoryTheme } from "victory-core";

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Dimensions,
  Platform,
  TouchableHighlight,
  StyleSheet,
} = ReactNative

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 49.8951;
const LONGITUDE = -97.1384;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class Maps extends Component{

  constructor(props){
    super(props);
    this.state ={
      isFirstLoad: true,
      mapRegion: undefined,
      mapRegionInput: undefined,
      annotations: [],
      longitude: -97.1384,
      latitude: 49.8951,
      longitudeDelta: 0.3922 * ASPECT_RATIO,
 
      latitudeDelta: 0.3922,
      coordinate: {
        latitude: 49.8951,
        longitude: -97.1384,
      },
      markers: [],
      showParentFilters: false,
      showChildFilters: false,
      isParentActive: false,
      showMapFilters: false,
      openModal: false,
      data: [],
      colors: [],
      totalResponses: 0,
      Choices: [],
      showMap: false,
      mapData: [],
      poll: null
    }
    this.onPolygonPress = this.onPolygonPress.bind(this)
    this.coordinate = this.coordinate.bind(this)
  } 

  renderActionButtonIcon(text, iconName){
    return(
      iconName.split('fa-')[1] ?  
          <Icon name={iconName.split('fa-')[1]} style={{color: '#fff', fontSize: 22}} />
      :
      iconName.split('f-')[1] ?
          <CustomIcon name={iconName.split('f-')[1]} style={{color: '#fff', fontSize: 22}} />
          :
          <Text>{text}</Text>
    )
  }

  getPoll(){
     this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
      this.props.getPollMapData(this.props.applicationToken,
        poll.Question.PollId,
        {south: this.state.latitude - this.state.latitudeDelta, west: this.state.longitude - this.state.longitudeDelta, north: this.state.latitude + this.state.latitudeDelta, east: this.state.longitude+ this.state.longitudeDelta},
        10,
        'GE',
      ).then((mapData) => {
        this.setState({
          mapData: mapData,
          poll: poll,
          showMap: true
        })
      })
     })
  }


onRegionChangeComplete(region){
  this.setState({
    longitude: region.longitude,
    latitude: region.latitude,
    longitudeDelta: region.longitudeDelta,
    latitudeDelta: region.latitudeDelta
  },
  () => {
   this.getPoll()
  }
)
}

coordinate(coordinates) {
	let x = coordinates.map(c => c.latitude)
	let y = coordinates.map(c => c.longitude)

	let minX = Math.min.apply(null, x)
	let maxX = Math.max.apply(null, x)

	let minY = Math.min.apply(null, y)
	let maxY = Math.max.apply(null, y)

	return {
		latitude: (minX + maxX) / 2,
		longitude: (minY + maxY) / 2
	}
}

onPolygonPress(l){
  if (l.Responses > 0){
    this.setState({
      data: Helper.parseResultArrayToData(l.Results),
      totalResponses: l.Responses,
      Name: l.Name,
      openModal: true
    })
  }
}

_renderItems(l,i){
  return (
    <Text>{l}</Text>
  )
}

_renderCloseBtn(){
return (
  <Icon style={{}} name='trash' size={14} color='white' />
)
}

showChildFilters(status,i){
  this.setState({
    selectedIndex: i,
    showChildFilters: status,
    showParentFilters: status ? false: true,
    isParentActive: status ? false: true,
  })
}

renderChildFilters(){
  let childFilters = [];
  childFilters.push(
    <ActionButton.Item key={'back'} size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.showChildFilters(false, null)}}>
      <Icon style={{}} name='arrow-left' size={14} color='white' />
    </ActionButton.Item>
  )
  this.props.mapFilters[this.state.selectedIndex].Children.map((l,i) => (
    
    childFilters.push(
      <ActionButton.Item key={i} size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {}}>
       { 
         l.Icon.split('fa-')[1] ?  
          <Icon name={l.Icon.split('fa-')[1]} style={{color: '#fff', fontSize: 22}} />
          :
          l.Icon.split('f-')[1] ?
          <CustomIcon name={l.Icon.split('f-')[1]} style={{color: '#fff', fontSize: 22}} />
          :
          <Text>{l.Text.split(' ')[1] ? l.Text.split(' ')[1] : l.Text}</Text>
      }
      </ActionButton.Item>
    )
  ))
  return childFilters;
}

render() {
    return (
      <View style={{flex: 1}}>
  
        <MapView
        style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
          onRegionChangeComplete={(r) => {this.onRegionChangeComplete(r)}}
        >
        
      {this.state.poll != null && this.state.mapData != null ? 
          this.state.mapData.map((l,i) => (
          
            <MapView.Polygon key={i}
            coordinates={Helper.parseCoordinatesArray(l.Polygons[0])}
              fillColor={l.Responses > 0 ? Helper.indexOfMax(l.Results) >= 0 ? Helper.extractColorsFromChoices(this.state.poll.Choices)[Helper.indexOfMax(l.Results)] : '#ccc' : "transparent"}
              strokeColor={l.Responses > 0 ? "rgba(0,0,0, 0.5)" : 'transparent'}
              strokeWidth={1}
              onPress={()=> { this.onPolygonPress(l) }}
          />
          
        ))
      :
      null
      }
        
        </MapView>
        
        {this.state.showParentFilters ?
                    <ActionButton buttonColor="#25AFD2" 
                    icon={this.renderActionButtonIcon('Tasks', 'fa-filter')}
                      useNativeFeedback={false}
                      degrees={0}
                      radius={100}
                      active={this.state.isParentActive}
                    >
                    {this.props.mapFilters.map((l,i) => (
                      <ActionButton.Item key={i} size={46} buttonColor='#25AFD2' title="All Tasks" onPress={() => {this.showChildFilters(true,i)}}>
                      {this.renderActionButtonIcon(l.Filter, l.Icon)}
                    </ActionButton.Item>
                    ))
                    }
                    </ActionButton>
                    :
                    null
  }
{this.state.showChildFilters ?
                  <ActionButton buttonColor="#25AFD2" 
                    icon={this.renderActionButtonIcon(this.props.mapFilters[this.state.selectedIndex].Filter, this.props.mapFilters[this.state.selectedIndex].Icon)}
                    useNativeFeedback={false}
                    degrees={0}
                    radius={110}
                    active={true}
                    
                  >
                  {this.renderChildFilters()}
                  </ActionButton>
                  :
                  null
}
<View style={{position: 'absolute', top: Platform.OS === 'ios'? 74 : 64, left: 10,right: 0, backgroundColor: 'transparent'}}>
 {this.state.poll != null ? 
 this.state.poll.Choices.map((l,i) => (
                <View key={i} style={{flex: 1, flexDirection: 'row', margin: 3, alignItems: 'center', }}>    
                  <View style={{backgroundColor: l.ChartColour, width: 16, height: 16, borderRadius: 8, margin: 3}}>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', marginRight: 10}}>
                  <Text style={{color: '#444', fontWeight: 'bold', backgroundColor: 'transparent'}}>{l.Choice}</Text>
                  </View>
                </View>
                ))
              :
              null
              }
</View>
<Modal
          open={this.state.openModal}
          offset={0}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{justifyContent: 'center', alignItems: 'center', padding: 10,}}>
            <Text>{this.state.Name}</Text>
          </View>
          {this.state.poll != null ? 
          <VictoryPie
          style={{
            labels: {
              fill: "#444",
              stroke: "none",
              fontSize: 14,
              fontWeight: "normal"
            }
          }}
          height={300}
          width={width-50}
          //padAngle={5}
          labels={(d) => d.y == 0 ? '' : d.y}
          labelComponent={<VictoryLabel angle={45}/>}
          data={this.state.data}
          
          colorScale={Helper.extractColorsFromChoices(this.state.poll.Choices)}
           events={[{
      target: "data",
      eventHandlers: {
        onPress: () => {
          return [
            {
              target: "data",
              mutation: (props) => {
                
              }
            }
          ];
        }
      }
    }]}
        />
        :
        null
  }
          </Modal>
      

      </View>
    );
  }
}

var styles = StyleSheet.create({
   container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: Platform.OS === 'ios'? 64 : 54,
    left: 0,
    right: 0,
    bottom: 0,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInput: {
    width: 150,
    height: 20,
    borderWidth: 0.5,
    borderColor: '#aaaaaa',
    fontSize: 13,
    padding: 4,
  },
  changeButton: {
    alignSelf: 'center',
    marginTop: 5,
    padding: 3,
    borderWidth: 0.5,
    borderColor: '#777777',
  },
});

function mapStateToProps(state){
  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    mapData: state.mapData.mapData,
    mapFilters: state.MAP_FILTERS.mapFilters
  }
}

export default connect (mapStateToProps)(Maps);
