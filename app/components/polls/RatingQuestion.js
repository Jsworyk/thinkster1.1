import React, {Component} from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text } from 'react-native-svg';
import Stars from 'react-native-stars';
import styles from './styles';

class RatingQuestion extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            stars: 0,
          }
    }

    render() {
        return this.renderRatingAnswers();
    }

    renderRatingAnswers(){
        //map the rating to the poll choice id..

        return (
            <View style={[styles.starContainer]}>
                <View style={[styles.starTextContainer]}>
                    <Text style={styles.starOutput}>{this.state.stars}</Text>
                </View>
                <Stars
                    half={false}
                    rating={0}
                    update={(val)=>{this.setState({stars: val})}}
                    spacing={4}
                    starSize={40}
                    count={5}
                    fullStar={require('../../assets/star-filled.png')}
                    emptyStar={require('../../assets/star-unfilled.png')}/>
                {this.renderRatingSelections()}
            </View>
        )
    }

    renderRatingSelections() {
        if (this.props.readonly) {
            return 
            (<View style={{marginTop:20, flex:1, alignSelf:'stretch'}}>
                <TouchableOpacity onPress={() => this.props.skipQuestion()}>
                    <Text style={[styles.mediumText, styles.starText]}>Skip Question</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.saveRating(this.state.stars)} style={{marginTop:10}}>
                    <Text style={[styles.largeText, styles.starText]}>Enter</Text>
                </TouchableOpacity>
            </View>)
        }
        return null;
    }
}

export default RatingQuestion;