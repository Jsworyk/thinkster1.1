import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import ActionButton from 'react-native-action-button';
import Comments from './Comments'
var fontColorContrast = require('font-color-contrast');
import LinearGradient from 'react-native-linear-gradient';
import ModalWrapper from 'react-native-modal-wrapper';
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
const Screen = Dimensions.get('window');
var positions = [0,32,64];
class PollComments extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      pollId: null,
      likeContainerVisible: false,
    }
  }

  getPoll(){
    this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
      this.setState({
                pollName: poll.Question.Question,
                myResponse: poll.Choices.map((l,i) => { if (l.MyAnswer === 1) {return l.Choice} }),
                pollId: poll.Question.PollId,
                poll: poll,
                backgroundColor: poll.Question.BackgroundColor
              })
    })
  }

  componentDidMount() {
      this.getPoll();
  }


truncate(string, stringLength){
  if (string.length > stringLength)
     return string.substring(0,stringLength)+'...';
  else
     return string;
};


open(id, type) {
  this.setState({
      likeContainerVisible: true,
      reactingTo: id,
      type: type
  })
}

close() {
  this.setState({open: false}, () => {
       Animated.timing(this._scaleAnimation, {
        duration: 50,
        toValue: 0
      }).start();
  })
}

getLikeContainerStyle() {
  return {
          transform: [{scaleY: this._scaleAnimation}],
          overflow: this.state.open ? 'visible': 'hidden',
        };
}

React(l){

  this.setState({
      likeContainerVisible: false
  })
  this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
    // change locally
    var poll = this.state.poll;
    poll.Question.Reacted = 1;
    poll.Question.Emoticon = l.Emoji;
    this.setState({
      poll: poll
    })
  });
}

  render() {
    
    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior='padding'>
      <View style={styles.container}>
      <LinearGradient
          start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
          locations={[0.2, 0.6, 1]}
          colors={['#1EA6CC', '#2FBCDA', '#3FD1E7']}
          style={{}}>

          <View style={{ height: 64, padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent' }}>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.pop()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
              <Icon style={{opacity: 1}} name='chevron-left' size={22} color='#fff' />
              </View>
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, marginTop: 12 }}>Comments</Text>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.Search()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
                <Image source={require('../../assets/search.png')} style={{ resizeMode: 'contain', width: 22, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
        {this.state.pollId != null ?
        <View style={[styles.card, {elevation: 3,backgroundColor: this.state.backgroundColor ? this.state.backgroundColor :'#FFD93E'}]}>
            
            <View style={{flex: 1}}>
            <View style={{backgroundColor: 'transparent', padding: 15,borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}>
               <View style={{flexDirection: 'row', margin: 5, justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {this.props.currentUserProfile.AvatarUrl ? 
                <Image
                style={{ borderRadius: 15, width: 30, height: 30 }}
                source={{ uri: this.props.currentUserProfile.AvatarUrl }}
              />
                :
                 <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15,justifyContent: 'center', alignItems: 'center'}}>
                 <Text style={{color: '#fff'}}>{(this.props.currentUserProfile.DisplayName).split(" ").map((n)=>n[0])}</Text>
                 </View>
                }
                
                <Text style={{margin: 2,marginLeft: 5, fontSize: 14, color: fontColorContrast(this.state.backgroundColor)}}>{this.props.currentUserProfile.DisplayName}</Text>
                </View>
                
              </View>
              <View style={{margin: 5,}}>
              <Text style={{ fontSize: 20, color: fontColorContrast(this.state.backgroundColor), fontWeight: 'bold', }}>
                {this.truncate(this.state.pollName, 200)}
              </Text>
              </View>
               <View style={{margin: 5, marginBottom: 0}}>
              <Text style={{ fontSize: 16, color: fontColorContrast(this.state.backgroundColor), fontWeight: '400', }}>
                {this.truncate(this.state.myResponse, 200)}
              </Text>
              </View> 
            </View>
        
            <View style={{paddingTop: 20, flex: 1, backgroundColor: 'transparent',borderTopLeftRadius: 15, borderTopRightRadius: 15,paddingBottom: 0 }}>
            <View style={[{elevation: 1,borderTopColor: '#000', borderTopWidth: 0, backgroundColor: '#fff',borderTopLeftRadius: 15, borderTopRightRadius: 15, flexDirection: 'row', justifyContent: 'space-between', padding: 15, paddingBottom: Platform.OS == 'ios' ? 15 : 10, paddingTop: Platform.OS == 'ios' ? 30 : 30}, styles.shadowTop]}>
             <Text style={{color: '#000', fontSize: 14, fontWeight: '500'}}>{this.props.total > 0 ? this.props.total == 1 ? this.props.total +  ' Comment' : this.props.total + ' Comments' : 'No Comments'}</Text>
            </View>

          {/* <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',left: 10, top: -18, }}>
          <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.FriendResponses({pollName: this.state.poll.Question.UrlSegment,})}} underlayColor='#999'>
                {
                  this.state.poll.RecentThreeFollowing ?
                  this.state.poll.RecentThreeFollowing.map((l,i) => (
                    l.AvatarThumbnailUrl == null ?
                  <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,padding: 3,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                  </View>
                  :
                  <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
                  </View>
                ))
                :
                null
              }
              { 
                this.state.poll.FollowingResponseCount > 3 ?
              
                  <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    <Text style={{color: '#fff'}}>{this.state.poll.FollowingResponseCount-3}+</Text>
                  </View>
            :
            null  
            }
            </TouchableOpacity>
            </View> */}


                  {/* <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: -18, }}>
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 300 }} onPress={() => { Actions.Map({ type: 'replace', pollName: this.props.pollName }) }} underlayColor='#999'>
                      <View style={[{ position: 'absolute', right: 32, margin: 2, backgroundColor: '#30415F', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                        <Image source={require('../../assets/map.png')} style={{ resizeMode: 'contain', width: 25, height: 25 }} />
                      </View>
                    </TouchableOpacity>

                    {this.state.poll.Question.Reacted == 0 ?
                      <TouchableOpacity rejectResponderTermination  style={{ zIndex: 100 }} onPress={() => { this.open(this.state.poll.Question.PollId, 'PO') }} underlayColor='#999'>
                        <View style={[{ position: 'absolute', right: 0, margin: 2, backgroundColor: '#30415F', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Image source={require('../../assets/heart-white.png')} style={{ width: 15, height: 15 }} />
                        </View>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity rejectResponderTermination  style={{ zIndex: 100 }} onPress={() => { this.open(this.state.poll.Question.PollId, 'PO') }} underlayColor='#999'>
                        <View style={[{ position: 'absolute', right: 0, margin: 2, backgroundColor: '#30415F', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3 }}>{this.state.poll.Question.Emoticon}</Text>
                        </View>
                      </TouchableOpacity>
                    }
                  </View> */}
            
            
          <View style={{flex: 1,padding: 5, paddingBottom: 10, backgroundColor: '#2BB7D7'}}>
            <Comments  {...this.props} targetId={this.state.pollId} targetTypeCode={'PO'} />
            </View>
            
            <View style={{elevation: 4,zIndex: 600, position: 'absolute',left: 10,top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                {
                    this.state.poll.RecentThreeFollowing[0] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View> 
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>

                  <View style={{elevation: 4,zIndex: 700, position: 'absolute',left: 42,top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[1] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[1].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  <View style={{elevation: 4,zIndex: 800, position: 'absolute',left: 74, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[2] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[2].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  {
                      this.state.poll.FollowingResponseCount > 3 ?
                  <View style={{elevation: 4,zIndex: 900, position: 'absolute',left: 106, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                   

                        <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ color: '#fff' }}>{this.state.poll.FollowingResponseCount - 3}+</Text>
                        </View>
                       
                    </TouchableOpacity>
                </View>
                 :
                 null
             }



            <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 42, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  
                  
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({pollName: this.props.pollName})}} underlayColor='#999'>
                              <View style={[{elevation: 4, right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                               
                                <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                              </View>
                              </TouchableOpacity>
                              
                            </View>
                            
                            <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 10, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            {this.state.poll.Question.Reacted == 0 ?
                            
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                             
                              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                            </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
                            </View>
                            </TouchableOpacity>
                            
                          }
                          </View>






          </View>
        
        
      </View>
          
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
        </View>
        :
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
        </View>
      }
        </View>
        <ModalWrapper
          style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
          visible={this.state.likeContainerVisible}
          onRequestClose={() => this.setState({ likeContainerVisible: false })}
          shouldCloseOnOverlayPress={true}
          >
                      <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                          {
                          this.props.reactionTypes.map((l, i) => (
                              <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                  <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                              </TouchableOpacity>
                          ))
                        }

                      </View>
      </ModalWrapper>
        </KeyboardAvoidingView>
       );
  }
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e4e4e4',
        //marginBottom: 50,
       // marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderRadius: 15,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      shadow:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowOffset: {
          height: 3,
          width: 0,
        },
      },
      shadowTop:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: -3,
          width: 0,
        },
      },
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})


reactMixin(PollComments.prototype, TimerMixin)
function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    total: state.TOTAL.total,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
  }
}

export default connect(mapStateToProps)(PollComments);
