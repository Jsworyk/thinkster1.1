import React,{ Component } from "react";
import { connect } from 'react-redux';
import AnswerChoice from "./AnswerChoice";
import { View, TextInput, Image, TouchableOpacity } from 'react-native';
import Stars from 'react-native-stars';
import styles from './styles';
import { Text } from 'react-native-elements';
import { Colors } from '../../lib/styles';
import { LoadingSpinner } from '../Images';
import RatingQuestion from './RatingQuestion';

class RenderAnswerSelections extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: 'FR',
            stars: 0,
            matches: null,
            loading: false,
          }
    }

    renderRatingAnswers(){
        //map the rating to the poll choice id..

        return (
            <RatingQuestion readonly={false} skipQuestion={() => this.skipQuestion()} saveRating={() => this.saveRating()}></RatingQuestion>
        )
    }

    skipQuestion(){
        this.props.onSkipAnswer();
    }

    saveRating(stars) {
        const choice = this.props.listViewData.find(choice => {
            return choice.Choice == stars
        })

        this.props.onSelectAnswer(choice.PollChoiceId);
    }

    searchForFollowers(text) {
        console.log(text);
        this.setState({loading: true});
        this.props.followerLookup(this.props.applicationToken, text).then((matches) => {
            this.setState({matches: matches});
            this.setState({loading: false});
        }).catch(() => {
            this.setState({loading: false});
        });
    }

    renderMatches() {
        if (this.state.matches) {
            return this.state.matches.map((m,idx) => {
                return (
                    <TouchableOpacity key={idx} onPress={() => this.props.onSelectAnswer(m.ProfileId)}>
                        <Image source={{uri: m.AvatarMicrothumbUrl}} style={styles.userImage} ></Image>
                    </TouchableOpacity>
                )
            })
        }
        return null;
    }

    renderFollowerAnswers() {
        // Need to show avatar holding area, and a text area...
        return (

            <View style={[styles.followerContainer]}>
                <View style={[ styles.followerImageContainer]}>
                    { this.renderMatches()}
                </View>
                <View style={[styles.followerImageContainer]}>
                    <TextInput
                        style={styles.input}
                        placeholder={this.props.currentQuestion.Question}
                        onChangeText={(text) => { this.searchForFollowers(text ) }}
                        underlineColorAndroid="transparent"
                    />
                </View>
            </View>
        )
    }

    renderRegularAnswers(){
        return this.props.listViewData.map((item,idx) => (
            <AnswerChoice key={idx} applicationToken={this.props.applicationToken} index={idx} item={item} submitAnswer={this.props.submitAnswer} currentQuestion={this.props.currentQuestion} selected={this.state.selected} onSelectedAnswer={(e) => this.props.onSelectAnswer(e)}></AnswerChoice>
          ))
    }

    render() {
        let answerType = this.props.currentQuestion.AnswerTypeCode;
        let innerContent = '';
        // answerType = 'FO';
        if (answerType === 'RA') {
            innerContent = this.renderRatingAnswers();
        } else if (answerType === 'FO') {
            innerContent = this.renderFollowerAnswers();
        } else {
            innerContent = this.renderRegularAnswers();
        }

        return (
            <View style={{margin: 10, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                {innerContent}
                {(this.state.loading ? <LoadingSpinner/> : null)}
            </View>
        );
    }
}

function mapStateToProps(state) {

    return {
      applicationToken: state.saveApplicationToken.applicationToken,
      currentQuestion: state.currentQuestion.question,
      poll: state.currentQuestion.question,
      currentAnswers: state.currentAnswers.answers,
      submissionStatus: state.submissionStatus.submissionStatus,
      isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
      responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility
    }
  }

export default connect(mapStateToProps)(RenderAnswerSelections);