import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    navContainer: {
        height: 64, padding: 10, justifyContent: 'space-between',
        alignItems: 'center', flexDirection: 'row', 
        backgroundColor: 'transparent'
    },
});

export default styles;