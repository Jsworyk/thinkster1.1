import { NavBar } from 'react-native-router-flux';
import SafeAreaView from 'react-native-safe-area-view';

import {
    Platform,
    Animated,
    I18nManager,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ViewPropTypes
  } from 'react-native';

class ThinksterDefaultNavBar extends NavBar{
    render() {

        let state = this.props.navigationState;
        let selected = state.children[state.index];
        while ({}.hasOwnProperty.call(selected, 'children')) {
            state = selected;
            selected = selected.children[selected.index];
        }
        const navProps = { ...this.props, ...selected };

        const wrapByStyle = (component, wrapStyle) => {
            if (!component) { return null; }
            return props => <View style={wrapStyle}>{component(props)}</View>;
        };

        const leftButtonStyle = [styles.leftButton, { alignItems: 'flex-start' }, this.props.leftButtonStyle, state.leftButtonStyle];
        const rightButtonStyle = [styles.rightButton, { alignItems: 'flex-end' }, this.props.rightButtonStyle, state.rightButtonStyle];

        const renderLeftButton = wrapByStyle(selected.renderLeftButton, leftButtonStyle) ||
        wrapByStyle(selected.component.renderLeftButton, leftButtonStyle) ||
        this.renderLeftButton;
        const renderRightButton = wrapByStyle(selected.renderRightButton, rightButtonStyle) ||
        wrapByStyle(selected.component.renderRightButton, rightButtonStyle) ||
        this.renderRightButton;
        const renderBackButton = wrapByStyle(selected.renderBackButton, leftButtonStyle) ||
        wrapByStyle(selected.component.renderBackButton, leftButtonStyle) ||
        this.renderBackButton;
        const renderTitle = selected.renderTitle ||
        selected.component.renderTitle ||
        this.props.renderTitle;
        const navigationBarBackgroundImage = this.props.navigationBarBackgroundImage ||
        state.navigationBarBackgroundImage;
        const navigationBarBackgroundImageStyle = this.props.navigationBarBackgroundImageStyle ||
        state.navigationBarBackgroundImageStyle;
        const navigationBarTitleImage = this.props.navigationBarTitleImage ||
        state.navigationBarTitleImage;
        let imageOrTitle = null;
        if (navigationBarTitleImage) {
            imageOrTitle = this.renderImageTitle();
        } else {
        imageOrTitle = renderTitle ? renderTitle(navProps)
            : state.children.map(this.renderTitle, this);
        }
        const contents = (
        <View>
            {imageOrTitle}
            {renderBackButton(navProps) || renderLeftButton(navProps)}
            {renderRightButton(navProps)}
        </View>
        );
        return (
        <SaveAreaView>
            <Animated.View
                style={[
                styles.header,
                this.props.navigationBarStyle,
                state.navigationBarStyle,
                selected.navigationBarStyle,
                ]}
            >
                {navigationBarBackgroundImage ? (
                <Image style={navigationBarBackgroundImageStyle} source={navigationBarBackgroundImage}>
                    {contents}
                </Image>
                ) : contents}
            </Animated.View>
            </SaveAreaView>
        );
    }
}
export default ThinksterDefaultNavBar;