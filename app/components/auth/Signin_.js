import React, {Component} from 'react';
var t = require('tcomb-form-native');
// const FBSDK = require('react-native-fbsdk');
// const {
//   LoginButton,
//   LoginManager,
//   AccessToken
// } = FBSDK;
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
//import TwitterButton from './TwitterButton'
var _ = require('lodash');

import { SocialIcon } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';

import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  KeyboardAvoidingView, 
  StatusBar,
  ScrollView,
  TouchableHighlight,
} from 'react-native';

const stylesheet = _.cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.borderWidth = 0;
stylesheet.textbox.error.borderWidth = 0;
stylesheet.textbox.normal.marginBottom = 0;
stylesheet.textbox.error.marginBottom = 0;

stylesheet.textboxView.normal.borderWidth = 0;
stylesheet.textboxView.error.borderWidth = 0;
stylesheet.textboxView.normal.borderRadius = 0;
stylesheet.textboxView.error.borderRadius = 0;
stylesheet.textboxView.error.borderBottomColor = 'red';
stylesheet.textboxView.normal.borderBottomWidth = 0.5;
stylesheet.textboxView.error.borderBottomWidth = 0.5;
stylesheet.textbox.normal.marginBottom = 5;
stylesheet.textbox.error.marginBottom = 5;

stylesheet.textboxView.normal.borderBottomColor = '#444';

var Form = t.form.Form;

// here we are: define your domain model
var Person = t.struct({
  email: t.String,
  password: t.String
});

var options = {
    stylesheet: stylesheet,
    fields: {
        password: {
            placeholderTextColor: '#a7a7aa',            
            password: true,
            secureTextEntry: true
    },
        email: {
            placeholderTextColor: '#a7a7aa',
        }
  },
   auto: 'placeholders',
  //  fields: {
  //   age: {
  //     // you can use strings or JSX
  //     error: 'Age must be in integers'
  //   }
  // }
}; // optional rendering options (see documentation)

class Signin extends Component{

   constructor(props) {
    super();
    this.state = {
      visible: false
    };
    
  }

  componentDidMount(){

   this.props.toggleDrawer(true);
    //this.refs.form.getComponent('firstName').refs.input.focus();
  }

  componentWillUnmount(){
    //this.props.toggleDrawer();
  }

  // loginWithFacebook(){
  //   const this_ = this;
  //    //console.log(LoginManager);
  //   LoginManager.logInWithReadPermissions(['public_profile','email']).then(
  //     function(result) {
  //       if (result.isCancelled) {
  //         alert('Login cancelled');
  //       } else {
  //         AccessToken.getCurrentAccessToken().then(
  //           (data) => {
  //             this_.setState({
  //               visible: true,
  //             })
      
  //     this_.props.loginWithFacebook(this_.props.applicationToken, data.accessToken).then(function (){
  //       this_.setState({
  //         visible: false,
  //       })
  //       if (this_.props.isLoggedin){
  //         this_.props.toggleDrawer(false);
  //          Actions.tabbar({ type: 'reset' });
  //       }else {
  //         this_.refs.toast.show('Login Failed.');
  //       }
  //     });
  //           }
  //         )
  //       }
  //     },
  //     function(error) {
  //       alert('Login fail with error: ' + error);
  //     }
  //   );

    
  // }

  onPress() {
    const this_ = this;
    // call getValue() to get the values of the form
    var value = this_.refs.form.getValue();
    if (value) { // if validation fails, value will be null
      
      this_.setState({
        visible: true,
        value: value
      })
      
      this.props.login(this_.props.applicationToken, value.email, value.password).then(function (){
        this_.setState({
          visible: false,
          value: value
        })
        if (this_.props.isLoggedin){
          this_.props.setUser(value);
          this_.props.toggleDrawer(false);
          Actions.tabbar({ type: 'reset' });
        }else {
          this_.refs.toast.show('Invalid credentials.');
        }
        
      });
    }
   // Actions.Profile();
  }

  render(){
    return (
      <View style={styles.container}>
          
        <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, height: 80}}>
            <Image source={require('../../assets/logo.png')} style={{width: 220,height: 40}}></Image>
        </View>
        <View style={styles.formContainer}>
          <ScrollView style={{}}>
          <Form
            ref="form"
            type={Person}
            options={options}
            value={this.state.value}
          />
       
        <View style={{marginTop: 5, justifyContent: 'space-between'}}>
        <TouchableHighlight style={styles.button} onPress={() => { this.onPress(); }} underlayColor='#fff'>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableHighlight>
        {/*<Text style={{paddingTop: 20, paddingBottom: 20, textAlign: 'center', backgroundColor: 'transparent', color: '#fff', fontSize: 18, fontFamily: 'Cabin'}}>OR</Text>*/}
        {/* <TouchableHighlight style={styles.button} onPress={() => { Actions.Signup(); }} underlayColor='#fff'>
          <Text style={styles.buttonText}>Signup</Text>
        </TouchableHighlight> */}
        </View>
         </ScrollView>  
        </View>

        
        {/*<View style={styles.middleContainer}>
          <Text>
            OR
          </Text>
        </View>
        */}
        {/*<View style={styles.bottomContainer}>*/}

        {/*<FBLogin
            style={{ flex: null, marginBottom: 10 , height: 35}}
            ref={(fbLogin) => { this.fbLogin = fbLogin }}
            loginBehavior={FBLoginManager.LoginBehaviors.Native}
            permissions={["email","user_friends"]}
            onLogin={function(e){console.log(e)}}
            onLoginFound={function(e){console.log(e)}}
            onLoginNotFound={function(e){console.log(e)}}
            onLogout={function(e){console.log(e)}}
            onCancel={function(e){console.log(e)}}
            onPermissionsMissing={function(e){console.log(e)}}
          />


          <TwitterButton />*/}

        {/*</View>*/}
          {/* <View style={{justifyContent: 'flex-end', alignItems: 'center', padding: 15, height: 100}}>
            <TouchableHighlight onPress={() => { 
              //this.loginWithFacebook(); 
              }} 
              underlayColor='#fff'>
                  <Image source={require('../../assets/loginFacebook.png')} style={{marginBottom: 5,height: 30,resizeMode: 'contain'}}></Image>
                  </TouchableHighlight>
                  <Image source={require('../../assets/loginTwitter.png')} style={{marginBottom: 5,height: 30,resizeMode: 'contain'}}></Image>
                 
            </View> */}
            <Toast
                    ref="toast"
                    style={{backgroundColor:'#fff'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'red'}}
                />
           <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
      </View>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    backgroundColor: '#ffffff',
    flex: 1
  },
  formContainer:{
    paddingTop: 20,
    paddingBottom: 0,
    paddingLeft: 10,
    paddingRight: 10, 
  }, 
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    }

});

function mapStateToProps(state){

  return {
  //  disableDrawer: state.disableDrawer
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    //AvatarUrl: state.CURRENT_USER.AvatarUrl
  }
}

export default connect (mapStateToProps)(Signin);
