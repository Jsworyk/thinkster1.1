import React, {Component} from 'react';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { SocialIcon } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from 'react-native-check-box'
import DateTimePicker from 'react-native-modal-datetime-picker';
import ActionSheet from 'react-native-actionsheet'
import Helper from '../../lib/helper'
import Modal from 'react-native-simple-modal';
import DropdownAlert from 'react-native-dropdownalert';
import DropDownHolder from '../../lib/DropdownHolder';

import {
    Keyboard,
    TouchableWithoutFeedback,
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  KeyboardAvoidingView, 
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
  Linking
} from 'react-native';
import { Logo } from '../Images';
const CANCEL_INDEX = 0
const options = [ 'Cancel', 'Male', 'Female']
class SignupNext extends Component{

   constructor(props) {
    super();
    this.state = {
      visible: false,
      username: '',
      password: '',
      checked: false
    };
    
  }

  componentDidMount(){
     this.props.toggleDrawer(true);
  }

  componentWillUnmount(){  
  }

  onClick() {
    this.setState({
        checked: !this.state.checked
    });
}

OpenLink(){
    Linking.canOpenURL('https://thinkster.ca/terms_and_conditions').then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + 'https://thinkster.ca/terms_and_conditions');
      } else {
        return Linking.openURL('https://thinkster.ca/terms_and_conditions');
      }
    }).catch(err => console.error('An error occurred', err));
    }

signup(){
    if (this.state.checked){
    this.setState({
        visible: true
    })
    this.props.signup(this.props.applicationToken, this.props.firstName, this.props.lastName, this.props.email, this.props.password, this.state.CountryCode, this.state.postal, this.state.birthdate, this.state.genderValue).then((status) => {
        

        if (status == true){
            //Actions.Signin({type: 'reset'});
            this.props.login(this.props.applicationToken, this.props.email, this.props.password).then(() => {
                this.setState({
                    visible: false
                })
                if (this.props.isLoggedin){
                  Actions.tabbar({ type: 'reset' });
                }
                
              });
        }else {
            this.setState({
                visible: false
            })
            var errorArray = []
            var errorString = status.map((l,i) => {
                errorArray.push(l.Argument+' '+l.Message)
            })
            
            DropDownHolder.getDropDown("signupNext").alertWithType('error', 'Error', errorArray.join())
        }
    })
}else {
    alert('You must agree to the terms & conditions')
}
}

_showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

_hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

_handleDatePicked = (date) => {
    this.setState({birthdate: date.toISOString().substring(0, 10)})
    this._hideDateTimePicker();
};

showGenderPicker(){
    this.ActionSheet.show()
}

onMenuItemPress(i){
    
    if (i == 1){
        this.setState({
            genderValue: 'MA',
            gender: 'Male'
        })
    }else if (i == 2){
        this.setState({
            genderValue: 'FE',
            gender: 'Female'
        })
    }
   }

renderCountries(l,i){
    return (
        <TouchableOpacity onPress={() => {this.setCountry(l)}} underlayColor='#999' >
        <View key={i} style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 5, padding: 5, borderBottomColor: '#999', borderBottomWidth: 0.5}}>
            <Text>{l.Name}</Text>
        </View>
        </TouchableOpacity>
    )
}

setCountry(l){
    this.setState({
        country: l.Name,
        CountryCode: l.CountryCode,
        showModal: false
    })
}

showCountriesModal(){
    this.setState({
        showModal: true
    })
}

getMaximumDate(){
    var d = new Date();
    var pastYear = d.getFullYear() - 13;
    d.setFullYear(pastYear);
    return d;
}

_keyExtractor = (item, index) => index

  render(){
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        
            <LinearGradient colors={['#58C1F2', '#0076B5']} style={styles.linearGradient}>
                <View style={styles.container}>
                    <Logo></Logo>
                    <View style={styles.formContainer}>
                        <TouchableOpacity onPress={() => { this.showGenderPicker()}} underlayColor='#999' >
                        <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                            {/* <Icon style={{ paddingTop: 0 }} name='envelope-o' size={16} color='#444' /> */}
                            <Image source={require('../../assets/gender.png')} style={{resizeMode: 'contain' , width: 20, height: 20}}/>
                            <TextInput
                                style={styles.input}
                                placeholder="GENDER"
                                value={this.state.gender}
                                onChangeText={(gender) => { this.setState({ gender }) }}
                                underlineColorAndroid="transparent"
                                editable={false} 
                            />
                            <Icon style={{ paddingTop: 0 }} name='chevron-down' size={16} color='#444' />
                        </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this._showDateTimePicker() }} underlayColor='#999' >
                        <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                            {/* <Icon style={{ paddingTop: 0 }} name='lock' size={18} color='#444' /> */}
                            <Image source={require('../../assets/birthdate.png')} style={{resizeMode: 'contain' , width: 20, height: 20}}/>
                            <TextInput
                                style={styles.input}
                                placeholder="BIRTHDATE"
                                onChangeText={(birthdate) => { this.setState({ birthdate }) }}
                                value={this.state.birthdate}
                                underlineColorAndroid="transparent"
                                secureTextEntry={false}
                                editable={false} 
                            />
                            <Icon style={{ paddingTop: 0 }} name='chevron-down' size={16} color='#444' />
                        </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.showCountriesModal() }} underlayColor='#999' >
                            <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                                <Icon style={{ paddingTop: 0 }} name='globe' size={20} color='#444' />
                                {/* <Image source={require('../../assets/birthdate.png')} style={{resizeMode: 'contain' , width: 20, height: 20}}/> */}
                                <TextInput
                                    pointerEvents="none"
                                    style={styles.input}
                                    placeholder="Country"
                                    onChangeText={(country) => { this.setState({ country }) }}
                                    value={this.state.country}
                                    underlineColorAndroid="transparent"
                                    secureTextEntry={false}
                                    editable={false} 
                                />
                                <Icon style={{ paddingTop: 0 }} name='chevron-down' size={16} color='#444' />
                            </View>
                        </TouchableOpacity>
                        <View style={[styles.fieldContainer, {borderBottomColor: '#444', borderBottomWidth: 1, paddingBottom: Platform.OS == 'ios' ? 10 : 5}]}>
                            {/* <Icon style={{ paddingTop: 0 }} name='lock' size={18} color='#444' /> */}
                            <Image source={require('../../assets/marker.png')} style={{resizeMode: 'contain' , width: 20, height: 20}}/>
                            <TextInput
                                style={styles.input}
                                placeholder="POSTAL CODE"
                                onChangeText={(postal) => { this.setState({ postal }) }}
                                underlineColorAndroid="transparent"
                                secureTextEntry={false}
                            />
                        </View>

                        <View style={styles.tocContainer}>
                            <CheckBox
                                style={{padding: 0}}
                                onClick={()=> {this.onClick()}}
                                isChecked={this.state.checked}
                            />
                            <View style={{padding: 4, flexDirection: 'row'}}>
                                <Text style={{fontSize: 13, color: '#999'}}>I agree to the </Text>
                                <TouchableOpacity onPress={() => {this.OpenLink()}} underlayColor='#999' >
                                    <Text style={{fontSize: 13, color: '#30415F'}}>Terms & Conditions</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View >
                            <TouchableOpacity style={styles.loginButton} onPress={() => { this.signup() }} underlayColor='#999' >
                                <Text style={styles.loginButtonText}>DONE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.socialButtonsContainer}>
                    <TouchableOpacity onPress={() => { Actions.pop() }} underlayColor='#999' >
                        <View style={styles.signupButton}>
                            <Icon style={{ paddingTop: 0 }} name='chevron-left' size={22} color='#fff' />
                        </View> 
                    </TouchableOpacity>
                    </View>
                    
                </View>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'red' }}
                    position='bottom'
                    positionValue={100}
                    fadeInDuration={500}
                    fadeOutDuration={1500}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    maximumDate={this.getMaximumDate()}
                />
                <ActionSheet
                ref={o => this.ActionSheet = o}
                options={options}
                cancelButtonIndex={CANCEL_INDEX}
                onPress={(i) => {this.onMenuItemPress(i)}}
            />
            <Modal
            open={this.state.showModal}
            offset={0}
            overlayBackground={'rgba(0, 0, 0, 0.75)'}
            animationDuration={200}
            animationTension={40}
            modalDidOpen={() => undefined}
            modalDidClose={() => this.setState({showModal: false})}
            closeOnTouchOutside={true}
            containerStyle={{
                justifyContent: 'center'
            }}
            modalStyle={{
                borderRadius: 15,
                margin: 10,
                padding: 10,
                backgroundColor: '#F5F5F5'
            }}
            disableOnBackPress={false}>
            <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
                <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{'Pick a Country'} </Text>
            </View>
            <FlatList
                extraData={this.props.Countries}
                data={this.props.Countries}
                keyExtractor={this._keyExtractor}
                renderItem={  ({item, index}) =>  this.renderCountries(item, index)  }
                />

            </Modal>
            <DropdownAlert ref={ref => DropDownHolder.setDropDown("signupNext", ref)} onClose={data => {}} messageNumOfLines={5} />
        </LinearGradient>
    </TouchableWithoutFeedback>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    flex: 1
  },
searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
searchIcon: {
    padding: 10,
},
input: {
    flex: 1,
    fontSize: 14,
    paddingRight: 10,
    paddingLeft: 20,
    backgroundColor: '#fff',
    color: '#444',
    
},
linearGradient: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 0
  },
  formContainer:{
    backgroundColor: '#fff',
    padding: 10,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center', 
    margin: 10, 
    borderRadius: 10, 
    marginTop: 10,
    shadowColor: '#1EA6CC',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    
  }, 
  fieldContainer: {
    
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    padding: Platform.OS == 'ios' ? 10 : 5,
    paddingLeft: 20,
    paddingRight: 20,
    width: 250,
    borderBottomColor: '#444',
    borderBottomWidth: 0,
    marginBottom: Platform.OS == 'ios' ? 10 : 5
  },
  tocContainer: {
    flexDirection: 'row', 
    margin: 5
  },
  fieldInner:{
    flex: 1,
    backgroundColor: 'transparent',
    padding: 5,
    marginLeft: 10,
    
  },
  loginButton: {
    backgroundColor: '#0076B5',
    padding: 5,
    width: 140,
    height: 35,
    borderRadius: 30,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginButtonText:{
      color: '#fff',
      fontSize: 16,
  },
  socialButtonsContainer: {
    flexDirection: 'row',
    marginTop: 25
  },
  signupButton: {
    backgroundColor: '#057AB8',
    padding: 15,
    flexDirection: 'row',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'rgba(0,118,181,0.68)',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    }

});

function mapStateToProps(state){

  return {
  //  disableDrawer: state.disableDrawer
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
  //  AvatarUrl: state.CURRENT_USER.AvatarUrl,
    Countries: state.COUNTRIES.Countries
  }
}

export default connect (mapStateToProps)(SignupNext);
