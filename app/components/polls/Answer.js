import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from 'react-native-loading-spinner-overlay';
import { Button, ButtonGroup, Divider} from 'react-native-elements'
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import ActionButton from 'react-native-action-button';
import FriendSuggest from './FriendSuggest'
import OverallChart from './OverallChart'
import Results from './Results'
import Modal from 'react-native-simple-modal';
import Helper from '../../lib/helper'
var fontColorContrast = require('font-color-contrast');
import CardView from 'react-native-cardview'
import LinearGradient from 'react-native-linear-gradient';
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

import {SearchButton, DrawerButton} from '../Buttons';
import {PollCategoryImage} from '../Images';

import AnswerChoice from './AnswerChoice';
import QuestionResultVisibility from './QuestionResultVisibility';

var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'

import {LoadingSpinner} from '../Images';
import { ThinksterNavBar } from '../Layout';
import RenderAnswerSelections from './RenderAnswerSelections';
import { Colors } from '../../lib/styles';
const Screen = Dimensions.get('window');

class Answer extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      selected: 'FR',
      drawerOpen: false
    }
    //this.renderRow = this.renderRow.bind(this);
    //this.submitAnswer = this.submitAnswer.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  submitAnswer(PollChoiceId) {
    this.props.submitAnswer(this.props.applicationToken, this.props.currentQuestion.PollId, PollChoiceId, this.state.selected).then(() => {
      this.setState({
        showResults: true
      })
      if (this.props.submissionStatus){
        //this.getNextQuestion();
        //Actions.Results({pollName: this.props.currentQuestion.UrlSegment});
      }
    })
	}

  componentDidMount() {
    if (this.props.isLoggedin) {
      this.getNextQuestion();
    }
  }

  getNextQuestion(){
    const _this = this;
    //this.setState({showAnswers: false});
    this.props.nextQuestion(this.props.applicationToken).then(() => {
      if (_this.props.currentQuestion == null && _this.props.currentAnswers == null){
        _this.setState({ 
            showMessage: true, 
            showAnswers: true,
            message: 'There are no questions to answer.'}, 
            () => {
              Actions.refresh({hideNavBar: true})
          });
      }else{
        _this.setState({ 
          question: _this.props.currentQuestion.Question, 
          listViewData: _this.props.currentAnswers, 
          showAnswers: true,
          responseVisibility: _this.props.responseVisibility
          
        });
      }
    });
  }

  setResponseSettings(responseVisibility){
     this.props.setResponseSettings(responseVisibility);
      this.setState({
        responseVisibility: responseVisibility
      })
  }

  onSubmitAnswer(e) {
    this.submitAnswer(e);
  }

  onSkipAnswer() {
    this.getNextQuestion();
  }


  toggleDrawer(){
    this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
    this.setState({
      drawerOpen: !this.state.drawerOpen
    })
  }

  render() {
    
    return (
      <View style={styles.container}>
       {this.state.showMessage ?
       null
       :
       <ThinksterNavBar  toggleDrawer={this.toggleDrawer} title="Poll"></ThinksterNavBar>

       }
        {
          (() => {
            if (this.state.showAnswers) {
              if (this.state.showMessage) {
                return <FriendSuggest {...this.props} sideMenu={false} hideNavBar={true}/>
              }
              return (
                <View style={styles.cardContainer}>

                <View style={[styles.card, {elevation: this.state.showResults ? 0 : 2, marginTop: Platform.OS === 'ios' ? 10 : 10}]}>
        
                  <View style={{flex: 1}}>
                  
                    <View style={{backgroundColor: this.props.poll.BackgroundColor ? this.props.poll.BackgroundColor : 'rgba(30, 166,204, 100)', padding: 10,borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}>
                      <View style={{flexDirection: 'row', margin: 5, justifyContent: 'flex-start', alignItems: 'center'}}>
                        <View style={{margin: 2,height: 24, width: 24, borderRadius: 12, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center'}}>

                          <PollCategoryImage requestorProfileId={this.props.poll.RequestorProfileId} category={this.props.poll.Category}></PollCategoryImage>
                        </View>
                        {this.props.poll.RequestorProfileId == null ?
                          <Text style={{margin: 2, fontSize: 12, color: fontColorContrast(this.props.poll.BackgroundColor)}}>{this.props.poll.Category}</Text>
                        :
                          <Text style={{margin: 2, fontSize: 12, color: fontColorContrast(this.props.poll.BackgroundColor)}}>{this.props.poll.Requestor}</Text>
                        }
                      </View>
                      <View style={{margin: 5, marginBottom: 20}}>
                        <Text style={{ fontSize: 20, color: fontColorContrast(this.props.poll.BackgroundColor), fontWeight: 'bold', }}>
                          {this.state.question}
                        </Text>
                      </View>
                    </View>

                    <QuestionResultVisibility setSelected={(sel) => { this.setState({selected: sel})}}></QuestionResultVisibility>
                    <ScrollView>
                        <RenderAnswerSelections followerLookup={this.props.followerLookup} onSkipAnswer={e => this.onSkipAnswer()} onSelectAnswer={(e) => this.onSubmitAnswer(e)} question={this.state.question} listViewData={this.state.listViewData}></RenderAnswerSelections>
                    </ScrollView>
                  </View>      
                  <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
                </View>
                </View>
              )
            }
            <LoadingSpinner></LoadingSpinner>

          })()
        }

          <Modal
            open={this.state.showResults}
            offset={0}
            overlayBackground={'rgba(0, 0, 0, 0.75)'}
            animationDuration={200}
            animationTension={40}
            modalDidOpen={() => undefined}
            modalDidClose={() => this.setState(
              {
                showResults: false
              },
              () => {
                this.getNextQuestion();
              })
            }
            closeOnTouchOutside={true}
            containerStyle={{
              justifyContent: 'center',
              paddingTop: Platform.OS === 'ios' ? 75 : 65,
            }}
            modalStyle={{
              flex: 1,
              borderRadius: 15,
              margin: 20,
              padding: 0,
              elevation: 5,
              backgroundColor: '#F5F5F5'
            }}
            disableOnBackPress={false}>
              <View style={{elevation: 5,flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <View style={{alignSelf: 'flex-end', margin: 5, marginHorizontal: 10, borderBottomColor: '#444', borderBottomWidth: 1}}>
                  <TouchableOpacity  onPress={() => {this.setState({showResults: false})}}>
                    <Text>Next Question</Text>
                  </TouchableOpacity>
                </View>
                <Results {...this.props} isPopup={true} pollName={this.props.currentQuestion ? this.props.currentQuestion.UrlSegment: ''}/>
              </View>
    
          </Modal>
        </View>
       );
  }

  /* <ActionButton
              buttonText="Skip"
              buttonTextStyle={{fontSize: 13}}
              buttonColor="#49abde"
              onPress={() => { //this.getNextQuestion()
              Actions.Comments();
              }}
          /> */
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.main,
        marginBottom: 50,
        //marginTop: Platform.OS === 'ios' ? 65 : 65,
      },

      cardContainer: {backgroundColor:Colors.gray, flex:1,},
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#ffffff',
        borderRadius: 15,
        borderColor: '#ffffff',
        borderWidth: 0,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
        
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
          height: 1,
          width: 2,
        },
        elevation: 1
      },
      normal: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)', flexGrow:1, justifyContent: 'center',padding: 5,paddingTop: 8, paddingBottom: 8,paddingLeft: 15, paddingRight: 15, alignItems: 'center'},
      selected: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)',flexGrow:1,backgroundColor: '#1EA6CC',padding: 5,paddingTop: 8,paddingBottom: 8, paddingLeft: 15, paddingRight: 15,justifyContent: 'center', alignItems: 'center'},
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    width: 50,
  },
  image: {
    width: 30,
  },
})

function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    poll: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility
  }
}

export default connect(mapStateToProps)(Answer);
