import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import FadeinView from '../core/FadeinView';
import ModalWrapper from 'react-native-modal-wrapper';
import SocketIOClient from 'socket.io-client';


const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Platform,
  FlatList,
  Keyboard,
  TouchableOpacity,
  TouchableHighlight,
  KeyboardAvoidingView,
  StyleSheet,
  Animated,
  TouchableWithoutFeedback
} = ReactNative

const loadingReplies = [];
const openLikeContainer = [];
const repliesLoaded = [];
const replies = [];
const replyingTo = [];
const reactions = [];
 
class UselessTextInput extends Component {
  
  focus() {
        this.refs.textInput.focus();
    }
    
  render() {
    return (
      <TextInput
        {...this.props}
        ref={'textInput'}
      />
    );
  }
}

class Comments extends Component{
    _keyExtractor = (item, index) => index;
  constructor(props){
    super(props);

    this.state = {
        comments: [],
        showLoader: true,
        loadingReplies: [],
        openLikeContainer: [],
        repliesLoaded: [],
        replies: [],
        replyingTo: [],
        reactions: reactions,
        isReplying: false,
        replyingToName: 'Poll',
        open: false,
        likeContainerVisible: false
    }
    
  }

componentWillMount(){

    this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false, transports: ['websocket'] });
    this.socket.on('connect', () => {
      this.socket.emit('authenticate', this.props.applicationToken, ['p1']);
    });
    this.socket.on('Comment', (data) => {
        this.appendComment(data)
    });

    this.socket.on('Reaction', (data) => {
        this.appendReaction(data)
    });

   const _this = this;
    if (this.props.isLoggedin) {
        this.props.getComments(this.props.applicationToken, 1, '', 5).then(() => {
            this.props.getReactionTypes(this.props.applicationToken).then(() => {
                 _this.setState({
                showLoader: false
            })
             })
           
        })
       
    }
}

setRepliesLoaded(i, parent){

    repliesLoaded[i] = true;
    loadingReplies[i] = true;
    _this = this;
    this.setState({
        repliesLoaded: repliesLoaded,
        loadingReplies: loadingReplies,
        replyingTo: []
    })
    
    this.props.getComments(this.props.applicationToken, 1, i, 5).then(() => {
            replies[i] = this.props.replies;
            loadingReplies[i] = false;
           _this.setState({
                repliesLoaded: repliesLoaded,
                loadingReplies: loadingReplies,
                replies: replies
            })
        })
}

setReplyTo(CommentId, name){
    this.setState({
        replyingTo: CommentId,
        isReplying: true,
        replyingToName: name,
    })
    this.commentBox.focus();

}

unsetReplying(){
    this.setState({
        replyingTo: '',
        isReplying: false,
        replyingToName: 'Poll',
    })
}

appendReaction(reaction){
    reactions[this.state.reactingTo] = true;
    this.setState({
        reactions: reactions
    })
}

appendComment(comment){
    if (replies[this.state.replyingTo] == null){
        replies[this.state.replyingTo] = [comment];
    }else{
        replies[this.state.replyingTo].push(comment);
    }
    
    this.setState({
        replies: replies,
    })
}

comment(){
    loadingReplies[this.state.replyingTo] = true;
    this.setState({
        loadingReplies: loadingReplies
    })
    Keyboard.dismiss();
    this.props.comment(this.props.applicationToken, 1, this.state.replyingTo, this.state.comment).then(() => {
        loadingReplies[this.state.replyingTo] = false;
        
        this.setState({
            comment: '',
            loadingReplies: loadingReplies,
            replyingTo: '',
            isReplying: false,
            replyingToName: 'Poll'
        })
    })
}

open(commentId) {
    this.setState({
        likeContainerVisible: true,
        reactingTo: commentId
    })
}

close() {
    this.setState({open: false}, () => {
         Animated.timing(this._scaleAnimation, {
          duration: 100,
          toValue: 0
        }).start();

    //   Animated.stagger(100,[
    //     //Animated.parallel(this.getImageAnimationArray(55, 0).reverse() ),
    //     Animated.timing(this._scaleAnimation, {
    //       duration: 100,
    //       toValue: 0
    //     })
    //   ]).start(cb);
    })
}

getLikeContainerStyle() {
    return {
            transform: [{scaleY: this._scaleAnimation}],
            overflow: this.state.open ? 'visible': 'hidden',
          };
}

React(l){
    this.setState({
        likeContainerVisible: false
    })
    this.props.React(this.props.applicationToken, 'PO', this.state.reactingTo, l.ReactionTypeCode).then(() => {

    });
}

renderComment(comment){
  return (
    <TouchableWithoutFeedback key={comment.CommentId} onPress={() => { }}>
      <View style={{ margin: 10, flex: 1}}>
              <View key={comment.CommentId} style={{flexDirection: 'row'}}>             
                    <View ref='userImage'>
                        {comment.AvatarUrl != null ?
                        <Image
                            style={{ borderRadius: 15, width: 30, height: 30 }}
                            source={{ uri: comment.AvatarUrl }}
                            />
                            :
                            <View style={{backgroundColor: '#ccc', width: 30, height: 30, borderRadius: 15}}>
                            </View> 
                        }
                    </View>
                    <View ref='commentMain' style={{justifyContent: 'center', marginLeft: 5, marginRight: 20}}>
                        <View style={{backgroundColor: '#f6f6f6', borderRadius: 5, padding: 5, borderWidth: 1, borderColor: this.state.replyingTo == comment.CommentId ? '#49abde' : '#fff'}}>
                        <Text style={{fontSize: 12, color: '#696868', fontWeight: 'bold'}}>
                            {comment.DisplayName}
                        </Text>
                        <View>
                        <View style={{ flexDirection: 'row', marginRight: 20 }}>
                        <Text style={{fontSize: 12, flexWrap: "wrap", color: '#696868'}}>
                            {comment.CommentText}
                        </Text>
                        </View>
                        </View>                        
                        </View>
                        <View ref='commentButtons'>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between',  }}>
                                <View style={{flexDirection: 'row',marginRight: 10}}>
                                <TouchableOpacity onPress={() => { this.open(comment.CommentId) }} underlayColor='#999'>
                                <Text style={{fontSize: 12, flexWrap: "wrap", color: '#696868', fontWeight: 'bold',margin: 5}}>
                                Like
                                </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.setReplyTo(comment.CommentId, comment.DisplayName) }} underlayColor='#999'>
                                <Text style={{fontSize: 12, flexWrap: "wrap", color: '#696868', fontWeight: 'bold',margin: 5}}>
                                Reply
                                </Text>
                                </TouchableOpacity>
                                </View>
                                <View ref='reactionCountContainer' style={{marginLeft: 10}}>
                                    <Text style={{fontSize: 12, flexWrap: "wrap", color: '#696868', fontWeight: 'bold',margin: 5}}>{this.state.reactions[comment.CommentId] ?  '1+' : null }</Text>
                                </View>
                            </View>
                        </View>
                         {
                            comment.CommentCount > 0 ?
                            <View style={{flexDirection: 'row'}}>
                                { !this.state.repliesLoaded[comment.CommentId] ?
                                <TouchableOpacity onPress={() => { this.setRepliesLoaded(comment.CommentId, comment.ReplyToCommentId) }} underlayColor='#999'>
                                    <View style={{ }}>
                                        <Text style={{fontSize: 12, flexWrap: "wrap", color: '#49abde', margin: 5}}>
                                            View Replies
                                        </Text> 
                                    </View>
                                </TouchableOpacity>
                                : null
                                }
                                {
                                this.state.loadingReplies[comment.CommentId] ?
                                    <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                                        <SpinKit isVisible={true} size={22} type='ThreeBounce' color='#134fa1' />
                                    </View>
                                 : 
                                 null
                                }
                            </View>
                            :
                            null
                        }
                        
                    </View>
              </View>
               {
                this.state.replies[comment.CommentId] != null ?
                    this.state.replies[comment.CommentId].map((l, i) => (
                        <View key={l.CommentId} style={{marginLeft: 20}}>
                            {this.renderComment(l)}
                        </View>
                    ))
                    : null
                }
        </View>
        </TouchableWithoutFeedback>
      )
}

  render(){
    return (
        
      <View style={{flex: 1, marginBottom: 55}}>
        <View style={styles.imageContainer}>
        </View>

        {this.state.showLoader == false ?
        <View style={styles.commentsContainer}>
            {/*<FlatList
                data={this.props.comments}
                keyExtractor={this._keyExtractor}
                renderItem={({item}) => this.renderComment(item)}
                />*/}
        <ScrollView>
                {
                    this.props.comments.map((l, i) => (
                        this.renderComment(l)
                    ))
                    
                }
        </ScrollView>
        <ModalWrapper
            style={{ borderRadius: 15, padding: 20,flexDirection: 'row' }}
            visible={this.state.likeContainerVisible}
            onRequestClose={() => this.setState({ likeContainerVisible: false })}
            shouldCloseOnOverlayPress={true}
            >
                        <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                            {this.state.showLoader == false ?
                            this.props.reactionTypes.map((l, i) => (
                                <TouchableOpacity key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                    <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                                </TouchableOpacity>
                            ))
                            :
                            null
                            }

                        </View>
        </ModalWrapper>
        </View>
         :
             <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                <SpinKit isVisible={true} size={30} type='WanderingCubes' color='#134fa1' />
            </View>
        }

        <KeyboardAvoidingView behavior='padding'>
            <View style={{backgroundColor: '#f6f6f6', borderTopWidth: 0, borderTopColor: '#49abde', marginTop: 0, justifyContent: 'flex-end'}}>
                        
                <FadeinView style={{justifyContent: 'flex-end', padding: 2, backgroundColor: 'transparent', marginLeft: 10, marginRight: 10, marginTop: 5}}>
                    <Text style={{color: '#49abde', fontWeight: 'bold', fontSize: 12}}>Replying to {this.state.replyingToName}</Text>
                </FadeinView>
            
            <View style={{height: 45, flexDirection: 'row' }} >
                
                <View style={{
                    flex: 1,
                    marginTop: 0,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 10,
                    padding: 4,
                    borderColor: '#a7a7aa',
                    borderWidth: 1,
                    borderRadius: 10 }}
                >
                <UselessTextInput
                style={{color: '#444', flex: 1,fontSize: 14 }}
                onChangeText={comment => this.setState({ comment })}
                value={this.state.comment}
                placeholder='Type comment...'
                placeholderTextColor='#a7a7aa'
                onSubmitEditing={() => {}}
                maxLength = {500}
                underlineColorAndroid='transparent'
                multiline={true}
                editable = {true}
                onBlur={() => { this.unsetReplying() }}
                ref={commentBox => this.commentBox = commentBox}
                />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center',marginRight: 10, }}>
                     <TouchableOpacity onPress={() => { this.comment() }} underlayColor='#999'>
                        <Text style={{color: this.state.comment ? '#2b8abb' : '#a7a7aa', fontSize: 14, fontWeight: 'bold'}}>Send</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
        </KeyboardAvoidingView>
     </View>
     
    )
  }
}

const styles = StyleSheet.create({
   
  indicatorContainer: {
       backgroundColor: '#2b8abb',
       height: 40
   },
   indicatorText: {
       fontSize: 14,
       color: '#fff'
   },
   indicatorSelectedText: {
       fontSize: 14,
       color: '#fff',
   },
   selectedBorderStyle: {
       height: 3,
       backgroundColor: '#fff'
   },
   imageContainer: {
    width: undefined,
    height: Platform.OS === 'ios'? 64 : 54,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0f6aa6'
  },
  commentsContainer: {
      flex: 1,
  },
  likeContainer: {
    position: 'absolute',
    left: -10,
    top: -60,
    padding: 10,
    flex: 1,
    backgroundColor: '#fff',
    borderColor: '#49abde',
    borderWidth: 2,
    flexDirection: 'row',
    borderRadius: 20,
  },
})

function mapStateToProps(state){
  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    comments: state.COMMENTS.comments,
    replies: state.REPLIES.replies,
    reactionTypes: state.REACTION_TYPES.reactionTypes
  }
}

export default connect (mapStateToProps)(Comments);
