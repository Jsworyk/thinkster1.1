import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import SearchBox from 'react-native-search-box';
import {SearchButton, DrawerButton} from '../Buttons';



const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Platform,
  TouchableHighlight,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} = ReactNative

class Search extends Component{


    constructor(props) {
        super(props);
        this._handleResults = this._handleResults.bind(this);
        this.state = {
            results: []
        }
    }

    componentDidMount(){
    this.props.toggleDrawer();
    Actions.refresh({hideNavBar: false})
    }

    componentWillUnmount(){
    this.props.toggleDrawer();
    }

    _handleResults(results) {
        this.setState({ results });
    }
    
    
    beforeFocus = () => {
        return new Promise((resolve, reject) => {
            // console.log('beforeFocus');
            resolve();
        });
    }
    
    onFocus = (text) => {
        return new Promise((resolve, reject) => {
            // console.log('onFocus', text);
            resolve();
        });
    }
    
    afterFocus = () => {
        return new Promise((resolve, reject) => {
            // console.log('afterFocus');
            resolve();
        });
    }
    
    onCancel = () => {
        return new Promise((resolve, reject) => {
            
            resolve();
        });
    }
    
    afterDelete = () => {
        return new Promise((resolve, reject) => {
            // console.log('afterDelete => toggle keyboard');
            this.refs.search_bar.focus();
            resolve();
        });
    }
    
    onSearch = (text) => {
        return new Promise((resolve, reject) => {
            this.props.search(this.props.applicationToken, text, '', 100).then((results) => {
                this.setState({
                    results: results
                },
                () => {
                    
                }
            )
            })
            resolve();
        });
    }
    
    onChangeText = (text) => {
        return new Promise((resolve, reject) => {
            this.props.search(this.props.applicationToken, text, '', 100).then((results) => {
                this.setState({
                    results: results
                },
                () => {
                    
                }
            )
            })
            resolve();
        });
    }

    renderItem(l,i){
        return (
            // <TouchableOpacity key={i} onPress={() => { l.Type == 'p' ? Actions.FriendResponses({pollName: l.Selector}) : l.Type == 'u' ? Actions.UserProfile({ProfileName: l.Selector}) : null }} underlayColor='#999'>
            <TouchableOpacity key={i} onPress={() => { l.Type == 'p' ? Actions.Question({pollName: l.Selector}) : l.Type == 'u' ? Actions.UserProfile({ProfileName: l.Selector}) : l.Type == 'm' ? Actions.ImagePost({Selector: l.Selector}) : null}} underlayColor='#999'>
                <View style={{margin: 5, alignItems: 'center', flexDirection: 'row',flex: 1,padding: 15,borderBottomColor: '#fff', borderBottomWidth: 0.5}}>
                {l.Type == 'u' && l.ThumbnailUrl ?
                <Image source={{uri: l.ThumbnailUrl}} style={{width: 30, height: 30, borderRadius: 15}}/>
                : l.Type == 'p' ?
                <View style={{justifyContent: 'center', alignItems: 'center', width: 30, height: 30, borderRadius: 15, backgroundColor: '#0074CD'}}>
                <Icon style={{backgroundColor: 'transparent'}} name='question' size={20} color='#fff' />
                </View>
                : l.Type == 'm' ?
                <View style={{justifyContent: 'center', alignItems: 'center', width: 30, height: 30, borderRadius: 15, backgroundColor: '#0074CD'}}>
                <Icon style={{backgroundColor: 'transparent'}} name='picture-o' size={16} color='#fff' />
                </View>
                :
                <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{color: '#fff'}}>{(l.Name).split(" ").map((n)=>n[0])}</Text>
                </View>
                }
                <View key={i} style={{flex: 1,marginLeft: 10, }}>
                    <Text style={{color: '#fff'}}>{l.Name}</Text>    
                </View>
                </View>
            </TouchableOpacity>
        )
    }

    _keyExtractor = (item, index) => index

  render(){
    return (
      <View style={{marginTop: Platform.OS === 'ios' ? 64 : 54, backgroundColor: 'rgba(30,166,204,0.75)', flex: 1, paddingBottom: 10}}>
        <View style={{backgroundColor: '#3FD2E8'}}>
        <SearchBox
        ref="search_bar"
        titleSearch="Search..."
        titleCancel="Cancel"
        onSearch={this.onSearch}
        onChangeText={this.onChangeText}
        onDelete={() => console.log('onDelete')}
        afterDelete={this.afterDelete}
        beforeFocus={this.beforeFocus}
        onFocus={this.onFocus}
        afterFocus={this.afterFocus}
        onCancel={this.onCancel}
        backgroundColor="#3FD2E8"
        inputStyle={{backgroundColor: 'rgba(30,166,204,0.75)'}}
        placeholderTextColor="#fff"
        tintColorSearch="#fff"
        tintColorDelete="#fff"
        titleCancelColor='#fff'
    />
    </View>
    <View>
    <FlatList
            extraData={this.state.results}
            data={this.state.results}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        
    </View>
     </View>
    )
  }
}

const styles = StyleSheet.create({

  indicatorContainer: {
       backgroundColor: '#2b8abb',
       height: 40
   },
   indicatorText: {
       fontSize: 14,
       color: '#fff'
   },
   indicatorSelectedText: {
       fontSize: 14,
       color: '#fff',

   },
   selectedBorderStyle: {
       height: 3,
       backgroundColor: '#fff'
   },
   imageContainer: {
    width: undefined,
    height: Platform.OS === 'ios'? 64 : 54,
    backgroundColor:'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
})


function mapStateToProps(state){

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
  }
}

export default connect (mapStateToProps)(Search);
