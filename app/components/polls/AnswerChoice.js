import React, { Component } from "react";
import { connect } from 'react-redux'
import CardView from 'react-native-cardview'
import {Dimensions, StyleSheet, TouchableOpacity, View, Text, Platform} from 'react-native';

const Screen = Dimensions.get('window');

class AnswerChoice extends Component{

    constructor(props) {
        super(props);
    }

    submitAnswer(pollChoiceId) {
        this.props.onSelectedAnswer(pollChoiceId);
    }



    render() {
        return (<TouchableOpacity key={this.props.index} onPress={() => {this.submitAnswer(this.props.item.PollChoiceId)}}>
        { Platform.OS == 'ios' ?
        <View style={[styles.shadow, styles.answerContainer]}>
          <Text style={styles.answerText}>{this.props.item.Choice}</Text>
        </View>
        :
        <CardView
                cardElevation={3}
                cardMaxElevation={3}
                cornerRadius={0}
                style={{backgroundColor: '#fff', margin: 5,justifyContent: 'center', alignItems: 'center',}}
                >
            <View style={[styles.answerContainer]}>
              <Text style={styles.answerText}>{this.props.item.Choice}</Text>
            </View>
        </CardView>
        }
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    answerText: {
        textAlign: 'center',
        color: '#1EA6CC',
        fontSize: 16,
        fontWeight: '400'
    },
    answerContainer:{
        flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 5, 
        margin: 5,marginBottom: 10, padding: 12, width: Screen.width - 80
    },
    shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
            height: 1,
            width: 2,
        },
        elevation: 1
    }
});

function mapStateToProps(state) {

    return {
      applicationToken: state.saveApplicationToken.applicationToken,
      currentQuestion: state.currentQuestion.question,
      poll: state.currentQuestion.question,
      currentAnswers: state.currentAnswers.answers,
      submissionStatus: state.submissionStatus.submissionStatus,
      isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
      responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility
    }
  }

export default connect(mapStateToProps)(AnswerChoice);