import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
//import NavBar, { NavButton, NavButtonText, NavTitle } from 'react-native-nav'
import Icon from 'react-native-vector-icons/FontAwesome';
//import MultipleChoice from 'react-native-multiple-choice'
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import { Button, ButtonGroup, Divider} from 'react-native-elements'
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import Interactable from 'react-native-interactable';
import ActionButton from 'react-native-action-button';
//import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons';

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
const Screen = Dimensions.get('window');

class Answer extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false
    }
    //this.renderRow = this.renderRow.bind(this);
    //this.submitAnswer = this.submitAnswer.bind(this);
  }

  submitAnswer(PollChoiceId) {
    
    this.setTimeout(function (){
      this.setState({showAnswers: false});
    }, 500);
    
//    this.props.submitAnswer(this.props.applicationToken, PollChoiceId, 'PU').then(() => {
    this.props.submitAnswer(this.props.applicationToken, PollChoiceId, 'PU').then(() => {

      if (this.props.submissionStatus){
        this.getNextQuestion();
      }
    })
	}

  componentWillMount(){
   
    if (this.props.isLoggedin) {
        if (this.props.currentQuestion == null && this.props.currentAnswers == null){
          //this.getNextQuestion();
          
        }else {
          this.setState({ 
              question: this.props.currentQuestion, 
              listViewData: this.props.currentAnswers, 
              showAnswers: true,
              responseVisibility: this.props.responseVisibility
          });
      }
      
    }
  }

  componentDidMount() {
    if (this.props.isLoggedin) {
      this.getNextQuestion();
    }
  }

  getNextQuestion(){
    const _this = this;
    this.setState({showAnswers: false});
    this.props.nextQuestion(this.props.applicationToken).then(() => {
      if (_this.props.currentQuestion == null && _this.props.currentAnswers == null){
        _this.setState({ 
            showMessage: true, 
            showAnswers: true,
            message: 'There are no questions to answer.'
     });
      }else{
      _this.setState({ 
        question: _this.props.currentQuestion, 
        //dataSource: _this.state.dataSource.cloneWithRows(_this.props.currentAnswers), 
        listViewData: _this.props.currentAnswers, 
        showAnswers: true,
        responseVisibility: _this.props.responseVisibility
        
     });
      }
    });
  }

  setResponseSettings(responseVisibility){
     this.props.setResponseSettings(responseVisibility);
      this.setState({
        responseVisibility: responseVisibility
      })
    }

  /*renderRow (rowData, sectionID) {
   return (
    <View style={{backgroundColor: '#fff'}}>
        <View style={{position: 'absolute', right: 0, height: 45, flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={ () => this.submitAnswer(rowData.PollChoiceId)}>
            <Animated.Image source={require('../../assets/check.png')} style={
              [styles.buttonRight, {
                opacity: this.state._deltaX.interpolate({
                  inputRange: [-75, -55],
                  outputRange: [1, 0],
                  extrapolateLeft: 'clamp',
                  extrapolateRight: 'clamp'
                }),
                transform: [{
                  scale: this.state._deltaX.interpolate({
                    inputRange: [-75, -55],
                    outputRange: [1, 0.7],
                    extrapolateLeft: 'clamp',
                    extrapolateRight: 'clamp'
                  })
                }]
              }
            ]} />
          </TouchableOpacity>
        </View>

        <Interactable.View
          ref='headInstance'
          horizontalOnly={true}
          snapPoints={this.state.snapPoints}
          dragToss={0.01}
          animatedValueX={this.state._deltaX}>
          <View style={{left: 0, right: 0, height: 50, backgroundColor: 'white'}}>
            <View style={styles.rowContent}>

            <View>
              <Text style={styles.rowSubtitle}>{rowData.Choice}</Text>
            </View>
          </View>
          </View>
        </Interactable.View>
      </View>
   )
 }*/

  render() {
    const buttons = ['Public', 'Private', 'Friends Only']
    return (
      <View style={{backgroundColor: '#fff', flex: 1, flexDirection: 'column', marginBottom: 50 }}>
        <View style={styles.imageContainer}></View>
        {this.state.showAnswers
        ?
        this.state.showMessage ?
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{textAlign: 'center'}}>{this.state.message}</Text>
          </View>
        :
         <View style={{flex: 1}}>
            {/*<Image source={require('../../assets/pollbackground.png')} style={styles.imageContainer} >  
            </Image>*/}
               
            <ButtonGroup
            onPress={(i) => this.setResponseSettings(i)}
            selectedIndex={this.state.responseVisibility}
            buttons={buttons}
            selectedBackgroundColor='#49abde'
            selectedTextStyle={{color: '#fff', fontSize: 12, }}
            textStyle={{color: '#a7a7aa', fontSize: 12, }}
            buttonStyle={{borderColor: '#49abde', color: '#49abde'}}
            innerBorderStyle={{color: '#49abde'}}
            containerStyle={{marginLeft: 35, marginRight: 35, marginTop: 30, marginBottom: 5, backgroundColor: '#fff', height: 25}} />
            <View style={{maxHeight: 350, height: undefined, marginTop: 25, marginLeft: 20, marginRight: 20,
              borderColor: '#a7a7aa',

            }}>
            
              <Text style={{ fontSize: 18, textAlign: 'center', color: '#0f6aa6', fontWeight: 'bold', marginBottom: 30 }}>
                {this.state.question}
              </Text>
            
          <SwipeListView
           closeOnRowBeginSwipe={true}
           disableRightSwipe={true}
           previewFirstRow={true}
           previewDuration={500}
           swipeToOpenPercent={20}
						dataSource={this.ds.cloneWithRows(this.state.listViewData)}
						renderRow={ data => (
							<TouchableHighlight
								onPress={ _ => console.log('You touched me') }
								style={styles.rowFront}
								underlayColor={'#fff'}
							>
							<View>
									<Text style={{color: '#49abde', fontSize: 14, fontWeight: 'bold', }}>{data.Choice}</Text>
								</View>
							</TouchableHighlight>
						)}
						renderHiddenRow={ (data, secId, rowId, rowMap) => (
							<View style={styles.rowBack}>
                <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]} onPress={ _ => this.submitAnswer(data.PollChoiceId) }>
								  <Image source={require('../../assets/check.png')} style={{height: 30, resizeMode: 'contain'}}></Image>
								</TouchableOpacity>
							</View>
						)}

						rightOpenValue={-50}
					/>
          </View>
             <ActionButton
              buttonText="Skip"
              buttonTextStyle={{fontSize: 13}}
              buttonColor="#49abde"
              onPress={() => { //this.getNextQuestion()
              Actions.Comments();
              }}
          />
          </View>
          : 
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
      </View>
       
        }
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
        </View>
       );
  }
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
  imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#0f6aa6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    width: 50,
  },
  image: {
    width: 30,
  },
})

reactMixin(Answer.prototype, TimerMixin)
function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility
  }
}

export default connect(mapStateToProps)(Answer);
