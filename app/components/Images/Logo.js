import React from 'react';

import {View, Image} from 'react-native';
import styles from './styles';

const Logo = () => (
    <View style={styles.logoContainer}>
        <Image source={require('../../assets/logo.png')} style={styles.logo}></Image>
    </View>
);

export default Logo;