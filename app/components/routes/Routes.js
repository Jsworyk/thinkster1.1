import React, { Component } from 'react'
import ReactNative, { Animated } from 'react-native';
import { connect } from 'react-redux'
import { Actions, ActionConst, NavBar } from 'react-native-router-flux';
import ControlPanel from '../core/ControlPanel'
import CustomNavBar from '../core/CustomNavBar'
import Drawer from 'react-native-drawer'
import Icon from 'react-native-vector-icons/FontAwesome';
import TabIcon from './TabIcon';

import {
  Router,
  Scene
} from 'react-native-router-flux'

import Home from '../../containers/Home';
import Profile from '../profile/Profile';
import UserProfile from '../profile/UserProfile';
import Followers from '../profile/Followers';
import Following from '../profile/Following';
import Signup from '../auth/Signup';
import ForgotPassword from '../auth/ForgotPassword';
import ResetPassword from '../auth/ResetPassword';
import SignupNext from '../auth/SignupNext';
import Signin from '../auth/Signin';
import Modal from '../Modal/Modal';
import AvatarModal from '../Modal/AvatarModal'; 
import Timeline from '../home/Timeline';
import Notifications from '../home/Notifications';
import Messages from '../home/Messages';
import Search from '../home/Search';
import ImagePost from '../image/ImagePost';
import Reactors from '../polls/Reactors';
import FriendResponses from '../polls/FriendResponses';
import Results from '../polls/Results';
import Poll from '../polls/Poll';
import PollComments from '../polls/PollComments';
import ResponseComments from '../polls/ResponseComments';
import ImageComments from '../image/ImageComments';
import FriendSuggest from '../polls/FriendSuggest';
import Answer from '../polls/Answer';
import Comments from '../polls/Comments';
import OverallChart from '../polls/OverallChart'
import Chat from '../chat/Chat';
import Map from '../polls/Maps';
import Menu from '../polls/Menu';


import Helper from '../../lib/helper'
import {Landing} from '../Landing';
import SafeAreaView from 'react-native-safe-area-view';
import ThinksterDefaultNavBar from './ThinksterDefaultNavBar';

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,

} = ReactNative

class navBar extends React.Component {
  render() {
    return (
      <View style={{backgroundColor: '#444'}}></View>
    );
  }
}

class Routes extends Component {

  constructor(props) {
    super(props);
    
    this.state =  {
    drawerOpen: false
  }
}

  renderLeftButton(){
    return(
      <TouchableOpacity onPress={() => Actions.pop()} underlayColor='#999' style={{width:42}}>
      <View style={{padding: 5, marginTop:12}}>
      <Icon style={{opacity: 1}} name='chevron-left' size={20} color='#fff' />
      </View>
      </TouchableOpacity>
    )
  }

  renderMenuButton(){
    return(
      <TouchableOpacity onPress={() => this.state.drawerOpen ? this.drawer.close() : this.drawer.open()} underlayColor='#999'>
      <View style={{padding: 0}}>
      <Icon style={{opacity: 1}} name='bars' size={20} color='#fff' />
      </View>
      </TouchableOpacity>
    )
  }



  renderRightButton(){
    return(
      <TouchableOpacity onPress={() => Actions.Options({type: 'reset'})} underlayColor='#999'>
      <View style={{padding: 5}}>
      <Icon style={{opacity: 1}} name='ellipsis-v' size={22} color='#fff' />
      </View>
      </TouchableOpacity>
    )
  }
  onEnterHandler(nextScreenName, lastScreenName) {
   console.log('yes')   
  }

  closeDrawer() {
    Helper.DateStamp("Closing Drawer");

    this.drawer.close();
    Helper.DateStamp("Closed Drawer");

  }

  openDrawer() {
    
    Helper.DateStamp("Opening Drawer");

    this.drawer.open();
  
    Helper.DateStamp("Opened Drawer");
  }

  render() {
    return (
      <Drawer
        ref={(ref) => this.drawer = ref}
        type="static"
        content={
          <ControlPanel {...this.props} closeDrawer={() => this.drawer.close()} />
        }
        acceptDoubleTap
        styles={{ main: { shadowColor: '#444', shadowOpacity: 0.9, shadowRadius: 8 } }}
        onOpen={() => {

            this.setState({drawerOpen: true})
        }}
        onClose={() => {

          this.setState({drawerOpen: false})
        }}
        captureGestures={false}
        tweenDuration={50}
        panThreshold={0.08}
        disabled={this.props.drawerState}
        openDrawerOffset={(viewport) => {
          return 100
        }}
        closedDrawerOffset={() => 0}
        panOpenMask={0.2}
        //acceptPan={false}
        negotiatePan
      >
        <Router>

          <Scene key="root" >
            <Scene key="tabbar" tabs={true} tabBarStyle={[{ height: 50, backgroundColor: '#fff', borderTopWidth: 0, borderTopColor: '#999', elevation: 4}, styles.shadow]}>
              <Scene {...this.props} key="Timeline" component={Timeline} title="NewsFeed" fontIcon='home' icon={TabIcon} 
                navigationBarStyle={{ backgroundColor: '#25AFD2' }} 
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}  
                type={ActionConst.REFRESH}
                hideNavBar={true}
                closeDrawer={() => this.closeDrawer()} 
                openDrawer={() => this.openDrawer()}
                drawerOpen={this.state.drawerOpen}
              />
              <Scene {...this.props} key="Notifications" component={Notifications} title="Notifications" fontIcon='bell-o' icon={TabIcon}
                navigationBarStyle={{ backgroundColor: '#1EA2CC' }} 
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}  
                //type={ActionConst.REFRESH} 
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
              />
              <Scene {...this.props} key="Landing" component={Landing} title="Landing" fontIcon='list-ul' icon={TabIcon} 
                navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
               
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}   
                //type={ActionConst.REFRESH} 
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
              />
              <Scene {...this.props} key="Messages" component={Messages} title="Messages" fontIcon='envelope-o' icon={TabIcon}
                navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}  
                //type={ActionConst.REFRESH} 
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
              />
              <Scene {...this.props} key="Profile" component={Profile} Icontitle='Profile' title="" fontIcon='user-o' icon={TabIcon}
                navigationBarStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}
                titleStyle={{ color: '#fff' }} 
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}  
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
                //type={ActionConst.REFRESH} 
              />

                          
              <Scene {...this.props} key="Polls" component={Answer} title="Polls" fontIcon='list-ul' 
                navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
               
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                rightButtonImage={require('../../assets/search.png')}
                rightButtonStyle={{width:42}}
                rightButtonIconStyle={{ width: 22, height: 22 }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                leftButtonStyle={{width:42}}
                drawerImage={require('../../assets/menu.png')}
                onRight={() => Actions.Search()}   
                //type={ActionConst.REFRESH} 
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
              />

              <Scene {...this.props} key="FriendSuggest" component={FriendSuggest} title="Discovery"
                navigationBarStyle={{ backgroundColor: '#1EA2CC' }} 
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                leftButtonIconStyle={{ width: 22, height: 18 }}
                renderBackButton={()=> this.renderLeftButton()}
                hideNavBar={true}
                closeDrawer={() => this.drawer.close()} 
                openDrawer={() => this.drawer.open()}
                drawerOpen={this.state.drawerOpen}
                />
            </Scene>
            <Scene {...this.props} key="Home" initial={true} component={Home} hideNavBar="true" />
            {/*<Scene {...this.props} key="Profile" component={Profile} />*/}
            <Scene {...this.props} key="Signup" component={Signup} title="Signup" hideNavBar="true"
              navigationBarStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}
                titleStyle={{ color: '#134fa1' }} 
                leftButtonIconStyle={{ width: 22, height: 18 }}
                
            />
            <Scene {...this.props} key="SignupNext" component={SignupNext} title="Signup Next" hideNavBar="true"
              navigationBarStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}
                titleStyle={{ color: '#134fa1' }} 
                leftButtonIconStyle={{ width: 22, height: 18 }}
                
            />
            <Scene {...this.props} key="OverallChart" component={OverallChart} title='Overall Results'
              navigationBarStyle={{ backgroundColor: '#1EA2CC', borderBottomWidth: 0 }}
              titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
              leftButtonIconStyle={{ width: 22, height: 18 }}
              renderBackButton={()=> this.renderLeftButton()}
              direction='vertical'
            />
            <Scene {...this.props} key="ResetPassword" component={ResetPassword} hideNavBar="true"  />
            <Scene {...this.props} key="ForgotPassword" component={ForgotPassword} hideNavBar="true"  />
            <Scene {...this.props} key="Signin" component={Signin} hideNavBar="true"  />
            <Scene {...this.props} key="Search" component={Search} title=''
              navigationBarStyle={{ backgroundColor: '#25AFD2', borderBottomWidth: 0 }}
              titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
              leftButtonIconStyle={{ width: 22, height: 18 }}
              renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="ImagePost" component={ImagePost} title='' hideNavBar={true}
              navigationBarStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}
              titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
              leftButtonIconStyle={{ width: 22, height: 18 }}
              renderBackButton={()=> this.renderLeftButton()}
              direction="vertical" 
            />
            <Scene {...this.props} key="Chat" component={Chat} title="Chat"
              navigationBarStyle={{ backgroundColor: '#4A9ECB', borderBottomWidth: 0 }}
              titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
              leftButtonIconStyle={{ width: 22, height: 18 }}
              renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="PollComments" component={PollComments} title="Comments"
              
            navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
            
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
             hideNavBar={true}
             closeDrawer={() => this.drawer.close()} 
             openDrawer={() => this.drawer.open()}
             drawerOpen={this.state.drawerOpen}
            />
            <Scene {...this.props} key="ResponseComments" component={ResponseComments} title="Comments"
              
            navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
            
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
             hideNavBar={true}
             closeDrawer={() => this.drawer.close()} 
             openDrawer={() => this.drawer.open()}
             drawerOpen={this.state.drawerOpen}
            />
            <Scene {...this.props} key="ImageComments" component={ImageComments} title="Comments"
              
            navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
            
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
             hideNavBar={true}
             closeDrawer={() => this.drawer.close()} 
             openDrawer={() => this.drawer.open()}
             drawerOpen={this.state.drawerOpen}
             
            />
            <Scene {...this.props} key="UserProfile" component={UserProfile} title=""
            navigationBarStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="Reactors" component={Reactors} title="Reactors"
            navigationBarStyle={{ backgroundColor: '#4A9ECB', borderBottomWidth: 0 }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }} 
             renderBackButton={()=> this.renderLeftButton()}
             
            />
            <Scene {...this.props} key="Followers" component={Followers} title="Followers"
            navigationBarStyle={{ backgroundColor: '#4A9ECB', borderBottomWidth: 0 }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }} 
             renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="Following" component={Following} title="Following"
            navigationBarStyle={{ backgroundColor: '#4A9ECB', borderBottomWidth: 0 }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="FriendResponses" component={FriendResponses} title="Responses"
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
             hideNavBar={true}
             closeDrawer={() => this.drawer.close()} 
             openDrawer={() => this.drawer.open()}
             drawerOpen={this.state.drawerOpen}
            />
            <Scene {...this.props} key="Results" component={Results} title="Results"
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
            />
            <Scene {...this.props} key="Question" component={Poll} title="Poll"
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             renderBackButton={()=> this.renderLeftButton()}
             hideNavBar={true}
             closeDrawer={() => this.drawer.close()} 
             openDrawer={() => this.drawer.open()}
             drawerOpen={this.state.drawerOpen}
            />
            <Scene {...this.props} key="Map" component={Map} title="Map"
            
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
             titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
             titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             leftButtonIconStyle={{ width: 22, height: 18 }}
             //navBar={ThinksterDefaultNavBar}
             renderBackButton={()=> this.renderLeftButton()}
            />


            <Scene {...this.props} key="Answer" component={Answer} title="" hideNavBar={true} />
            

            
            <Scene {...this.props} key="Modal" component={Modal} panHandlers={null}
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             direction="vertical" 
             renderBackButton={()=> this.renderLeftButton()}
             />
             <Scene {...this.props} key="AvatarModal" component={AvatarModal} panHandlers={null}
              navigationBarStyle={{ backgroundColor: '#1EA2CC' }}
                titleStyle={{ color: '#fff', backgroundColor: 'transparent' }}
                titleWrapperStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
             direction="vertical" 
             renderBackButton={()=> this.renderLeftButton()}
             
             />
          </Scene>
        </Router>
      </Drawer>
    )
  }
}

const styles = StyleSheet.create({
  shadow:{
    shadowColor: '#999',
    shadowOpacity: 0.5,
    shadowRadius: 4,
    shadowOffset: {
      height: 1,
      width: 2,
    },
  },
})


function mapStateToProps(state) {

  return {
    drawerState: state.toggleDrawer.drawerState
  }
}

export default connect(mapStateToProps)(Routes);


class ThinksterRouterNavBar extends NavBar {
  constructor(props) {
    super(props);
  }

  render() {
      let state = this.props.navigationState;
      let selected = state.children[state.index];
      while ({}.hasOwnProperty.call(selected, 'children')) {
        state = selected;
        selected = selected.children[selected.index];
      }
      const navProps = { ...this.props, ...selected };
  
      const wrapByStyle = (component, wrapStyle) => {
        if (!component) { return null; }
        return props => <View style={wrapStyle}>{component(props)}</View>;
      };
  
      const leftButtonStyle = [styles.leftButton, { alignItems: 'flex-start' }, this.props.leftButtonStyle, state.leftButtonStyle];
      const rightButtonStyle = [styles.rightButton, { alignItems: 'flex-end' }, this.props.rightButtonStyle, state.rightButtonStyle];
  
      const renderLeftButton = wrapByStyle(selected.renderLeftButton, leftButtonStyle) ||
        wrapByStyle(selected.component.renderLeftButton, leftButtonStyle) ||
        this.renderLeftButton;
      const renderRightButton = wrapByStyle(selected.renderRightButton, rightButtonStyle) ||
        wrapByStyle(selected.component.renderRightButton, rightButtonStyle) ||
        this.renderRightButton;
      const renderBackButton = wrapByStyle(selected.renderBackButton, leftButtonStyle) ||
        wrapByStyle(selected.component.renderBackButton, leftButtonStyle) ||
        this.renderBackButton;
      const renderTitle = selected.renderTitle ||
        selected.component.renderTitle ||
        this.props.renderTitle;
      const navigationBarBackgroundImage = this.props.navigationBarBackgroundImage ||
        state.navigationBarBackgroundImage;
      const navigationBarBackgroundImageStyle = this.props.navigationBarBackgroundImageStyle ||
        state.navigationBarBackgroundImageStyle;
      const navigationBarTitleImage = this.props.navigationBarTitleImage ||
        state.navigationBarTitleImage;
      let imageOrTitle = null;
      if (navigationBarTitleImage) {
        imageOrTitle = this.renderImageTitle();
      } else {
        imageOrTitle = renderTitle ? renderTitle(navProps)
        : state.children.map(this.renderTitle, this);
      }
      const contents = (

        <View>
          {imageOrTitle}
          {renderBackButton(navProps) || renderLeftButton(navProps)}
          {renderRightButton(navProps)}
        </View>
      );
      return (
        <Animated.View
          style={[
            styles.header,
            this.props.navigationBarStyle,
            state.navigationBarStyle,
            selected.navigationBarStyle,
          ]}
        >
          {navigationBarBackgroundImage ? (
            <Image style={navigationBarBackgroundImageStyle} source={navigationBarBackgroundImage}>
              {contents}
            </Image>
          ) : contents}
        </Animated.View>

      );
    }
}