#!/bin/sh

# Fix RN version 47
yarn list react-native |grep react-native |grep 0.47
if [ $? -eq 0 ];
then
    echo "Checking that ReactNative v 47 is patched"
    cd node_modules/react-native
    patch -N -p1 --dry-run --silent < ../../patches/rn47x.patch >/dev/null 2>&1
    if [ $? -eq 0 ];
    then
        # apply the patch
        patch -N -p1 < ../../patches/rn47x.patch
    else
        echo "Patch already applied"
    fi
else
    echo "React Native not version 47, skipping patch"
fi
