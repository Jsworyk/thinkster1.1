import React, {Component, PropTypes} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  ScrollView,
  Dimensions,
  Platform,
  StatusBar,
  FlatList,
  TouchableHighlight,
  TouchableOpacity,
  Keyboard,
  TextInput,
  AppState
} from 'react-native';

import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var width = Dimensions.get('window').width;
import { List, Card, Button, ListItem } from 'react-native-elements'
import SocketIOClient from 'socket.io-client';
import ModalWrapper from 'react-native-modal-wrapper';
var fontColorContrast = require('font-color-contrast');
import Modal from 'react-native-simple-modal';
import LinearGradient from 'react-native-linear-gradient';
import CardView from 'react-native-cardview'

import Helper from '../../lib/helper'

import {SearchButton, DrawerButton} from '../Buttons';
import {LoadingSpinner} from '../Images';
import { ThinksterNavBar } from '../Layout';


var Reactors = [];
var positions = [0,32,64];
var reportReasons = [{name: 'Solicitation/Spam/bot', id: 'SO'}, {name: 'Copyright Infringement', id: 'CO'}, {name: 'Links to illegal content or filesharing sites', id: 'IL'}, {name: 'Pornographic Content', id: 'PO'}, {name: 'Other (describe below)', id: 'OT'}]
class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class Timeline extends Component {

   constructor(props){
    super(props);
    
    this.state = {
      loading: false,
      showTimeline: false,
      Reactors: [],
      refreshing: false,
      showLoader: true,
      likeContainerVisible: false,
      openReportModal: false,
      dialogOffset: 0,
      details: '',
      drawerOpen: false,
      appState: AppState.currentState
    }
    this.addTimelineItemsBottom = this.addTimelineItemsBottom.bind(this)
    this.getTimeline = this.getTimeline.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  static propTypes = {
    closeDrawer: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired
  };

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      this.props.getNotifications(this.props.applicationToken, '',15).then(() => {
        
     })
     this.props.getChatrooms(this.props.applicationToken).then(() => { 
       
     })
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  openReportModal(TargetTypeCode, TargetId){
    this.setState({
      openReportModal: true,
      TargetTypeCode: TargetTypeCode,
      TargetId: TargetId
    })
  }

  _keyboardDidShow () {
    this.setState({
      dialogOffset: -100
    })
  }

  _keyboardDidHide () {
    this.setState({
      dialogOffset: 0
    })
  }

  flagContent(){
    this.props.flagContent(this.props.applicationToken, this.state.TargetTypeCode, this.state.TargetId, this.state.reasonCode, this.state.details).then((status) => {
      if (status){
        this.setState({
          openReportModal: false
        },
        () => {
          alert('Content reported!')
        }
      )
      }
    })
  }

  renderReportReasons(l,i){
    return (
      <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.selectReason(l.id, i)}} underlayColor='#999'>
        <View style={{backgroundColor: this.state.selectedReason == i ? '#1EA6CC' : '#ccc', borderRadius: 15, margin: 5, padding: 8}}>
          <Text style={{fontSize: 12}}>{l.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  selectReason(id,i){
    this.setState({
      reasonCode: id,
      selectedReason: i
    })
  }

  componentWillMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  addTimelineItemsBottom(){
    console.log('Reached bottom');
    this.setState({
      loading: true,
      refreshing: false,
      timelineItems : []
    })

    if (Platform.OS == 'ios'){
        if (!this.onEndReachedCalledDuringMomentum) {
        this.props.getTimeline(this.props.applicationToken, this.props.FromTimestampLast , 10, 'bottom').then(() => {
          
          this.onEndReachedCalledDuringMomentum = true;
          this.setState({
            loading: false,
            refreshing: false
          })
        })
      }
    }else {
      
        this.props.getTimeline(this.props.applicationToken, this.props.FromTimestampLast , 10, 'bottom').then(() => {
          this.setState({
            loading: false,
            refreshing: false
          })
        })

    }
  }

  finishLoading() {
      this.setState({
        loading: false
      })
  }

  getTimeline(){
    this.setState({
      loading: true,
      refreshing: true,
      timelineItems : []
    })
    
    this.props.getTimeline(this.props.applicationToken,  '' , 10, '').then(() => {
      this.setState({
        refreshing: false,
        loading: false
      })
    })
  }

  componentDidMount(){
    AppState.addEventListener('change', this._handleAppStateChange);
    this.props.getTimeline(this.props.applicationToken, this.props.FromTimestamp ? this.props.FromTimestamp : '', 10, '').then(() => {
      this.setState({
        showTimeline: true,
        timelineItems: this.props.timelineItems
      })
      this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
        this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false, transports: ['websocket'] });
        this.socket.on('connect', () => {
          this.socket.emit('authenticate', this.props.applicationToken, ['PR'+this.props.currentUserProfile.ProfileId]);
        });
        this.socket.on('Notification', (data) => {
          
          this.props.addNotification(data)
        });
        this.socket.on('Reaction', (data) => {
          
          this.props.timelineItems.map((l,i) => {
              if (l.TargetId == data.TargetId) {
                this.props.addTimelineReaction(i,data)
              } 
            }) 
      });
      this.socket.on('ChatMessage', (data) => {
        var messageToUpdate = this.props.chatrooms.filter(function(item) { 
          return item.ChatRoomId === data.ChatRoomId;  
        });
        var messagesList = this.props.chatrooms.filter(function(item) { 
          return item.ChatRoomId !== data.ChatRoomId;  
        });
        
    
        if (data.Messages[0].ProfileId !== this.props.currentUserProfile.ProfileId){
         var newMessage = {
            ChatRoomId: data.ChatRoomId,
            ChatPreview: data.Messages[0].Message,
            ChatRoomAvatar: data.Messages[0].AvatarUrl,
            RoomName: data.Messages[0].DisplayName,
            LastMessage: data.Messages[0].Created,
            Unread: 1
          }
          messagesList.unshift(newMessage)
          this.props.addChatroom(newMessage)
        }else{
          
          if (messageToUpdate.length == 0){
            var newMessage = {
              ChatRoomId: data.ChatRoomId,
              ChatPreview: this.props.lastMessageSent,
              ChatRoomAvatar: this.props.lastMessageSentAvatarUrl,
              RoomName: this.props.lastMessageSentDisplayName,
              LastMessage: data.Messages[0].Created,
              Unread: 0
            }
            messagesList.unshift(newMessage)
            this.props.addChatroom(newMessage)
          }else{
            messageToUpdate[0].ChatPreview = data.Messages[0].Message;
            messageToUpdate[0].LastMessage = data.Messages[0].Created
            messagesList.unshift(messageToUpdate[0])
          }
        }
      
        this.props.setChatrooms(messagesList.sort(function(a, b) {
          return new Date(b.LastMessage) - new Date(a.LastMessage);
        }));
      });

      })
      
      this.props.getNotifications(this.props.applicationToken, '',15).then(() => {
         
      })
      this.props.getChatrooms(this.props.applicationToken).then(() => { 
        
      })
      this.props.getReactionTypes(this.props.applicationToken).then(() => {
        
      })
      
    })
   
   
  //Actions.refresh({hideNavBar: false})

  
  }

  open(id, type) {
    this.setState({
        likeContainerVisible: true,
        reactingTo: id,
        type: type
    })
}

close() {
    this.setState({open: false}, () => {
         Animated.timing(this._scaleAnimation, {
          duration: 50,
          toValue: 0
        }).start();
    })
}

getLikeContainerStyle() {
    return {
            transform: [{scaleY: this._scaleAnimation}],
            overflow: this.state.open ? 'visible': 'hidden',
          };
}

React(l){
  
    this.setState({
        likeContainerVisible: false
    })
    this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
      
    });
}


extractText( str ){
  var ret = "";

  if ( /"/.test( str ) ){
    ret = str.match( /"(.*?)"/ )[1];
  } else {
    ret = str;
  }

  return ret;
}

truncate(string, stringLength){
  if (string.length > stringLength)
     return string.substring(0,stringLength)+'...';
  else
     return string;
}

secondaryImageOutput(l) {
     return ( <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.ImagePost({Selector: l.SecondaryImageSelector})}} underlayColor='#999'>
        <Image source={{uri: l.SecondaryImage}} resizeMode="cover"
        style={{ height: 280,borderRadius: 15, backgroundColor: 'transparent'}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, marginRight: Platform.OS == 'ios' ? 0 : 25}}>
            <View style={{flexDirection: 'row',alignItems: 'center'}}>
            {l.Image ? 
            <Image source={{uri: l.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
            :
              <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15,justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
              </View>
            }
            <TouchableOpacity rejectResponderTermination  onPress={() => {l.ProfileId == this.props.ProfileId ? Actions.Profile() : Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
              <Text style={[{color: '#fff', marginLeft: 10, fontWeight: 'bold',fontSize: 12}, styles.shadowDark]}>{l.DisplayName}</Text>
            </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row',alignItems: 'center'}}>
            <Text style={{color: '#ccc', textAlign: 'center', fontSize: 11}}>
            { 
              Helper.durationOutput(l.Created) 
            }
            </Text>
            </View>
          </View>
        </Image>
        </TouchableOpacity>
     )
}

mainTimelineItemHeader(l, truncatedText, truncatedInner) {
  return (
    <View style={{paddingBottom: Platform.OS === 'ios' ? 0 : 120, flex: Platform.OS === 'ios' ? 0 : 1, backgroundColor: l.BackgroundColor ? l.BackgroundColor : '#FFD700', borderTopRightRadius: Platform.OS === 'ios' ? 15 : 0, borderTopLeftRadius: Platform.OS === 'ios' ? 15 : 0}}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, marginRight: Platform.OS == 'ios' ? 0 : 25}}>
      <View style={{flexDirection: 'row',alignItems: 'center'}}>
      {l.Image ? 
        <Image source={{uri: l.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
        :
        <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
        </View>
        }
      <TouchableOpacity rejectResponderTermination  onPress={() => {l.ProfileId == this.props.ProfileId ? Actions.Profile() : Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
          <Text style={{color: fontColorContrast(l.BackgroundColor), marginLeft: 10, fontWeight: 'bold', fontSize: 12}}>{l.DisplayName}</Text>
      </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row',alignItems: 'center'}}>
        <Text style={{color: fontColorContrast(l.BackgroundColor), textAlign: 'center', fontSize: 11}}>
          {
          Helper.durationOutput(l.Created)
          }
        </Text>
      </View>
      </View>
      <View  
        style={{
          padding : 10,
          flexWrap: 'wrap'
        }}
        >
        <View style={{alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap', paddingRight: Platform.OS === 'ios' ? 0 : 10}}>
          <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.Question({isPopup: false, pollName: pollName})}} underlayColor='#999'>
            <Text style={{ fontSize: 20, lineHeight: Platform.OS == 'ios' ? 20 : 24, fontWeight: 'bold', color: fontColorContrast(l.BackgroundColor), margin: 0, }}>{truncatedText}</Text>
          </TouchableOpacity>
          
          </View>
          <View style={{marginVertical: 15, alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
            <Text style={{fontSize: 16, lineHeight: 20, margin: 0, color: fontColorContrast(l.BackgroundColor)}}>{truncatedInner}</Text>
          </View>
      </View>
      
      </View>
  )


}

avatarThumbnailOutput(i, l) {
  return (
    <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
    {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
  </View>
  )
}

displayNameOutput(i, l) {
  if (Platform.OS == 'ios') {
  return (
    <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,padding: 3,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
        <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
    </View>
  )
} else {
  return (
    <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15,justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
    </View>
  )
}
}

renderExtraFollowingResponse(l) {
  if (l.FollowingResponseCount > 3) {
    return (
      <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
        <Text style={{color: '#fff'}}>{l.FollowingResponseCount-3}+</Text>
    </View>
    )
  }
  return null;
}

  renderItem(l, i){
    var regex = /\[\[([upm]):([^:]+):([^\]]+)\]\]/g;
    var matches = l.Text.match(/\[\[([upm]):([^:]+):([^\]]+)\]\]/g);
    var PrasedTextArray = []
    var ids = []
    var inners = []    
    var types = []
    var pollName = null;
   
    for (var i in matches){
      PrasedTextArray.push(matches[i].replace(regex, function(string, first, second, third){
        if (first == 'p'){
          pollName = second;
        }
        ids.push(second)
        types.push(first)
        return (
          third
        )
      }))
    }

    var lastMatch = ''
    var lastMatchedIndex = 0;
    var finalIndex = 0;
    while ((m = regex.exec(l.Text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      
      if (m.index != 0){

        var text_to_get = l.Text.substring(lastMatchedIndex + lastMatch.length, m.index )
          inners.push(text_to_get); 
          
      }
      lastMatch = m[0]
      lastMatchedIndex = m.index;
      finalIndex = regex.lastIndex;
    }
    if (finalIndex < l.Text.length){
      var text_to_get = l.Text.substring(finalIndex, l.Text.length)
      inners.push(text_to_get); 
    }
    
    var truncatedText = this.truncate(PrasedTextArray.length > 1 ? PrasedTextArray[1] : PrasedTextArray[0], 200);
    var truncatedInner = this.truncate(this.extractText(inners[inners.length-1]), 200);
    return (

      <View key={i} style={styles.cardContainer}>
       

       { Platform.OS == 'ios' ?
            <View style={[styles.card, {} ]}>
              {l.SecondaryImage ?
                this.secondaryImageOutput(l, truncatedText)            
              :
                this.mainTimelineItemHeader(l, truncatedText, truncatedInner)
              }
              <View style={[styles.cardAction, {borderBottomRightRadius : 15, borderBottomLeftRadius : 15, backgroundColor: l.TargetTypeCode == 'RE' ? '#fff' : 'transparent', position : l.TargetTypeCode == 'ME' ? 'absolute' : 'relative', bottom: 0, left: 0, right: 0 }]}>
                
              <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',left: 10, top: -18, }}>
              {l.TargetTypeCode == 'RE' ?
              <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.FriendResponses({pollName: pollName, item: l})}} underlayColor='#999'>
                {
                  l.RecentThreeFollowing ?
                  l.RecentThreeFollowing.map((l,i) => (
                    l.AvatarThumbnailUrl == null ?
                      this.displayNameOutput(i, l)
                        :
                      this.avatarThumbnailOutput(i, l)
                  ))
                  :
                  null
                }
              { 
                this.renderExtraFollowingResponse(l)
              }
            </TouchableOpacity>
            :
            l.TargetTypeCode == 'ME' ?
            <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => {Actions.Reactors({TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode})}} underlayColor='#999'>
            {
              l.RecentThreeFollowing ?
              l.RecentThreeFollowing.map((l,i) => (
                l.AvatarThumbnailUrl == null ?
              <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,padding: 3,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
              </View>
              :
              <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
              </View>
            ))
            :
            null
          }
          { l.FollowingResponseCount > 3 ?
          
              <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                <Text style={{color: '#fff'}}>{l.FollowingResponseCount-3}+</Text>
              </View>
          
        :
        null  
        }
        </TouchableOpacity>
        :
        null
          }
              
                </View>

                <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',right: 10, top: -18, backgroundColor: 'transparent' }}>
                {l.TargetTypeCode == 'RE' ?
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({pollName: pollName})}} underlayColor='#999'>
                  <View style={[{ position: 'absolute', right: 64,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    {/* <Icon style={{}} name='map' size={16} color='white' /> */}
                    <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                  </View>
                  </TouchableOpacity>
                  :
                  null
                }
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { l.SecondaryImage ? Actions.ImageComments({Selector: l.SecondaryImageSelector, item: l}) : Actions.ResponseComments({pollName: pollName, item: l})}} underlayColor='#999'>
                  <View style={[{ position: 'absolute', right: 32,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    {/* <Icon style={{}} name='comment' size={16} color='white' /> */}
                    <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                  </View>
                </TouchableOpacity>
                {l.Reacted == 0 ?
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                      <View style={[{position: 'absolute', right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                  {/* <Icon style={{}} name='heart' size={16} color={'#fff'} /> */}
                        <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                      </View>
                    </TouchableOpacity>
                  :
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                <View style={[{position: 'absolute', right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                  <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{l.Emoticon}</Text>
                </View>
                </TouchableOpacity>
              }

                </View>
                {l.TargetTypeCode == 'RE' && l.FollowingResponseCount > 0 ? 
                  <Text style={{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5}}>
                  {l.RecentThreeFollowing.map(function(ele){
                    return ele.DisplayName;
                }).join(",") + (l.FollowingResponseCount > 3 ? (l.FollowingResponseCount == 4) ? ' and 1 other answered this question' : ' and ' + (l.FollowingResponseCount-3) +' others answered this question' : ' answered this question')}
                </Text>
                :
                null
                }
                {l.TargetTypeCode == 'ME' && l.FollowingReactionCount > 0 ? 

                  <Text style={[{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5, color: '#fff', fontWeight: 'bold'}, styles.shadowDark]}>
                  {
                    l.RecentThreeFollowing.map(function(ele){
                  return ele.DisplayName;
                  }).join(",") + (l.FollowingReactionCount > 3 ? (l.FollowingReactionCount == 4) ? ' and 1 other reacted to this image' : ' and ' + (l.FollowingReactionCount-3) +' others reacted to this image' : ' reacted to this image')}
                </Text>
                :
                null
                }
                {
                  l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0 ?
                  <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                  {''}
                  </Text>
                  :
                  null
                }
                {
                  l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ?
                  <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                  {''}
                  </Text>
                  :
                  null
                }
                <TouchableOpacity rejectResponderTermination  style={{padding: 5, position: 'absolute', top: (l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0) ? 30 : (l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ) ? 30 : 40, right: 15, bottom: 0, flex: 1,}}  onPress={() => {this.openReportModal(l.TargetTypeCode, l.TargetId)} } underlayColor='#999'>
                  <Icon style={[{backgroundColor: 'transparent'},styles.shadowDark]} name='ellipsis-v' size={18} color={l.TargetTypeCode == 'ME' ? '#fff' : '#444'} />
                </TouchableOpacity>
              </View>
              
            </View>
            :
            <CardView
            cardElevation={4}
            cardMaxElevation={4}
            cornerRadius={10}
            style={{backgroundColor: 'transparent',padding: 0,flex: 1 }}
            >
            {l.SecondaryImage ?
              this.secondaryImageOutput(l)
              :
              this.mainTimelineItemHeader(l, truncatedText, truncatedInner)
              }
                <View style={[styles.cardAction, { paddingTop: 20, borderBottomRightRadius : 15, borderBottomLeftRadius : 15, backgroundColor: l.TargetTypeCode == 'RE' ? 'transparent' : 'transparent', position : l.TargetTypeCode == 'ME' ? 'absolute' : 'absolute', bottom: 0, left: 0, right: 0 }]}>
                  <View style={{backgroundColor: l.TargetTypeCode == 'RE' ? '#fff' : 'transparent', paddingBottom: l.FollowingResponseCount > 0 ? 15 : 30}}>
                  {l.TargetTypeCode == 'RE' && l.FollowingResponseCount > 0 ? 
                    <Text style={{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5}}>
                    {l.RecentThreeFollowing.map(function(ele){
                      return ele.DisplayName;
                  }).join(",") + (l.FollowingResponseCount > 3 ? (l.FollowingResponseCount == 4) ? ' and 1 other answered this question' : ' and ' + (l.FollowingResponseCount-3) +' others answered this question' : ' answered this question')}
                  </Text>
                  :
                  null
                  }
                  {l.TargetTypeCode == 'ME' && l.FollowingReactionCount > 0 ? 
  
                    <Text style={[{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5, color: '#fff', fontWeight: 'bold'}, styles.shadowDark]}>
                    {
                      l.RecentThreeFollowing.map(function(ele){
                    return ele.DisplayName;
                    }).join(",") + (l.FollowingReactionCount > 3 ? (l.FollowingReactionCount == 4) ? ' and 1 other reacted to this image' : ' and ' + (l.FollowingReactionCount-3) +' others reacted to this image' : ' reacted to this image')}
                  </Text>
                  :
                  null
                  }
                  {
                    l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0 ?
                    <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                    {''}
                    </Text>
                    :
                    null
                  }
                  {
                    l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ?
                    <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                    {''}
                    </Text>
                    :
                    null
                  }
                  <TouchableOpacity rejectResponderTermination  style={{padding: 5, position: 'absolute', top: (l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0) ? Platform.OS == 'ios' ? 30 : 30 : (l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ) ? 30 : 40, right: Platform.OS == 'ios' ? 15 : 25, bottom: Platform.OS == 'ios' ? 0 : 0, flex: 1,}}  onPress={() => {this.openReportModal(l.TargetTypeCode, l.TargetId)} } underlayColor='#999'>
                    <Icon style={[{backgroundColor: 'transparent'},styles.shadowDark]} name='ellipsis-v' size={18} color={l.TargetTypeCode == 'ME' ? '#fff' : '#444'} />
                  </TouchableOpacity>

                    </View>


                  <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 84, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    {l.TargetTypeCode == 'RE' ?
                      <TouchableOpacity rejectResponderTermination  style={{zIndex: 100, }} onPress={() => { Actions.Map({pollName: pollName})}} underlayColor='#999'>
                      <View style={[{right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                        <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                      </View>
                      </TouchableOpacity>
                      :
                      null
                    } 
                  </View>
                  <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 52, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}} onPress={() => { l.SecondaryImage ? Actions.ImageComments({Selector: l.SecondaryImageSelector, item: l}) : Actions.ResponseComments({pollName: pollName, item: l})}} underlayColor='#999'>
                    <View style={[{right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                      <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                    </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 20, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    {l.Reacted == 0 ?
                    
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                    <View style={[{right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                      <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                    </View>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                    <View style={[{right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                      <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{l.Emoticon}</Text>
                    </View>
                    </TouchableOpacity>
                  }
                  </View>

                  <View style={{elevation: 4,zIndex: 600, position: 'absolute',left: 10, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                {
                    l.RecentThreeFollowing[0] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>

                  <View style={{elevation: 4,zIndex: 700, position: 'absolute',left: 42, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    l.RecentThreeFollowing[1] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[1].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[1].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[1].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  <View style={{elevation: 4,zIndex: 800, position: 'absolute',left: 74, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    l.RecentThreeFollowing[2] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[2].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[2].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[2].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  {
                      l.FollowingResponseCount > 3 ?
                  <View style={{elevation: 4,zIndex: 900, position: 'absolute',left: 106, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                   

                        <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ color: '#fff' }}>{l.FollowingResponseCount - 3}+</Text>
                        </View>
                       
                    </TouchableOpacity>
                </View>
                 :
                 null
             }

                </View>
   
            </CardView>
       }
          </View>
      
    )
  }

  toggleDrawer(){
    Helper.DateStamp("Drawer State is", this.state.drawerOpen)
    
    this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
    this.setState({
      drawerOpen: !this.state.drawerOpen
    })

  }

  _keyExtractor = (item, index) => index

  render () {
    return (
      <View style={styles.container}>

        <StatusBar
     backgroundColor="#057AB8"
     setBarStyle={{borderBottomWidth: 0, opacity: 0.9}}
     barStyle="light-content"/>

       <ThinksterNavBar toggleDrawer={this.toggleDrawer} title="Timeline"></ThinksterNavBar>

      <View style={{flex: 1, justifyContent: 'center', backgroundColor:"#FFF"}}>
        {
          (()=> {
            if (this.state.showTimeline) {
              let content = null;
              if (this.props.timelineItems.length > 0) {
                let extraProps = {};
                if (Platform.OS === 'ios') {
                  extraProps.onEndReachedThreshold = 0;
                  extraProps.onMomentumScrollBegin = () => { this.onEndReachedCalledDuringMomentum = false; } 
                }
                console.log("Loading State is ",this.state.loading);
                content = (
                  <FlatList
                    {...extraProps}
                    refreshing={this.state.refreshing}
                    onRefresh={this.getTimeline}
                    extraData={this.props.timelineItems}
                    data={this.props.timelineItems}
                    onEndReached={this.addTimelineItemsBottom}
                    keyExtractor={this._keyExtractor}
                    renderItem={  ({item, index}) =>  this.renderItem(item, index)  }
                    ListFooterComponent={(
                      this.state.loading ? 
                        <LoadingSpinner></LoadingSpinner>
                      :
                        null
                    )}
                  />
                )
              } else {
                content = (
                  <View style={[{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderColor: '#ccc', borderWidth: 0.5, padding: 10, margin: 10}, styles.shadow]}>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                      <Image source={require('../../assets/check.png')} style={{width: 50, height: 50}}/>
                    </View>
                    <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 10, margin: 5}}>
                      <Text style={{color: '#444', fontSize: 16, fontWeight: 'bold'}}>Welcome to Thinkster</Text>
                      <Text style={{color: '#999', fontSize: 14, fontWeight: 'normal', margin: 5, textAlign: 'center'}}>When you follow people, you'll see their photos and their responses here.</Text>
                    </View>
                    <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.FriendSuggest({sideMenu: true, hideNavBar: false}) }} underlayColor='#999'>
                        <View style={{ backgroundColor: '#1EA2CC', borderColor: '#fff', borderRadius: 5, margin: 5, padding: 3,paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ backgroundColor: 'transparent', padding: 3, fontSize: 14, color: '#fff', fontWeight: 'normal' }}>{'Find People to Follow'}</Text>
                        </View>
                    </TouchableOpacity>
                  </View>
                );
              }
              return content;
            }
            return (
              <LoadingSpinner></LoadingSpinner>
            )
          })()
        }
      </View>
   
      <ModalWrapper
          style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
          visible={this.state.likeContainerVisible}
          onRequestClose={() => this.setState({ likeContainerVisible: false })}
          shouldCloseOnOverlayPress={true}
          >
            <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                {
                  this.props.reactionTypes ?
                this.props.reactionTypes.map((l, i) => (
                    <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                        <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                    </TouchableOpacity>
                ))
                :
                null
              }

            </View>
      </ModalWrapper>
      <Modal
          open={this.state.openReportModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openReportModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
            <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{'Report this Content'} </Text>
          </View>

          <ScrollView style={{height: 230}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 5}}>
            {reportReasons.map((l,i) => (
              this.renderReportReasons(l,i)
            ))}
          </View>
          </ScrollView>

              <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5
              }}>
                  <UselessTextInput
                      style={{ paddingBottom: 10, color: '#444', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={details => this.setState({ details })}
                      value={this.state.details}
                      placeholder='Details...'
                      placeholderTextColor='#d7d7d7'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={true}
                      editable={true}
                  />
              </View>
              <Button
                title='Report'
                buttonStyle={{margin: 5,marginBottom: 10, height: 35}}
                backgroundColor='#1EA6CC'
                color='#fff'
                textStyle={{ fontSize: 14, fontWeight: 'normal' }}
                onPress={() => { this.flagContent() }}
              />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
   cardContainer:{
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#fff',
    margin: Platform.OS == 'ios' ? 10 : 5,
    marginBottom: 5,
    
  },
  shadowDark:{
    shadowColor: '#444',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    shadowOffset: {
      height: 2,
      width: 2,
    },
  },
  card:{
    backgroundColor: '#ffffff',
    borderRadius: 15,
    borderColor: '#ffffff',
    borderWidth: 0,
    shadowColor: 'rgba(0, 0, 0, 0.90)',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 5,
      width: 2,
    },
    
  },
  cardTitleContainer:{
    flex: 1,
    height: 170,
  },
  cardTitle:{
    position: 'absolute',
    top: 120,
    left: 26,
    backgroundColor: 'transparent',
    padding: 16,
    fontSize: 24,
    color: '#000000',
    fontWeight: 'bold',
  },

  cardContent:{
    padding:0,
    color: 'rgba(0, 0, 0, 0.54)',
  },

  cardAction:{
    borderStyle: 'solid',
    borderTopColor: 'rgba(0, 0, 0, 0.1)',
    //borderTopWidth: 1,
    //padding: 15,
    paddingBottom: Platform.OS == 'ios' ?  15 : 15,
  },
  container: {
    flex: 1,
    backgroundColor: '#1EA2CC',
    justifyContent: 'center',
    marginBottom: 50,
   // marginTop: Platform.OS === 'ios' ? 64 : 54,
  },
  leftConatiner: {
    width: 60,

  },
  rightConatiner: {
    paddingRight: 10,
    flex: 1
    //width: 300
  },
  
  buttonContainer: {
    borderTopWidth: 0.5,
    borderTopColor: '#ccc',
    flexDirection: 'row',
    marginTop: 10,
    padding: 2,
    justifyContent: 'space-around',
    flex: 1

  },
  button: {
    margin: 0,padding: 3, backgroundColor: '#fff', width: width*0.5, height: 25
  },
  smallText: {
    fontSize: 11,
    color: '#696868',
    margin: 2,
    opacity: 0.8,
    //fontFamily: 'Cabin',
  },
  countText: {
    fontSize: 12,
    color: '#134fa1',
    margin: 2,
    opacity: 0.8,
   // fontFamily: 'Cabin',
  },
  countContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 8,
    justifyContent: 'flex-start'
  },
  textContent: {
    margin: 2,
    fontSize: 14,
    color: '#696868',
   // fontFamily: 'Cabin',

  },
  title: {
    fontSize: 38,
    backgroundColor: 'transparent'
  },
  username: {
    color: '#2b8abb',
    fontWeight: 'bold',
    //fontFamily: 'Cabin',
  },
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
 shadow:{
  shadowColor: 'rgba(0,0,0,0.5)',
  shadowOpacity: 0.5,
  shadowRadius: 5,
  shadowOffset: {
    height: 3,
    width: 0,
  },
  elevation: 2
},

});

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
    //AvatarUrl: state.CURRENT_USER.AvatarUrl,
    ProfileId: state.CURRENT_USER.ProfileId,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    timelineItems: state.timeline.timelineItems,
    FromTimestampFirst: state.NEWS_FEED_FIRST_TIMESTAMP.FromTimestamp,
    FromTimestampLast: state.NEWS_FEED_LAST_TIMESTAMP.FromTimestamp,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
    chatrooms: state.chatrooms.chatrooms,
    lastMessageSentProfileId: state.SET_LAST_SENDING_MESSAGE.ProfileId,
    lastMessageSentDisplayName: state.SET_LAST_SENDING_MESSAGE.DisplayName,
    lastMessageSentAvatarUrl: state.SET_LAST_SENDING_MESSAGE.AvatarUrl,
    lastMessageSent: state.SET_LAST_SENDING_MESSAGE.message,
  }
}

export default connect (mapStateToProps)(Timeline);