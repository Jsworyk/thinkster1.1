import React, { Component } from 'react'
import ReactNative, { Platform } from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
var fontColorContrast = require('font-color-contrast');
import CardView from 'react-native-cardview' 
import Carousel from 'react-native-snap-carousel';
import Modal from 'react-native-simple-modal';
import Helper from '../../lib/helper';

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  FlatList,
  TouchableHighlight,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} = ReactNative

var positions = [0,32,64];

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
class ProfileResponses extends Component{

  constructor(props){
    super(props);
    
    this.state = {
      dataSource:[],
      showLoading: true,
      showQuestions: false,
      openModal: false
    }
    this.renderItem = this.renderItem.bind(this);
    this.addProfileResponsesItemsBottom = this.addProfileResponsesItemsBottom.bind(this)
  }
  
  componentWillMount(){
    if (this.props.isLoggedin) {
        if (this.props.profileResponses == null){
          //this.getPollResponses();
        }else {
          this.setState({
          dataSource: this.props.profileResponses,
          showLoading: false
        })
      }
    }
  }

  componentDidMount(){
      this.getProfileResponses();
      
  }

  getProfileResponses(){
    const _this = this;
    this.props.getProfileResponses(this.props.applicationToken, this.props.currentUserProfile.ProfileId, '', 10, true, '').then(() => {
      if (this.props.profileResponses.length > 0 && (!this.props.profileResponses[0].action || this.props.profileResponses[0].action !== 'add')){
        this.props.profileResponses.unshift({action: 'add'})
        
      } 
      _this.setState({
        //dataSource: _this.props.profileResponses,
        showLoading: false
      })
    });
  }

  addProfileResponsesItemsBottom(){
    this.setState({
      refreshing: false
    })
    if (Platform.OS == 'ios'){
      if (!this.onEndReachedCalledDuringMomentum) {
      
        this.props.getProfileResponses(this.props.applicationToken, this.props.currentUserProfile.ProfileId, this.props.profileResponses[this.props.profileResponses.length - 1].Timestamp , 10, true, 'bottom').then(() => {
          this.setState({
            refreshing: false
          })
          this.onEndReachedCalledDuringMomentum = true;
        })
      } 
    }else {
      
        this.props.getProfileResponses(this.props.applicationToken, this.props.currentUserProfile.ProfileId, this.props.profileResponses[this.props.profileResponses.length - 1].Timestamp , 10, true, 'bottom').then(() => {
          this.setState({
            refreshing: false
          })
        })

    }
  }

extractText( str ){
  var ret = "";

  if ( /"/.test( str ) ){
    ret = str.match( /"(.*?)"/ )[1];
  } else {
    ret = str;
  }

  return ret;
}

truncate(string, stringLength){
  if (string.length > stringLength)
     return string.substring(0,stringLength)+'...';
  else
     return string;
}


renderItem(l, i){
  if (i != 0 && l.Text){
  
     var regex = /\[\[([upm]):([^:]+):([^\]]+)\]\]/g;
    var matches = l.Text.match(/\[\[([upm]):([^:]+):([^\]]+)\]\]/g);
    var PrasedTextArray = []
    var ids = []
    var inners = []    
    var pollName = null;
    for (var i in matches){
      PrasedTextArray.push(matches[i].replace(regex, function(string, first, second, third){
        if (first == 'p'){
          pollName = second;
        }
        ids.push(second)
        return (
          third
        )
      }))
    }

    var lastMatch = ''
    var lastMatchedIndex = 0;
    var finalIndex = 0;
    while ((m = regex.exec(l.Text)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      
      if (m.index != 0){

        var text_to_get = l.Text.substring(0, m.index )
          inners.push(text_to_get); 
          
      }
      lastMatch = m[0]
      lastMatchedIndex = m.index;
      finalIndex = regex.lastIndex;
    }
    if (finalIndex < l.Text.length){
      var text_to_get = l.Text.substring(finalIndex, l.Text.length)
      inners.push(text_to_get); 
    }
 
  
  return (
      <View key={i} style={styles.cardContainer}>
        { Platform.OS == 'ios' 
          ? 
            <View style={[styles.card, {} ]}>
            
              <View style={{backgroundColor: l.BackgroundColor ? l.BackgroundColor : '#FFD700', borderTopRightRadius: 15, borderTopLeftRadius: 15}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10}}>
                  <View style={{flexDirection: 'row',alignItems: 'center'}}>
                    {l.Image ? 
                      <Image source={{uri: l.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
                    :
                      <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                      </View>
                    }
                    <Text style={{color: fontColorContrast(l.BackgroundColor), marginLeft: 10, fontWeight: '600', fontSize: 12}}>{this.props.currentUserProfile.DisplayName}</Text>
                  </View>
                  <View style={{flexDirection: 'row',alignItems: 'center'}}>
                    <Text style={{color: fontColorContrast(l.BackgroundColor), textAlign: 'center', fontSize: 11}}>
                      {
                        Helper.durationOutput(l.Created)
                      }
                    </Text>
                  </View>
                </View>
                <View  
                  style={{
                    padding : 10,
                    flexWrap: 'wrap'
                  }}
                  >
                  <View style={{alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
                    <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.Question({pollName: pollName})}} underlayColor='#999'>
                      <Text style={{fontSize: 20, lineHeight: 20,fontWeight: 'bold', color: fontColorContrast(l.BackgroundColor), margin: 0}}>{this.truncate(PrasedTextArray.length > 1 ? PrasedTextArray[1] : PrasedTextArray[0], 200)}</Text>
                    </TouchableOpacity>
                    
                  </View>
                  <View style={{marginVertical: 15, alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
                    <Text style={{fontSize: 16, lineHeight: 20, margin: 0, color: fontColorContrast(l.BackgroundColor)}}>{this.truncate(this.extractText(inners[inners.length-1]), 200)}</Text>
                  </View>
                </View>
              
              </View>
          
              <View style={[styles.cardAction, {borderBottomRightRadius : 15, borderBottomLeftRadius : 15, backgroundColor: '#fff', }]}>
                
                <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',left: 10, top: -18, }}>
                  {l.TargetTypeCode == 'RE' ?
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.FriendResponses({pollName: pollName, item: l})}} underlayColor='#999'>
                      {
                        l.RecentThreeFollowing ?
                        l.RecentThreeFollowing.map((l,i) => (
                          l.AvatarThumbnailUrl == null ?
                        <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                          <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                        </View>
                        :
                        <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                          {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
                        </View>
                        ))
                        :
                        null
                      }
                      { l.FollowingResponseCount > 3 ?
                    
                        <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                          <Text style={{color: '#fff'}}>{l.FollowingResponseCount-3}+</Text>
                        </View>
                        :
                        null  
                      }
                    </TouchableOpacity>
                  :
                  l.TargetTypeCode == 'ME' ?
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => {}} underlayColor='#999'>
                      {
                        l.RecentThreeFollowing ?
                        l.RecentThreeFollowing.map((l,i) => (
                          l.AvatarThumbnailUrl == null ?
                        <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                          <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                        </View>
                        :
                        <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                          {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
                        </View>
                      ))
                      :
                      null
                    }
                    { l.FollowingResponseCount > 3 ?
          
                      <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                        <Text style={{color: '#fff'}}>{l.FollowingResponseCount-3}+</Text>
                      </View>
                  
                      :
                      null  
                      }
                  </TouchableOpacity>
                  :
                  null
                }
              
              </View>

              <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',right: 10, top: -18, backgroundColor: 'transparent' }}>
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({pollName: pollName})}} underlayColor='#999'>
                    <View style={[{ position: 'absolute', right: 32,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                      {/* <Icon style={{}} name='map' size={16} color='white' /> */}
                      <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.ResponseComments({pollName: pollName, item: l})}} underlayColor='#999'>
                <View style={[{ position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                  <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                </View>
                </TouchableOpacity>
                {
                //   l.Reacted == 0 ?
                // <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                // <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                //   <Icon style={{}} name='heart' size={16} color={'#fff'} />
                // </View>
                // </TouchableOpacity>
                // :
                // <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(l.TargetId, l.TargetTypeCode)  }} underlayColor='#999'>
                // <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                //   <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 5, paddingLeft: 3}}>{l.Emoticon}</Text>
                // </View>
                // </TouchableOpacity>
              }

              </View>
                {l.TargetTypeCode == 'RE' && l.FollowingResponseCount > 0 ? 
                  <Text style={{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5}}>
                  {l.RecentThreeFollowing.map(function(ele){
                    return ele.DisplayName;
                }).join(",") + (l.FollowingResponseCount > 3 ? (l.FollowingResponseCount == 4) ? ' and 1 other answered this question' : ' and ' + (l.FollowingResponseCount-3) +' others answered this question' : ' answered this question')}
                </Text>
                :
                null
                }
                {l.TargetTypeCode == 'ME' && l.FollowingReactionCount > 0 ? 

                  <Text style={{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5}}>
                  {
                    l.RecentThreeFollowing.map(function(ele){
                  return ele.DisplayName;
                  }).join(",") + (l.FollowingReactionCount > 3 ? (l.FollowingReactionCount == 4) ? ' and 1 other reacted to this image' : ' and ' + (l.FollowingReactionCount-3) +' others reacted to this image' : ' reacted to this image')}
                </Text>
                :
                null
                }
                {
                  l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0 ?
                  <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                  {''}
                  </Text>
                  :
                  null
                }
                {
                  l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ?
                  <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                  {''}
                  </Text>
                  :
                  null
                }
                
              </View>
            </View>
            :
            <CardView
            cardElevation={4}
            cardMaxElevation={4}
            cornerRadius={10}
            style={{backgroundColor: 'transparent',padding: 0,flex: 1 }}
            >
            {l.SecondaryImage ?
              <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.ImagePost({Selector: l.SecondaryImageSelector})}} underlayColor='#999'>
              <Image source={{uri: l.SecondaryImage}} resizeMode="cover"
              style={{ height: 280,borderRadius: 15, backgroundColor: 'transparent'}}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, marginRight: Platform.OS == 'ios' ? 0 : 25}}>
                  <View style={{flexDirection: 'row',alignItems: 'center'}}>
                  {l.Image ? 
                  <Image source={{uri: l.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
                  :
                  <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15,justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                  </View>
                  }
                  <TouchableOpacity rejectResponderTermination  onPress={() => {l.ProfileId == this.props.ProfileId ? Actions.Profile() : Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                  <Text style={[{color: '#fff', marginLeft: 10, fontWeight: 'bold',fontSize: 12}, styles.shadowDark]}>{l.DisplayName}</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'row',alignItems: 'center'}}>
                  <Text style={{color: '#ccc', textAlign: 'center', fontSize: 11}}>
                  { 
                    parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) == 0 ? parseInt( (Math.abs(new Date() - new Date(l.Created)) / 1000) / 60 ) + 'min' :  parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) / 30) + 'm' 
                  }
                  </Text>
                  </View>
                </View>
              </Image>
              </TouchableOpacity>
              :
                <View style={{paddingBottom: 120, flex: 1, backgroundColor: l.BackgroundColor ? l.BackgroundColor : '#FFD700', borderTopRightRadius: 0, borderTopLeftRadius: 0}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, marginRight: Platform.OS == 'ios' ? 0 : 25}}>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                {l.Image ? 
                  <Image source={{uri: l.Image}} style={{width: 30, height: 30, borderRadius: 15}}/>
                  :
                  <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                  </View>
                  }
                <TouchableOpacity rejectResponderTermination  onPress={() => {l.ProfileId == this.props.ProfileId ? Actions.Profile() : Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                    <Text style={{color: fontColorContrast(l.BackgroundColor), marginLeft: 10, fontWeight: 'bold', fontSize: 12}}>{l.DisplayName}</Text>
                </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                <Text style={{color: fontColorContrast(l.BackgroundColor), fontSize: 11}}>
                {
                parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(l.Created)) / 36e5) / 24) / 30) + 'm' 
              }
                </Text>
                </View>
                </View>
                <View  
                  style={{
                    padding: 10,
                    
                    flexWrap: 'wrap'
                  }}
                  >
                  <View style={{alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap', paddingRight: 10}}>
                    <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.Question({isPopup: false, pollName: pollName})}} underlayColor='#999'>
                            <Text style={{ fontSize: 20, lineHeight: Platform.OS == 'ios' ? 20 : 24, fontWeight: 'bold', color: fontColorContrast(l.BackgroundColor), margin: 0, }}>{this.truncate(PrasedTextArray.length > 1 ? PrasedTextArray[1] : PrasedTextArray[0], 200)}</Text>
                    </TouchableOpacity>
                    
                    </View>
                    <View style={{marginVertical: 15, paddingRight: 10, alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
                      <Text style={{fontSize: 16, lineHeight: 20, margin: 0, color: fontColorContrast(l.BackgroundColor)}}>{this.truncate(this.extractText(inners[inners.length-1]), 200)}</Text>
                    </View>
                </View>
                
                </View>
              }
                <View style={[styles.cardAction, { paddingTop: 20, borderBottomRightRadius : 15, borderBottomLeftRadius : 15, backgroundColor: l.TargetTypeCode == 'RE' ? 'transparent' : 'transparent', position : l.TargetTypeCode == 'ME' ? 'absolute' : 'absolute', bottom: 0, left: 0, right: 0 }]}>
                  <View style={{backgroundColor: l.TargetTypeCode == 'RE' ? '#fff' : 'transparent', paddingBottom: 15}}>
                  {l.TargetTypeCode == 'RE' && l.FollowingResponseCount > 0 ? 
                    <Text style={{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5}}>
                    {l.RecentThreeFollowing.map(function(ele){
                      return ele.DisplayName;
                  }).join(",") + (l.FollowingResponseCount > 3 ? (l.FollowingResponseCount == 4) ? ' and 1 other answered this question' : ' and ' + (l.FollowingResponseCount-3) +' others answered this question' : ' answered this question')}
                  </Text>
                  :
                  null
                  }
                  {l.TargetTypeCode == 'ME' && l.FollowingReactionCount > 0 ? 

                    <Text style={[{fontSize: 11,paddingLeft: 10, paddingTop: 40, paddingRight: 50, paddingBottom: 5, color: '#fff', fontWeight: 'bold'}, styles.shadowDark]}>
                    {
                      l.RecentThreeFollowing.map(function(ele){
                    return ele.DisplayName;
                    }).join(",") + (l.FollowingReactionCount > 3 ? (l.FollowingReactionCount == 4) ? ' and 1 other reacted to this image' : ' and ' + (l.FollowingReactionCount-3) +' others reacted to this image' : ' reacted to this image')}
                  </Text>
                  :
                  null
                  }
                  {
                    l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0 ?
                    <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                    {''}
                    </Text>
                    :
                    null
                  }
                  {
                    l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ?
                    <Text style={{backgroundColor: 'transparent', fontSize: 11,paddingLeft: 10, paddingTop: 20, paddingRight: 50, paddingBottom: 5}}>
                    {''}
                    </Text>
                    :
                    null
                  }
                  {/* <TouchableOpacity rejectResponderTermination  style={{padding: 5, position: 'absolute', top: (l.TargetTypeCode == 'ME' && l.FollowingReactionCount == 0) ? Platform.OS == 'ios' ? 30 : 10 : (l.TargetTypeCode == 'RE' && l.FollowingResponseCount == 0 ) ? 30 : 40, right: Platform.OS == 'ios' ? 15 : 25, bottom: Platform.OS == 'ios' ? 0 : 0, flex: 1,}}  onPress={() => {this.openReportModal(l.TargetTypeCode, l.TargetId)} } underlayColor='#999'>
                    <Icon style={[{backgroundColor: 'transparent'},styles.shadowDark]} name='ellipsis-v' size={18} color={l.TargetTypeCode == 'ME' ? '#fff' : '#444'} />
                  </TouchableOpacity> */}

                    </View>


                  <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 52, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    {l.TargetTypeCode == 'RE' ?
                      <TouchableOpacity rejectResponderTermination  style={{zIndex: 100, }} onPress={() => { Actions.Map({pollName: pollName})}} underlayColor='#999'>
                      <View style={[{right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                        <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                      </View>
                      </TouchableOpacity>
                      :
                      null
                    } 
                  </View>
                  <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 20, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}} onPress={() => { l.SecondaryImage ? Actions.ImageComments({Selector: l.SecondaryImageSelector, item: l}) : Actions.ResponseComments({pollName: pollName, item: l})}} underlayColor='#999'>
                    <View style={[{right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                      <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                    </View>
                    </TouchableOpacity>
                  </View>
                

                  <View style={{elevation: 4,zIndex: 600, position: 'absolute',left: 10, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                {
                    l.RecentThreeFollowing[0] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                    
                  </TouchableOpacity>
                  :
                  null
              }
                  </View>

                  <View style={{elevation: 4,zIndex: 700, position: 'absolute',left: 42, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    l.RecentThreeFollowing[1] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[1].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[1].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[1].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                    
                  </TouchableOpacity>
                  :
                  null
              }
                  </View>
                  <View style={{elevation: 4,zIndex: 800, position: 'absolute',left: 74, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    l.RecentThreeFollowing[2] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  
                      {
                    l.RecentThreeFollowing[2].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(l.RecentThreeFollowing[2].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: l.RecentThreeFollowing[2].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                  :
                  null
              }
                  </View>
                  {
                      l.FollowingResponseCount > 3 ?
                  <View style={{elevation: 4,zIndex: 900, position: 'absolute',left: 106, top: l.TargetTypeCode == 'RE' ? 0 : 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {l.TargetTypeCode == 'RE' ? Actions.FriendResponses({ pollName: pollName, item: l }) : Actions.Reactors({ TargetId: l.TargetId, TargetTypeCode: l.TargetTypeCode }) }} underlayColor='#999'>
                  

                        <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ color: '#fff' }}>{l.FollowingResponseCount - 3}+</Text>
                        </View>
                      
                    </TouchableOpacity>
                </View>
                :
                null
            }

                </View>
  
            </CardView>
              }
            
          </View>
      
    )
  }
  else {
    return (
      <View style={{margin: 10, }}>
        <View style={{backgroundColor: '#1EA6CC', justifyContent: 'center', alignItems: 'center', padding: 5, borderTopLeftRadius: 15, borderTopRightRadius: 15}}>
          <Text style={{color: '#fff', fontSize: 14}}>
            Profile Questions
          </Text>
        </View>
        <View style={{backgroundColor: '#134fa2', flexDirection: 'row', alignItems: 'center',  padding: 10,borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}>
          <View style={{flex: 1,margin: 10, }}>
          <Text style={{color: '#fff', fontSize: 14,flexWrap: 'wrap' }}>
            Find out what your friends think about you!
          </Text>
          </View>
          <TouchableOpacity style={{margin: 5, justifyContent: 'center', alignItems: 'center'}} onPress={() => { this.props.openModal() }} underlayColor='#999'>
          <View style={{padding: 5, backgroundColor: '#444', justifyContent: 'center', alignItems: 'center', borderRadius: 5}}>
            <Icon name='plus' size={18} color={'#fff'} />
          </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

 _keyExtractor = (item, index) => index

  render(){
    return (

      <View style={styles.container}>
      
      
       
      {this.state.showLoading
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#134fa2'/>
        </View>
        :
        this.state.dataSource.length == 0 ?
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10}}>
          <Text>No Recent Responses</Text>
        </View>
        :
        Platform.OS == 'ios' ?
        <FlatList
            refreshing={this.state.refreshing}
            onEndReached={this.addProfileResponsesItemsBottom}
            onEndReachedThreshold={0}
            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
            extraData={this.props.profileResponses}
            data={this.props.profileResponses}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        :
        <FlatList
            refreshing={this.state.refreshing}
            onEndReached={this.addProfileResponsesItemsBottom}
            extraData={this.props.profileResponses}
            data={this.props.profileResponses}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
      }
      
      
     </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: 50,
      },
    cardContainer:{
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#fff',
        margin: 10,
        marginBottom: 5
        
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.5,
        shadowRadius: 3,
        shadowOffset: {
          height: 2,
          width: 2,
        },
      },
      card:{
        flex: 1,
        backgroundColor: '#ffffff',
        borderRadius: 15,
        borderColor: '#ffffff',
        borderWidth: 0,
        shadowColor: 'rgba(0, 0, 0, 0.90)',
        shadowOpacity: 0.5,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      cardTitleContainer:{
        flex: 1,
        height: 170,
      },
      cardTitle:{
        position: 'absolute',
        top: 120,
        left: 26,
        backgroundColor: 'transparent',
        padding: 16,
        fontSize: 24,
        color: '#000000',
        fontWeight: 'bold',
      },
    
      cardContent:{
        padding:0,
        color: 'rgba(0, 0, 0, 0.54)',
      },
    
      cardAction:{
        borderStyle: 'solid',
        borderTopColor: 'rgba(0, 0, 0, 0.1)',
        //borderTopWidth: 1,
        //padding: 15,
        paddingBottom: Platform.OS == 'ios' ?  15 : 15,
      },
  subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  countText: {
    fontSize: 14,
    color: '#2b8abb',
    margin: 2,
  },
  username: {
    color: '#696868',
    fontWeight: 'bold',
    fontSize: 16,
  },
})

function mapStateToProps(state){

  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    ProfileId: state.CURRENT_USER.ProfileId,
    
    profileResponses: state.PROFILE_RESPONSES.profileResponses,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile
  }
}

export default connect (mapStateToProps)(ProfileResponses);
