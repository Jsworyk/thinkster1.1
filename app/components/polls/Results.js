import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import { Button, ButtonGroup, Divider} from 'react-native-elements'
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import ActionButton from 'react-native-action-button';
import FriendSuggest from './FriendSuggest'
import OverallChart from './OverallChart'
import Modal from 'react-native-simple-modal';
import Helper from '../../lib/helper'
var fontColorContrast = require('font-color-contrast');
import ModalWrapper from 'react-native-modal-wrapper';
import {
  VictoryAxis,
  VictoryChart,
  VictoryGroup,
  VictoryStack,
  VictoryCandlestick,
  VictoryErrorBar,
  VictoryBar,
  VictoryLine,
  VictoryArea,
  VictoryScatter,
  VictoryTooltip,
  VictoryZoomContainer,
  VictoryVoronoiContainer,
  VictorySelectionContainer,
  VictoryBrushContainer,
  VictoryCursorContainer,
  VictoryPie,
  VictoryLabel,
  VictoryLegend,
  createContainer
} from "victory-native";
import { VictoryTheme } from "victory-core";
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative
var positions = [0,32,64];
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
const Screen = Dimensions.get('window');

class Results extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      selected: 'FR',
      likeContainerVisible: false
    }
    //this.renderRow = this.renderRow.bind(this);
    //this.submitAnswer = this.submitAnswer.bind(this);
  }

  
  componentDidMount() {
    if (this.props.isLoggedin) {
      this.getResults();
    }
  }

  getResults(){
    const _this = this;
    
    this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
      
        _this.setState({ 
            question: poll.Question, 
            listViewData: poll.Choices, 
            showAnswers: true,
            poll: poll,
            backgroundColor: poll.Question.BackgroundColor
         });
    })
  }

  truncate(string, stringLength){
    if (string.length > stringLength)
       return string.substring(0,stringLength)+'...';
    else
       return string;
  }

  renderItem(l,i){
    
    return (
      
      <View key={i} style={[styles.shadow, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderRadius: 5, margin: 5,marginBottom: 10, padding: 12, width: Screen.width - 80}]}>
        <View style={{flex: 1,flexDirection: 'row', flexWrap: 'wrap' }}>
        <Text style={{textAlign: 'center', color: '#1EA6CC', fontSize: 16, fontWeight: '400' }}>{l.Choice}</Text>
        </View>
        <View style={{justifyContent: 'flex-end', flexDirection: 'row', flexWrap: 'wrap', marginLeft: 20}}>
        <Text style={{ color: '#1EA6CC', fontSize: 14, fontWeight: 'bold',}}>{Math.round((l.SelectionCount* 100) / this.state.question.ResponseCount)}%</Text>
        </View>
        <View style={{opacity: 0.4, borderRadius: 5,margin: 0,position: 'absolute', top: 0, left: 0, bottom: 0, backgroundColor: '#1EA6CC', width: Math.round((((l.SelectionCount* 100) / this.state.question.ResponseCount) / 100) * (Screen.width - 80))}}></View>
      </View>
      
    )
  }



open(id, type) {
  this.setState({
      likeContainerVisible: true,
      reactingTo: id,
      type: type
  })
}

close() {
  this.setState({open: false}, () => {
       Animated.timing(this._scaleAnimation, {
        duration: 50,
        toValue: 0
      }).start();
  })
}

getLikeContainerStyle() {
  return {
          transform: [{scaleY: this._scaleAnimation}],
          overflow: this.state.open ? 'visible': 'hidden',
        };
}

React(l){

  this.setState({
      likeContainerVisible: false
  })
  this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
    // change locally
    var poll = this.state.poll;
    poll.Question.Reacted = 1;
    poll.Question.Emoticon = l.Emoji;
    this.setState({
      poll: poll
    })
  });
}

  render() {
    
    return (
      <View style={[styles.container, {backgroundColor: this.props.isPopup ? 'transparent' : '#e4e4e4', marginBottom: this.props.isPopup ? 0 : 0}]}>
        {this.state.showAnswers
        ?
       
        <View style={[this.props.isPopup ? {} : styles.card, {margin: this.props.isPopup ? 0 : 15, marginTop: this.props.isPopup ? 0 : Platform.OS === 'ios' ? 70 : 54}]}>
        
            <View style={{flex: 1}}>
            <View style={{paddingBottom: 20, backgroundColor: 'transparent',}} >
            <View style={{backgroundColor: this.state.backgroundColor ? this.state.backgroundColor : 'rgba(30, 166,204, 100)', padding: 10,borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}>
              <View style={{flexDirection: 'row', margin: 5, justifyContent: 'flex-start', alignItems: 'center'}}>
                {/* <View style={{margin: 2,height: 20, width: 20, borderRadius: 10, backgroundColor: this.state.question.BackgroundColor ? this.state.question.BackgroundColor : 'rgba(30, 166,204, 100)'}}></View> */}
                <Text style={{margin: 2, fontSize: 12, color: fontColorContrast(this.state.backgroundColor)}}>{this.state.question.Category}</Text>
              </View>
              <View style={{margin: 5, marginBottom: 20}}>
              <Text style={{ fontSize: 20, color: fontColorContrast(this.state.backgroundColor), fontWeight: '500', }}>
                {
                  //this.truncate(this.state.question.Question, 60)
                  this.state.question.Question
                }
              </Text>
              </View>

              {/* <View style={{zIndex: 300, flexDirection: 'row', position: 'absolute',left: 10, bottom: 18, }}>
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.FriendResponses({pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                
                {
                  this.state.poll.RecentThreeFollowing ?
                  this.state.poll.RecentThreeFollowing.map((l,i) => (
                    l.AvatarThumbnailUrl == null ?
                  <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,padding: 3,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
                  </View>
                  :
                  <View key={i} style={[{ position: 'absolute', left: positions[i],margin: 2, backgroundColor: 'transparent', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 40, height: 40, borderRadius: 20}}/>}
                  </View>
                ))
                :
                null
              }
              { 
                this.state.poll.FollowingResponseCount > 3 ?
              
                  <View style={[{ position: 'absolute', left: 96,margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                    <Text style={{color: '#fff'}}>{this.state.poll.FollowingResponseCount-3}+</Text>
                  </View>
              :
              null  
              }
            </TouchableOpacity>
              
                </View> */}

                {/* <View style={{flexDirection: 'row', position: 'absolute',right: 10, bottom: 18, }}>
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({pollName: this.state.poll.Question.UrlSegment,})}} underlayColor='#999'>
                <View style={[{ position: 'absolute', right: 64,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadowDark]}>
                  
                  <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                </View>
                </TouchableOpacity>
                <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({ pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                <View style={[{ position: 'absolute', right: 32,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadowDark]}>
                 
                  <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                </View>
                </TouchableOpacity>
                {this.state.poll.Question.Reacted == 0 ?
          <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
          <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
            
            <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
          </View>
          </TouchableOpacity>
          :
          <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
          <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
            <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
          </View>
          </TouchableOpacity>
        }
            </View> */}

            </View>
          
            <View style={{zIndex: 600, position: 'absolute',left: 10,bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                {
                    this.state.poll.RecentThreeFollowing[0] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View> 
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>

                  <View style={{zIndex: 700, position: 'absolute',left: 42, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[1] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[1].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  <View style={{zIndex: 800, position: 'absolute',left: 74, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[2] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                  
                      {
                    this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[2].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  {
                      this.state.poll.FollowingResponseCount > 3 ?
                  <View style={{zIndex: 900, position: 'absolute',left: 106, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999'>
                   

                        <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ color: '#fff' }}>{this.state.poll.FollowingResponseCount - 3}+</Text>
                        </View>
                       
                    </TouchableOpacity>
                </View>
                 :
                 null
             }


                          <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 74, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                              <View style={[{elevation: 4, right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                                <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                              </View>
                              </TouchableOpacity>
                              
                            </View>
                            <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 42, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                            </View>
                            </TouchableOpacity>
                            </View>
                            <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 10, bottom: 0, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            {this.state.poll.Question.Reacted == 0 ?
                            
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                            </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
                            </View>
                            </TouchableOpacity>
                            
                          }
                          </View>

            </View>
            <ScrollView style={{marginTop: 30}}>
        <View style={{margin: 10, marginTop: 30, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {
            this.state.listViewData.map((l,i) => (
              this.renderItem(l,i)
            ))
          }
        </View>
        </ScrollView>

        
      </View>
          
          
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
        </View>
        : 
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
      </View>
       
        }

<Modal
          open={this.state.showResults}
          offset={0}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({
            showResults: false
          },
          () => {
            this.getNextQuestion();
          }
          )
          }
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center',
            paddingTop: Platform.OS === 'ios' ? 75 : 65,
          }}
          modalStyle={{
            flex: 1,
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
         
         
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{alignSelf: 'flex-end', margin: 5, marginHorizontal: 10, borderBottomColor: '#444', borderBottomWidth: 1}}>
          <TouchableOpacity rejectResponderTermination   onPress={() => {this.setState({showResults: false})}}>
            <Text>Next Question</Text>
          </TouchableOpacity>
        </View>
      <ScrollView>
      <VictoryPie
        style={{
          labels: {
            fill: "#444",
            stroke: "none",
            fontSize: 14,
            fontWeight: "normal"
          }
        }}
        height={300}
        width={Screen.width-50}
        labels={(d) => d.y == 0 ? '' : d.y}
        labelComponent={<VictoryLabel angle={45}/>}
        data={Helper.parseChoicesArrayToData(this.state.listViewData)}
        
        colorScale={Helper.extractColorsFromChoices(this.state.listViewData)}
         events={[{
    target: "data",
    eventHandlers: {
      onPress: () => {
        return [
          {
            target: "data",
            mutation: (props) => {
              
            }
          }
        ];
      }
    }
  }]}
      />
      <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 1}}>
      {this.state.listViewData.map((l,i) => (
              <View key={i} style={{flexDirection: 'row', margin: 2, alignItems: 'center'}}>    
                <View style={{backgroundColor: l.ChartColour ? l.ChartColour : '#999', width: 16, height: 16, borderRadius: 8, margin: 3}}>
                </View>
                <Text style={{color: '#444', fontWeight: 'bold', backgroundColor: 'transparent'}}>{l.Choice}</Text>
              </View>
              ))}
  </View>
  </ScrollView>
  
      </View>
      
      
    
  
        </Modal>
        <ModalWrapper
          style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
          visible={this.state.likeContainerVisible}
          onRequestClose={() => this.setState({ likeContainerVisible: false })}
          shouldCloseOnOverlayPress={true}
          >
                      <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                          {
                          this.props.reactionTypes.map((l, i) => (
                              <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                  <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                              </TouchableOpacity>
                          ))
                        }

                      </View>
      </ModalWrapper>
        </View>
       );
  }

  /* <ActionButton
              buttonText="Skip"
              buttonTextStyle={{fontSize: 13}}
              buttonColor="#49abde"
              onPress={() => { //this.getNextQuestion()
              Actions.Comments();
              }}
          /> */
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      card:{
        flex: 1,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#ffffff',
        borderRadius: 15,
        borderColor: '#ffffff',
        borderWidth: 0,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
          height: 1,
          width: 2,
        },
      },
      normal: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)', flexGrow:1, justifyContent: 'center',padding: 5,paddingTop: 8, paddingBottom: 8,paddingLeft: 15, paddingRight: 15, alignItems: 'center'},
      selected: {flexGrow:1,backgroundColor: '#1EA6CC',padding: 5,paddingTop: 8,paddingBottom: 8, paddingLeft: 15, paddingRight: 15,justifyContent: 'center', alignItems: 'center'},
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    poll: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
  }
}

export default connect(mapStateToProps)(Results);
