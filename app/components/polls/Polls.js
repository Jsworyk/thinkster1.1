import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
//import NavBar, { NavButton, NavButtonText, NavTitle } from 'react-native-nav'
import Icon from 'react-native-vector-icons/FontAwesome';
//import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';


const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Platform,
  TouchableHighlight,
  StyleSheet,

} = ReactNative

import NewQuestions from './NewQuestions'
import RecentlyAnswered from './RecentlyAnswered'

class Polls extends Component{


  constructor(props){
    super(props);
  }

  _renderTitleIndicator() {
       return <PagerTitleIndicator
                style={styles.indicatorContainer}
                itemTextStyle={styles.indicatorText}
                selectedItemTextStyle={styles.indicatorSelectedText}
                selectedBorderStyle={styles.selectedBorderStyle}
                titles={['New', 'Recently Answered']} />;
   }

 //   <IndicatorViewPager
 //     style={{flex:1, backgroundColor:'#f6f6f6', flexDirection: 'column-reverse'}}
 //     indicator={this._renderTitleIndicator()}>
 //
 //     <View style={{backgroundColor:'#fff'}}>
 //
 //     </View>
 //
 //     <View style={{backgroundColor:'#fff'}}>
 //           <RecentlyAnswered {...this.props}/>
 //     </View>
 // </IndicatorViewPager>

  render(){
    return (
      <View style={{marginBottom: 50, flex: 1}}>
        <Image source={require('../../assets/pollbackground.png')} style={styles.imageContainer} ></Image>

    <NewQuestions {...this.props}/>
     </View>
    )
  }
}


const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'flex-start',
  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  image: {
    width: 30,
  },
});

const styles = StyleSheet.create({

  indicatorContainer: {
       backgroundColor: '#2b8abb',
       height: 40
   },
   indicatorText: {
       fontSize: 14,
       color: '#fff'
   },
   indicatorSelectedText: {
       fontSize: 14,
       color: '#fff',

   },
   selectedBorderStyle: {
       height: 3,
       backgroundColor: '#fff'
   },
   imageContainer: {
    width: undefined,
    height: Platform.OS === 'ios'? 64 : 54,
    backgroundColor:'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
})


function mapStateToProps(state){

  return {

  }
}

export default connect (mapStateToProps)(Polls);
