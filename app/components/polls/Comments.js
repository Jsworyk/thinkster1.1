import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import FadeinView from '../core/FadeinView';
import ModalWrapper from 'react-native-modal-wrapper';
import SocketIOClient from 'socket.io-client';


const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Platform,
  FlatList,
  Keyboard,
  TouchableOpacity,
  TouchableHighlight,
  KeyboardAvoidingView,
  StyleSheet,
  Animated,
  TouchableWithoutFeedback
} = ReactNative

const loadingReplies = [];
const openLikeContainer = [];
const repliesLoaded = [];
const replies = [];
const replyingTo = [];
const reactions = [];
 
class UselessTextInput extends Component {
  
  focus() {
        this.refs.textInput.focus();
    }
    
  render() {
    return (
      <TextInput
        {...this.props}
        ref={'textInput'}
      />
    );
  }
}

class Comments extends Component{
    _keyExtractor = (item, index) => index;
  constructor(props){
    super(props);

    this.state = {
        comments: [],
        showLoader: true,
        loadingReplies: [],
        openLikeContainer: [],
        repliesLoaded: [],
        replies: [],
        replyingTo: null,
        reactions: reactions,
        isReplying: false,
        replyingToName: 'Poll',
        open: false,
        likeContainerVisible: false
    }
    
  }

  componentWillUnmount(){
    this.socket.close()
  }

componentWillMount(){

    this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false, transports: ['websocket'] });
    this.socket.on('connect', () => {
      this.socket.emit('authenticate', this.props.applicationToken, [this.props.targetTypeCode+this.props.targetId]);
    });
    this.socket.on('Comment', (data) => {
        console.log(data)
        this.appendComment(data)
    });

    this.socket.on('Reaction', (data) => {
        
        this.props.comments.map((l,i) => {
            if (l.CommentId == data.TargetId) {

              this.props.addCommentReaction(i,data.Delta)
                // this.setState({
                //     comments: this.props.comments
                // })
            
            } 
          })
          
        //this.appendReaction(data)
    });

   const _this = this;
    if (this.props.isLoggedin) {
        this.props.setComments([]);
        this.props.setTotal(0);
        this.props.getComments(this.props.applicationToken, this.props.targetTypeCode, this.props.targetId, '','', 10).then(() => {
            _this.setState({
                showLoader: false,
                comments: this.props.comments
            }) 
        })
    }
}

setRepliesLoaded(i, parent){

    repliesLoaded[i] = true;
    loadingReplies[i] = true;
    _this = this;
    this.setState({
        repliesLoaded: repliesLoaded,
        loadingReplies: loadingReplies,
        replyingTo: []
    })
    
    this.props.getComments(this.props.applicationToken,this.props.targetTypeCode, this.props.targetId, i, '', 10).then(() => {
            replies[i] = this.props.replies;
            loadingReplies[i] = false;
           _this.setState({
                repliesLoaded: repliesLoaded,
                loadingReplies: loadingReplies,
                replies: replies
            })
        })
}

setReplyTo(CommentId, name){
    
    this.setState({
        replyingTo: CommentId,
        isReplying: true,
        replyingToName: name,
    }
    )
    
    this.commentBox.focus();

}

unsetReplying(){
    this.setState({
        replyingTo: null,
        isReplying: false,
        replyingToName: 'Poll',
    })
}

appendReaction(reaction){
    reactions[this.state.reactingTo] = true;
    this.setState({
        reactions: reactions
    })
}

appendComment(comment){
    if (comment.TargetId == this.props.targetId){
    if (comment.ReplyToCommentId == null){
        //var comments = [...this.state.comments, comment];
        // this.setState({
        //     comments: comments,
        // })
        this.props.addComment(comment);

    }else{
        if (replies[comment.ReplyToCommentId] == null){
            replies[comment.ReplyToCommentId] = [comment];
        }else{
            replies[comment.ReplyToCommentId].push(comment);
        }
        this.setState({
            replies: replies,
        })
    }
}
    
}

comment(){
    loadingReplies[this.state.replyingTo] = true;
    this.setState({
        loadingReplies: loadingReplies
    })
    Keyboard.dismiss();
    this.props.comment(this.props.applicationToken,this.props.targetTypeCode, this.props.targetId, this.state.replyingTo, this.state.comment).then(() => {
        loadingReplies[this.state.replyingTo] = false;
        this.setState({
            comment: '',
            loadingReplies: loadingReplies,
            isReplying: false,
            replyingTo: null,
            replyingToName: 'Poll'
        })
    })
}

open(commentId) {
    this.setState({
        likeContainerVisible: true,
        reactingTo: commentId
    })
}

close() {
    this.setState({open: false}, () => {
         Animated.timing(this._scaleAnimation, {
          duration: 50,
          toValue: 0
        }).start();
    })
}

getLikeContainerStyle() {
    return {
            transform: [{scaleY: this._scaleAnimation}],
            overflow: this.state.open ? 'visible': 'hidden',
          };
}

React(l){
    this.setState({
        likeContainerVisible: false
    })
    this.props.React(this.props.applicationToken, 'CO', this.state.reactingTo, l.ReactionTypeCode).then(() => {

    });
}

renderComment(comment){
    
  return (
    <TouchableWithoutFeedback key={comment.CommentId} onPress={() => { }}>
      <View style={{padding: 10, margin: 0, flex: 1, backgroundColor: '#2AB5D6',borderBottomColor: 'rgba(0,0,0,0.1)', borderBottomWidth: comment.ReplyToCommentId == null ? 0.5 : 0}}>
              <View key={comment.CommentId} style={{flexDirection: 'row'}}>             
                    <View ref='userImage'>
                        {comment.AvatarUrl != null ?
                        <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: comment.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                        <Image
                            style={{ borderRadius: 15, width: 30, height: 30 }}
                            source={{ uri: comment.AvatarUrl }}
                            />
                        </TouchableOpacity>
                            :
                            comment.AvatarMicrothumbUrl != null ?
                            <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: comment.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                            <Image
                            style={{ borderRadius: 15, width: 30, height: 30 }}
                            source={{ uri: comment.AvatarMicrothumbUrl }}
                            />
                            </TouchableOpacity>
                            :
                            <View style={{backgroundColor: '#ccc', width: 30, height: 30, borderRadius: 15}}>
                            </View> 
                        }
                    </View>
                    <View ref='commentMain' style={{flex: 1, justifyContent: 'center', marginLeft: 10, marginRight: 0}}>
                        <View style={{backgroundColor: 'transparent', borderRadius: 5, padding: 0, borderWidth: 0, borderColor: this.state.replyingTo == comment.CommentId ? '#49abde' : '#fff'}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: comment.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                        <Text style={{fontSize: 12, color: '#fff', fontWeight: 'normal'}}>
                            {comment.DisplayName}
                        </Text>
                        </TouchableOpacity>
                        <Text style={{fontSize: 12, color: '#e4e4e4', fontWeight: 'normal'}}>
                            {
                                parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) == 0 ? parseInt( (Math.abs(new Date() - new Date(comment.Created)) / 1000) / 60 ) + 'min' :  parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(comment.Created)) / 36e5) / 24) / 30) + 'm'
                            }
                        </Text>
                        </View>
                        <View>
                        <View style={{ flexDirection: 'row', marginRight: 5 }}>
                        <Text style={{fontSize: 12, flexWrap: "wrap", color: '#fff'}}>
                            {comment.CommentText}
                        </Text>
                        </View>
                        </View>                        
                        </View>
                        <View ref='commentButtons'>
                            <View style={{flex: 1, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{flexDirection: 'row',marginRight: 5}}>
                                <TouchableOpacity onPress={() => { this.open(comment.CommentId) }} underlayColor='#999'>
                                    <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                        <Icon style={{ textAlign: 'center', opacity: 0.8, marginTop: 1 }} name='heart' size={12} color={comment.Reacted == 1 ? 'red' : 'rgba(46,63,95,0.6)'} />
                                        <Text style={{ textAlign: 'center', fontSize: 11, marginLeft: 5, color: '#fff' }}>
                                              {comment.ReactionCount ? comment.ReactionCount : 0}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.setReplyTo(comment.CommentId, comment.DisplayName) }} underlayColor='#999'>
                                    <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginLeft: 8 }}>
                                        <Icon style={{ textAlign: 'center', opacity: 0.8, marginTop: 1 }} name='comment' size={12} color='rgba(0,0,0,0.20)' />
                                        <Text style={{ textAlign: 'center', fontSize: 11, marginLeft: 5, color: '#fff' }}>
                                              Reply
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                </View>
                                {
                            comment.CommentCount > 0 ?
                            <View style={{flexDirection: 'row',flex:1, justifyContent: 'flex-end', alignItems: 'center'}}>
                                { !this.state.repliesLoaded[comment.CommentId] ?
                                <TouchableOpacity onPress={() => { this.setRepliesLoaded(comment.CommentId, comment.ReplyToCommentId) }} underlayColor='#999'>
                                    <View style={{backgroundColor: '#23ADD0', padding: 6, borderRadius: 10}}>
                                        <Text style={{fontSize: 11, color: '#fff',}}>
                                            View Replies
                                        </Text> 
                                    </View>
                                </TouchableOpacity>
                                : null
                                }
                                {
                                this.state.loadingReplies[comment.CommentId] ?
                                    <View style={{padding: 5, justifyContent: 'center', alignItems: 'center'}}>
                                        <SpinKit isVisible={true} size={22} type='ThreeBounce' color='#fff' />
                                    </View>
                                 : 
                                 null
                                }
                            </View>
                            :
                            null
                        }
                                {/* <View ref='reactionCountContainer' style={{marginLeft: 10}}>
                                    <Text style={{fontSize: 12, flexWrap: "wrap", color: '#ccc', fontWeight: 'bold',margin: 5}}>{this.state.reactions[comment.CommentId] ?  '1+' : null }</Text>
                                </View> */}
                            </View>
                        </View>
                         
                    </View>
              </View>
               {
                this.state.replies[comment.CommentId] != null ?
                    this.state.replies[comment.CommentId].map((l, i) => (
                        <View key={l.CommentId} style={{marginLeft: 20}}>
                            {this.renderComment(l)}
                        </View>
                    ))
                    : null
                }
        </View>
        </TouchableWithoutFeedback>
      )
}

  render(){
    return (
        
      <View style={{flex: 1,}}>
          
        
        {this.state.showLoader == false ?
            
        <View style={styles.commentsContainer}>
            {/*<FlatList
                data={this.props.comments}
                keyExtractor={this._keyExtractor}
                renderItem={({item}) => this.renderComment(item)}
                />*/}
        <ScrollView>
                {
                    this.props.comments.map((l, i) => (
                        this.renderComment(l)
                    ))
                    
                }
        </ScrollView>
        <ModalWrapper
            style={{ borderRadius: 15, padding: 20,flexDirection: 'row' }}
            visible={this.state.likeContainerVisible}
            onRequestClose={() => this.setState({ likeContainerVisible: false })}
            shouldCloseOnOverlayPress={true}
            >
                        <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                            {this.state.showLoader == false ?
                            this.props.reactionTypes.map((l, i) => (
                                <TouchableOpacity key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                    <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                                </TouchableOpacity>
                            ))
                            :
                            null
                            }

                        </View>
        </ModalWrapper>
        </View>
         :
             <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                <SpinKit isVisible={true} size={30} type='WanderingCubes' color='#fff' />
            </View>
        }

        
            <View style={[{ backgroundColor: 'transparent', borderTopWidth: 0, borderTopColor: '#49abde', marginTop: 0, justifyContent: 'flex-end'}]}>
                
            <View style={{height: 35, flexDirection: 'row' }} >
                
                <View style={{
                    flex: 1,
                    marginTop: 0,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 0,
                    padding: 0,
                    borderColor: '#f6f6f6',
                    borderWidth: 0,
                    borderBottomWidth: 1,
                    }}
                >
                <UselessTextInput
                    style={{color: '#fff', flex: 1,fontSize: 14 }}
                    onChangeText={comment => this.setState({ comment })}
                    value={this.state.comment}
                    placeholder='Type comment...'
                    placeholderTextColor='rgba(255,255,255,0.6)'
                    onSubmitEditing={() => {}}
                    maxLength = {1000}
                    underlineColorAndroid='transparent'
                    multiline={false}
                    editable = {true}
                    onBlur={() => { this.unsetReplying() }}
                    ref={commentBox => this.commentBox = commentBox}
                />
                </View>
                <View style={{justifyContent: 'flex-end', alignItems: 'center',marginRight: 10, }}>
                     <TouchableOpacity onPress={() => { this.comment() }} underlayColor='#999'>
                        <Text style={{color: this.state.comment ? '#fff' : 'rgba(255,255,255,0.4)', fontSize: 14, fontWeight: 'bold'}}>Send</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {/* <FadeinView style={{justifyContent: 'flex-end', padding: 2, backgroundColor: 'transparent', marginLeft: 10, marginRight: 10, marginTop: 5}}>
                    <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 12}}>Replying to {this.state.replyingToName}</Text>
                </FadeinView> */}
            </View>
            
            
     </View>
     
    )
  }
}

const styles = StyleSheet.create({
   
  indicatorContainer: {
       backgroundColor: '#2b8abb',
       height: 40
   },
   indicatorText: {
       fontSize: 14,
       color: '#fff'
   },
   indicatorSelectedText: {
       fontSize: 14,
       color: '#fff',
   },
   selectedBorderStyle: {
       height: 3,
       backgroundColor: '#fff'
   },
   imageContainer: {
    width: undefined,
    height: Platform.OS === 'ios'? 64 : 54,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0f6aa6'
  },
  commentsContainer: {
      flex: 1,
  },
  likeContainer: {
    position: 'absolute',
    left: -10,
    top: -60,
    padding: 10,
    flex: 1,
    backgroundColor: '#fff',
    borderColor: '#49abde',
    borderWidth: 2,
    flexDirection: 'row',
    borderRadius: 20,
  },
  shadow:{
    shadowColor: '#444',
    shadowOpacity: 0.9,
    shadowRadius: 6,
    shadowOffset: {
      height: 4,
      width: 2,
    },
  },
})

function mapStateToProps(state){
  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    ProfileId: state.CURRENT_USER.ProfileId,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    comments: state.comments.comments,
    replies: state.replies.replies,
    reactionTypes: state.REACTION_TYPES.reactionTypes
  }
}

export default connect (mapStateToProps)(Comments);
