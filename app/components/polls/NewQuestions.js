import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import { List, Card, Button, ListItem } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';
var Spinner = require('react-native-spinkit');
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'

const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  TouchableHighlight,
  StyleSheet,

} = ReactNative


const list = [
  {
    question: 'What is the biggest threat to humanity?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '459',
    answers: [
    'Money',
    'Fame',
    'Power',
    'Education',
    'Society',
    ]
  },
  {
    question: 'What is your favorite color?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '12k',
    answers: [
    'Black',
    'Blue',
    'Red',
    'Green',
    'Orange',
    'Pink',
    'Brown',
    'Yellow',
    ]
  },
  {
    question: 'Who should be the next President of United Stated?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '34',
    answers: [
    'John Doe',
    'Joe Phillip',
    'Hassan Zia',
    'Sultan Khan',
    ]
  },
  {
    question: 'Where do you want to go in this summer vacation?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '38k'
  },
  {
    question: 'What is the biggest threat to humanity?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '459'
  },
  {
    question: 'What is your favorite color?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '12k'
  },
  {
    question: 'Who should be the next President of United Stated?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '34'
  },
  {
    question: 'Where do you want to go in this summer vacation?',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '38k'
  },


  ]

class NewQuestions extends Component{


  constructor(props){
    super(props);
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });
    this.state = {
      ds:list,
      dataSource:ds,
      showLoading: true,
      showQuestions: false
    }
    this.renderRow = this.renderRow.bind(this);
  }

  componentDidMount(){

              this.setState({
                dataSource:this.state.dataSource.cloneWithRows(this.state.ds),
                showQuestions: true,
                showLoading: false
              })

  }


  onQuestionPress(question,answers){

    this.props.gotoQuestion(question,answers);
    this.setTimeout(
      () => {
          Actions.Answer();
      },
      300
    )

  }

  renderRow (rowData, sectionID) {
   return (

               <ListItem
                containerStyle={{padding: 0, margin: 0}}
                 key={sectionID}
                 title={rowData.question}
                 titleStyle={styles.username}
                 onPress={() => this.onQuestionPress(rowData.question,rowData.answers)}
                 subtitle={
                   <View style={styles.subtitleView}>

                   <View style={{flexDirection: 'row',paddingLeft: 5, alignItems: 'flex-start', justifyContent: 'flex-start'}}>
                     <Icon style={{opacity: 1,margin: 2}} name='comment-o' size={14} color='#2b8abb' />
                     <Text style={[styles.countText, {}]}>
                       {rowData.subtitle}
                     </Text>
                   </View>

                   </View>

                 }
                 hideChevron={true}
               />

   )
 }

  render(){
    return (
      <View style={{ flex: 1, marginBottom: 50}}>
      {this.state.showLoading
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
        <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#2b8abb'/>
        </View>
        :
      <Card containerStyle={{margin: 5, marginBottom: 50}}>
        <ListView
          renderRow={this.renderRow}
          dataSource={this.state.dataSource}
        />
      </Card>
      }

     </View>
    )
  }
}

const styles = StyleSheet.create({

  subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  countText: {
    fontSize: 12,
    color: '#2b8abb',
    margin: 2,
    opacity: 0.8
  },
  username: {
    color: '#696868',
    fontWeight: 'bold',
    fontSize: 14,
  },
})

reactMixin(NewQuestions.prototype, TimerMixin)
function mapStateToProps(state){

  return {

  }
}

export default connect (mapStateToProps)(NewQuestions);
