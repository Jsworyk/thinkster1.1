import { StyleSheet } from 'react-native';
import { Colors, DefaultStylesObject } from '../../lib/styles';


const styles = StyleSheet.create({
    ...DefaultStylesObject,
    landingContainer: {
        flex: 1,
        flexDirection: 'column',
    },
    rectangleOutline: {
        width:63,
        height: 101,
        borderWidth: 1,
        borderColor: Colors.white,
        alignItems:'center',
        justifyContent: 'center',
    },
    landingImageContainer: {
        flex:1, 
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    discoverImage : {
        width:45,
        height: 55,
    },
    opinionImage: {
        height: 100,
        width:100,
    },
    landingText: {
        alignSelf: 'center',
        fontSize: 25,
        width:300,
        color: Colors.white,
        textAlign:'center',
    }
});

export default styles;