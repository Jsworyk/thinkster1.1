import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import ReactNative from 'react-native'
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import reducer from './app/reducers';
import AppContainer from './app/containers/AppContainer'

global.XMLHttpRequest = global.originalXMLHttpRequest ?
  global.originalXMLHttpRequest :
  global.XMLHttpRequest;
global.FormData = global.originalFormData ?
  global.originalFormData :
  global.FormData;
  
const {
  View,
  Text,
  AsyncStorage
  
} = ReactNative

// making sure logger only works in dev mode.
const loggerMiddleware = createLogger({predicate : (getState, action) => __DEV__ });

//configuring store. redux pattern
function configureStore(initialState){
  const enhancer = compose(
     autoRehydrate(),
    applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
);
return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

import {
  AppRegistry
} from 'react-native';

class App extends Component{
  constructor() {
    super()
    this.state = { rehydrated: false }
  }

  componentWillMount(){
    persistStore(store, {storage: AsyncStorage}, () => {
      this.setState({ rehydrated: true })
    })
  }

  render() {
    if(!this.state.rehydrated){
      return <View></View>
    }
    return (
       <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }

}

AppRegistry.registerComponent('Thinkster', () => App);
