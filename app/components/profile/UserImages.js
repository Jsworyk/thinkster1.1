import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import PhotoGrid from 'react-native-photo-grid';
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  DeviceEventEmitter,
  NativeModules,
  StyleSheet,

} = ReactNative

var RNUploader = NativeModules.RNUploader;

class UserImages extends Component{

  constructor() {
    super();
    this.state = { showImages: false,
    avatarSource: null,
    videoSource: null,
    };
  this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {

   // Build an array of 60 photos.
   this.props.getProfileImages(this.props.applicationToken, this.props.ProfileId, '', 30, false).then((profileImages) => {
    this.setState({
      showImages: true,
      profileImages: profileImages
    })
   })
   

   // upload progress
	DeviceEventEmitter.addListener('RNUploaderProgress', (data)=>{
	  let bytesWritten = data.totalBytesWritten;
	  let bytesTotal   = data.totalBytesExpectedToWrite;
	  let progress     = data.progress;
	  
	  console.log( "upload progress: " + progress + "%");
	});
 }

 postImage(MediaId,Name,ExposureTypeCode){
   this.props.postImage(this.props.applicationToken,MediaId, Name, ExposureTypeCode).then(() => {

   })
 }

uploadImage(){
  
  RNFetchBlob.fetch('POST', 'https://thinkster.ca/uploads', {
    'Content-Type' : 'multipart/form-data',
  }, [
    { name : 'files[]', filename: 'avatar.jpeg', data: RNFetchBlob.wrap(this.state.avatarSource)},
    { name : 'token', data: this.props.imageUploadToken},
    
  ]).then((resp) => {
    console.log(resp.json())
  }).catch((err) => {
    console.log(err);
  })
}

 selectPhotoTapped() {
   const options = {
     quality: 1.0,
     maxWidth: 500,
     maxHeight: 500,
     storageOptions: {
       skipBackup: true
     }
   };

   ImagePicker.showImagePicker(options, (response) => {
     if (response.didCancel) {
       
     }
     else if (response.error) {
       
     }
     else if (response.customButton) {
       
     }
     else {
       console.log(response)
       let source = { uri: response.uri };
       this.setState({
         avatarSource: response.uri.split("file://").pop(),
         data: response.data
       });
       this.uploadImage();
     }
   });
 }
 
  render(){
    return (
      <View style={{flex: 1, marginBottom: 0, backgroundColor: '#fff'}}>
          { this.state.showImages && this.state.profileImages.length > 0 ? 
          <PhotoGrid
            data = { this.state.profileImages }
            itemsPerRow = { 3 }
            itemMargin = { 2 }
            renderItem = { this.renderItem }
          />
          :
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 10}}>
          <Text>No Images Shared</Text>
          </View>
          }
          
          </View>
    )
  }

   renderItem(item, itemSize) {
     return(
       <TouchableHighlight
         key = { item.MediaId }
         style = {{ width: itemSize, height: itemSize }}
         onPress = { () => {
           this.props.toggleDrawer(true);
           Actions.ImagePost({Selector: item.Selector, image: item.Url, MediaId: item.MediaId})
           //Actions.Modal({image: item.Url, MediaId: null})
         }}>
         <Image
           resizeMode = "cover"
           style = {{ flex: 1 }}
           source = {{ uri: item.ThumbnailUrl }}
         />
       </TouchableHighlight>
     )
   }

}

function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
  //  AvatarUrl: state.CURRENT_USER.AvatarUrl,
    imageUploadToken: state.IMAGE_UPLOAD_TOKEN.imageUploadToken,
    profileImages: state.profileImages.profileImages
  }
}

export default connect (mapStateToProps)(UserImages);

