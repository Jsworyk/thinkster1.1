import React from 'react';

import {View, Image} from 'react-native';
import styles from './styles';
var SpinKit = require('react-native-spinkit');

const LoadingSpinner = ({isVisible = true, size = 40}) => (
    <View style={styles.spinnerContainer}>
        <SpinKit isVisible={isVisible} size={size} type='WanderingCubes' color='#134fa1' />
    </View>
  );

export default LoadingSpinner;