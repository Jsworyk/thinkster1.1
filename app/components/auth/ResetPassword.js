import React, {Component} from 'react';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { SocialIcon } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, {DURATION} from 'react-native-easy-toast';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropdownAlert from 'react-native-dropdownalert';
import DropDownHolder from '../../lib/DropdownHolder';

import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  KeyboardAvoidingView, 
  StatusBar,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Logo } from '../Images';

class ResetPassword extends Component{

   constructor(props) {
    super();
    this.state = {
      visible: false,
      username: '',
      password: ''
    };
    
  }

  componentDidMount(){
     this.props.toggleDrawer(true);
     if (this.props.codeSent){
        DropDownHolder.getDropDown("resetPassword").alertWithType('success', 'Reset Code Sent!', 'We have just emailed you a password reset code. This code can only be used on this page and will expire in 8 hours. Please check the email account you used when you signed up for Thinkster');
       }
  }

  componentWillUnmount(){  
  }

  componentWillMount(){
    
  }

  resetPassword(){
      this.props.resetPassword(this.props.applicationToken, this.props.ClientCode, this.state.resetCode, this.state.newPassword).then((status) => {
        if (status == true){
            //this.dropdown.alertWithType('success', '', 'Your password has been reset.');
            Actions.Signin({type: 'replace', passwordUpdated: true})
        }else{
            var errorArray = []
            var errorString = status.map((l,i) => {
                errorArray.push(l.Argument+' '+l.Message)
            })
            DropDownHolder.getDropDown("resetPassword").alertWithType('error', 'Validation Error', errorArray.join())
        }
        
      })
  }

  render(){
    return (
        <LinearGradient colors={['#3FD2E8', '#1EA6CC']} style={styles.linearGradient}>
            <View style={styles.container}>
                <Logo></Logo>
                <View style={styles.formContainer}>
                    <View style={styles.fieldContainer}>
                        <Icon style={{ paddingTop: 0 }} name='user' size={18} color='#444' />
                        <TextInput
                            style={styles.input}
                            placeholder="RESET CODE"
                            onChangeText={(resetCode) => { this.setState({ resetCode }) }}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={styles.fieldContainer}>
                        <Icon style={{ paddingTop: 0 }} name='lock' size={18} color='#444' />
                        <TextInput
                            style={styles.input}
                            placeholder="NEW PASSWORD"
                            onChangeText={(newPassword) => { this.setState({ newPassword }) }}
                            underlineColorAndroid="transparent"
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={styles.loginButton}>
                        <TouchableOpacity onPress={() => { this.resetPassword() }} underlayColor='#999' >
                            <Text style={styles.loginButtonText}>RESET</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.socialButtonsContainer}>
                    <View style={styles.signupButton}>
                        <Text style={{ color: '#e4e4e4', marginRight: 20 }}>Or</Text>
                        <TouchableOpacity onPress={() => { Actions.Signup({type: 'replace'}) }} underlayColor='#999' >
                        <View style={{ borderBottomColor: '#fff', borderBottomWidth: 0.5 }}>
                            <Text style={{ color: '#fff' }}>SIGN UP</Text>
                        </View>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </View>
            <Toast
                ref="toast"
                style={{ backgroundColor: 'red' }}
                position='bottom'
                positionValue={100}
                fadeInDuration={500}
                fadeOutDuration={1500}
                opacity={0.8}
                textStyle={{ color: '#fff' }}
            />
            <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
            <DropdownAlert ref={ref => DropDownHolder.setDropDown("resetPassword", ref)} onClose={data => {}} messageNumOfLines={5} />            
        </LinearGradient>
    )
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios'? 35 : 35,
    flex: 1
  },
searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
searchIcon: {
    padding: 10,
},
input: {
    flex: 1,
    fontSize: 14,
    paddingRight: 10,
    paddingLeft: 20,
    backgroundColor: '#fff',
    color: '#444',
},
linearGradient: {
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 0
  },
  formContainer:{
    backgroundColor: '#fff',
    padding: 10,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 25,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center', 
    margin: 10, 
    borderRadius: 10, 
    marginTop: 20,
    shadowColor: '#1EA6CC',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  }, 
  fieldContainer: {
    
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: 250,
    borderBottomColor: '#444',
    borderBottomWidth: 1,
    marginBottom: 20
  },
  fieldInner:{
    flex: 1,
    backgroundColor: 'transparent',
    padding: 5,
    marginLeft: 10,
    
  },
  loginButton: {
    backgroundColor: '#06A4CD',
    padding: 5,
    width: 140,
    height: 35,
    borderRadius: 30,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginButtonText:{
      color: '#fff',
      fontSize: 16,
  },
  socialButtonsContainer: {
    flexDirection: 'row',
    marginTop: 25
  },
  signupButton: {
    backgroundColor: '#057AB8',
    padding: 15,
    flexDirection: 'row',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'rgba(0,118,181,0.68)',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 4
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: '#134fa1',
    alignSelf: 'center'
  },
  button: {
    height: 40,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 1,
    opacity: 0.8,
    borderRadius: 5,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    middleContainer: {
      height: 100,
      alignSelf: 'center'
    },
    bottomContainer:{
      alignSelf: 'center',
      height: 50,
      justifyContent: 'flex-end'
    }

});

function mapStateToProps(state){

  return {
  //  disableDrawer: state.disableDrawer
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    //AvatarUrl: state.CURRENT_USER.AvatarUrl,
    Countries: state.COUNTRIES.Countries,
    ClientCode: state.CLIENT_CODE.ClientCode
  }
}

export default connect (mapStateToProps)(ResetPassword);
