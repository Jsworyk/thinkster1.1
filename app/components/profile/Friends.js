
import React, {Component} from 'react';
import { connect } from 'react-redux'
import {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  RecyclerViewBackedScrollView,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import { List, Card, ListItem, Button, ButtonGroup } from 'react-native-elements'
import ActionButton from 'react-native-action-button';
var Spinner = require('react-native-spinkit');

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '17%'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '41%'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '17%'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '41%'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '17%'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '41%'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '17%'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '41%'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: '17%'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: '41%'
  },
]

class Friends extends React.Component {
  constructor(props){
    super(props);
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });
    this.state = {
      ds:list,
      dataSource:ds,
      friends: [],
      pendingReceived: [],
      selected: 0,
      showAll: false,
      visible: true
    }
    this.getFriends = this.getFriends.bind(this)
  }

  componentDidMount(){
    this.setState({
      dataSource:this.state.dataSource.cloneWithRows(this.state.ds),
    })
    this.getFriends();
  }

 renderRow (rowData, sectionID) {
  return (
      <View>
        <ListItem roundAvatar
          key={sectionID}
          title={rowData.name}
        />
      </View>
  )
}

  getFriends(){
    this.props.getFriends(this.props.applicationToken, 1, 10).then(() => {
      this.setState({
        visible: false,
        showAll: true,
        friends: this.props.friends
      })
    })
  }

  addFriend(){
    this.props.addFriend(this.props.applicationToken, 18).then(() => {
    })
  }

  setSelected(selected){
    if (selected == 0){
      this.setState({
        selected: selected,
        visible: false,
        showAll: true,
      })
    }else{
      this.setState({
        selected: selected,
        visible: true,
        showAll: false,
      })
      this.props.getPendingReceived(this.props.applicationToken, 5).then(() => {
        this.setState({
          visible: false,
          pendingReceived: this.props.pendingReceived
        })
      })
    }
  }

  confirmFriendRequest(profileId){
    this.props.confirmFriendRequest(this.props.applicationToken, profileId).then(() => {
      this.setState({
        visible: true,
        showAll: false,
      })
      this.props.getPendingReceived(this.props.applicationToken, 5).then(() => {
        this.setState({
          visible: false,
          pendingReceived: this.props.pendingReceived
        })
      })
    })
  }

  declineFriendRequest(profileId){
    this.props.declineFriendRequest(this.props.applicationToken, profileId).then(() => {
      this.setState({
        visible: true,
        showAll: false,
      })
      this.props.getPendingReceived(this.props.applicationToken, 5).then(() => {
        this.setState({
          visible: false,
          pendingReceived: this.props.pendingReceived
        })
      })
    })
  }

  render() {
    const buttons = ['All', 'Pending'];
    return (
      <View style={{flex: 1, marginBottom: 50}}>
          <ButtonGroup
            onPress={(i) => this.setSelected(i)}
            selectedIndex={this.state.selected}
            buttons={buttons}
            selectedBackgroundColor='#49abde'
            selectedTextStyle={{color: '#fff', fontSize: 12, }}
            textStyle={{color: '#a7a7aa', fontSize: 12, }}
            buttonStyle={{borderColor: '#49abde', color: '#49abde'}}
            innerBorderStyle={{color: '#49abde'}}
            containerStyle={{marginLeft: 35, marginRight: 35, marginTop: 10, marginBottom: 5, backgroundColor: '#fff', height: 25}} />
      {this.state.visible
        ?
        <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
          <Spinner style={{}} isVisible={true} size={40} type='WanderingCubes' color='#134fa2'/>
        </View>
        :
        this.state.showAll ?
      <ScrollView style={{marginTop: 0}}>
        <Card
            containerStyle={{margin: 5}}
            title='Friends'
            titleStyle={{color: '#696868',  fontWeight: 'bold'}}
            >
            {
              this.state.friends.length != 0 ? 
              this.state.friends.map((l, i) => {
                return (
                  <ListItem 
                    roundAvatar
                    containerStyle={{paddingRight: 50}}
                    avatar={{uri:l.AvatarUrl}}
                    key={i}
                    title={l.DisplayName}
                    titleStyle={styles.username}
                    hideChevron={true}
                    
                  />
                )}
              )
                  :
                  <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: '#696868'}}>You don't have any frineds yet.</Text>
                  </View>
            }
          </Card>

      </ScrollView>
      :
      <ScrollView style={{marginTop: 0}}>
        <Card
            containerStyle={{margin: 5}}
            title='Pending Requests'
            titleStyle={{color: '#696868',  fontWeight: 'bold'}}
            >
            {
              this.state.pendingReceived.length != 0 ? 
              this.state.pendingReceived.map((l, i) => {
                return (
                  <ListItem 
                    roundAvatar
                    containerStyle={{paddingRight: 50}}
                    avatar={{uri:l.AvatarUrl}}
                    key={i}
                    title={l.DisplayName}
                    titleStyle={styles.username}
                    hideChevron={true}
                    subtitle={
                      <View style={styles.subtitleView}>
                        <TouchableOpacity onPress={ () => this.confirmFriendRequest(l.ProfileId) }>
                          <Text style={{color: '#134fa1'}}>Confirm Request</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{paddingLeft: 10 }} onPress={ () => this.declineFriendRequest(l.ProfileId) }>
                          <Text style={{color: '#134fa1'}}>Decline</Text>
                        </TouchableOpacity>
                      </View>
                    }
                  />
                )}
             
              )
              :
              <View style={{margin: 1,flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: '#696868'}}>No pending requests.</Text>
              </View>
            }
          </Card>

         
      </ScrollView>
      }
      <ActionButton
          buttonColor="#2b8abb"
          onPress={() => this.addFriend()}
      />
</View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  row:{
    flex:1,
    flexDirection:'row',
    padding:18,
    borderBottomWidth: 1,
    borderColor: '#d7d7d7',
  },
  selectionText:{
    fontSize:15,
    paddingTop:3,
    color:'#b5b5b5',
    textAlign:'right'
  },
    subtitleView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 5,
    paddingLeft: 10
  },
  card: {
     borderWidth: 1,
     backgroundColor: '#fff',
     borderColor: 'rgba(0,0,0,0.1)',
     margin: 5,
     height: 150,
     padding: 15,
     shadowColor: '#ccc',
     shadowOffset: { width: 2, height: 2, },
     shadowOpacity: 0.5,
     shadowRadius: 3,
   },
   username: {
     color: '#696868',
     fontSize: 14,
   },
   subtitle: {
     color: '#696868'
   }
});

function mapStateToProps(state) {
  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    pendingReceived: state.SET_PENDING_RECEIVED.pendingReceived,
    friends: state.SET_FRIENDS.friends
  }
}

export default connect(mapStateToProps)(Friends);
