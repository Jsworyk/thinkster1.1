import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import ActionButton from 'react-native-action-button';
import SocketIOClient from 'socket.io-client';
import Helper from '../../lib/helper';
var fontColorContrast = require('font-color-contrast');
import ModalWrapper from 'react-native-modal-wrapper';
import LinearGradient from 'react-native-linear-gradient';
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,
  KeyboardAvoidingView,
  FlatList
} = ReactNative

var reactions = [];
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
const Screen = Dimensions.get('window');
var poll = null;
class FriendResponses extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      responses: null,
      pollName: '',
      reactions: [],
      showResponses: false,
      likeContainerVisible: false,
      drawerOpen: false,
      showItem: false
    }
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  getPoll(){
   // this.props.setTotal(0);

    this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
      this.getFriendResponses(poll)
      this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false, transports: ['websocket'] });
      this.socket.on('connect', () => {
        this.socket.emit('authenticate', this.props.applicationToken, ['PO'+poll.Question.PollId]);
      });
      
      this.socket.on('Reaction', (data) => {
        this.props.friendResponses.map((l,i) => {
          if (l.PollResponseId == data.TargetId) {
            this.props.addReaction(i,data.Delta);
          } 
        })
      
      });
    })
  }

  reactToResponse(id){
    this.props.React(this.props.applicationToken, 'RE', id, 'LO').then(() => {

    });
  }

  getFriendResponses(poll){
    
    this.props.getFriendResponses(this.props.applicationToken, poll.Question.PollId).then(() => {
      
      this.setState({
        showResponses: true,
        poll: poll,
        showItem: true,
        pollName: poll.Question.Question,
        myResponse: poll.Choices.map((l,i) => { if (l.MyAnswer === 1) {return l.Choice} })
      })
    })
  
  }

  componentDidMount() {
      this.getPoll();
      this.props.getMapFilters(this.props.applicationToken);
  }

  componentWillMount() {
    // this.setState({
    //     item: this.props.item,
    //     showItem: true
    // })
}


open(id, type) {
  this.setState({
      likeContainerVisible: true,
      reactingTo: id,
      type: type
  })
}

close() {
  this.setState({open: false}, () => {
       Animated.timing(this._scaleAnimation, {
        duration: 50,
        toValue: 0
      }).start();
  })
}

getLikeContainerStyle() {
  return {
          transform: [{scaleY: this._scaleAnimation}],
          overflow: this.state.open ? 'visible': 'hidden',
        };
}

React(l){

  this.setState({
      likeContainerVisible: false
  })
  this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
    // change locally
    var poll = this.state.poll;
    poll.Question.Reacted = 1;
    poll.Question.Emoticon = l.Emoji;
    this.setState({
      poll: poll
    })
  });
}

extractText( str ){
  var ret = "";

  if ( /"/.test( str ) ){
    ret = str.match( /"(.*?)"/ )[1];
  } else {
    ret = str;
  }

  return ret;
}

truncate(string, stringLength){
  if (string.length > stringLength)
     return string.substring(0,stringLength)+'...';
  else
     return string;
}

  renderItem(l,i){
    
    return (
      <View key={i} style={{ borderBottomColor: 'rgba(0,0,0,0.1)', borderBottomWidth: 0.5, margin: 5 }}>
                    <View style={[{ borderRadius: 5, padding: 5, marginBottom: 5, backgroundColor: '#5D7698', }]}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ margin: 5 }}>
                          {l.AvatarThumbnailUrl ? 
                          <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                          <Image
                            style={{ borderRadius: 15, width: 30, height: 30 }}
                            source={{ uri: l.AvatarThumbnailUrl }}
                          />
                          </TouchableOpacity>
                          : 
                          null
                          }
                        </View>
                        <View style={{ flex: 1, margin: 5, }}>
                          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                          <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.UserProfile({ProfileName: l.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                            <Text style={{ color: '#fff', fontSize: 12, fontWeight: '400' }}>{l.DisplayName}</Text>
                          </TouchableOpacity>
                            <Text style={{ fontSize: 12, color: '#ccc' }}>
                            {parseInt(Math.abs(new Date() - new Date(l.ResponseDate)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(l.ResponseDate)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.ResponseDate)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(l.ResponseDate)) / 36e5) / 24) + 'd'}
                            </Text>
                          </View>
                          <View style={{ marginTop: 10, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{flexDirection: 'row', flexWrap: 'wrap', width: 165}}>
                            <Text style={{ color: '#fff', fontSize: 14, fontWeight: '500', }}>{l.Choice}</Text>
                            </View>
                            <TouchableOpacity rejectResponderTermination  onPress={() => {this.reactToResponse(l.PollResponseId)}} underlayColor='#999'>
                            <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', padding: 5 }}>
                              <Icon style={{ textAlign: 'center', opacity: 0.8, marginTop: 1 }} name='heart' size={12} color={l.Reacted == 1 ? 'red' : '#fff'} />
                              <Text style={{ textAlign: 'center', fontSize: 11, marginLeft: 5, color: '#fff' }}>
                                {
                                  l.ReactionCount
                                }
                              </Text>
                            </View>
                            </TouchableOpacity>
                          </View>
                          <View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
    )
}

toggleDrawer(){
  
  this.state.drawerOpen ? this.props.closeDrawer() : this.props.openDrawer();
  this.setState({
    drawerOpen: !this.state.drawerOpen
  })
}

_keyExtractor = (item, index) => index

render() {
  
  return (
    <KeyboardAvoidingView style={{flex: 1}} behavior='padding'>
    <View style={styles.container}>
    <LinearGradient
          start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
          locations={[0.2, 0.6, 1]}
          colors={['#1EA6CC', '#2FBCDA', '#3FD1E7']}
          style={{}}>

          <View style={{ height: 64, padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent' }}>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.pop()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
              <Icon style={{opacity: 1}} name='chevron-left' size={22} color='#fff' />
              </View>
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, marginTop: 12 }}>Responses</Text>
            <TouchableOpacity rejectResponderTermination  onPress={() => Actions.Search()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
                <Image source={require('../../assets/search.png')} style={{ resizeMode: 'contain', width: 22, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      {this.state.showItem ?
      <View style={[styles.card, {elevation: 3, backgroundColor: this.state.poll.Question.BackgroundColor ? this.state.poll.Question.BackgroundColor : '#FFD700'}]}>
          
          <View style={{flex: 1}}>
          <View style={{  backgroundColor: 'transparent', padding: 10,borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}>
             <View style={{flexDirection: 'row', margin: 5, justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
              {this.props.currentUserProfile.AvatarUrl ? 
              <Image source={{uri: this.props.currentUserProfile.AvatarUrl}} style={{width: 30, height: 30, borderRadius: 15}}/>
              :
              <View style={{backgroundColor: '#0074CD', width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{color: '#fff'}}>{(this.props.currentUserProfile.DisplayName).split(" ").map((n)=>n[0])}</Text>
              </View>
              }
              <TouchableOpacity rejectResponderTermination  onPress={() => {Actions.UserProfile({ProfileName: this.props.currentUserProfile.ProfileName, hideNavBar: false})}} underlayColor='#999'>
                <Text style={{color: fontColorContrast(this.state.poll.Question.BackgroundColor), marginLeft: 10, fontWeight: '600', fontSize: 12}}>{this.props.currentUserProfile.DisplayName}</Text>
              </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'row',alignItems: 'center'}}>
              {/* <Text style={{color: fontColorContrast(this.state.item.BackgroundColor), textAlign: 'center', fontSize: 11}}>
                  {
                      parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) <= 24 ? parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) + 'h' : Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) < 2 ? '1d' : Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) < 31 ? Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) + 'd' : (Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) == 30 || Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) == 31) ? '1m' : Math.floor(Math.floor(parseInt(Math.abs(new Date() - new Date(this.state.item.Created)) / 36e5) / 24) / 30) + 'm' 
                  }
              </Text> */}
            </View>
            </View>
            <View style={{margin: 5,}}>
              <TouchableOpacity rejectResponderTermination  onPress={() => { Actions.Question({isPopup: false, pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                  <Text style={{fontSize: 18, lineHeight: 20,fontWeight: 'bold', color: fontColorContrast(this.state.poll.Question.BackgroundColor), margin: 0}}>{this.state.pollName}</Text>
              </TouchableOpacity>
            </View>
            <View style={{margin: 5,alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap'}}>
            <Text style={{fontSize: 16, lineHeight: 20, margin: 0, color: fontColorContrast(this.state.poll.Question.BackgroundColor)}}>{this.state.myResponse}</Text>
            </View>
          </View>
        
          
      
      <View style={{paddingTop: 20, flex: 1, backgroundColor: 'transparent',borderTopLeftRadius: 15, borderTopRightRadius: 15,paddingBottom: 0 }}>
        <View style={[{elevation: 1,borderTopColor: '#000', borderTopWidth: 0, backgroundColor: '#fff',borderTopLeftRadius: 15, borderTopRightRadius: 15, flexDirection: 'row', justifyContent: 'space-between', padding: 15}, styles.shadowTop]}>
        <Text style={{color: '#000', fontSize: 14, fontWeight: '500'}}>{this.state.poll.Question.FollowingResponseCount} Responses</Text>
          {/* <Text style={{color: '#fff', fontWeight: 'bold'}}>...</Text> */}
        </View>


{/*               

<View style={{flexDirection: 'row', position: 'absolute',right: 10, top: -18, }}>
<TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
<View style={[{ position: 'absolute', right: 64,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
  
  <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
</View>
</TouchableOpacity>
<TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
<View style={[{ position: 'absolute', right: 32,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
 
  <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
</View>
</TouchableOpacity>
{this.state.poll.Question.Reacted == 0 ?
  <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
  <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
    
    <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
  </View>
  </TouchableOpacity>
  :
  <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
  <View style={[{position: 'absolute', right: 0,margin: 2,backgroundColor: '#30415F', borderRadius: 20,padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
    <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
  </View>
  </TouchableOpacity>
}
</View> */}




          
        <View style={{flex: 1,padding: 5, backgroundColor: '#2BB7D7'}}>
          {
                    this.state.showResponses ?
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      extraData={this.props.friendResponses}
                      data={this.props.friendResponses}
                      keyExtractor={this._keyExtractor}
                      renderItem={({ item, index }) => this.renderItem(item, index)}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                      <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#fff' />
                    </View>
          }
          </View>
          <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 74, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({ pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                              <View style={[{elevation: 4, right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                                <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                              </View>
                              </TouchableOpacity>
                              
                            </View>
                            <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 42, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({ pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                            </View>
                            </TouchableOpacity>
                            </View>
                            <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 10, top: 5, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            {this.state.poll.Question.Reacted == 0 ?
                            
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                            </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999'>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
                            </View>
                            </TouchableOpacity>
                            
                          }
                          </View>
        </View>
       
          
      
    </View>
        
      <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
      </View>
      :
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
        <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
      </View>
    }
      </View>
      <ModalWrapper
          style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
          visible={this.state.likeContainerVisible}
          onRequestClose={() => this.setState({ likeContainerVisible: false })}
          shouldCloseOnOverlayPress={true}
          >
                      <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                          {
                          this.props.reactionTypes.map((l, i) => (
                              <TouchableOpacity rejectResponderTermination  key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                  <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                              </TouchableOpacity>
                          ))
                        }

                      </View>
      </ModalWrapper>
      </KeyboardAvoidingView>
     );
}

  // render() {
    
  //   return (
  //     <View style={styles.container}>
  //       {this.state.showResponses ?
  //       <View style={styles.card}>
            
  //           <View style={{flex: 1}}>
  //           <View style={{backgroundColor: 'transparent', padding: 10,borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
  //             <View style={{flexDirection: 'row', margin: 5, justifyContent: 'space-between', alignItems: 'center'}}>
  //               <View style={{flexDirection: 'row', alignItems: 'center'}}>
  //               <Image
  //                         style={{ borderRadius: 15, width: 30, height: 30 }}
  //                         source={{uri: this.props.currentUserProfile.AvatarUrl}}
  //                       />
  //               <Text style={{margin: 2,marginLeft: 5, fontSize: 14}}>{this.props.currentUserProfile.DisplayName}</Text>
  //               </View>
  //               {/* <View>
  //                 <Text>2h</Text>
  //               </View> */}
  //             </View>
  //             <View style={{margin: 5,}}>
  //             <Text style={{ fontSize: 20, color: '#444', fontWeight: '500', }}>
  //               {this.state.pollName}
  //             </Text>
  //             </View>
  //             <View style={{margin: 5, marginBottom: 20}}>
  //             <Text style={{ fontSize: 16, color: '#444', fontWeight: '400', }}>
  //               {this.state.myResponse}
  //             </Text>
  //             </View>
  //           </View>
          
  //       <View style={{flex: 1, backgroundColor: '#2BB7D7',borderTopLeftRadius: 15, borderTopRightRadius: 15, }}>
  //         <View style={{backgroundColor: '#23ADD0',borderTopLeftRadius: 15, borderTopRightRadius: 15, flexDirection: 'row', justifyContent: 'space-between', padding: 15}}>
  //            <Text style={{color: '#fff', fontSize: 14, fontWeight: '500'}}>{''}</Text>
  //          {/* <Text style={{color: '#fff', fontWeight: 'bold'}}>...</Text> */}
  //         </View>
  //         <View style={{flexDirection: 'row', position: 'absolute',right: 10, top: -18, }}>
  //               <TouchableOpacity rejectResponderTermination  style={{zIndex: 300}} onPress={() => { Actions.Map({type: 'replace', pollName: this.props.pollName, poll: this.state.poll})}} underlayColor='#999'>
  //               <View style={[{ position: 'absolute', right: 85,margin: 2,backgroundColor: '#30415F', borderRadius: 25, width: 50, height: 50, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
  //                 <Icon style={{}} name='map' size={14} color='white' />
  //               </View>
  //               </TouchableOpacity>
  //               <TouchableOpacity rejectResponderTermination  style={{zIndex: 200}} onPress={() => { Actions.PollComments({type: 'replace', pollName: this.props.pollName})}} underlayColor='#999'>
  //               <View style={[{ position: 'absolute', right: 45,margin: 2,backgroundColor: '#30415F', borderRadius: 25, width: 50, height: 50, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
  //                 <Icon style={{}} name='comment' size={16} color='white' />
  //               </View>
  //               </TouchableOpacity>
  //               <TouchableOpacity rejectResponderTermination  style={{zIndex: 100}}  onPress={() => { Actions.FriendResponses({type: 'replace', pollName: this.props.pollName})}} underlayColor='#999'>
  //               <View style={[{position: 'absolute', right: 10,margin: 2,backgroundColor: '#30415F', borderRadius: 25, width: 50, height: 50, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
  //                 <Icon style={{}} name='heart' size={16} color='red' />
  //               </View>
  //               </TouchableOpacity>
  //           </View>
  //         <View style={{padding: 10, flex: 1}}>
            
  //                 {
  //                   this.state.showResponses ?
  //                   <FlatList
  //                     showsVerticalScrollIndicator={false}
  //                     extraData={this.props.friendResponses}
  //                     data={this.props.friendResponses}
  //                     keyExtractor={this._keyExtractor}
  //                     renderItem={({ item, index }) => this.renderItem(item, index)}
  //                   />
  //                   :
  //                   null
  //                 }
            
  //           </View>
  //         </View>
        
        
  //     </View>
          
          
          
       
        
  //       <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />

  //       </View>

  //       :
  //       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
  //         <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
  //     </View>
  //       }
  //       </View>
  //      );
  // }
  
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e4e4e4',
        //marginBottom: 50,
        //marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#FFD93E',
        //borderRadius: 15,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
      },
      shadow:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowOffset: {
          height: 3,
          width: 0,
        },
      },
      shadowTop:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        shadowOffset: {
          height: -3,
          width: 0,
        },
      },
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    width: 50,
  },
  image: {
    width: 30,
  },
})

reactMixin(FriendResponses.prototype, TimerMixin)
function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    friendResponses: state.friendResponses.friendResponses,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
    total: state.TOTAL.total
  }
}

export default connect(mapStateToProps)(FriendResponses);
