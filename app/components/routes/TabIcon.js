import ReactNative from 'react-native'
import { connect } from 'react-redux'
import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions, ActionConst } from 'react-native-router-flux'

const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,

} = ReactNative

class TabIcon extends Component {

  render() {
    if (this.props.title == 'Notifications' && this.props.unreadNotifications.length > 0){
        return( <View style={{ }}>
          <View style={{zIndex: 100, top: -5, left: 15, position: 'absolute',padding: 5,height: 18, borderRadius: 10, backgroundColor: 'red', justifyContent: 'center' }}>
            <Text style={{fontSize: 11, padding: 0, backgroundColor: 'transparent', textAlign: 'center', color: '#fff'}}>
              {this.props.unreadNotifications.length}
            </Text>
          </View>
          <Icon size={this.props.selected ? 18 : 18}
          style={{ opacity: this.props.selected ? 1 : 0.7, fontWeight: 'bold', color: this.props.selected ? '#0f6aa6' : '#a7a7aa' }} name={this.props.fontIcon} />
        </View>
        )
    }else if (this.props.title == 'Messages' && this.props.unreadChatrooms.length > 0){
      return( <View style={{ }}>
        <View style={{zIndex: 100, top: -5, left: 15, position: 'absolute',padding: 5,height: 18, borderRadius: 10, backgroundColor: 'red', justifyContent: 'center' }}>
          <Text style={{fontSize: 11, padding: 0, backgroundColor: 'transparent', textAlign: 'center', color: '#fff'}}>
            {this.props.unreadChatrooms.length}
          </Text>
        </View>
        <Icon size={this.props.selected ? 18 : 18}
        style={{ opacity: this.props.selected ? 1 : 0.7, fontWeight: 'bold', color: this.props.selected ? '#0f6aa6' : '#a7a7aa' }} name={this.props.fontIcon} />
      </View>
      )
  }else if (this.props.title == 'Landing'){
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Image source={require('../../assets/check.png')} style={{width: 30, height: 30}}/>
      </View>
    )
  }else if (this.props.Icontitle == 'Profile'){
    
    return (
      this.props.currentUserProfile && this.props.currentUserProfile.AvatarUrl ?
      <View style={{justifyContent: 'center', alignItems: 'center',}}>
        <Image
            style={{borderRadius: 15, width: 30, height: 30,borderColor: '#0f6aa6', borderWidth: this.props.selected ? 1 : 0}}
            source={{uri: this.props.currentUserProfile.AvatarUrl}}
          />
      </View>
      :
      <Icon size={this.props.selected ? 18 : 18}
      style={{ opacity: this.props.selected ? 1 : 0.7, fontWeight: 'bold', color: this.props.selected ? '#0f6aa6' : '#a7a7aa' }} name={this.props.fontIcon} />
    
    )
  }else {
      return( <Icon size={this.props.selected ? 18 : 18}
        style={{ opacity: this.props.selected ? 1 : 0.7, fontWeight: 'bold', color: this.props.selected ? '#0f6aa6' : '#a7a7aa' }} name={this.props.fontIcon} />
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    unreadNotifications: state.notifications.unreadNotifications,
    unreadChatrooms: state.chatrooms.unreadChatrooms,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile
  }
}

export default connect(mapStateToProps)(TabIcon);