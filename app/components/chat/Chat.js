import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import { connect } from 'react-redux'
import {GiftedChat, Bubble} from 'react-native-gifted-chat';
import CustomActions from './CustomActions';
import Icon from 'react-native-vector-icons/FontAwesome';
var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import SocketIOClient from 'socket.io-client';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      startingMessageId: null,
      isLoadingEarlier: false,
    };
    this.onSend = this.onSend.bind(this);
    this.renderCustomActions = this.renderCustomActions.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);
    
  }

  componentWillUnmount(){
    this.props.toggleDrawer(false);
    this.props.setChatMessages([])
  }

  renderCustomActions(props) {
    /*if (Platform.OS === 'ios') {
      return (
        <CustomActions
          {...props}
        />
      );
    }
    const options = {
      'Action 1': (props) => {
        alert('option 1');
      },
      'Action 2': (props) => {
        alert('option 2');
      },
      'Cancel': () => {},
    };
    return (
      <Actions
        {...props}

      />
    );*/
  }

onReceive(message) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: message.ChatRoomMessageId,
          text: message.Message,
          createdAt: new Date(message.Created),
          user: {
            _id: message.ProfileId,
            name: message.DisplayName,
            avatar: message.AvatarUrl,
          },
        }),
      };
    });
}

onLoadEarlier() {
  this.getChatMessages(this.state.messages[this.state.messages.length - 1]._id)
}

  componentWillMount() {
    this.socket = SocketIOClient('https://push.thinkster.ca', {jsonp: false, transports: ['websocket']});
    this.socket.on('connect', () => {
      this.socket.emit('authenticate', this.props.applicationToken);
    });
    this.socket.on('ChatMessage', (data) => {
      if (data.Messages[0].ProfileId !== this.props.currentUserProfile.ProfileId){
        if (data.ChatRoomId == this.props.chatRoomId){
          this.onReceive(data.Messages[0]);
        }
        
      }
    });
    this.getChatMessages(this.state.startingMessageId)
    const _this = this;
    
  }

  getChatMessages(startingMessageId){
    this.setState((previousState) => {
      return {
        isLoadingEarlier: true,
      };
    })
    this.props.getChatMessages(this.props.applicationToken, this.props.chatRoomId, startingMessageId, 15).then((messages) => {
      var msgs = [];
      messages.slice(0).reverse().map((l, i) => {
       msgs.push({
         _id: l.ChatRoomMessageId,
          text: l.Message,
          createdAt: l.Created,
          user: {
            _id: l.ProfileId,
            name: l.DisplayName,
            avatar: l.AvatarUrl,
          },
       })
      })
      
      this.setState({messages: [...this.state.messages, ...msgs], isLoadingEarlier: false });
      
    });
  }

  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
    
    this.props.sendMessage(this.props.applicationToken, this.props.chatRoomId, messages[0].text);
  }

  renderBubble(props) { 
    return ( 
    <Bubble {...props} 
      wrapperStyle={{
          left: {
            backgroundColor: '#E5F5F9',
          },
          right: {
            backgroundColor: '#1EA6CC'
          }
        }} />
      )
  }

  render() {
    return (
      <View style={{flex: 1, marginTop: Platform.OS === 'ios' ? 64 : 54,}}>
     <View style={{flex: 1, padding: 5}}>
      <GiftedChat
        loadEarlier={this.state.loadEarlier}
        onLoadEarlier={this.onLoadEarlier}
        isLoadingEarlier={this.state.isLoadingEarlier}
        keyboardShouldPersistTaps="handled"
        isAnimated={true}
        messages={this.state.messages}
        onSend={this.onSend}
        renderBubble={this.renderBubble}
        renderActions={this.renderCustomActions}
        user={{
          _id: this.props.currentUserProfile.ProfileId,
        }}
      />
      </View>
      </View>
    );
  }
}


const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
      borderTopWidth: 0,
      borderBottomColor: 'rgba(0, 0, 0, 0.1)',
      borderBottomWidth: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',

      padding: 16,
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  image: {
    width: 30,
  },
})

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
});

reactMixin(Chat.prototype, TimerMixin)
function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    messages: state.SET_CHAT_MESSAGES.messages,
    user: state.CURRENT_USER.ProfileId,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
  }
}

export default connect (mapStateToProps)(Chat);
