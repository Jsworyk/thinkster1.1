import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import ImageZoom from 'react-native-image-pan-zoom';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';

const {
  ScrollView,
  View,
  TextInput,
  Platform,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,
  DeviceEventEmitter,
  NativeModules,
} = ReactNative

var RNUploader = NativeModules.RNUploader;

class AvatarModal extends Component{

   constructor(props) {
    super(props)
    this.state = {
        visible: false
    }
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }

  componentWillUnmount(){
    this.props.toggleDrawer(false);
  }

  componentWillMount() {
    Actions.refresh({ renderRightButton: this._renderRightButton });
}

_renderRightButton = () => {
    return(
        <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => this.selectPhotoTapped() } >
            <Icon name="ellipsis-v" size={26} color='#fff' />
        </TouchableOpacity>
    );
};


componentDidlMount(){
    // upload progress
	DeviceEventEmitter.addListener('RNUploaderProgress', (data)=>{
        let bytesWritten = data.totalBytesWritten;
        let bytesTotal   = data.totalBytesExpectedToWrite;
        let progress     = data.progress;
        
        console.log( "upload progress: " + progress + "%");
      });
}

uploadImage(fileName){
    this.setState({
      visible: true
    })
    this.props.getProfileImageUploadToken(this.props.applicationToken).then(() => {
        
      RNFetchBlob.fetch('POST', 'https://thinkster.ca/uploads', {
        'Content-Type' : 'multipart/form-data',
      }, [
        { name : 'files[]', filename: fileName, data: RNFetchBlob.wrap(this.state.avatarSource)},
        { name : 'token', data: this.props.imageUploadToken},
        
      ]).then((resp) => {
        
        if (this.props.imageType == 'avatar'){
          this.props.postImage(this.props.applicationToken,resp.json().Files[0].MediaId, 'avatar', '', 'PU', 0).then(() => {
          this.props.setAvatar(this.props.applicationToken, resp.json().Files[0].MediaId).then(() => {
            this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
                this.setState({
                    visible: false,
                })
            });
          });
            
        });
        }else{
          
          this.props.postImage(this.props.applicationToken,resp.json().Files[0].MediaId, 'banner', '', 'PU', 0).then(() => {
            this.props.setBanner(this.props.applicationToken, resp.json().Files[0].MediaId).then(() => {
              this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
                  this.setState({
                      visible: false,
                  })
              });
            });
              
          });
        }

        
      }).catch((err) => {
        console.log(err);
      })
    })
  }

  selectPhotoTapped() {
    
       const options = {
         quality: 1.0,
         maxWidth: 500,
         maxHeight: 500,
         storageOptions: {
           skipBackup: true
         }
       };
    
       ImagePicker.showImagePicker(options, (response) => {
         if (response.didCancel) {
           
         }
         else if (response.error) {
           alert(response.error)
         }
         else if (response.customButton) {
           
         }
         else {
           let source = { uri: response.uri };
           this.setState({
             avatarSource: response.uri.split("file://").pop(),
             data: response.data
           });
           var fileparts = response.uri.split("/");
           this.uploadImage(response.uri.split("/")[fileparts.length-1]);
         }
       });
     }



  render(){
    return (
      <View style={{flex: 1, backgroundColor: '#000', marginTop: Platform.OS === 'ios'? 64 : 54}}>
       <StatusBar
     backgroundColor="#134fa1"
     setBarStyle={{borderBottomWidth: 0, opacity: 0.9}}
     barStyle="light-content"/>
     
        <View>
          
        <ImageZoom cropWidth={Dimensions.get('window').width}
                      cropHeight={Dimensions.get('window').height}
                      imageWidth={Dimensions.get('window').width}
                      imageHeight={300} 
                      onLongPress={() => {}}
                      >
               <Image style={{width: Dimensions.get('window').width, height:300}}
                      source={{uri: this.props.imageType == 'avatar' ? this.props.currentUserProfile.AvatarUrl : this.props.currentUserProfile.BannerUrl}}/>
           </ImageZoom>
        </View>
        <Spinner visible={this.state.visible} textContent={""} textStyle={{ color: '#FFF' }} />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
})


function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    imageUploadToken: state.IMAGE_UPLOAD_TOKEN.imageUploadToken,
  }
}

export default connect (mapStateToProps)(AvatarModal);

