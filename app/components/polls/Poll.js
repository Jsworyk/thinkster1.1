import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import Spinner from 'react-native-loading-spinner-overlay';
import { Button, ButtonGroup, Divider} from 'react-native-elements'
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import ActionButton from 'react-native-action-button';
import FriendSuggest from './FriendSuggest'
import OverallChart from './OverallChart'
import Results from './Results'
import Modal from 'react-native-simple-modal';
import Helper from '../../lib/helper'
var fontColorContrast = require('font-color-contrast');
import ModalWrapper from 'react-native-modal-wrapper';
import LinearGradient from 'react-native-linear-gradient';
import CardView from 'react-native-cardview'
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  Animated,
  Slider,
  Dimensions,
  Platform,
  AlertIOS,
  ListView,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,

} = ReactNative

var reactMixin = require('react-mixin')
import TimerMixin from 'react-timer-mixin'
import SafeAreaView from 'react-native-safe-area-view';
import { Colors } from '../../lib/styles';

const Screen = Dimensions.get('window');
var positions = [0,32,64];
class Poll extends Component {

  constructor(props) {
    super(props);
    //this._deltaX = new Animated.Value(0);
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      showAnswers: false,
      listViewData: [],
      visible: false,
      damping: 1-0.6,
      tension: 300,
      message: '',
      showMessage: false,
      selected: 'FR',
      likeContainerVisible: false
    }
    //this.renderRow = this.renderRow.bind(this);
    //this.submitAnswer = this.submitAnswer.bind(this);
  }


open(id, type) {
  this.setState({
      likeContainerVisible: true,
      reactingTo: id,
      type: type
  })
}

close() {
  this.setState({open: false}, () => {
       Animated.timing(this._scaleAnimation, {
        duration: 50,
        toValue: 0
      }).start();
  })
}

getLikeContainerStyle() {
  return {
          transform: [{scaleY: this._scaleAnimation}],
          overflow: this.state.open ? 'visible': 'hidden',
        };
}

React(l){

  this.setState({
      likeContainerVisible: false
  })
  this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
    // change locally
    var poll = this.state.poll;
    poll.Question.Reacted = 1;
    poll.Question.Emoticon = l.Emoji;
    this.setState({
      poll: poll
    })
  });
}

  submitAnswer(PollChoiceId) {
    this.setState({
      showResults: true
    })
    
    this.props.submitAnswer(this.props.applicationToken, this.props.currentQuestion.PollId, PollChoiceId, this.state.selected).then(() => {
      if (this.props.submissionStatus){
        //this.getNextQuestion();
        //Actions.Results({pollName: this.props.currentQuestion.UrlSegment});
      }
    })
    }
    
    getNextQuestion(){
        const _this = this;
        this.setState({showAnswers: false});
        this.props.nextQuestion(this.props.applicationToken).then((poll) => {
          if (_this.props.currentQuestion == null && _this.props.currentAnswers == null){
            _this.setState({ 
                showMessage: true, 
                showAnswers: true,
                message: 'There are no questions to answer.'
         },
         () => {
           Actions.refresh({hideNavBar: true})
         }
        );
          }else{
            
          _this.setState({
            //poll: _this.props.currentQuestion,
            poll: poll,
            question: poll.Question.Question, 
            listViewData: poll.Choices,
            showAnswers: true,
            responseVisibility: _this.props.responseVisibility,
            backgroundColor: poll.Question.BackgroundColor,
         });
          }
          
        });
      }


    componentDidMount() {
        if (this.props.isLoggedin) {
          this.getPoll();
        }
      }
    
      getPoll(){
        const _this = this;
        
        this.props.getPoll(this.props.applicationToken, this.props.pollName).then((poll) => {
          
            _this.setState({ 
                question: poll.Question.Question, 
                poll: poll,
                IsAnswered: poll.Question.IsAnswered == 1 ? true : false,
                listViewData: poll.Choices, 
                showAnswers: true,
                backgroundColor: poll.Question.BackgroundColor
             });
        })
      }

  setResponseSettings(responseVisibility){
     this.props.setResponseSettings(responseVisibility);
      this.setState({
        responseVisibility: responseVisibility
      })
  }

  renderItem(l,i){
    return (
        this.state.IsAnswered ?
        
        <View key={i} style={[styles.shadow, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderRadius: 5, margin: 5,marginBottom: 10, padding: 12, width: Screen.width - 80}]}>
          {l.MyAnswer == 1 ?
            <View style={{width: 40,justifyContent: 'center', alignItems: 'center'}}>
            <Image
            style={{borderWidth: 1, borderColor: '#fff', borderRadius: 13, width: 26, height: 26,alignSelf: 'center' }}
            source={{ uri: this.props.currentUserProfile.AvatarUrl }}
          /> 
          </View>
          :
          <View style={{width: 40,}}></View>
          }
          <View style={{flex: 1,flexDirection: 'row', flexWrap: 'wrap' }}>
          <Text style={{textAlign: 'center', color: '#1EA6CC', fontSize: 16, fontWeight: '400' }}>{l.Choice}</Text>
          </View>
          <View style={{justifyContent: 'flex-end', flexDirection: 'row', flexWrap: 'wrap', marginLeft: 20}}>
          <Text style={{ color: '#1EA6CC', fontSize: 14, fontWeight: 'bold',}}>{Math.round((l.SelectionCount* 100) / this.state.poll.Question.ResponseCount)}%</Text>
          </View>
          <View style={{opacity: 0.4, borderRadius: 5,margin: 0,position: 'absolute', top: 0, left: 0, bottom: 0, backgroundColor: '#1EA6CC', width: Math.round((((l.SelectionCount* 100) / this.state.poll.Question.ResponseCount) / 100) * (Screen.width - 80))}}></View>
        </View>
        :
      <TouchableOpacity key={i} onPress={() => {this.submitAnswer(l.PollChoiceId)}}>
      <View style={[styles.shadow, {flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 5, margin: 5,marginBottom: 10, padding: 12, width: Screen.width - 80}]}>
        <Text style={{textAlign: 'center', color: '#1EA6CC', fontSize: 16, fontWeight: '400' }}>{l.Choice}</Text>
      </View>
      </TouchableOpacity>
    )
  }

  truncate(string, stringLength){
    if (string.length > stringLength)
       return string.substring(0,stringLength)+'...';
    else
       return string;
  }

  render() {
    
    return (
      <View style={styles.container}>
      <SafeAreaView>
      <LinearGradient
          start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
          locations={[0.2, 0.6, 1]}
          colors={['#1EA6CC', '#2FBCDA', '#3FD1E7']}
          style={{}}>

          <View style={{ height: 64, padding: 10, justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', backgroundColor: 'transparent' }}>
            <TouchableOpacity onPress={() => Actions.pop()} underlayColor='#999' style={{width: 42}}>
              <View style={{ marginTop: 12 }}>
                <Icon style={{opacity: 1}} name='chevron-left' size={22} color='#fff' />
              </View>
            </TouchableOpacity>
            <Text style={{ color: '#fff', fontSize: 20, marginTop: 12 }}>Poll</Text>
            <TouchableOpacity onPress={() => Actions.Search()} underlayColor='#999'>
              <View style={{ marginTop: 12 }}>
                <Image source={require('../../assets/search.png')} style={{ resizeMode: 'contain', width: 22, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
        </SafeAreaView>
        {this.state.showAnswers
        ?
        <View style={styles.cardContainer}>
        <View style={[styles.card, {elevation: this.state.showResults ? 0 : 2,}]}>
        
            <View style={{flex: 1}}>
            <View style={{paddingBottom: Platform.OS == 'ios' ? 0 : 20, backgroundColor: 'transparent',}} >
            <View style={{ marginBottom: Platform.OS == 'ios' ? 40 : 20, backgroundColor: this.state.backgroundColor ? this.state.backgroundColor : 'rgba(30, 166,204, 100)', padding: 10,borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}>
              <View style={{flexDirection: 'row', margin: 5, justifyContent: 'flex-start', alignItems: 'center'}}>
                <View style={{margin: 2,height: 24, width: 24, borderRadius: 12, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center'}}>
                  { 
                  this.state.poll.Question.Category == 'Entertainment' ?
                    <Image source={require('../../assets/categories/entertainment.png')} style={{resizeMode: 'contain', width: 20, height: 20}}/>
                    :
                    this.state.poll.Question.Category == 'Personality' ?
                    <Image source={require('../../assets/categories/personality.png')} style={{resizeMode: 'contain', width: 16, height: 20}}/>
                    :
                    this.state.poll.Question.Category == 'Food' ?
                    <Image source={require('../../assets/categories/food.png')} style={{resizeMode: 'contain', width: 20, height: 20}}/>
                    :
                    this.state.poll.Question.Category == 'Relationships' ?
                    <Image source={require('../../assets/categories/relationships.png')} style={{resizeMode: 'contain', width: 18, height: 16}}/>
                    :
                    this.state.poll.Question.Category == 'Politics' ?
                    <Image source={require('../../assets/categories/politics.png')} style={{resizeMode: 'contain', width: 18, height: 14}}/>
                    :
                  null
                  }
                </View>
                <Text style={{margin: 2, fontSize: 12, color: fontColorContrast(this.state.backgroundColor)}}>{this.state.poll.Question.Category}</Text>
              </View>
              <View style={{margin: 5, marginBottom: 20}}>
              <Text style={{ fontSize: 20, color: fontColorContrast(this.state.backgroundColor), fontWeight: 'bold', }}>
                {/* {this.truncate(this.state.question, 60)} */}
                {this.state.question}
              </Text>
              </View>
             
            </View>

            <View style={{zIndex: 600, position: 'absolute',left: 10,bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                {
                    this.state.poll.RecentThreeFollowing[0] ?
                <TouchableOpacity style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999' rejectResponderTermination>
                  
                      {
                    this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View> 
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>

                  <View style={{zIndex: 700, position: 'absolute',left: 42, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[1] ?
                    <TouchableOpacity style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999' rejectResponderTermination>
                  
                      {
                    this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[1].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[1].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  <View style={{zIndex: 800, position: 'absolute',left: 74, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  {
                    this.state.poll.RecentThreeFollowing[2] ?
                    <TouchableOpacity style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999' rejectResponderTermination>
                  
                      {
                    this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.poll.RecentThreeFollowing[2].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.poll.RecentThreeFollowing[2].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  {
                      this.state.poll.FollowingResponseCount > 3 ?
                  <View style={{zIndex: 900, position: 'absolute',left: 106, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                    <TouchableOpacity style={{ zIndex: 200 }} onPress={() => { Actions.FriendResponses({ pollName: this.state.poll.Question.UrlSegment })  }} underlayColor='#999' rejectResponderTermination>
                   

                        <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                          <Text style={{ color: '#fff' }}>{this.state.poll.FollowingResponseCount - 3}+</Text>
                        </View>
                       
                    </TouchableOpacity>
                </View>
                 :
                 null
             }


                          <View style={{elevation: 4, zIndex: 800, position: 'absolute',right: 74, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                  <TouchableOpacity style={{zIndex: 300}} onPress={() => { Actions.Map({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999' rejectResponderTermination>
                              <View style={[{elevation: 4, right: 0, padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                                <Image source={require('../../assets/map.png')} style={{resizeMode: 'contain', width: 25, height: 25}}/>
                              </View>
                              </TouchableOpacity>
                              
                            </View>
                            <View style={{elevation: 4, zIndex: 700, position: 'absolute', right: 42, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            <TouchableOpacity style={{zIndex: 200}} onPress={() => { Actions.PollComments({type: 'replace', pollName: this.state.poll.Question.UrlSegment})}} underlayColor='#999' rejectResponderTermination>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/chat.png')} style={{width: 22, height: 22}}/>
                            </View>
                            </TouchableOpacity>
                            </View>
                            <View style={{elevation: 4, zIndex: 600, position: 'absolute', right: 10, bottom: 15, flexDirection: 'row',  backgroundColor: 'transparent' }}>
                            {this.state.poll.Question.Reacted == 0 ?
                            
                            <TouchableOpacity style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999' rejectResponderTermination>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Image source={require('../../assets/heart-white.png')} style={{width: 15, height: 15}}/>
                            </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={{zIndex: 100}}  onPress={() => { this.open(this.state.poll.Question.PollId, 'PO')  }} underlayColor='#999' rejectResponderTermination>
                            <View style={[{elevation: 4, right: 0,padding: 5,margin: 2,backgroundColor: '#30415F', borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                              <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 16, paddingBottom: 6, paddingLeft: 3}}>{this.state.poll.Question.Emoticon}</Text>
                            </View>
                            </TouchableOpacity>
                            
                          }
                          </View>
            </View>
            { this.state.IsAnswered ? null
            : 
             Platform.OS == 'ios' ?
            <View style={[styles.shadow,{margin: 20, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 0}]}>
            
            <TouchableOpacity style={[ this.state.selected == 'PR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'PR'})}}>
            
              <Icon style={{opacity: 1,margin: 2}} name='lock' size={18} color= {this.state.selected == 'PR' ? '#fff' : '#1EA6CC'} />
              
            </TouchableOpacity>
            
              <TouchableOpacity style={[this.state.selected == 'FR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'FR'})}}>
              
                <Text style={{opacity: 1, margin: 2,color:  this.state.selected == 'FR' ? '#fff' : '#1EA6CC'}}>Friends Only</Text>
              
              </TouchableOpacity>
              <TouchableOpacity style={[this.state.selected == 'PU' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'PU'})}}>
              
                <Icon style={{opacity: 1,margin: 2}} name='globe' size={18} color= {this.state.selected == 'PU' ? '#fff' : '#1EA6CC'} />
              
              </TouchableOpacity>
            
            </View>
            :
            <CardView
              cardElevation={2}
              cardMaxElevation={2}
              cornerRadius={0}
              style={{backgroundColor: '#fff', margin: 20, marginVertical: 10, justifyContent: 'center', alignItems: 'center',}}
              >
            <View style={[{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 0}]}>
            
            <TouchableOpacity style={[ this.state.selected == 'PR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'PR'})}}>
            
              <Icon style={{opacity: 1,margin: 0,paddingBottom: 5}} name='lock' size={18} color= {this.state.selected == 'PR' ? '#fff' : '#1EA6CC'} />
              
            </TouchableOpacity>
            
              <TouchableOpacity style={[this.state.selected == 'FR' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'FR'})}}>
              
                <Text style={{opacity: 1, margin: 0,paddingBottom: 5,color:  this.state.selected == 'FR' ? '#fff' : '#1EA6CC'}}>Friends Only</Text>
              
              </TouchableOpacity>
              <TouchableOpacity style={[this.state.selected == 'PU' ? styles.selected : styles.normal ,{}]} onPress={() => {this.setState({selected: 'PU'})}}>
              
                <Icon style={{opacity: 1,margin: 0,paddingBottom: 5}} name='globe' size={18} color= {this.state.selected == 'PU' ? '#fff' : '#1EA6CC'} />
              
              </TouchableOpacity>
            
            </View> 
            </CardView>
           
            }
            <ScrollView>
        <View style={{margin: 10, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {
            this.state.listViewData.map((l,i) => (
              this.renderItem(l,i)
            ))
          }
        </View>
        </ScrollView>
       
      </View>
          
          
        <Spinner visible={this.state.visible} textContent={""} textStyle={{color: '#FFF'}} />
        </View>
        </View>
        : 
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
      </View>
       
        }

<Modal
          open={this.state.showResults}
          offset={0}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({
            showResults: false
          },
          () => {
            this.getNextQuestion();
          }
          )
          }
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center',
            paddingTop: Platform.OS === 'ios' ? 75 : 65,
          }}
          modalStyle={{
            flex: 1,
            borderRadius: 15,
            margin: 20,
            padding: 0,
            elevation: 5,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
         
         
      <View style={{elevation: 5,flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <View style={{alignSelf: 'flex-end', margin: 5, marginHorizontal: 10, borderBottomColor: '#444', borderBottomWidth: 1}}>
  <TouchableOpacity  onPress={() => {this.setState({showResults: false})}}>
    <Text>Next Question</Text>
  </TouchableOpacity>
  </View>
    <Results {...this.props} isPopup={true} pollName={this.state.poll ? this.state.poll.Question.UrlSegment : ''}/>
      </View>
      
        </Modal>
        <ModalWrapper
          style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
          visible={this.state.likeContainerVisible}
          onRequestClose={() => this.setState({ likeContainerVisible: false })}
          shouldCloseOnOverlayPress={true}
          >
                      <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                          {
                          this.props.reactionTypes.map((l, i) => (
                              <TouchableOpacity key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                  <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                              </TouchableOpacity>
                          ))
                        }

                      </View>
      </ModalWrapper>
        </View>
       );
  }

  /* <ActionButton
              buttonText="Skip"
              buttonTextStyle={{fontSize: 13}}
              buttonColor="#49abde"
              onPress={() => { //this.getNextQuestion()
              Actions.Comments();
              }}
          /> */
  
  renderSeparator(option){
    return (
      <View></View>
    )
  }

  renderText(option) {
    return (
      <Text style={{ color: '#696868', fontSize: 16, marginTop: 10, marginBottom: 10, marginLeft: 10 }}>{option}</Text>
    )
  }

  renderIndicator() {
    return (
      <Icon name='check-circle' size={20} color='#49abde' style={NavStyles.icon} />
    )
  }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.main,
        //marginBottom: 50,
        //marginTop: Platform.OS === 'ios' ? 65 : 65,
      },
      cardContainer: {backgroundColor:Colors.gray, flex:1,},
      card:{
        flex: 1,
        margin: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor: '#ffffff',
        borderRadius: 15,
        borderColor: '#ffffff',
        borderWidth: 0,
        shadowColor: 'rgba(0, 0, 0, 0.40)',
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
          height: 5,
          width: 2,
        },
        
      },
      shadow:{
        shadowColor: '#999',
        shadowOpacity: 0.6,
        shadowRadius: 6,
        shadowOffset: {
          height: 1,
          width: 2,
        },
        elevation: 2
      },
      shadowDark:{
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowOffset: {
          height: 3,
          width: 0,
        },
      },
      normal: {borderRightWidth: 0.5, borderRightColor: 'rgba(0, 0, 0, 0.20)', flexGrow:1, justifyContent: 'center',padding: 5,paddingTop: 8, paddingBottom: 8,paddingLeft: 15, paddingRight: 15, alignItems: 'center'},
      selected: {flexGrow:1,backgroundColor: '#1EA6CC',padding: 5,paddingTop: 8,paddingBottom: 8, paddingLeft: 15, paddingRight: 15,justifyContent: 'center', alignItems: 'center'},
    imageContainer: {
    width: undefined,
    //height: Platform.OS === 'ios'? 64 : 54, 
    backgroundColor: '#f6f6f6',
    paddingTop: Platform.OS === 'ios'? 30 : 30,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  standalone: {
		marginTop: 30,
		marginBottom: 30,
	},
	standaloneRowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'center',
		height: 50,
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15
	},
	backTextWhite: {
		color: '#FFF'
	},
	rowFront: {
		alignItems: 'center',
		backgroundColor: '#fff',
		borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		justifyContent: 'center',
		height: 50,
    marginBottom: 5
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
    borderBottomColor: '#a7a7aa',
		borderBottomWidth: 0.5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
    marginBottom: 5
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 50
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: '#fff',
		right: 0,
	},



})

const NavStyles = StyleSheet.create({
  statusBar: {
    backgroundColor: '#49abde',
    opacity: 0.7
  },
  navBar: {
    backgroundColor: '#49abde',
    borderBottomColor: '#49abde'
  },
  icon: {
    justifyContent: 'center',

  },
  title: {
    color: '#fff',
    fontWeight: 'bold'
  },
  buttonText: {
    color: 'rgba(231, 37, 156, 0.5)',
  },
  navButton: {
    flex: 1,
    justifyContent: 'center',
    width: 50,
  },
  image: {
    width: 30,
  },
})


function mapStateToProps(state) {

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    currentQuestion: state.currentQuestion.question,
    poll: state.currentQuestion.question,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
    currentAnswers: state.currentAnswers.answers,
    submissionStatus: state.submissionStatus.submissionStatus,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    responseVisibility: state.SET_RESPONSE_SETTINGS.responseVisibility,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
  }
}

export default connect(mapStateToProps)(Poll);
