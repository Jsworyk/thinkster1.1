import React, { Component, PropTypes } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
var Spinner = require('react-native-spinkit');
//import FBLoginView from '../components/FBLoginView';
//import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import PushNotificationController from "./PushNotificationController";
const {
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
  AlertIOS,
  AsyncStorage,
  TouchableHighlight,
  StyleSheet,

} = ReactNative



class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    }
  }

  searchPressed() {
    //this.props.toggleDrawer();
    //Actions.Profile();
  }

  componentDidMount() {

    // AsyncStorage.getItem('reduxPersist:saveApplicationToken',(err, res) => {
    //     if (res == null){
    //       this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', 'temp123').then(() => {
    //         if (!this.props.isLoggedin){
    //           Actions.Signin({ type: 'reset' });
    //         }else {
    //           this.props.toggleDrawer(false);
    //           Actions.tabbar({ type: 'reset' });
    //         }
    //       });
    //     }else{
    //       var result = JSON.parse(res);
    //       if (result.applicationToken == null){
    //         this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', 'temp123').then(() => {
    //           if (!this.props.isLoggedin){
    //             Actions.Signin({ type: 'reset' });
    //           }else {
    //             this.props.toggleDrawer(false);
    //             Actions.tabbar({ type: 'reset' });
    //           }
    //         });
    //       }else{
    //         this.props.checkApplicationToken(this.props.applicationToken).then(() => {
    //           if (!this.props.applicationTokenStatus){
    //             this.props.getApplicationToken('b7b77c851cbfe5d23e3de48b5eed2c67','8d113b2412c13ccf445fcd7ae6666a96', 'temp123').then(() => {
    //               if (!this.props.isLoggedin){
    //                 Actions.Signin({ type: 'reset' });
    //               }else {
    //                 this.props.toggleDrawer(false);
    //                 Actions.tabbar({ type: 'reset' });
    //               }
    //             });
    //           }else{
    //             if (!this.props.isLoggedin){
    //               Actions.Signin({ type: 'reset' });
    //             }else {
    //               this.props.toggleDrawer(false);
    //               Actions.tabbar({ type: 'reset' });
    //             }
    //           }
    //         });
    //       }
    //     }
    // })

  }

  render() {
    return (
      <View style={styles.container}>
        <PushNotificationController {...this.props}/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <Spinner isVisible={true} size={40} type='WanderingCubes' color='#fff' />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 0,
    padding: 0,
    backgroundColor: '#1FA8CD',
    flex: 1,
    flexDirection: 'column'
  }
});

function mapStateToProps(state) {
  return {
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationTokenStatus: state.SET_APPLICATION_TOKEN_STATUS.applicationTokenStatus,
    applicationToken: state.saveApplicationToken.applicationToken
  }
}

export default connect(mapStateToProps)(Home);
