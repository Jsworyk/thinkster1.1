/**
  * Represents an Application Token Response
  * @typedef ApplicationToken
  *
  * @property {boolean} [Success] -
  * @property {string} [Message] -
  * @property {string} [ApplicationToken] -
  */


import * as types from './types';
import Api from '../lib/api'
import axios from 'axios'
import Config from 'react-native-config'


export function toggleDrawer(drawerState){
  return {
    type: types.TOGGLE_DRAWER,
    drawerState: drawerState
  }
}

export function selectedQuestion(question){
  return {
    type: types.SELECTED_QUESTION,
    question: question
  }
}

export function selectedAnswers(answers){
  return {
    type: types.SELECTED_ANSWERS,
    answers: answers
  }
}

export function addNotification(notification){
  return {
    type: types.ADD_NOTIFICATION,
    notification: notification
  }
}

export function addChatroom(chatroom){
  return {
    type: types.ADD_CHATROOM,
    chatroom: chatroom
  }
}

export function signup(ApplicationToken, FirstName, LastName, EmailAddress, Password, CountryCode, PostalCode, BirthDate, GenderCode) {
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/signup', {
      ApplicationToken: ApplicationToken,
      FirstName: FirstName,
      LastName: LastName,
      EmailAddress: EmailAddress,
      Password: Password,
      CountryCode: CountryCode,
      PostalCode: PostalCode,
      BirthDate: BirthDate,
      GenderCode: GenderCode
    })
      .then(function (response) {
        if (response.data.Success == true) {
          return true;
        } else {
          if (response.data.Code == 'ValidationFailure'){
            return response.data.Validation;
          }else {
            return [{Argument: '', Message: response.data.Message}];
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function login(applicationToken, email, password) {
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/thinksterlogin', {
      ApplicationToken: applicationToken,
      EmailAddress: email,
      Password: password
    })
      .then(function (response) {
        
        if (response.data.Success == true) {
          dispatch(setLoggedinStatus(true));
          //dispatch(currentUser(response.data));
        } else {
          dispatch(setLoggedinStatus(false));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function loginWithFacebook(applicationToken, accessToken) {
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/facebooklogin', {
      ApplicationToken: applicationToken,
      AccessToken: accessToken
    })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setLoggedinStatus(true));
          dispatch(currentUser(response.data));
        } else {
          dispatch(setLoggedinStatus(false));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function checkApplicationToken(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/checkapplicationtoken', {
      ApplicationToken: applicationToken
    })
      .then(function (response) {
        
        if (response.data.Code == "InvalidToken") {
          dispatch(setApplicationTokenStatus(false));
          dispatch(setLoggedinStatus(false));
        } else {
          dispatch(saveApplicationToken(applicationToken));
          dispatch(setApplicationTokenStatus(true));
          dispatch(setLoggedinStatus(response.data.LoggedIn));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getPasswordResetCode(ApplicationToken, ProfileName){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getpasswordresetcode', {
      ApplicationToken: ApplicationToken,
      ProfileName: ProfileName
    })
      .then(function (response) {
        
        if (response.data.Success == true) {
          dispatch(setClientCode(response.data.ClientCode))
          return true;
        }else {
          return response.data.Message;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setClientCode(ClientCode) {
  return {
    type: types.CLIENT_CODE,
    ClientCode: ClientCode
  }
}

export function resetPassword(ApplicationToken, ClientCode, ResetCode, NewPassword){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/resetpassword', {
      ApplicationToken: ApplicationToken,
      ClientCode: ClientCode,
      ResetCode: ResetCode,
      NewPassword: NewPassword
    })
      .then(function (response) {
        if (response.data.Success == true) {
          return true;
        }else {
          if (response.data.Code == 'ValidationFailure'){
            return response.data.Validation;
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function flagContent(ApplicationToken, TargetTypeCode, TargetId, FlagTypeCode, Details){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/flagcontent', {
      ApplicationToken: ApplicationToken,
      TargetTypeCode: TargetTypeCode,
      TargetId: TargetId,
      FlagTypeCode: FlagTypeCode,
      Details: Details
    })
      .then(function (response) {
        if (response.data.Success == true) {
          return true;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

/**
 *  
 * @param {*} ApplicationId 
 * @param {*} ApplicationSecret 
 * @param {*} FCMToken 
 * @returns {ApplicationToken}
 */
export function getApplicationToken(ApplicationId, ApplicationSecret, FCMToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getapplicationtoken', {
      ApplicationId: ApplicationId,
      ApplicationSecret: ApplicationSecret,
      FCMToken: FCMToken
    })
      .then(function (response) {
       // alert(JSON.stringify(response.data))
        if (response.data.Success == true) {
          dispatch(setApplicationTokenStatus(true));
          dispatch(saveApplicationToken(response.data.ApplicationToken));
          dispatch(setLoggedinStatus(false));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setApplicationTokenStatus(applicationTokenStatus) {
  return {
    type: types.SET_APPLICATION_TOKEN_STATUS,
    applicationTokenStatus: applicationTokenStatus
  }
}

export function setLoggedinStatus(loggedin) {
  return {
    type: types.SET_LOGGEDIN_STATUS,
    isLoggedin: loggedin
  }
}

export function saveApplicationToken(applicationToken){
  return {
    type: types.SAVE_APPLICATION_TOKEN,
    applicationToken: applicationToken
  }
}

export function currentUser(user){
  return {
    type: types.CURRENT_USER,
    ProfileId: user.ProfileId,
    DisplayName: user.DisplayName,
    AvatarUrl: user.AvatarUrl
  }
}

export function setUser(user){
  return {
    type: types.SET_USER,
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
    city: user.city,
    birthdate: user.birthdate
  }
}

export function logout(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/signout', {
      ApplicationToken: applicationToken
    })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setLoggedinStatus(false));
          dispatch(currentUser({}));
          dispatch(setProfileResponses(null))

        } else {
          dispatch(setLoggedinStatus(true));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function nextQuestion(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/nextquestion', {
          ApplicationToken: applicationToken
      })
      .then(function (response) {
        console.log(response.data)
        if (response.data.Success) {
          //Stores new Question to the currentQuestion Prop
          dispatch(currentQuestion(response.data.Question));
          // Stores new Choices to the currentAnswers Prop
          dispatch(currentAnswers(response.data.Choices));
          return response.data;
        }else {
          if (response.data.Code == 'NoQuestions'){
            dispatch(currentQuestion(null));
          dispatch(currentAnswers(null));
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function currentQuestion(question){
  return {
    type: types.CURRENT_QUESTION,
    question: question
  }
}

export function currentAnswers(answers){
  return {
    type: types.CURRENT_ANSWERS,
    answers: answers
  }
}

export function submitAnswer(applicationToken, PollId, Response, ResponseVisibility = 'PU'){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/submitanswer', {
        ApplicationToken: applicationToken,
        PollId: PollId,
        //PollChoiceId: PollChoiceId,
        Response: Response,
        ResponseVisibility: ResponseVisibility
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(submissionStatus(true));
        }else{
          dispatch(submissionStatus(false));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function submissionStatus(submissionStatus){
  return {
    type: types.SUBMISSION_STATUS,
    submissionStatus: submissionStatus
  }
}

export function getComments(applicationToken, targetTypeCode, targetId, replyToCommentId, lastCommentId, numberOfComments){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getcomments', {
          ApplicationToken: applicationToken,
          TargetTypeCode: targetTypeCode,
          TargetId: targetId,
          LastCommentId: lastCommentId,
          ReplyToCommentId: replyToCommentId,
          NumberOfComments: numberOfComments
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
          if (replyToCommentId == ''){
            dispatch(setComments(response.data.Comments));
            dispatch(setTotal(response.data.TotalRecords));
          }else {
            dispatch(setReplies(response.data.Comments,replyToCommentId));
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function comment(applicationToken, TargetTypeCode, targetId, replyToCommentId, commentText){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/Comment', {
          ApplicationToken: applicationToken,
          TargetTypeCode: TargetTypeCode,
          TargetId: targetId,
          ReplyToCommentId: replyToCommentId,
          CommentText: commentText
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getImage(applicationToken, Selector){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getimage', {
          ApplicationToken: applicationToken,
          Selector: Selector
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setAvatar(applicationToken, MediaId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/setavatar', {
          ApplicationToken: applicationToken,
          MediaId: MediaId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          //return response.data.Image;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setBanner(applicationToken, MediaId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/setbanner', {
          ApplicationToken: applicationToken,
          MediaId: MediaId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          //return response.data.Image;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getReactionTypes(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getreactiontypes', {
          ApplicationToken: applicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setReactionTypes(response.data.ReactionTypes))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function React(applicationToken, targetTypeCode, targetId, reactionTypeCode){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/react', {
          ApplicationToken: applicationToken,
          TargetTypeCode: targetTypeCode,
          TargetId: targetId,
          ReactionTypeCode: reactionTypeCode
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setComments(comments){
  return {
    type: types.COMMENTS,
    comments: comments
  }
}

export function setTotal(total){
  return {
    type: types.TOTAL,
    total: total
  }
}

export function setReplies(replies, id){
  return {
    type: types.REPLIES,
    replies: replies,
    id: id
  }
}

export function setReactionTypes(reactionTypes){
  return {
    type: types.REACTION_TYPES,
    reactionTypes: reactionTypes
  }
}

export function getPollResponses(applicationToken, ProfileId, PageNumber = 1, PageSize = 10, ExposureType = 'PU'){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getpollresponses', {
          ApplicationToken: applicationToken,
          PageNumber: PageNumber,
          PageSize: PageSize,
          ProfileId: ProfileId,
          ExposureType: ExposureType
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setPollResponses(response.data.Responses));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function followerLookup(applicationToken,namePrefix, MaxResults = 5){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/followerlookup', {
          ApplicationToken: applicationToken,
          MaxResults: MaxResults,
          NamePrefix: namePrefix,
      })
      .then(function (response) {
        console.log(response);
        if (response.data.Success == true) {
          return response.data.Matches;
        }
      })
      .catch(function (error) {
        throw new Error("Follower Lookup Failed");
        // handle errors here.
      });
  }
}


export function suggestFollowers(applicationToken, MaxResults = 20, PollCategoryId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/suggestfollowers', {
          ApplicationToken: applicationToken,
          MaxResults: MaxResults,
          PollCategoryId: PollCategoryId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data.Suggestions;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setPollResponses(pollResponses){
  return {
    type: types.SET_POLL_RESPONSES,
    pollResponses: pollResponses
  }
}

export function setResponseSettings(responseVisibility){
  return {
    type: types.SET_RESPONSE_SETTINGS,
    responseVisibility: responseVisibility
  }
}

export function getChatrooms(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getchatrooms', {
          ApplicationToken: applicationToken
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setChatrooms(response.data.ChatRooms));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getChatMessages(applicationToken, chatRoomId, startingMessageId = null, numOfMessages = 10){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getchatmessages', {
          ApplicationToken: applicationToken,
          ChatRoomId: chatRoomId,
          StartingMessageId: startingMessageId,
          NumberOfMessages: numOfMessages
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data.Messages;
          //dispatch(setChatMessages(response.data.Messages));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function updateReadDate(applicationToken, chatRoomId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/updatereaddate', {
          ApplicationToken: applicationToken,
          ChatRoomId: chatRoomId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function sendMessage(applicationToken, destination, message){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/sendmessage', {
          ApplicationToken: applicationToken,
          Destination: destination,
          Message: message,
          
      })
      .then(function (response) {
        
        if (response.data.Success == true) {
          //dispatch(setChatMessages(response.data.Messages));
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setChatrooms(chatrooms){
  return {
    type: types.CHATROOMS,
    chatrooms: chatrooms
  }
}

export function setChatMessages(messages){
  return {
    type: types.SET_CHAT_MESSAGES,
    messages: messages
  }
}

export function setLastSendingMessage(ProfileId, DisplayName, AvatarUrl, message){
  return {
    type: types.SET_LAST_SENDING_MESSAGE,
    ProfileId: ProfileId,
    DisplayName: DisplayName,
    AvatarUrl: AvatarUrl,
    message: message
  }
}

export function addFriend(applicationToken, profileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/addfriend', {
          ApplicationToken: applicationToken,
          ProfileId: profileId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getFriends(applicationToken, pageNumber, pageSize){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/friendlist', {
          ApplicationToken: applicationToken,
          PageNumber: pageNumber,
          PageSize: pageSize
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setFriends(response.data.Friends))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setFriends(friends){
  return {
    type: types.SET_FRIENDS,
    friends: friends
  }
}

export function getPendingReceived(applicationToken, maxResults){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/pendingfriendrequests', {
          ApplicationToken: applicationToken,
          MaxResults: maxResults,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setPendingReceived(response.data.Friends))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setPendingReceived(pendingReceived){
  return {
    type: types.SET_PENDING_RECEIVED,
    pendingReceived: pendingReceived
  }
}

export function confirmFriendRequest(applicationToken, profileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/confirmfriendrequest', {
          ApplicationToken: applicationToken,
          ProfileId: profileId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function declineFriendRequest(applicationToken, profileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/declinefriendrequest', {
          ApplicationToken: applicationToken,
          ProfileId: profileId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getProfileResponses(ApplicationToken, ProfileId, FromTimestamp, Count, isCurrentUser, direction){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getprofileresponses', {
          ApplicationToken: ApplicationToken,
          ProfileId: ProfileId,
          FromTimestamp: FromTimestamp,
          Count: Count
      })
      .then(function (response) {
        
        if (response.data.Success == true) {
          if (isCurrentUser){
            if (direction == 'bottom'){
              
                dispatch(addProfileResponsesItemsBottom(response.data.Items))
              
            }else{
              dispatch(setProfileResponses(response.data.Items))
            }
            
          }else {
            if (direction == 'bottom'){
              
              dispatch(addOtherProfileResponsesItemsBottom(response.data.Items))
            
          }else{
            dispatch(setOtherProfileResponses(response.data.Items))
          }
            //return response.data.Items;
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getfollowerquestionslist(ApplicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getfollowerquestionslist', {
          ApplicationToken: ApplicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data.Questions;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function togglefollowerquestion(ApplicationToken, PollId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/togglefollowerquestion', {
          ApplicationToken: ApplicationToken,
          PollId: PollId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getCurrentUserProfile(applicationToken, profileName){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getprofile', {
          ApplicationToken: applicationToken,
          ProfileName: profileName
      })
      .then(function (response) {
        if (response.data.Success == true) {
          if (profileName == ''){
          dispatch(setCurrentUserProfile(response.data.Profile))
          }else {
            return response.data.Profile;
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getFollowingCompatibility(applicationToken, PollCategoryId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getfollowingcompatibility', {
          ApplicationToken: applicationToken,
          PollCategoryId: PollCategoryId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setFollowingCompatibility(response.data.Compatibility))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function follow(applicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/follow', {
          ApplicationToken: applicationToken,
          ProfileId: ProfileId
      })
      .then(function (response) {
        
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function pass(applicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/pass', {
          ApplicationToken: applicationToken,
          ProfileId: ProfileId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function unfollow(applicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/unfollow', {
          ApplicationToken: applicationToken,
          ProfileId: ProfileId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function block(applicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/block', {
          ApplicationToken: applicationToken,
          ProfileId: ProfileId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getMapFilters(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getmapfilters', {
          ApplicationToken: applicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setMapFilters(response.data.Filters))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getCompatibility(applicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getcompatibility', {
          ApplicationToken: applicationToken,
          ProfileId: ProfileId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data.Compatibility;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function deleteNotification(applicationToken, NotificationId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/deletenotification', {
          ApplicationToken: applicationToken,
          NotificationId: NotificationId
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function deleteImage(ApplicationToken, MediaId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/deleteimage', {
      ApplicationToken: ApplicationToken,
      MediaId: MediaId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function postImage(ApplicationToken, MediaId, Name, Description, ExposureTypeCode, UseAsAvatar){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/postimage', {
      ApplicationToken: ApplicationToken,
      MediaId: MediaId,
      Name: Name,
      Description: Description,
      ExposureTypeCode: ExposureTypeCode,
      UseAsAvatar: UseAsAvatar
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function markNotificationRead(ApplicationToken, NotificationId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/marknotificationread', {
      ApplicationToken: ApplicationToken,
      NotificationId: NotificationId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function markAllNotificationsRead(ApplicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/markallnotificationsread', {
      ApplicationToken: ApplicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getPollMapData(ApplicationToken, PollId, Bounds, Zoom, VisualizationTypeCode, OmitRegions, Filters){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getpollmapdata', {
      ApplicationToken: ApplicationToken,
      PollId: PollId,
      Bounds: Bounds,
      Zoom: Zoom,
      VisualizationTypeCode: VisualizationTypeCode,
      OmitRegions: OmitRegions,
      Filters: Filters
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setMapData(response.data.MapData))
          return response.data.MapData;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

/**
 * 
 * @param {*} ApplicationToken 
 * @returns {}
 */
export function getPollCategories(ApplicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getpollcategories', {
      ApplicationToken: ApplicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setPollCategories(response.data.Categories))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getProfileImages(ApplicationToken, ProfileId, FromTimestamp, Count, isCurrentUser){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getprofileimages', {
      ApplicationToken: ApplicationToken,
      ProfileId: ProfileId,
      FromTimestamp: FromTimestamp,
      Count: Count
      })
      .then(function (response) {
        if (response.data.Success == true) {
          if (isCurrentUser){
          dispatch(setProfileImages(response.data.Images))
          }else {
            return response.data.Images;
          }
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getReactors(ApplicationToken, TargetTypeCode, TargetId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getreactors', {
      ApplicationToken: ApplicationToken,
      TargetTypeCode: TargetTypeCode,
      TargetId: TargetId
      })
      .then(function (response) {
        
        if (response.data.Success == true) {   
          return response.data.Reactors;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getFollowers(ApplicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getfollowers', {
      ApplicationToken: ApplicationToken,
      ProfileId: ProfileId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          if (ProfileId == ""){
            dispatch(setFollowers(response.data.Users))
          }
          return response.data.Users;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getFollowing(ApplicationToken, ProfileId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getfollowing', {
      ApplicationToken: ApplicationToken,
      ProfileId: ProfileId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          if (ProfileId == ""){
            dispatch(setFollowing(response.data.Users))
          }
          return response.data.Users;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setFollowers(followers){
  return {
    type: types.FOLLOWERS,
    followers: followers
  }
}

export function setFollowing(following){
  return {
    type: types.FOLLOWING,
    following: following
  }
}

export function nextSuggestedPerson(ApplicationToken, Count){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/nextsuggestedperson', {
      ApplicationToken: ApplicationToken,
      Count: Count,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          
           // dispatch(setNextSuggestedPerson({Suggestion: response.data.Suggestion, Compatibility: response.data.Compatibility}))
           dispatch(setNextSuggestedPerson({Suggestions: response.data.Suggestions}))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

// export function setNextSuggestedPerson(data){
//   return {
//     type: types.NEXT_SUGGESTED_PERSON,
//     Suggestion: data.Suggestion,
//     Compatibility: data.Compatibility
//   }
// }

export function setNextSuggestedPerson(data){
  return {
    type: types.NEXT_SUGGESTED_PERSON,
    Suggestions: data.Suggestions,
    //Compatibility: data.Compatibility
  }
}

export function setPollCategories(pollCategories){
  return {
    type: types.POLL_CATEGORIES,
    pollCategories: pollCategories
  }
}

export function setMapFilters(mapFilters){
  return {
    type: types.MAP_FILTERS,
    mapFilters: mapFilters
  }
}

export function setMapData(mapData){
  return {
    type: types.MAP_DATA,
    mapData: mapData
  }
}

export function setProfileImages(items){
  return {
    type: types.PROFILE_IMAGES,
    profileImages: items
  }
}

export function setFollowingCompatibility(items){
  return {
    type: types.FOLLOWING_COMPATIBILITY,
    followingCompatibility: items
  }
}

export function setProfileResponses(items){
  return {
    type: types.PROFILE_RESPONSES,
    profileResponses: items
  }
}

export function setOtherProfileResponses(items){
  return {
    type: types.OTHER_PROFILE_RESPONSES,
    profileResponses: items
  }
}

export function setCurrentUserProfile(profile){
  return {
    type: types.CURRENT_USER_PROFILE,
    currentUserProfile: profile
  }
}

export function getAvailableCountries(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getavailablecountries', {
          
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setAvailableCountries(response.data.Countries))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setAvailableCountries(Countries){
  return {
    type: types.COUNTRIES,
    Countries: Countries
  }
}

export function getProfileImageUploadToken(applicationToken){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getprofileimageuploadtoken', {
          ApplicationToken: applicationToken,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setProfileImageUploadToken(response.data.Token))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getTimeline(applicationToken, FromTimestamp, Count, direction){
  
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/gettimeline', {
          ApplicationToken: applicationToken,
          FromTimestamp: FromTimestamp,
          Count: Count,
          IncludeImages: true
      })
      .then(function (response) {
        
        if (response.data.Success == true) {
          if (direction == 'bottom'){
            dispatch(addTimelineItemsBottom(response.data.Items))
            
          }else if (direction == 'top'){
            
            dispatch(addTimelineItemsTop(response.data.Items))
            
          }else{
            dispatch(setTimeline(response.data.Items))
            
          }
          dispatch(setNewsFeedLastTimestamp(response.data.Items[response.data.Items.length - 1].Timestamp))
          dispatch(setNewsFeedFirstTimestamp(response.data.Items[0].Timestamp))
          
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setNotificationsLastTimestamp(FromTimestamp){
  return {
    type: types.NOTIFICATIONS_LAST_TIMESTAMP,
    FromTimestamp: FromTimestamp
  }
}

export function setNewsFeedLastTimestamp(FromTimestamp){
  return {
    type: types.NEWS_FEED_LAST_TIMESTAMP,
    FromTimestamp: FromTimestamp
  }
}

export function setNewsFeedFirstTimestamp(FromTimestamp){
  return {
    type: types.NEWS_FEED_FIRST_TIMESTAMP,
    FromTimestamp: FromTimestamp
  }
}

export function getNotifications(applicationToken, FromTimestamp, Count, direction){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getnotifications', {
          ApplicationToken: applicationToken,
          FromTimestamp: FromTimestamp,
          Count: Count
      })
      .then(function (response) {
        if (response.data.Success == true) {
          if (direction == 'bottom'){
            dispatch(addNotificationsBottom(response.data.Notifications))
          }else{
            dispatch(setNotifications(response.data.Notifications));
          }
            
           dispatch(setNotificationsLastTimestamp(response.data.Notifications[response.data.Notifications.length - 1].Timestamp))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function getFriendResponses(applicationToken, PollId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getfriendresponses', {
          ApplicationToken: applicationToken,
          PollId: PollId,
      })
      .then(function (response) {
        if (response.data.Success == true) {
          dispatch(setFriendResponses(response.data.Responses))
          dispatch(setTotal(response.data.ResponseCount))
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setFriendResponses(friendResponses){
  return {
    type: types.FRIEND_RESPONSES,
    friendResponses: friendResponses
  }
}

export function getPoll(applicationToken, PollId){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/getpoll', {
          ApplicationToken: applicationToken,
          PollId: PollId,
      })
      .then(function (response) {
        
        if (response.data.Success == true) {
          return response.data;
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function search(applicationToken, SearchString, Offset = '', Count = 10){
  return (dispatch, getState) => {
    return axios.post(Config.API_URL+'/search', {
          ApplicationToken: applicationToken,
          SearchString: SearchString,
          Offset: Offset,
          Count: Count
      })
      .then(function (response) {
        if (response.data.Success == true) {
          return response.data.SearchResults
        }
      })
      .catch(function (error) {
        // handle errors here.
      });
  }
}

export function setProfileImageUploadToken(imageUploadToken){
  return {
    type: types.IMAGE_UPLOAD_TOKEN,
    imageUploadToken: imageUploadToken
  }
}

export function setTimeline(items){
  return {
    type: types.TIMELINE,
    timelineItems: items
  }
}

export function addNotificationsBottom(items){
  return {
    type: types.ADD_NOTIFICATIONS_BOTTOM,
    notifications: items
  }
}

export function addTimelineItemsBottom(items){
  return {
    type: types.ADD_TIMELINE_ITEMS_BOTTOM,
    timelineItems: items
  }
}

export function addTimelineItemsTop(items){
  return {
    type: types.ADD_TIMELINE_ITEMS_TOP,
    timelineItems: items
  }
}

export function addProfileResponsesItemsBottom(items){
  return {
    type: types.ADD_PROFILE_RESPONSES_ITEMS_BOTTOM,
    profileResponses: items
  }
}

export function addOtherProfileResponsesItemsBottom(items){
  return {
    type: types.ADD_OTHER_PROFILE_RESPONSES_ITEMS_BOTTOM,
    profileResponses: items
  }
}

export function setNotifications(items){
  return {
    type: types.NOTIFICATIONS,
    notifications: items
  }
}

export function addReaction(index, delta){
  return {
    type: types.ADD_REACTION,
    index: index,
    delta: delta
  }
}

export function addTimelineReaction(index, data){
  return {
    type: types.ADD_TIMELINE_REACTION,
    index: index,
    data: data
  }
}

export function addCommentReaction(index, delta){
  return {
    type: types.ADD_COMMENT_REACTION,
    index: index,
    delta: delta
  }
}

export function addProfileResponseReaction(index, Emoticon){
  return {
    type: types.ADD_PROFILE_RESPONSE_REACTION,
    index: index,
    Emoticon: Emoticon
  }
}

export function addComment(comment){
  return {
    type: types.ADD_COMMENT,
    comment: comment,
  }
}

export function removeLocalNotification(index){
  return {
    type: types.REMOVE_NOTIFICATION,
    index: index
  }
}

export function removeLocalImage(index){
  return {
    type: types.REMOVE_LOCAL_IMAGE,
    index: index
  }
}

export function removeLocalUnreadNotification(){
  return {
    type: types.REMOVE_UNREAD_NOTIFICATION,
  }
}

export function showImagePostDialog(status,MediaId){
  return {
    type: types.IMAGE_POST_DIALOG,
    status: status,
    MediaId: MediaId
  }
}

