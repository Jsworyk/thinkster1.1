import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import ImageZoom from 'react-native-image-pan-zoom';
//import NavBar, { NavButton, NavButtonText, NavTitle } from 'react-native-nav'
import Icon from 'react-native-vector-icons/FontAwesome';
import ActionSheet from 'react-native-actionsheet'
import Helper from '../../lib/helper'

const {
  ScrollView,
  View,
  TextInput,
  Platform,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,

} = ReactNative

const CANCEL_INDEX = 0
const DESTRUCTIVE_INDEX = 1
const options = [ 'Cancel', 'Delete', 'Use as Avatar']

class Modal extends Component{

   constructor(props) {
    super(props)
    this.state = {
      selected: ''
    }
    this.onMenuItemPress = this.onMenuItemPress.bind(this)
    this.showActionSheet = this.showActionSheet.bind(this)
  }

  showActionSheet() {
    this.ActionSheet.show()
  }

  onMenuItemPress(i){
    
    if (i == 1){
       this.props.deleteImage(this.props.applicationToken, this.props.MediaId).then(() => {
        var index = Helper.findElement(this.props.profileImages, 'MediaId', this.props.MediaId, true);
        this.props.removeLocalImage(index);
        Actions.pop();
       })
        
    }else if (i == 2){
      this.props.setAvatar(this.props.applicationToken, this.props.MediaId).then(() => {
        
        this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
            
        });
        Actions.pop();
      });
    }
   }

  componentWillUnmount(){
    this.props.toggleDrawer(false);
  }

  componentWillMount() {
    if (this.props.MediaId != null){
      Actions.refresh({ renderRightButton: this._renderRightButton });
    }
}

_renderRightButton = () => {
    return(
        <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => this.showActionSheet() } >
            <Icon name="ellipsis-v" size={26} color='#fff' />
        </TouchableOpacity>
    );
};

  render(){
    return (
      <View style={{flex: 1, backgroundColor: '#000', marginTop: Platform.OS === 'ios'? 64 : 54}}>
       <StatusBar
     backgroundColor="#134fa1"
     setBarStyle={{borderBottomWidth: 0, opacity: 0.9}}
     barStyle="light-content"/>
     
        <View>
          
        <ImageZoom cropWidth={Dimensions.get('window').width}
                      cropHeight={Dimensions.get('window').height}
                      imageWidth={Dimensions.get('window').width}
                      imageHeight={300} 
                      onLongPress={() => {}}
                      >
               <Image style={{width: Dimensions.get('window').width, height:300}}
                      source={{uri: this.props.image}}/>
           </ImageZoom>
        </View>
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={DESTRUCTIVE_INDEX}
          onPress={this.onMenuItemPress}
        />

      </View>
    )
  }
}


const styles = StyleSheet.create({
  imageContainer: {
   width: undefined,
   height: Platform.OS === 'ios'? 64 : 54,
   backgroundColor:'transparent',
   justifyContent: 'center',
   alignItems: 'center',
 },
})


function mapStateToProps(state){
  return {
    disableDrawer: state.disableDrawer,
    isLoggedin: state.SET_LOGGEDIN_STATUS.isLoggedin,
    applicationToken: state.saveApplicationToken.applicationToken,
    DisplayName: state.CURRENT_USER.DisplayName,
   // AvatarUrl: state.CURRENT_USER.AvatarUrl,
    profileImages: state.profileImages.profileImages
  }
}

export default connect (mapStateToProps)(Modal);

