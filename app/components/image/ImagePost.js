import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
var SpinKit = require('react-native-spinkit');
import ImageZoom from 'react-native-image-pan-zoom';
import ModalWrapper from 'react-native-modal-wrapper';
import SocketIOClient from 'socket.io-client';
import ActionSheet from 'react-native-actionsheet'
import Modal from 'react-native-simple-modal';
import Helper from '../../lib/helper'
import { List, Card, ListItem, Button } from 'react-native-elements'
const {
  ScrollView,
  View,
  TextInput,
  ListView,
  Image,
  Text,
  Platform,
  TouchableHighlight,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback
} = ReactNative
var positions = [0,32,64];
var elevations = [2,4,6];
const CANCEL_INDEX = 0
const DESTRUCTIVE_INDEX = 1
const options = [ 'Cancel', 'Delete', 'Flag Content', 'Use as Banner', 'Use as Avatar'];
var reportReasons = [{name: 'Solicitation/Spam/bot', id: 'SO'}, {name: 'Copyright Infringement', id: 'CO'}, {name: 'Links to illegal content or filesharing sites', id: 'IL'}, {name: 'Pornographic Content', id: 'PO'}, {name: 'Other (describe below)', id: 'OT'}]
const Screen = Dimensions.get('window');

class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props}

      />
    );
  }
}

class ImagePost extends Component{

    constructor(props) {
        super(props);
        this.state = {
            showImage: false,
            optionsVisible: true,
            likeContainerVisible: false,
            openReportModal: false,
            dialogOffset: 0,
            details: '',
            options: [ 'Cancel' , 'Flag Content' ]
        }
        this.onMenuItemPress = this.onMenuItemPress.bind(this)
        this.showActionSheet = this.showActionSheet.bind(this)
        this._keyboardDidHide = this._keyboardDidHide.bind(this)
        this._keyboardDidShow = this._keyboardDidShow.bind(this)
    }

  showActionSheet() {
    this.ActionSheet.show()
  }

  selectReason(id,i){
    this.setState({
      reasonCode: id,
      selectedReason: i
    })
  }

  componentWillMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  onMenuItemPress(i){
    
    if (i == 1){
      this.state.image.Author.ProfileName == this.props.currentUserProfile.ProfileName ?

       this.props.deleteImage(this.props.applicationToken, this.state.image.MediaId).then(() => {
        var index = Helper.findElement(this.props.profileImages, 'MediaId', this.state.image.MediaId, true);
        this.props.removeLocalImage(index);
        this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
          
        });
        Actions.pop();
       })
       :
       this.openReportModal()
        
    }else if (i== 2){
      this.openReportModal()
    }else if (i == 3){
      this.props.setBanner(this.props.applicationToken, this.state.image.MediaId).then(() => {
        
        this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
            
        });
        Actions.pop();
      });
    }else if (i == 4){
      this.props.setAvatar(this.props.applicationToken, this.state.image.MediaId).then(() => {
        
        this.props.getCurrentUserProfile(this.props.applicationToken, '').then(() => {
            
        });
        Actions.pop();
      });
    }
   }


flagContent(){
  this.props.flagContent(this.props.applicationToken, 'ME', this.state.image.MediaId, this.state.reasonCode, this.state.details).then((status) => {
    if (status){
      this.setState({
        openReportModal: false
      },
      () => {
        alert('Content reported!')
      }
    )
    }
  })
}

renderReportReasons(l,i){
  return (
    <TouchableOpacity key={i} onPress={() => {this.selectReason(l.id, i)}} underlayColor='#999'>
      <View style={{backgroundColor: this.state.selectedReason == i ? '#1EA6CC' : '#ccc', borderRadius: 15, margin: 5, padding: 8}}>
        <Text style={{fontSize: 12}}>{l.name}</Text>
      </View>
    </TouchableOpacity>
  )
}


 openReportModal(){
  this.setState({
    openReportModal: true,
  })
}

_keyboardDidShow () {
  this.setState({
    dialogOffset: -100
  })
}

_keyboardDidHide () {
  this.setState({
    dialogOffset: 0
  })
}

    componentDidMount(){
    this.props.toggleDrawer();
    this.props.getImage(this.props.applicationToken, this.props.Selector).then((data) => {
      this.setState({
          showImage: true,
          image: data.Image,
          options: data.Image.Author.ProfileName == this.props.currentUserProfile.ProfileName ? [ 'Cancel', 'Delete', 'Flag Content', 'Use as Banner', 'Use as Avatar'] : [ 'Cancel', 'Flag Content'] ,
          RecentThreeFollowing: data.RecentThreeFollowing
        })
    })
   
    }

    componentWillUnmount(){
    this.props.toggleDrawer();
    }

    toggleOptions(){
      this.setState({
        optionsVisible: !this.state.optionsVisible 
      })
    }

  open(id, type) {
    this.setState({
        likeContainerVisible: true,
        reactingTo: id,
        type: type
    })
}

close() {
    this.setState({open: false}, () => {
         Animated.timing(this._scaleAnimation, {
          duration: 50,
          toValue: 0
        }).start();
    })
}

getLikeContainerStyle() {
    return {
            transform: [{scaleY: this._scaleAnimation}],
            overflow: this.state.open ? 'visible': 'hidden',
          };
}

React(l){
  
    this.setState({
        likeContainerVisible: false
    })
    this.props.React(this.props.applicationToken, this.state.type, this.state.reactingTo, l.ReactionTypeCode).then(() => {
      
      // change locally
      var image = this.state.image;
      image.Reacted = 1;
      image.Emoticon = l.Emoji;
      this.setState({
        image: image
      })
      
    });
}

  render(){
    
    return (
      <View style={{backgroundColor: '#000', flex: 1,paddingTop: Platform.OS === 'ios' ? 24 : 24}}>
          {this.state.showImage ?
          <View style={{flex: 1, justifyContent: 'space-around'}}>
          {this.state.optionsVisible ?
          <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 15, position: 'absolute', top: 0, left: 0 , right: 0, zIndex: 100}}>
            <View style={{flexDirection: 'row',alignItems: 'center', left: 10}}>
            {this.state.image.Author.AvatarThumnailUrl ?
              <Image source={{uri: this.state.image.Author.AvatarThumnailUrl}} style={{width: 30, height: 30, borderRadius: 15}}/>
              :
              <View style={{width: 30, height: 30, borderRadius: 15, backgroundColor: '#ccc'}}></View>
            }
            <TouchableOpacity onPress={() => {Actions.UserProfile({ProfileName: this.state.image.Author.ProfileName, hideNavBar: false})}} underlayColor='#999'>
              <Text style={{color: '#fff', marginLeft: 10, fontWeight: '600', fontSize: 12}}>{this.state.image.Author.DisplayName}</Text>
            </TouchableOpacity>
              </View>

              <View style={{flexDirection: 'row',alignItems: 'center', right: 10}}>
              <TouchableOpacity style={{marginRight: 15, padding: 5}}  onPress={() => {this.showActionSheet()} } underlayColor='#999'>
                  <Icon style={{}} name='ellipsis-h' size={22} color={'#fff'} />
                </TouchableOpacity>
              <TouchableOpacity style={{marginBottom: 3}}  onPress={() => {Actions.pop()}} underlayColor='#999'>
              <Icon size={20}
                  style={{ opacity: 1, color: '#fff',margin: 0,fontWeight: 'normal'}} name={'close'} />
              </TouchableOpacity>
            </View>
          </View>
          :
          null
          }
          
          <View style={{ justifyContent: 'center', flex: 1 }} >
          <TouchableWithoutFeedback onPress={() => {this.toggleOptions()}} >
            <Image source={{uri: this.state.image.Url}} style={styles.backgroundImage} />
            </TouchableWithoutFeedback>
          </View>
          
          {this.state.optionsVisible ?
          <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 0, marginHorizontal: 20,position: 'absolute', bottom: 70, left: 0 , right: 0 , zIndex: 100}}>
            <Text style={{color: '#fff'}}>{this.state.image.Name}</Text>
          </View>
          :
          null
          } 
           {this.state.optionsVisible ?
          <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 20, position: 'absolute', bottom: Platform.OS == 'ios' ? 10 : 10, left: 0 , right: 0 , zIndex: 100}}>
              <TouchableOpacity onPress={() => { this.open(this.state.image.MediaId, 'ME')  }} underlayColor='#999'>
              <View style={{flexDirection: 'row',alignItems: 'center'}}>
                {this.state.image.Reacted == 1 ? 
                <Text style={{alignSelf: 'center', textAlign: 'center', fontSize: 18, padding: 2 }}>{this.state.image.Emoticon}</Text>
                :
                <Icon size={20}
                  style={{ opacity: 1, color: '#999',margin: 0,fontWeight: 'normal', padding: 2}} name={'heart'} />
                }
              </View>
              </TouchableOpacity>

             
                
              <TouchableOpacity style={{flexDirection: 'row', zIndex: 200,flex: 1, justifyContent: 'center'}} onPress={() => {Actions.Reactors({hideNavBar: false, TargetId: this.state.image.MediaId, TargetTypeCode: 'ME'})}} underlayColor='#999'>
            {
              this.state.RecentThreeFollowing ?
              this.state.RecentThreeFollowing.map((l,i) => (
                l.AvatarThumbnailUrl == null ?
              <View key={i} style={[{elevation: elevations[i],marginLeft: -10, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1,borderRadius: 15, width: 30, height: 30, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                <Text style={{color: '#fff'}}>{(l.DisplayName).split(" ").map((n)=>n[0])}</Text>
              </View>
              :
              <View key={i} style={[{elevation: elevations[i],marginLeft: -10, margin: 2, backgroundColor: 'transparent', borderRadius: 15, width: 30, height: 30, justifyContent: 'center', alignItems: 'center'}, styles.shadow]}>
                {<Image source={{uri: l.AvatarThumbnailUrl}} style={{borderColor: '#fff', borderWidth: 1,width: 30, height: 30, borderRadius: 15}}/>}
              </View>
            ))
            :
            null
          }
          
        </TouchableOpacity>
              <TouchableOpacity onPress={() => {Actions.ImageComments({Selector: this.props.Selector, hideNavBar: false})}} underlayColor='#999'>
              <View style={{flexDirection: 'row',alignItems: 'center'}}>
              <Icon size={20}
                  style={{ opacity: 1, color: '#999',margin: 0,fontWeight: 'normal', padding: 2}} name={'comment'} />
            </View>
            </TouchableOpacity>
          </View>
          :
          null
          }
           {/* <View style={{elevation: 4,zIndex: 600,flex: 1,alignSelf: 'center',  position: 'absolute', bottom: 10,  backgroundColor: 'transparent' }}>
                {
                   this.state.RecentThreeFollowing[0] ?
                <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {Actions.Reactors({ hideNavBar: false, TargetId: this.state.image.MediaId, TargetTypeCode: 'ME' }) }} underlayColor='#999'>
                  
                      {
                    this.state.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: -20, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: -20, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>

                  <View style={{elevation: 4,zIndex: 600,flex: 1, alignSelf: 'center', position: 'absolute', bottom: 10,  backgroundColor: 'transparent' }}>
                  {
                    this.state.RecentThreeFollowing[0] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {Actions.Reactors({ hideNavBar: false, TargetId: this.state.image.MediaId, TargetTypeCode: 'ME' }) }} underlayColor='#999'>
                  
                      {
                    this.state.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 32, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 32, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      }
                     
                  </TouchableOpacity>
                   :
                   null
               }
                  </View>
                  <View style={{elevation: 4,zIndex: 600,flex: 1, alignSelf: 'center', position: 'absolute', bottom: 10,  backgroundColor: 'transparent' }}>
                  {
                    this.state.RecentThreeFollowing[0] ?
                    <TouchableOpacity rejectResponderTermination  style={{ zIndex: 200 }} onPress={() => {Actions.Reactors({ hideNavBar: false, TargetId: this.state.image.MediaId, TargetTypeCode: 'ME' }) }} underlayColor='#999'>
                  
                      {
                    this.state.RecentThreeFollowing[0].AvatarThumbnailUrl == null ?
                          <View style={[{ left: 0, margin: 2, backgroundColor: '#0074CD', borderColor: '#fff', borderWidth: 1, padding: 3, borderRadius: 20, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            <Text style={{ color: '#fff' }}>{(this.state.RecentThreeFollowing[0].DisplayName).split(" ").map((n) => n[0])}</Text>
                          </View>
                          :
                          <View style={[{ left: 0, margin: 2, backgroundColor: 'transparent', borderRadius: 20, padding: 3, width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }, styles.shadow]}>
                            {<Image source={{ uri: this.state.RecentThreeFollowing[0].AvatarThumbnailUrl }} style={{ borderColor: '#fff', borderWidth: 1, width: 40, height: 40, borderRadius: 20 }} />}
                          </View>
                      
                      }
                  </TouchableOpacity>
                   :
                   null
               }
                  </View> */}
          </View>
          :
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
            <SpinKit isVisible={true} size={40} type='WanderingCubes' color='#134fa1' />
          </View>
          }
          <ModalWrapper
            style={{ borderRadius: 15, padding: 20,flexDirection: 'row', margin : 10}}
            visible={this.state.likeContainerVisible}
            onRequestClose={() => this.setState({ likeContainerVisible: false })}
            shouldCloseOnOverlayPress={true}
            >
                        <View style={{flexWrap: "wrap",flexDirection: 'row'}}>
                            {
                            this.props.reactionTypes.map((l, i) => (
                                <TouchableOpacity key={i} onPress={() => {this.React(l)}} underlayColor='#999'>
                                    <Text style={{flexDirection: 'row', padding: 5, flexWrap: "wrap" }}> {l.Emoji} </Text>
                                </TouchableOpacity>
                            ))
                          }

                        </View>
        </ModalWrapper>
        <Modal
          open={this.state.openReportModal}
          offset={this.state.dialogOffset}
          overlayBackground={'rgba(0, 0, 0, 0.75)'}
          animationDuration={200}
          animationTension={40}
          modalDidOpen={() => undefined}
          modalDidClose={() => this.setState({openReportModal: false})}
          closeOnTouchOutside={true}
          containerStyle={{
            justifyContent: 'center'
          }}
          modalStyle={{
            borderRadius: 15,
            margin: 20,
            padding: 0,
            backgroundColor: '#F5F5F5'
          }}
          disableOnBackPress={false}>
          <View style={{ borderBottomColor: '#1EA6CC', borderBottomWidth: 0, justifyContent: 'center', alignItems: 'center', padding: 15}}> 
            <Text style={{fontSize: 16, color: '#1EA6CC', fontWeight: 'bold'}}>{'Report this Content'} </Text>
          </View>

          <ScrollView style={{height: 230}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', margin: 5}}>
            {reportReasons.map((l,i) => (
              this.renderReportReasons(l,i)
            ))}
          </View>
          </ScrollView>

              <View style={{
                  padding: 5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  borderBottomColor: '#1EA6CC',
                  borderBottomWidth: 0.5
              }}>
                  <UselessTextInput
                      style={{ paddingBottom: 10, color: '#444', fontSize: 14, fontWeight: 'normal' }}
                      onChangeText={details => this.setState({ details })}
                      value={this.state.details}
                      placeholder='Details...'
                      placeholderTextColor='#d7d7d7'
                      autoFocus={false}
                      underlineColorAndroid='transparent'
                      multiline={true}
                      editable={true}
                  />
              </View>
              <Button
                title='Report'
                buttonStyle={{margin: 5,marginBottom: 10, height: 35}}
                backgroundColor='#1EA6CC'
                color='#fff'
                textStyle={{ fontSize: 14, fontWeight: 'normal' }}
                onPress={() => { this.flagContent() }}
              />
        </Modal>
        <ActionSheet
          ref={o => this.ActionSheet = o}
          options={this.state.options}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={this.state.image ? this.props.currentUserProfile.ProfileName == this.state.image.Author.ProfileName ? 1 : -1 : -1}
          onPress={this.onMenuItemPress}
        />
     </View>
    )
  }
}

const styles = StyleSheet.create({

    backgroundImage: {
   //   flex: 1,
        height: Screen.height-250 ,
        width: Screen.width,
        resizeMode: 'contain',
      }
})


function mapStateToProps(state){

  return {
    applicationToken: state.saveApplicationToken.applicationToken,
    reactionTypes: state.REACTION_TYPES.reactionTypes,
    profileImages: state.profileImages.profileImages,
    currentUserProfile: state.CURRENT_USER_PROFILE.currentUserProfile,
  }
}

export default connect (mapStateToProps)(ImagePost);
